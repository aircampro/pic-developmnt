// Declarations of interrupts used
//
// ============== Chosen Interrupts ============================================
// #include "lwNx.h"                                                            // liddar from lightware

#ifdef TIMER1_NEEDED                                                            // if you use Ethernet IP
extern void Timer1_interrupt();                                                 // This is used to increment Net_Ethernet_Intern_userTimerSec every second to prevent lock up
#endif
// extern void InitTimer2();
#ifdef UART2_INTERUPT                                                           // SimpleBGC needed for gimbal control
extern void UART2_interrupt();                                                  // simpleBGC gimbal protocol
#endif
#ifdef TIMER2_NEEDED                                                            // extended ACK
extern void Timer2_3Interrupt();                                                // extended timer ACK interrupt
#endif
#if defined(GPS_INCLUDED) || defined(GPS_INCLUDED2)                             // GPS being used on UART3
extern void interruptGPS();                                                     // interrupt for various gps units
#endif
#ifdef UART4_INTERUPT                                                           // XStream encoder is on UART4
void UART4_interrupt();                                                         // XS Encoder camera protocol
#endif
// ============ Always used routines ===========================================
extern uint32_t hex2int(unsigned char *hex);                                    // converts hex to integer
extern void Timer_5Interrupt();                                                 // global timer tick counter
//#ifdef UART6_INTERUPT                                                           // liddar included
//extern lwResponsePacket LwNxResponse;                                           // structure to contain response from interrupt
//#endif