#include "Struts.h"                                                             // common struts for UART
#include "ports.h"                                                              // IP ports
#include"config.h"                                                              // config
//#include "Transceiver_events.h"
#include "definitions.h"                                                        // global defines
#include "stdint.h"
#include<built_in.h>
#include "__NetEthInternal.h"                                                   // tcpip stack
#include "SBGC_COMMAND.h"                                                       // sbgc commands
#include "SBGC_PARSER.h"                                                        // sbgc general
#include "SBGC_cmd_helpers.h"                                                   // sbgc helpers
#include "camera.h"                                                             // camera helpers
#include "p32mx795f512l.h"                                                      // mcu library
#include "pic32mx_eth.h"                                                        // eth phy library
//#include "lwNx.h"                                                               // liddar from lightware

#ifdef GEF_EGD_PLC
#include "EGD.h"                                                                // Library for communicating UDP to GEC Fanuc PLC
#endif

#include "CRC.h"
//#include "io.h"
//#define PRINT_DEBUG_MESSAGES                                                    // Print messages to UART5 when enabled

#ifdef UART2_INTERUPT
SerialObject_t UART2;                                                           // Define UART2 as UART object.
#endif

#if (CAMERA_TYPE == XS_CAM)
EthUDPObject_t XS_DATA;                                                         // Define struct to store XStream input data
#endif
#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
EthUDPObject_t SBGC_DATA;                                                       // Define struct to store SBGC input data
#endif
#ifdef RUN_CAM_USED
EthUDPObject_t RunC_DATA;                                                       // Define struct to store run cam input data
#endif
#ifdef YI_CAM_USED
EthUDPObject_t YiCam_DATA;                                                      // Define struct to store yi action cam input data
TcpStateTimers YiTimer;                                                         // State timeouts for each TCP Step for the Socket used for Yi Cam
uint8_t g_YiCamReqState;                                                        // State of YiCam write messages
#endif
#ifdef SEQ_CAM_USED
EthUDPObject_t SeqCam_DATA;                                                     // Define struct to store sequioa cam input data
TcpStateTimers SeqTimer;                                                        // State timeouts for each TCP Step for the Socket used for Yi Cam
uint8_t g_SeqCamReqState;                                                       // State of Sequioa Cam write messages
#endif

#ifdef GEF_EGD_PLC
EthUDPObject_t EGD_DATA;                                                        // Define struct to store EGD Data from GEC Fanuc PLC
#endif

#ifdef UART4_INTERUPT                                                           // read the XS camera on serial UART4 (for test purposes)
SerialObject_t UART4;                                                           // Define UART4 as UART object.
#endif

unsigned char tmp;
#ifdef USE_CAN
void CanUDP_doData();
void bitWrite(char *x, char n, char value);
#endif

#ifdef UART6_INTERUPT                                                           // read the liddar on serial UART6 (for test purposes)
SerialObject_t UART6;                                                           // Define UART6 as UART object.
// lwResponsePacket LwNxResponse;                                                  // LightWare liddar packet and status
#endif

#ifdef UART5_INTERUPT                                                           // read the Run Cam camera on serial UART5 (for test purposes)
SerialObject_t UART5;                                                           // Define UART5 as UART object.
#endif

#ifdef UART1_INTERUPT                                                           // serial UART1 (for test purposes)
SerialObject_t UART1;                                                           // Define UART1 as UART object.
#endif

// ================= Function definitions ======================================
void StartTimeout();
void Status(int16_t address, int16_t state);
void Init_Ether();
uint16_t CRC16_Calc( unsigned char *Packet,uint8_t Packet_length);
uint8_t CheckTimout();
// uint8_t Pay_checksum(unsigned char *pdata, uint16_t sz)                         // A Function to calculate a checksum for a struct
uint16_t Net_Ethernet_Intern_UserUDP(UDP_Intern_Dsc *udpDsc);                   // Process a UDP Packet which came in
#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
uint8_t ReadSBGC(unsigned char *pdata, uint16_t psz, uint16_t arrPosition);     // Read the data portion of the UDP datagram and get SBGC data.
uint8_t SBGC_process_UDP_InBuffer( SBGC_cmd_board_info_t *boardinf );           // Process the UDP buffer for SimpleBGC
uint8_t check_payload(unsigned int buffStartPos, SBGC_cmd_board_info_t *boardinf );
#endif 
void StartTCPCommunication();                                                   // Initialise the TCP Stack
void TCPStateCheck( SOCKET_Intern_Dsc *used_tcp_socket);                        // Check the State of the YiCam socket
void Net_Ethernet_Intern_UserTCP(SOCKET_Intern_Dsc *used_tcp_socket);                                                                         // ==================== eNd SBGC ===================================
#if defined(YI_CAM_USED)
uint8_t XY_OpenTCPSocketRawJson( SOCKET_Intern_Dsc **sock1 );                   // Opens RAW tcp socket for JSON communication for Yi Action Cam
uint8_t XY_CloseTCPSocketRawJson( SOCKET_Intern_Dsc *sock1 );                   // Closes RAW tcp socket for JSON communication for Yi Action Cam
void resetXYActions();
#endif
#if defined(SEQ_CAM_USED)
uint8_t SEQ_OpenTCPSocketHttpJson( SOCKET_Intern_Dsc **sock1 );                 // Opens http api tcp socket for JSON communication for Sequoia Cam
uint8_t SEQ_CloseTCPSocketHttpJson( SOCKET_Intern_Dsc *sock1 );                 // Closes http api tcp socket for JSON communication for Sequoia Cam
void resetXYActions();
#endif
#if defined(YI_CAM_USED) || defined(SEQ_CAM_USED)
void ProcessTcpMsg();
#endif                                                                          // ================== eNd Yi or Parrot (JSON TCP/IP) ===============
#ifdef RUN_CAM_USED
uint8_t ReadRunCam( unsigned char *pdata, uint16_t psz );                       // Read the data portion of the UDP datagram and get Run Cam data.
#endif                                                                          // ================== Run Cam ======================================
#if (CAMERA_TYPE == XS_CAM)
int8_t XS_process_UDP_InBuffer();                                               // Process the in buffer from XStream and decide what it means
int8_t XS_process_SERIAL_InBuffer();                                            // Process the in buffer from XStream and decide what it means
#endif                                                                          // ================== AMP XS Encoder / Decoder =====================
uint8_t sizeToChar( unsigned char* stringX, unsigned char val );
#ifdef USE_CAN
void Can2_Test();
void Init_Can();
#endif
void bitWrite(char *x, char n, char value);
char bitRead(char *x, char n);
char ARP(unsigned char *NodeIPAddr);
void boot_arp();
void StopTimer1();
void Init_MCU();
#ifdef TIMER1_NEEDED                                                            // if you use Ethernet IP
void Init_Timer1();
#endif
#ifdef TIMER2_NEEDED
void Init_Timer2();
void Init_Timer2_3();
#endif
void Init_Timer5();
Init_PHYPins();
void Init_UART( uint16_t parityValue );

void Init_Node();
unsigned char checksum(unsigned char *fdata, unsigned int sz);                  // A Function to calculate a checksum for old version of simpleBGC
uint8_t DataChangeDetected( uint16_t dataVal, uint16_t dataLast, uint8_t delta);// Function to detect a data change by more than X
float32_t Scale_Raw_Value( int16_t raw_min, int16_t raw_max, float32_t scale_min, float32_t scale_max, int16_t raw_value);

char RH;

//Extern unsigned int UART1TCP;
#ifdef USE_CAN
extern unsigned int    Can1_RX_MSG_ID;
extern unsigned int    Can1_TX_MSG_ID;
extern unsigned int    Can2_RX_MSG_ID;
extern unsigned int    Can2_TX_MSG_ID;
extern unsigned int    Can1_Rcv_Flags;
extern unsigned int    Can2_Rcv_Flags;
unsigned char   RHtemp;
unsigned char   getSBGCRequest[15];                                             // SBGC over UDP request buffer
#endif

unsigned char   dyna[31] ;                                                      // buffer for dynamic response
#ifdef CAN_UDP_USED
// Ethernet
 //flag for sending a TPC message
unsigned char EtherConnState;
unsigned char OpenSktStatus ;
unsigned char CloseTcpSocket =0;
unsigned char StartEthComms =0;
unsigned int RXBUF_pnt = 0;                                                     // pointer to the UDP received RX buffer
bit Send_UDP_Trap_msg;                                                          // when a TrapV2 Notification message needs to be sent this is SET to 1
#endif                                                                          // ============= eNd CanBus over UDP =================================

unsigned char g_XYtcpBuffer[XY_MSG_MAX_SND_LEN];                                // Buffer to send JSON requests to the webserver set in the main program
unsigned char xyTcpSendBuff[XY_MSG_MAX_SND_LEN];                                // TCP message send buffer for Yi Action Camera used in the TCP handler

uint8_t SendTCPMsg =0U;
uint8_t Eth_Process_Status;                                                     // saved status of Net_Ethernet_Intern_doPacket() procedure
uint32_t EthTimePrevCounter =0U;
unsigned bit TimoutRunning;
unsigned int Ethernet_Fault =0U;
unsigned int TCPByteTosSend;
unsigned char Ether_Link_Present = FALSE;
unsigned char Ether_Link_Present_Was = 10U;
uint16_t EFinish = 0U;                                                          // Flag set to 1 when a complete message has been received
#ifdef USE_CAN
unsigned int    Can2_Init_Flags, Can2_Rcv_Flags;                                // can flags
#endif
extern Net_Ethernet_Intern_arpCacheStruct Net_Ethernet_Intern_arpCache[];       // ARP cash, 3 entries max
Net_Ethernet_Intern_arpCacheStruct Private_Ethernet_Intern_arpCache[3];
//extern SOCKET_Intern_Dsc socket_Intern[];

uint8_t SendTCP=0U;
uint8_t Eth_State_Was =10U;
uint8_t Eth_Process_Was=10U;
uint32_t Status_Count =1U;
uint16_t UDP_CRC_Reg =0U;
uint16_t UDP_CRC_Good_Reg =0U;
uint16_t UDP_CRC_Bad_Reg =0U;
uint8_t SYN=0x16U;                                                        // SYN
uint8_t bad_ether=0U;

//volatile unsigned char gRemEtherMAC[6];                                       // global variable conatining remote target MAC address (only one byte ?)
Net_Ethernet_Intern_arpCacheStruct gRemoteUnit;                                 // Declare the ARP cache structure to get the MAC Address
unsigned char *ptr_ru = (unsigned char *) &gRemoteUnit;                         // Define the pointer to the container struct for arpCache

//extern unsigned char timer_counter;                                           // A timer counter from the interrupt

H_B Heart;
//SOCKET_Intern_Dsc *used_socket;

#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
volatile uint8_t g_SBGC_Stx_Char;                                               // Global start transmission char set in main and also read in interrupt
#endif
#ifdef YI_CAM_USED
COMGS_YiOptActive_t hmiReqActive;                                               // structure to hold requests for each send message
uint8_t XYRequest=0U;                                                           // Current button request (HMI Action Queue)
#endif

//******************************************************************************

/*
 * Function Name:
 *   Net_Ethernet_Intern_UserUDP
 * Description:
 *   This is UDP module routine. It is internally called by the library. The user accesses to the UDP request by using some of the
 *   Net_Ethernet_Intern_get routines. The user puts data in the transmit buffer by using some of the Net_Ethernet_Intern_put routines.
 *   The function must return the length in bytes of the UDP reply, or 0 if nothing to transmit. If you don't need
 *   to reply to the UDP requests, just define this function with a return(0) as single statement.
 *
 * Arguments:
 * void Net_Ethernet_Intern_UserUDP(UDP_Intern_Dsc *udpDsc);
 * udpDsc : pointer to global udpRecord_Intern variable, which contains properties of last received UDP Packet
   typedef struct {
  char remoteIP[4];              // Remote IP address
  char remoteMAC[6];             // Remote MAC address
  unsigned int remotePort;       // Remote UDP port
  unsigned int destPort;         // Destination UDP port
  unsigned int dataLength;       // Current UDP datagram payload size
  unsigned int broadcastMark;    // =0 -> Not broadcast; =1 -> Broadcast
  } UDP_Intern_Dsc;
 *Returns:
 *   0 - there should not be a reply to the request.
 *   Length of UDP reply data field - otherwise. This will use flushUDP to send what you wrote with putbytes
 */
uint16_t Net_Ethernet_Intern_UserUDP(UDP_Intern_Dsc *udpDsc)
{
#ifdef USE_CAN
  uint16_t Can2_UDP_RX_Packet[8];
#endif
  //int counter=0;
  unsigned char len;

      if (UDP_Test_Mode)
      {
        len = 0;                                                                // my reply length
                                                                                // reply is made of the remote host IP address in human readable format
        ByteToStr(udpDsc->remoteIP[0], dyna);                                   // first IP address byte
        dyna[3] = '.';
        ByteToStr(udpDsc->remoteIP[1], dyna + 4);                               // second
        dyna[7] = '.';
        ByteToStr(udpDsc->remoteIP[2], dyna + 8);                               // third
        dyna[11] = '.';
        ByteToStr(udpDsc->remoteIP[3], dyna + 12);                              // fourth

        dyna[15] = ':';                                                         // add separator

        // then remote host port number
        WordToStr(udpDsc->remotePort, dyna + 16);
        dyna[21] = '[';
        WordToStr(udpDsc->destPort, dyna + 22);
        dyna[27] = ']';
        dyna[28] = 0;

        // the total length of the request is the length of the dynamic string plus the text of the request
        len = 28 + udpDsc->dataLength;

        // puts the dynamic string into the transmit buffer
        Net_Ethernet_Intern_putBytes(dyna, 28);

        // then puts the request string converted into upper char into the transmit buffer
        while(udpDsc->dataLength--)
        {
           Net_Ethernet_Intern_putByte(toupper(Net_Ethernet_Intern_getByte()));
        }
     }
     else if (~UDP_Test_Mode)
     {
       //timer_tmp5_1=0;
       // ANY port Heart Beat

        //Net_Ethernet_Intern_sendUDP(udpDsc->remoteIP, 11111, 11111, &SYN,1);
        if(udpDsc->remoteIP[3] == Air.IPAddr[3])
        {
            Air.Link_Alive   = TRUE;
            Air.Link_Alive_Time=Heart.time;
            if(Air.Link_Alive_Was != Air.Link_Alive)
            {
              Air.Link_Alive_Was = Air.Link_Alive;
              Status(ETHER_LINK_STATE,AIR_LINK_UP);
            }
        }
        else if (udpDsc->remoteIP[3] == MasGs.IPAddr[3])
        {
             MasGs.Link_Alive = TRUE;
             MasGs.Link_Alive_Time=Heart.time;
             if(MasGs.Link_Alive_Was != MasGs.Link_Alive)
             {
               MasGs.Link_Alive_Was = MasGs.Link_Alive;
               Status(ETHER_LINK_STATE,MASTER_LINK_UP);
             }
        }
        else if (udpDsc->remoteIP[3] == FirGs.IPAddr[3])
        {
             FirGs.Link_Alive = TRUE;
             FirGs.Link_Alive_Time=Heart.time;
             if(FirGs.Link_Alive_Was != FirGs.Link_Alive)
             {
                Status(ETHER_LINK_STATE,FIRE_LINK_UP);
             }
        }

      switch(udpDsc->destPort)                                                  // Test the destination port for the received UDP packet
      {
#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
      // ====================== SimpleBGC message ============================== From STM32 Gimbal Controller
      
         case(SBGC_Dest_Port):                                                  // Port is for SimpleBGC UDP receive packet
         {
            SBGC_DATA.UDP_Index =CLEAR;                                         // Reset buffer index
            SBGC_DATA.UDP_Buffer_Len =  udpDsc->dataLength;

            while(udpDsc->dataLength--)                                         // Keep filling SBGC_DATA.UDP_Buffer until UDP receive buffer is empty
            {
               SBGC_DATA.UDP_Buffer[SBGC_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
               SBGC_DATA.UDP_Index++ % UINT8_MAX;
               if ( SBGC_CMD_MAX_BYTES <= SBGC_DATA.UDP_Index )                 // counted past the end of the UDP receive buffer
               {
                  return(0);                                                    // return with no reply message was longer than max expected for SimpleBGC
               }
            }
            SBGC_DATA.UDP_Index--;
            SBGC_DATA.State = ETH_WAIT_ACK_CRC;                               // We have received a Packet for COM PIC, now we need to test the CRC
            UDP_CRC_Reg |= 0b10;                                                // Set UDP_UART2 CRC check needed bit
            UDP_CRC_Bad_Reg &= 0xFFFD;                                          // Clear Bad bit
            break;                                                              // Message was sent to us from the AIR COM PIC for SBGC
         }

      // ==================== SimpleBGC ACK ====================================  If required (most messages get confirm from STM32 controller so dont need ack from COM PIC but use if needed
      
         case(SBGC_Ack_Port):                                                   // Port is for SimpleBGC UDP ACK receive
         {
            T2CON = DISABLE_T2;                                                 // Stop time our
            SBGC_DATA.UDP_CRC_Index= CLEAR;                                     // Reset buffer index
            while(udpDsc->dataLength--)                                         // Keep filling SBGC_DATA.UDP_CRC buffer until UDP receive buffer is empty
            {
                SBGC_DATA.UDP_CRC[SBGC_DATA.UDP_CRC_Index] = Net_Ethernet_Intern_getByte();
                SBGC_DATA.UDP_CRC_Index++ % UINT8_MAX;
                if ( 5 <= XS_DATA.UDP_CRC_Index )                               // counted past the end of the UDP ACK receive buffer which is UDP_CRC[]
                {
                  return(0);                                                    // return with no reply message was longer than max expected for SimpleBGC extra CRC
                }
            }
            SBGC_DATA.UDP_CRC_Index--;
            SBGC_DATA.UDP_Send_trys =CLEAR;                                     // We received a good ACK clear the trys.
            SBGC_DATA.State = ETH_RECEIVED_ACK_CRC;
            //status(UDPSTA_Add,UDP_PACKET_ACK_RECEIVED);
            break;
         }
#endif
#if (CAMERA_TYPE == XS_CAM)
      // ================== XSStream messages ================================== From A.M.P. Camera Encoder
     
         case(XS_Dest_Port):                                                    // Port is for XStream encoder UDP receive packet
         {
            XS_Data.UDP_Index =CLEAR;                                           // Reset buffer index
            XS_Data.UDP_Buffer_Len =  udpDsc->dataLength;

            while(udpDsc->dataLength--)                                         // Keep filling XS_DATA.UDP_Buffer until UDP receive buffer is empty
            {
               XS_DATA.UDP_Buffer[XS_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
               XS_DATA.UDP_Index++ % UINT8_MAX;
               if ( SBGC_CMD_MAX_BYTES <= XS_DATA.UDP_Index )                   // counted past the end of the UDP receive buffer
               {
                  return(0);                                                    // return with no reply message was longer than max expected for XStream
               }                                                                // As we use a common UDP receive structure the max len is actually SimpleBGC
            }
            XS_DATA.UDP_Index--;
            XS_DATA.State = ETH_WAIT_ACK_CRC;                                   // We have received a Packet for UART2, now we need to test the CRC
            UDP_CRC_Reg |= 0b10;                                                // Set UDP_UART2 CRC check needed bit
            UDP_CRC_Bad_Reg &= 0xFFFD;                                          // Clear Bad bit
            break;                                                              //case(UART2.From_Port)
         }
      
      // =============== XSStream ACK messages ================================= If required encoder usually replies with OK or AUTH so no need from COM PIC (leave if required)
      
         case(XS_Ack_Port):                                                     // Port is for XStream ACK CRC
         {
            //T2CON = DISABLE_T2;                                               // Stop time our
            XS_DATA.UDP_CRC_Index= CLEAR;                                       // Reset buffer index
            while(udpDsc->dataLength--)                                           // Keep filling UART2 UDP CRC burrer untill UDP buffer is empty
            {
               XS_DATA.UDP_CRC[XS_DATA.UDP_CRC_Index] = Net_Ethernet_Intern_getByte();
               XS_DATA.UDP_CRC_Index++ % UINT8_MAX;
               if ( 5 <= XS_DATA.UDP_CRC_Index )                                // counted past the end of the UDP ACK receive buffer which is UDP_CRC[]
               {
                  return(0);                                                    // return with no reply message was longer than max expected for XStream
               }
            }
            XS_DATA.UDP_CRC_Index--;
            XS_DATA.UDP_Send_trys =CLEAR;                                       // We received a good ACK clear the trys.
            XS_DATA.State = ETH_RECEIVED_ACK_CRC;
            status(UDPSTA_Add,UDP_PACKET_ACK_RECEIVED);
            break;
         }
         //break;                                                                 //switch(udpDsc->destPort)
#endif
#if defined(RUN_CAM_USED)
      // ================== Run Cam messages ================================== From Run Cam camera

         case(RC_Dest_Port):                                                    // Port is for Run Cam camera UDP receive packet
         {
            RunC_DATA.UDP_Index =CLEAR;                                         // Reset buffer index
            RunC_DATA.UDP_Buffer_Len =  udpDsc->dataLength;

            while(udpDsc->dataLength--)                                         // Keep filling RunC_DATA.UDP_Buffer until UDP receive buffer is empty
            {
               RunC_DATA.UDP_Buffer[RunC_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
               RunC_DATA.UDP_Index++ % UINT8_MAX;
               if ( RC_CMD_MAX_BYTES <= RunC_DATA.UDP_Index )                   // counted past the end of the UDP receive buffer
               {
                  return(0);                                                    // return with no reply message was longer than max expected for XStream
               }                                                                // As we use a common UDP receive structure the max len is actually SimpleBGC
            }
            RunC_DATA.UDP_Index--;
            RunC_DATA.State = ETH_WAIT_ACK_CRC;                                 // We have received a Packet for UART, now we need to test the CRC
            UDP_CRC_Reg |= 0b10;                                                // Set UDP_UART CRC check needed bit
            UDP_CRC_Bad_Reg &= 0xFFFD;                                          // Clear Bad bit
            break;
         }
      // =============== Run Cam ACK messages ================================= If required camera usually has its own serial reply so not always needed here if you want to use

         case(RC_Ack_Port):                                                     // Port is for Run Cam ACK CRC
         {
            RunC_DATA.UDP_CRC_Index= CLEAR;                                     // Reset buffer index
            while(udpDsc->dataLength--)                                         // Keep filling UART UDP CRC burrer untill UDP buffer is empty
            {
               RunC_DATA.UDP_CRC[RunC_DATA.UDP_CRC_Index] = Net_Ethernet_Intern_getByte();
               RunC_DATA.UDP_CRC_Index++ % UINT8_MAX;
               if ( 5 <= RunC_DATA.UDP_CRC_Index )                              // counted past the end of the UDP ACK receive buffer which is UDP_CRC[]
               {
                  return(0);                                                    // return with no reply message was longer than max expected for XStream
               }
            }
            RunC_DATA.UDP_CRC_Index--;
            RunC_DATA.UDP_Send_trys =CLEAR;                                     // We received a good ACK clear the trys.
            RunC_DATA.State = ETH_RECEIVED_ACK_CRC;
            status(UDPSTA_Add,UDP_PACKET_ACK_RECEIVED);
            break;
         }
         break;
#endif
#ifdef GEF_EGD_PLC
         case(GEF_EGD_UDP_DATA_PORT):                                           // Port is reading EGD Protocol
         {
            EGD_DATA.UDP_Index =CLEAR;                                          // Reset buffer index
            EGD_DATA.UDP_Buffer_Len =  udpDsc->dataLength;

            while(udpDsc->dataLength--)                                         // Keep filling RunC_DATA.UDP_Buffer until UDP receive buffer is empty
            {
               EGD_DATA.UDP_Buffer[EGD_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
               EGD_DATA.UDP_Index++ % UINT8_MAX;
               if ( GEF_EGD_UDP_MAX_LEN <= EGD_DATA.UDP_Index )                 // counted past the end of the UDP receive buffer
               {
                  return(0);                                                    // return with no reply message was longer than max expected for XStream
               }                                                                // As we use a common UDP receive structure the max len is actually SimpleBGC
            }
            EGD_DATA.UDP_Index--;
            EGD_DATA.State = ETH_WAIT_ACK_CRC;                                  // We have received a Packet for UART, now we need to test the CRC
            UDP_CRC_Reg |= 0b10;                                                // Set UDP_UART CRC check needed bit
            UDP_CRC_Bad_Reg &= 0xFFFD;                                          // Clear Bad bit
            break;
         }
#endif

     }
#ifdef OTHER_UART
     // Process UDP packets for UART5
     // If UDP packet comes from destination port (UART5RXPort) then sent packet out UART5
       if (udpDsc->destPort==UART5fromPort)
       {
        // Keep sending data on UART untill reqLength =0 i.e. the ethernet buffer is empty
         while(udpDsc->dataLength--)
         {
            UART5_Write(Net_Ethernet_Intern_getByte());
         }

       }
#endif

#ifdef USE_CAN
     // Process UDP packets for CAN
     if (udpDsc->destPort==Can2fromPort)
     {
        // Load first two bytes into message ID
        Lo(Can2_TX_MSG_ID)=Net_Ethernet_Intern_getByte();
        Hi(Can2_TX_MSG_ID)=Net_Ethernet_Intern_getByte();

        // load the remaining of the message into Can2_UDP_RX_Packet EXLUDING the MASK which is the last byte in the UDP Packet.

        reqLength = udpDsc->dataLength;
        while(counter < reqLength)
        {
           Can2_UDP_RX_Packet[counter] = Net_Ethernet_Intern_getByte();
           counter=++counter % UINT16_MAX;
        }

        // Sent message out CAN2
        CAN2Write(Can2_TX_MSG_ID, Can2_UDP_RX_Packet, udpDsc->dataLength , Can2_Send_Flags);
     }
#endif
   }
   return(0u);                                                                  // back to the library with the length to reply with (in this case nothing)
 }
 
/*-----------------------------------------------------------------------------
 *      Pay_checksum:  Calculate the payload chacksum for ABP message
 *
 *  Parameters: unsigned char *pdata, uint16_t sz
 *  Return:     char checksum
 *----------------------------------------------------------------------------*/
unsigned char Pay_checksum(unsigned char *pdata, uint16_t sz)                   // A Function to calculate a checksum for a struct
{
  int i;
  unsigned char checksum;
  for(i=0, checksum=0; i<sz; i++)
    checksum+=pdata[i];
  return checksum;
}

 /*
 * Function Name:
 *   Test_CRC()
 * Description:
 *   Tests incoming UDP ACK CRC packets
 * Arguments:
 *   None
 * Returns:
 *   None
 * NOTES:
 *   <notes>
 */
void Test_CRC()
 {
  unsigned int crc_test;
  if (UDP_CRC_Reg & 0b10)                                                        // do we have a UDP_UART2 Packet CRC to Test?
  {
   crc_test = CRC16_Calc(SBGC_DATA.UDP_Buffer,SBGC_DATA.UDP_Buffer_Len-2);

   if ((Hi(crc_test) == SBGC_DATA.UDP_Buffer[SBGC_DATA.UDP_Buffer_Len-2]) && (Lo(crc_test) == SBGC_DATA.UDP_Buffer[SBGC_DATA.UDP_Buffer_Len-1]))
      {
       // CRC was good
       SBGC_DATA.UDP_CRC[0] = 0x06;                                                 // ACK
       SBGC_DATA.UDP_CRC[1] = Hi(crc_test);                                         // ACK
       SBGC_DATA.UDP_CRC[2] = Lo(crc_test);                                         // ACK
       UDP_CRC_Good_Reg |= 0b10;                                                //Set good CRC
       UDP_CRC_Reg      &= 0xFFFD;                                              // Clear CRC
      }
      else
      {
       //CRC was bad
       UDP_CRC_Good_Reg &= 0xFFFD;                                              // Clear good CRC
       UDP_CRC_Bad_Reg  |= 0b10;                                                // Set bad CRC
       UDP_CRC_Reg      &= 0xFFFD;                                              // Clear CRC
       //Status(UDPSTA_Add,UDP_UART2_Packet_BAD_CRC);
      }
  }
 }

 // void do_UDP_to_UART2()
 // {
   
 //  UART2.UDP_Buffer[UART2.UDP_Buffer_Len-2] =0; //Delete the UDP CRC
 //  UART2_write_text(UART2.UDP_Buffer);
 //  Status(UDPSTA_Add,UDP_UART2_Packet_RECEIVED);
   //Send an ACK

   
 //  Net_Ethernet_Intern_sendUDP(UART2.RemoteIpAddr, UART2.ACK_Port, UART2.ACK_Port, UART2.UDP_CRC,3);
 //  UDP_CRC_Good_Reg &= 0xFFFD; // Clear CRC
 //  Heart.Enabled =TRUE; // Re-enable heart beat
 // }

#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
//================ SBGC GIMBAL READ ============================================
//
//==============================================================================
/*******************************************************************************
* Function Name: ReadSBGC
********************************************************************************
* Summary:
*  ReadSBGC function performs following functions:
*   1. Reads the UDP buffer data and checks for it being a simpleBGC message
*
*
* Parameters:
*  unsigned char *pdata, uint16_t psz, uint16_t arrPosition
*  pdata = UDP data, psz=size, arrPosition=starting position of message
*
* Return:
* Byte representing the type of SimpleBGC message found or SBGC_NO_CMD_FOUND
*
*******************************************************************************/
uint8_t ReadSBGC(unsigned char *pdata, uint16_t psz, uint16_t arrPosition)      // Read the data portion of the UDP datagram and get SBGC data.
{
  uint8_t start=0;
  uint8_t i;
  uint16_t cmd_state=SBGC_NO_CMD_FOUND;                                         // Initialise the command state and set to the one found once STX has been seen.
  //unsigned int paylen;                                                          // payload length
  //unsigned int nexthead=0;                                                      // next head byte in header
  
  //unsigned char headchksum;                                                     // header checksum
  //unsigned char chksum_calc;                                                    // calculated checksum
  
  arrPosition=0;                                                                // start at the begining of the message
  
  for (i = 0; i < psz ; i++)                                                    // For each char in the buffer
  {
    if (!start)                                                                 // if we dont already have a 0XAA start byte for index 0
    {
      if(pdata[i] == g_SBGC_Stx_Char)                                           // Look for the STX being SimpleBGC
      {
       start =1;                                                                // We got the start so get the comamnd
       arrPosition=i;                                                           // return the start position so we strip leading 0 or F if needed.
      }
    }
    else                                                                        // You saw the simpleBGC start so look at the next byte and check if the cmd is expected.
    {
      switch(pdata[i])                                                          // For the 2nd byte after the STX char.
      {
        case SBGC_CMD_GET_ANGLES:                                               // command was a SBGC get Angles
          return (SBGC_CMD_GET_ANGLES);
          break;
        case SBGC_CMD_AUTO_PID:                                                 // command was a SBGC auto pid response
          return (SBGC_CMD_AUTO_PID);
          break;
        case SBGC_CMD_GET_ANGLES_EXT:                                           // Command get angles ext
          return (SBGC_CMD_GET_ANGLES_EXT);
          break;
        case SBGC_CMD_REALTIME_DATA_3:                                          // command was a SBGC realtime data.
          return (SBGC_CMD_REALTIME_DATA_3);
          break;
        case SBGC_CMD_REALTIME_DATA_4:
          return (SBGC_CMD_REALTIME_DATA_4);
          break;
        case SBGC_CMD_CONFIRM:                                                  // command was a SBGC confirm
          return (SBGC_CMD_CONFIRM);
          break;
        case SBGC_CMD_ERROR:                                                    // command was a SBGC error response
          return (SBGC_CMD_ERROR);
          break;
        case SBGC_CMD_I2C_READ_REG_BUF:                                         // i2c read message   (not used atm)
          return (SBGC_CMD_I2C_READ_REG_BUF);
          break;
        case SBGC_CMD_SET_ADJ_VARS_VAL:                                         // set adjustable vars vals message
          return (SBGC_CMD_SET_ADJ_VARS_VAL);
          break;
        case SBGC_CMD_READ_ADJ_VARS_CFG:                                        // read adjustable vars config message
          return (SBGC_CMD_READ_ADJ_VARS_CFG);
          break;
        case SBGC_CMD_CALIB_INFO:                                               // calibration information command
          return (SBGC_CMD_CALIB_INFO);
          break;
        case SBGC_CMD_REALTIME_DATA_CUSTOM:                                     // Realtime data message (custom)
          return(SBGC_CMD_REALTIME_DATA_CUSTOM);
          break;
        case SBGC_CMD_EVENT:                                                    // Command Event
          return(SBGC_CMD_EVENT);
          break;
        case SBGC_CMD_READ_PARAMS_3:                                            // command was pide request or config read/write part1
          return(SBGC_CMD_READ_PARAMS_3);
          break;
        case SBGC_CMD_BOARD_INFO:                                              // command was request for information on the firmware/state ofthe board
          return(SBGC_CMD_BOARD_INFO);
          break;
        case SBGC_CMD_RESET:                                                    // Reset or
          return(SBGC_CMD_RESET);
          break;
        case SBGC_CMD_I2C_READ_REG_BUF:                                         // i2c read (EEPROM OR IMU)
          return (SBGC_CMD_I2C_READ_REG_BUF);
          break;
        case SBGC_CMD_READ_EXTERNAL_DATA:                                       // Read external data from EEPROM
          return(SBGC_CMD_READ_EXTERNAL_DATA);
          break;
        case SBGC_CMD_SCRIPT_DEBUG:                                             // return from running the script
          return(SBGC_CMD_SCRIPT_DEBUG);
          break;
        default:                                                                // Nothing matched the support.
          return (cmd_state);
          break;
      }
    }
    return (cmd_state);                                                         // return the recognised supported command state.
  }
}
// ======================= SBGC PAYLOAD CHECK ==================================
/*******************************************************************************
* Function Name: check_payload
********************************************************************************
* Summary:
*  check_payload function performs following functions:
*   1. Checks the CRC for a simpleBGC message
*
*
* Parameters:
*  unsigned int buffStartPos - start pos in the buffer
*  SBGC_cmd_board_info_t *boardinf - board revision info
*
* Return:
* 1 if okay 0 if not
*
*******************************************************************************/
uint8_t check_payload(unsigned int buffStartPos, SBGC_cmd_board_info_t *boardinf )
{
   unsigned char *ptr_udp = (unsigned char *) &SBGC_DATA.UDP_Buffer;            // Define the pointer to the container struct
   uint16_t sz_udp=sizeof(SBGC_DATA.UDP_Buffer);                                // Size of the udp payload
   unsigned char paychksum;                                                     // payload checksum sent
   unsigned char calcCRC;                                                       // calculated payload CRC
   uint16_t paylength;                                                          // payload length
   uint16_t payStartLen;                                                        // Paylaod start length
   uint16_t payStartPos;                                                        // Payload Start position
   uint16_t newpayCRC;                                                          // new payload checksum
   uint16_t newCRC;                                                             // 16 bit CRC
  
   payStartLen=SBGC_PAYLOAD_LENGTH_POS+buffStartPos;                            // location in buffer of payload length
   payStartPos=SBGC_LEN_OF_HEADER+buffStartPos;                                 // start location in buffer of payload
   
   paylength= (uint16_t) SBGC_DATA.UDP_Buffer[payStartLen];                     // Read the SBGC payload message length from the SGBC Packet in the data segment of the UDP datagram
   if (sz_udp >= (paylength+SBGC_LEN_OF_HEADER))                                // check we have a full message.
   {
      if (boardinf->FIRMWARE_VER >= 2680U)                                      // New version of code is now a 16 bit checksum
      {
          newpayCRC=((SBGC_DATA.UDP_Buffer[paylength+SBGC_LEN_OF_HEADER-1]<<8) | SBGC_DATA.UDP_Buffer[paylength+SBGC_LEN_OF_HEADER]);  // read the SBGC payload crc from the UDP data segment
          newCRC = crc16_arc_calculate((sz_udp-SBGC_LEN_OF_HEADER),(ptr_udp+payStartPos));               // Calculate new 16 bit CRC for new version
          if (newpayCRC == newCRC)                                              // They match
          {
            return(1);                                                          // return payload CRC okay.
          }
      }
      else
      {
         paychksum=SBGC_DATA.UDP_Buffer[paylength+SBGC_LEN_OF_HEADER];          // read the SBGC payload crc from the UDP data segment
         calcCRC = Pay_checksum((ptr_udp+payStartPos),(sz_udp-SBGC_LEN_OF_HEADER));      // calculate the payload checksum from the data in the UDP data segment
         if (paychksum == calcCRC)                                              // They match
         {
            return(1);                                                          // return payload CRC okay.
         }
      }
   }
   return (0);                                                                  // return error (the CRC in UDPdata and that calc from payload didnt match).
}
// ===================== SBGC GIMBAL MESSAGE ===================================
//
/*******************************************************************************
* Function Name: SBGC_process_UDP_InBuffer
********************************************************************************
* Summary:
*  Process the SGBC message on the relevant UDP port
*
* Parameters:
*  SBGC_cmd_board_info_t *boardinf
*
* Return:
* uint8_t  returns a code to say the type of message
*
*******************************************************************************/
uint8_t SBGC_process_UDP_InBuffer( SBGC_cmd_board_info_t *boardinf )            // Process the in buffer from Gimbal and decide what it means
{
   uint16_t msgCmdType;                                                         // The Cmd Type found in the SBGC Packet

   uint16_t msgStart=0;                                                         // pointer in the buffer to a STX in message
   uint16_t startPayPos;                                                        // start position of the payload in the UDP data buffer

   SBGC_DATA.UDP_Buffer[SBGC_DATA.UDP_Buffer_Len-2] =0;                         //Delete the UDP CRC

   msgCmdType=ReadSBGC(&SBGC_DATA.UDP_Buffer,sizeof(SBGC_DATA.UDP_Buffer),msgStart);    // Read the buffer to verify type and return start location
   
   if ((check_payload(msgStart,boardinf)==1)&&(msgCmdType!=SBGC_NO_CMD_FOUND))           // Check the CRC payload is correct before copying also chk type supported.
   {
      startPayPos=msgStart+SBGC_START_PAYLOAD_POS;                              // go to the start position for payload.
      switch(msgCmdType)                                                        // For the state of the switches from the Commander GS
      {
        case SBGC_CMD_GET_ANGLES:                                               // command was a SBGC get Angles
          memcpy(&getangles, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(getangles));  // Copy the payload to the get angles readback message container.
          break;
        case SBGC_CMD_AUTO_PID:                                                 // command was a SBGC auto pid response
          memcpy(&pidreadbk, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(pidreadbk));  // Copy the payload to the pid readback message container.
          break;
        case SBGC_CMD_GET_ANGLES_EXT:                                           // Command get angles ext
          memcpy(&getanglesext, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(getanglesext));    // Copy the payload to the get ext angles readback message container.
          break;
        case SBGC_CMD_REALTIME_DATA_3:                                          // command was a SBGC realtime data.
          memcpy(&realdata3, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(realdata3));    // Copy the payload to the realtime data 3 readback message container.
          break;
        case SBGC_CMD_REALTIME_DATA_4:
          memcpy(&realdata4, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(realdata4));    // Copy the payload to the realtime data 4 readback message container.
          break;
        case SBGC_CMD_CONFIRM:                                                  // command was a SBGC confirm
          memcpy(&cmdconf, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(cmdconf));        // Copy the payload to the command confirmation readback message container.
          break;
        case SBGC_CMD_ERROR:                                                    // command was a SBGC error response
          memcpy(&cmderror, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(cmderror));      // Copy the payload to the command confirmation readback message container.
          break;
        case SBGC_CMD_SET_ADJ_VARS_VAL:                                         // command was a SBGC set adj vars val of 7 in response to our get adj vars val
          memcpy(&getvar_rd, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(getvar_rd));     // Copy the payload to the command confirmation readback message container.
          break;
        case SBGC_CMD_BOARD_INFO:                                               // command was request for information on the firmware/state ofthe board
          memcpy(&boardinforb, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(boardinforb));
          break;
        case SBGC_CMD_READ_PARAMS_3:                                            // command was request for pide tune or config part 1 read/write
          memcpy(&readparam3, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(readparam3));
          break;
        case SBGC_CMD_RESET:                                                    // Reset issued feedback response
          break;
        case SBGC_CMD_I2C_READ_REG_BUF:                                         // i2c EEPROM or IMU read
          break;
        case SBGC_CMD_READ_EXTERNAL_DATA:                                       // external EEPROM read
          break;
        case SBGC_CMD_SCRIPT_DEBUG:                                             // start / stop script feedback
          memcpy(&scriptstate, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(scriptstate));
          break;
        case SBGC_CMD_EVENT:                                                    // motor state change event
          memcpy(&eventCmd, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(eventCmd));
          break;
        case SBGC_CMD_REALTIME_DATA_CUSTOM:                                     // realtime data custom frame readback
          memcpy(&realtimedatacust, &SBGC_DATA.UDP_Buffer[startPayPos], sizeof(realtimedatacust));
          break;
      }
    }
    
    if (msgCmdType!=SBGC_NO_CMD_FOUND)                                          // Message was recognised okay
    {
       //Status(UDPSTA_Add,UDP_UART2_Packet_RECEIVED);

       //Send an ACK to the COM PIC (May not be needed)
       //
       Net_Ethernet_Intern_sendUDP(SBGC_DATA.RemoteIpAddr, SBGC_Ack_Port, SBGC_Ack_Port, SBGC_DATA.UDP_CRC,3);
       UDP_CRC_Good_Reg &= 0xFFFD;                                              // Clear CRC
       Heart.Enabled =TRUE;                                                     // Re-enable heart beat
   }
   return (msgCmdType);                                                         // return a code to say the type of message.
}
#endif                                                                          // ================ eNd simpleBGC protocol ==============================
#ifdef RUN_CAM_USED
/*******************************************************************************
* Function Name: ReadRunCam
********************************************************************************
* Summary:
*  Process the Run Cam message on the relevant UDP port
*
* Parameters:
*  unsigned char *pdata, uint16_t psz
*
* Return:
* uint8_t  returns a code to say the type of message or failure
*
*******************************************************************************/
uint8_t ReadRunCam( unsigned char *pdata, uint16_t psz )                        // Read the data portion of the UDP datagram and get Run Cam data.
{
  uint8_t start=0;
  uint8_t i;
  uint16_t cmd_state=SBGC_NO_CMD_FOUND;                                         // Initialise the command state and set to the one found once STX has been seen.
  uint16_t arrPosition;                                                         // position of the STX in the UDP message data segment
  
  //unsigned int paylen;                                                          // payload length
  //unsigned int nexthead=0;                                                      // next head byte in header

  //unsigned char headchksum;                                                     // header checksum
  //unsigned char chksum_calc;                                                    // calculated checksum

  arrPosition=0;                                                                // start at the begining of the message

  for (i = 0; i < psz ; i++)                                                    // For each char in the buffer
  {
    if ((!start) && ((g_RunCamSent!=RC_PROTO_CMD_READ_SETTING_DETAIL) && (g_RunCamSent!=RC_PROTO_CMD_GET_SETTINGS)))           // if we dont already have a 0XAA start byte for index 0 (setting details have no STX)
    {
      if(pdata[i] == RC_STX_CHAR)                                               // Look for the STX being Run Cam protocol
      {
       start =1;                                                                // We got the start so get the comamnd
       arrPosition=i;                                                           // return the start position so we strip leading 0 or F if needed.
      }
    }
    else if (g_RunCamSent==RC_PROTO_CMD_READ_SETTING_DETAIL)                    // Setting detail doesnt seem to have a STX so handle here as well
    {
       if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamRepReadSetup))
          return(RC_MSG_FAIL);
       else
          memcpy(&RCCamRepReadSetup, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamRepReadSetup));
    }
    else if (g_RunCamSent==RC_PROTO_CMD_GET_SETTINGS)                           // Setting detail doesnt seem to have a STX so handle here as well
    {
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCGetSettings))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCGetSettings, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCGetSettings));
          break;
    }
    else                                                                        // You saw the start transmission byte
    {
      switch(g_RunCamSent)                                                      // Global send for Run Cam it doesnt reply with the message id
      {
          case RC_PROTO_CMD_GET_DEVICE_INFO:                                    // 0x00U INFO
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamRepInfo))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamRepInfo, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamRepInfo));
          break;
          
          case RC_PROTO_CMD_CAMERA_CONTROL:                                     // 0x01U CONTROL REQUEST (no reply ?)
          break;
          
          case RC_PROTO_CMD_5KEY_SIMULATION_PRESS:                              // 0x02U Key pad press
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamKeyConfirm))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamKeyConfirm, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamKeyConfirm));
          break;
          
          case RC_PROTO_CMD_5KEY_SIMULATION_RELEASE:                            // 0x03U Key pad release
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamKeyConfirm))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamKeyConfirm, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamKeyConfirm));
          break;

          case RC_PROTO_CMD_5KEY_CONNECTION:                                    // 0x04U Connection to camera
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamHandshakeConfirm))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamHandshakeConfirm, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamHandshakeConfirm));
          break;

          case RC_PROTO_CMD_GET_SETTINGS:                                       // 0x10U Sub settings
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamHandshakeConfirm))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamHandshakeConfirm, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamHandshakeConfirm));
          break;
          
          case RC_PROTO_CMD_READ_SETTING_DETAIL:                                // 0x11U read
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCGetSettings))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCGetSettings, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCGetSettings));
          break;

          case RC_PROTO_CMD_WRITE_SETTING:                                      // 0x12U write
          if (strlen(&RunC_DATA.UDP_Buffer[arrPosition]) != sizeof(RCCamRepWriteSetup))
             return(RC_MSG_FAIL);
          else
             memcpy(&RCCamRepWriteSetup, &RunC_DATA.UDP_Buffer[arrPosition], sizeof(RCCamRepWriteSetup));
          break;
          
          case RC_PROTO_CMD_DISP_FILL_REGION:
          break;
          
          case RC_PROTO_CMD_DISP_WRITE_CHAR:
          break;
          
          case RC_PROTO_CMD_DISP_WRITE_HORT_STRING:
          break;
          
          case RC_PROTO_CMD_DISP_WRITE_VERT_STRING:
          break;
          
          case RC_PROTO_CMD_DISP_WRITE_CHARS:
          break;
          
          default:
          return(RC_MSG_FAIL);                                                  // Incorrect message was sent already
          break;
      }
      return(RC_MSG_OK);                                                        // Good Run Cam message
    }
  }

  return(RC_MSG_FAIL);                                                          // Bad Run Cam message no STX
}
#endif                                                                          // =============== eNd Run Cam ===================================
#if (CAMERA_TYPE == XS_CAM)
/*******************************************************************************
* Function Name: XS_process_UDP_InBuffer
********************************************************************************
* Summary:
*  Process the AMP Encoder/Decoder message on the relevant UDP port
*
* Parameters:
*  none
*
* Return:
* uint8_t  returns a code to say the type of message or failure
*
*******************************************************************************/
//=================== XS UDP MESSAGE PROCESSING ================================
// Simple Handler ORIG_SIMPLE does not cater for return values from the encoder
//        e.g. $GETHUE >>
//        << +OK,20
//==============================================================================
#ifdef ORIG_SIMPLE_CAM                                                          // ====== If we ignore $GET $SYS and $DISK messsages (simple reply) =========================
int8_t XS_process_UDP_InBuffer()                                                // Process the in buffer from XStream and decide what it means
{
  
  //  XS_DATA.UDP_Buffer[XS_DATA.UDP_Buffer_Len-2] =0;                              // Delete the UDP CRC (?? check if still valid in COM PIC)
  
  if (XS_DATA.UDP_Buffer[0] == 43)                                              // Valid Reply only begins with a + sign
  {
     if (!strncmp(&XS_DATA.UDP_Buffer,"+OK",3))                                 // Return XS_OK if we match (command complete)
     {
        return (XS_OK);
     }
     else if (!strncmp(&XS_DATA.UDP_Buffer,"+AUTH",5))                          // Return XS_AUTH if we match (need to login)
     {
        return (XS_AUTH);
     }
     else                                                                        // Return an Error code
     {
       return (XS_ERROR);
     }
  }
  else
  {
    return (XS_ERROR);                                                          // Return and Error code
  }

}
#endif
#ifndef ORIG_SIMPLE_CAM
int8_t XS_process_UDP_InBuffer()                                                // Process the in buffer from XStream and decide what it means
{
  uint8_t switchValue;                                                          // what tells you if it was extended or not
  unsigned char* token;                                                         // what is extracted from the , field
  uint8_t fieldNo=0;                                                            // counter representing the number of fields separated by a comma in the message
  
  //  XS_DATA.UDP_Buffer[XS_DATA.UDP_Buffer_Len-2] =0;                              // Delete the UDP CRC (?? check if still valid in COM PIC)
  fieldNo=0;
  token = strtok(&XS_DATA.UDP_Buffer, ",");                                     // Returns first token
  if (token != NULL)                                                            // Conatains no comma's
  {
     if (XS_DATA.UDP_Buffer[0] == 43)                                           // Valid Reply only begins with a + sign
     {
        if (!strncmp(&XS_DATA.UDP_Buffer,"+OK",3))                              // Return XS_OK if we match (command complete)
        {
           return (XS_OK);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+AUTH",5))                       // Return XS_AUTH if we match (need to login)
        {
           return (XS_AUTH);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+FAIL",5))                       // Return XS_FAIL if the command failed
        {
           return (XS_FAIL);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+WRONG",6))                      // Return XS_WRONG if we got a wrong channel request
        {
           return (XS_WRONG);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+INVALID",8))                    // Return XS_INVALID if we got a wrong parameter value sent
        {
           return (XS_INVALID);
        }
        else
        {
          return (XS_ERROR);                                                    // Return an Error code
        }
     }
     else
     {
       return (XS_ERROR);                                                       // Return and Error code
     }
  }
  else
  {
     if (!strncmp(token,"+OK",3))                                               // it was XS_OK with value fields after it
     {
        fieldNo=0;
        token = strtok(&XS_DATA.UDP_Buffer, ",");                               // Returns first token that was preceeding the comma
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {

             case 0:                                                            // 1st case <Time field> or other value
             sprintf( g_encTime,"%s\n", token);
             if (!strncmp(g_encTime,"OK",2))                                    // if it was a 2nd OK its a reply to stop/mark all channels so just report ok back
             {
                return(XS_OK);                                                  // return an ok reposonse if got +ok,ok etc
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 1:                                                            // 2nd case is that we have more fields than one after a comma
             sprintf( g_strField2,"%s\n", token);
             if(isdigit(g_strField2[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField2 = atol(g_strField2);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX ;
             break;

             case 2:                                                            // 3rd case is that we have more fields than one after a comma
             sprintf( g_strField3,"%s\n", token);
             if(isdigit(g_strField3[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField3 = atol(g_strField3);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 3:                                                            // 4th case is that we have more fields than one after a comma
             sprintf( g_strField4,"%s\n", token);
             if(isdigit(g_strField4[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField4 = atol(g_strField4);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 4:                                                            // 5th case is that we have more fields than one after a comma
             sprintf( g_strField5,"%s\n", token);
             if(isdigit(g_strField5[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField5 = atoi(g_strField5);                        // convert to int8_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;
             
             case 5:                                                            // 5th case is that we have more fields than one after a comma
             sprintf( g_strField5,"%s\n", token);
             if(isdigit(g_strField5[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField5 = atoi(g_strField5);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 6:                                                            // 6th case is that we have more fields than one after a comma
             sprintf( g_strField6,"%s\n", token);
             if(isdigit(g_strField6[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField6 = atoi(g_strField6);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 7:                                                            // 7th case is that we have more fields than one after a comma
             sprintf( g_strField7,"%s\n", token);
             if(isdigit(g_strField7[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField7 = atoi(g_strField7);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 8:                                                            // 8th case is that we have more fields than one after a comma
             sprintf( g_strField8,"%s\n", token);
             if(isdigit(g_strField8[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField8 = atoi(g_strField8);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;
             
             default:                                                           // Too much to parse
             return(XS_ERROR);                                                  // return an error
             break;
          }
        }
        
        token = strtok(g_encTime, ":");                                         // Returns first token that was preceeding the : char i.e. its a time
        if (token != NULL)                                                      // It was a time string that was returned (it contained a : )
           return(XS_TIME);                                                     // return time request found
        else
        {
           //if((encTime[0]>=0x30) && (encTime[0]<=0x39))                       // It was an ascii number with no : so not a time assume this is a value to return
           //{
           if(isdigit(g_encTime[0]))                                            // the 1st char was an ascii number with no : so not a time assume this is a value to return
           {
              g_okField1 = atol(g_encTime);                                     // convert the value to a number
           }
           else
           {
              strcpy(g_strField1,g_encTime);                                    // copy it to a string field
           }
        }
        return (XS_OK+fieldNo);                                                 // return an okay plus the number of fields parsed
     }
     else if (!strncmp(token,"+DISKSTATUS",11))                                 // it was a diskstatus request
     {
        fieldNo=0;
        token = strtok(&XS_DATA.UDP_Buffer, ",");                               // Returns first token
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {
             case 0:                                                            // 1st case <diskstatus> diskStatus string
             sprintf( g_diskStatus,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 1:                                                            // 2nd case is <totalsize>
             g_totalSizeXS = atol(token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 2:                                                            // 3rd case is <remaining>
             g_remSizeXS = atol(token);
             token = strtok(NULL, ",");
             fieldNo=0;                                                         // reset the field number for next time it is used
             return(XS_DISK);                                                   // Tell the upper caller it was a DISKSTATUS reply
             break;

             default:
             return(XS_ERROR);                                                  // return an error
             break;
          }
        }
     }
     else if (!strncmp(token,"+SYSSTATUS",10))                                  // it was a sysstatus request
     {
        fieldNo=0;
        token = strtok(&XS_DATA.UDP_Buffer, ",");                                     // Returns first token
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {
             case 0:                                                            // 1st case <systemstatus>
             sprintf( g_sysStatus,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 1:                                                            // 2nd case is <numchannels> = 4
             //numChan = atol(token);                                           Dont bother as always set to 4 for our encoder
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 2:                                                            // 3rd case is <channelstatus> No.1
             sprintf( g_ch1Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 3:                                                            // 4th case is <channelstatus> No.2
             sprintf( g_ch2Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 4:                                                            // 5th case is <channelstatus> No.3
             sprintf( g_ch3Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 5:                                                            // 6th case is <channelstatus> No.4
             sprintf( g_ch4Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo=0;                                                         // reset the field number for next time it is used
             return(XS_SYS);                                                    // Tell the upper caller it was a SYSSTATUS reply
             break;
             
             default:
             return(XS_ERROR);
             break;
          }
        }
     }
  }
}
#endif                                                                          // end NOT ORIG_SIMPLE_CAM
#ifdef UART4_INTERUPT                                                           // This is set if we are enabling UART4 with XS encoder serial
//=================== XS SERIAL MESSAGE PROCESSING =============================
//
/*******************************************************************************
* Function Name: XS_process_SERIAL_InBuffer
********************************************************************************
* Summary:
*  Process the XS message on the relevant serial port
*
* Parameters:
*  none
*
* Return:
* uint8_t  returns a code to say the type of message or failure
*
*******************************************************************************/
int8_t XS_process_SERIAL_InBuffer()                                             // Process the in buffer from XStream and decide what it means
{
  uint8_t switchValue;                                                          // what tells you if it was extended or not
  unsigned char* token;                                                         // what is extracted from the , field
  uint8_t fieldNo=0;                                                            // counter representing the number of fields separated by a comma in the message
  
  switchValue=UART4.State - UART4_PACKET_IN_BUFFER;                             // SUBTRACT the ready to get the g_extended state which was added at time of end message (which identified various char on parsing)
  
  if (UART4.Buffer[0] == 43)                                                    // Valid Reply only begins with a + sign
  {
     switch(switchValue)
     {
        case 0:                                                                 // no extended message
        if (!strncmp(&UART4.Buffer,"+OK",3))                                    // Return XS_OK if we match (command complete)
        {
           return (XS_OK);
        }
        else if (!strncmp(&UART4.Buffer,"+AUTH",5))                             // Return XS_AUTH if we match (need to login)
        {
           return (XS_AUTH);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+FAIL",5))                       // Return XS_FAIL if the command failed
        {
           return (XS_FAIL);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+WRONG",6))                      // Return XS_WRONG if we got a wrong channel request
        {
           return (XS_WRONG);
        }
        else if (!strncmp(&XS_DATA.UDP_Buffer,"+INVALID",8))                    // Return XS_INVALID if we got a wrong parameter value sent
        {
           return (XS_INVALID);
        }
        else                                                                    // Return an Error code as it was +wrong or +fail (prog just ignores these)
        {
           return (XS_ERROR);
        }
        break;
        
        case 3:                                                                 // +OK extended was found we asume its a time request other get calls not used
        fieldNo=0;
        token = strtok(&UART4.Buffer, ",");                                     // Returns first token that was preceeding the comma
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {
             case 0:                                                            // The command should be +OK
             if (!strncmp(token,"+OK",3))
             {
               token = strtok(NULL, ",");                                       // get the next field separated by a comma
               fieldNo++ % UINT8_MAX;                                                       // increase the field number
             }
             else
             {
               token = NULL;                                                    // terminate wrong message was parsed
               return(XS_ERROR);                                                // return an error
             }
             break;

             case 1:                                                            // 1st case <Time field> or other value
             sprintf( g_encTime,"%s\n", token);
             if (!strncmp(g_encTime,"OK",2))                                    // if it was a 2nd OK its a reply to stop/mark all channels so just report ok back
             {
                return(XS_OK);                                                  // return an ok reposonse if got +ok,ok etc
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 2:                                                            // 2nd case is that we have more fields than one after a comma
             sprintf( g_strField2,"%s\n", token);
             if(isdigit(g_strField2[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField2 = atol(g_strField2);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 3:                                                            // 3rd case is that we have more fields than one after a comma
             sprintf( g_strField3,"%s\n", token);
             if(isdigit(g_strField3[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField3 = atol(g_strField3);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;
             
             case 4:                                                            // 4th case is that we have more fields than one after a comma
             sprintf( g_strField4,"%s\n", token);
             if(isdigit(g_strField4[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField4 = atol(g_strField4);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 5:                                                            // 5th case is that we have more fields than one after a comma
             sprintf( g_strField5,"%s\n", token);
             if(isdigit(g_strField5[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField5 = atoi(g_strField5);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 6:                                                            // 6th case is that we have more fields than one after a comma
             sprintf( g_strField6,"%s\n", token);
             if(isdigit(g_strField6[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField6 = atoi(g_strField6);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;

             case 7:                                                            // 7th case is that we have more fields than one after a comma
             sprintf( g_strField7,"%s\n", token);
             if(isdigit(g_strField7[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField7 = atoi(g_strField7);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;
             
             case 8:                                                            // 8th case is that we have more fields than one after a comma
             sprintf( g_strField8,"%s\n", token);
             if(isdigit(g_strField8[0]))                                        // the 1st char was an ascii number then convert it
             {
                g_okField8 = atoi(g_strField8);                                 // convert to int32_t
             }
             token = strtok(NULL, ",");                                         // check for another comma
             fieldNo++ % UINT8_MAX;
             break;
             
             default:
             return(XS_ERROR);
             break;
          }
        }
        token = strtok(g_encTime, ":");                                         // Returns first token that was preceeding the : char i.e. its a time
        if (token != NULL)                                                      // It was a time string that was returned (it contained a : )
           return(XS_TIME);                                                     // return time request found
        else
        {
           //if((encTime[0]>=0x30) && (encTime[0]<=0x39))                       // It was an ascii number with no : so not a time assume this is a value to return
           //{
           if(isdigit(g_encTime[0]))                                            // the 1st char was an ascii number with no : so not a time assume this is a value to return
           {
              g_okField1 = atol(g_encTime);                                     // convert the value to a number
           }
           else
           {
              strcpy(g_strField1,g_encTime);                                    // copy it to a string field
           }
        }
        return (XS_OK+fieldNo);                                                 // return an okay plus the number of fields parsed
        break;
        
        case 10:                                                                // Should be a +SYSSTATUS reply
        fieldNo=0;
        token = strtok(&UART4.Buffer, ",");                                     // Returns first token
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {
             case 0:                                                            // The command should be +SYSSTATUS
             if (!strncmp(token,"+SYSSTATUS",10))
             {
               token = strtok(NULL, ",");                                       // get the next field
               fieldNo++ % UINT8_MAX;
             }
             else
             {
               token = NULL;                                                    // terminate wrong message was parsed
               return(XS_ERROR);                                                // return an error
             }
             break;

             case 1:                                                            // 1st case <systemstatus>
             sprintf( g_sysStatus,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 2:                                                            // 2nd case is <numchannels> = 4
             //numChan = atol(token);                                           Dont bother as always set to 4 for our encoder
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;

             case 3:                                                            // 3rd case is <channelstatus> No.1
             sprintf( g_ch1Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;
             
             case 4:                                                            // 4th case is <channelstatus> No.2
             sprintf( g_ch2Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;
             
             case 5:                                                            // 5th case is <channelstatus> No.3
             sprintf( g_ch3Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;
             
             case 6:                                                            // 6th case is <channelstatus> No.4
             sprintf( g_ch4Status,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo=0;                                                         // reset the field number for next time it is used
             return(XS_SYS);                                                    // Tell the upper caller it was a SYSSTATUS reply
             break;
             
             default:
             return(XS_ERROR);
             break;
          }
        }
        break;
        
        case 11:                                                                // Should mean we have a +DISKSTATUS
        fieldNo=0;
        token = strtok(&UART4.Buffer, ",");                                     // Returns first token
        while (token != NULL)                                                   // read each delimetered field in the string
        {
          switch (fieldNo)
          {
             case 0:                                                            // The command should be +DISKSTATUS
             if (!strncmp(token,"+DISKSTATUS",10))
             {
               token = strtok(NULL, ",");                                       // get the next field
               fieldNo++ % UINT8_MAX;
             }
             else
             {
               token = NULL;                                                    // terminate wrong message was parsed
               return(XS_ERROR);                                                // return an error
             }
             break;
             
             case 1:                                                            // 1st case <diskstatus> diskStatus string
             sprintf( g_diskStatus,"%s\n", token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;
             
             case 2:                                                            // 2nd case is <totalsize>
             g_totalSizeXS = atol(token);
             token = strtok(NULL, ",");
             fieldNo++ % UINT8_MAX;
             break;
             
             case 3:                                                            // 3rd case is <remaining>
             g_remSizeXS = atol(token);
             token = strtok(NULL, ",");
             fieldNo=0;                                                         // reset the field number for next time it is used
             return(XS_DISK);                                                   // Tell the upper caller it was a DISKSTATUS reply
             break;
             
             default:
             return(XS_ERROR);                                                  // return an error
             break;
          }
        }
        break;
     }
  }
  else
  {
     return (XS_ERROR);                                                         // Return and Error code
  }

}
#endif
#endif                                                                          // ======================= eNd XS Encoder / Decoder ======================

/*******************************************************************************
* Function Name: sizeToChar
********************************************************************************
* Summary:
*  String function to calculate a length up to the given char specified
*
* Parameters:
*   unsigned char* stringX, unsigned char val
*
* Return:
* uint8_t  returns length (up to the char)
*
*******************************************************************************/
uint8_t sizeToChar( unsigned char* stringX, unsigned char val )
{
   uint8_t lentoChar=0U;

   while (*stringX++ != val) lentoChar++ % UINT8_MAX;
   return(lentoChar);                                                           // if you want to include the char then add 1 to it
}

/*
 * Function Name:
 *   do_UART_to_UDP()
 * Description:
 *   Sends incoming packetsd from UART out UDP
 * Arguments:
 *   None
 * Returns:
 *   None
 * NOTES:
 *   <notes>
 */
#ifdef DO_COMPIC
char do_UART_to_UDP()
{
    unsigned int ethOk;
    char str1[32]={"No State\n\r"};
    // @@250919 init to zero not 1 because Net_Ethernet_Intern_sendUDP returns a zero on error.
    char stat =0;
   // char str1[100]={"No known state"};
   
   //Uart5_write_text("in doUART\n\r");
    // If Packet comes from UART 2 send it UDP to remote IP address, sourcePort 10002 (UART2RXPort), destination port 6767 (UART2TXPort)
   if (UART2.State == UART2_SEND_UDP)
   {
    Heart.Enabled = FALSE;
   // wait for the etthernet state not to be busy txbusy or rxbusy
    //  ethOk = (ETHSTAT & 0x60)>>5;
    ethOk = (ETHSTAT & 0x40)>>5;      // ignore RXBUSY only wait for TXBUSY
    //ETHCON1CLR = 0x8300;
    T2CON = ENABLE_T2;
    while (! (ethOk==0) )
    {
       //ethOk = (ETHSTAT & 0x60)>>5;
       ethOk = (ETHSTAT & 0x40)>>5;      // just wait for not TXBUSY ignore RXBUSY
       sprintf(str1,"ETH_NOT_FREE : %d\n\r",ETHSTAT);

       Uart5_write_text(&str1);
    }
     UART2.Index_Was = UART2.Index;
    // T5CON         = 0x00; //Stop timer 5 i.e. stop Heart beat so we dont try sending a UDP Packet whilst sending this one.
     if(Air.Link_Alive | MasGs.Link_Alive | FirGs.Link_Alive)
     {

      stat = Net_Ethernet_Intern_sendUDP(UART2.RemoteIpAddr, UART2.From_Port, UART2.Dest_Port, &UART2.Buffer,UART2.Index);
       Status(UDPSTA_Add,stat);//UDP_STATE: -UDP Packet sent or  UDP_STATE: -Packet Send Failed
       // @@12092019 commented this out for now ---- if (!stat){Init_Ether(); uart5_write("Ether Reset\n\r");}

       Heart.Enabled = TRUE;
       UART2.UDP_Byte_Sent += UART2.Index;
       UART2.UDP_Packets_Sent=++UART2.UDP_Packets_Sent % UINT16_MAX;
       UART2.UDP_Send_trys++ % UINT8_MAX;
       UART2.Index=CLEAR;
       UART2.State = UART2_CHECK_ACK_TIME_OUT;
     }
     else
     {
       Heart.Enabled = TRUE;
       //UART2.UDP_Byte_Sent += UART2.Index;
       //UART2.UDP_Packets_Sent++;
       //UART2.UDP_Send_trys++;
       UART2.Index_Was = UART2.Index;
       UART2.Index = CLEAR;
       UART2.State = CLEAR;
       UART2.UDP_Send_Fail++ % UINT8_MAX;
       Status(ETHER_LINK_STATE,LINK_DOWN);
     }

    //T2CON = ENABLE_T2;       // Enable Timeout
   }
    return (stat);
  }
#endif

#ifdef USE_CAN
void do_CAN_to_UDP()
{

   uint16_t Can2_UDP_TX_Packet[12];
   int MSGLen;
   int counter;
   //unsigned short Mask=255;
   counter =0;
   MSGLen=1;
   

         //Can_Rcv_Flags =0; //Clear the message flag
         if (Can_msg_rcvd=CAN2Read(&Can2_RX_MSG_ID, Can2_Msg_Rcvd, &Can2_MSG_Len, &Can2_Rcv_Flags))      
 {
          LATB0_bit = ~PORTB;
/*// CLEAR OUT TX STRING
            while(counter<12)
             counter++;
            }*/

      Can2_UDP_TX_Packet[0] = Lo(Can2_RX_MSG_ID);

      Can2_UDP_TX_Packet[1] = Hi(Can2_RX_MSG_ID);

      counter = 0;
      while(counter < Can2_MSG_Len)
      {
         Can2_UDP_TX_Packet[counter+2] = Can2_Msg_Rcvd[counter];
       // CLEAR OUT TX STRING
            while(counter<12){
             Can2_UDP_TX_Packet[counter]=0;
             counter++;
            }
         counter++;
       }
       counter = 0;

      //MSGLen=strlen(Can2_UDP_TX_Packet);
      MSGLen=Can2_MSG_Len+2;

      for (counter =0; counter <10;)
      {
        if (Remote_Node[counter].Link_Alive)
        {
         Net_Ethernet_Intern_sendUDP(Remote_Node->IPAddr[counter],Can2fromPort,Can2destPort,&Can2_UDP_TX_Packet, MSGLen);
         counter++;
        }
      }
  }

 }

/*******************************************************************************
* Function Name: Can2_Test
********************************************************************************
* Summary:
*  Can test
*
* Parameters:
*   none
*
* Return:
*   none
*
*******************************************************************************/
void Can2_Test()
{
  Can_RxTx_Data1[0] = 0xFE;
  Can_RxTx_Data1[1] = 0xFF;
      while(1){


        Can_RxTx_Data1[0]++;                                                           // increment received data
        Can_RxTx_Data1[1]++;                                                           // increment received data
        Can_RxTx_Data1[2]=0X02;                                                        // increment received data
        delay_ms(500);
        CAN2Write(Can2_TX_MSG_ID, Can_RxTx_Data1, 8, Can2_Send_Flags);                           // send incremented data back
      }

}
#endif
/*******************************************************************************
* Function Name: bitWrite
********************************************************************************
* Summary:
*  Can test
*
* Parameters:
*   char *x, char n, char value
*
* Return:
*   none
*
*******************************************************************************/
void bitWrite(char *x, char n, char value) {
   if (value)
      *x |= (1 << n);
   else
      *x &= ~(1 << n);
}
/*******************************************************************************
* Function Name: bitRead
********************************************************************************
* Summary:
*  Can test
*
* Parameters:
*   char *x, char n
*
* Return:
*   char 1 if match 0 not
*
*******************************************************************************/
char bitRead(char *x, char n) {
   return (*x & (1 << n)) ? 1 : 0;
}

//=========================== ETHERNET INIT ====================================
//
/*******************************************************************************
* Function Name: Init_Ether()
********************************************************************************
* Summary:
*  Reset and initialise the NIC
*
* Parameters:
*   none
*
* Return:
*   none
*
*******************************************************************************/
void Init_Ether()
{
  // added this here to reset the NIC @@12092019
  IEC1CLR = _IEC1_ETHIE_MASK;                                                   // disable NIC interrupts 0x10000000
  // used to be ETHCON1CLR = 0x00008300;                                         the 300 isnt in the cpu header file under ETHCON1
  ETHCON1CLR =  _ETHCON1_ON_MASK | _ETHCON1_TXRTS_POSITION | _ETHCON1_RXEN_MASK;// turn the ethernet controller off 0x00008109
  while(ETHSTAT&_ETHSTAT_ETHBUSY_MASK);                                         // Wait activity abort by polling the ETHBUSY bit (bit 7) 0x80
  ETHIENCLR = 0x000063ef;                                                       // Disable any Ethernet controller interrupt generation
  ETHIRQCLR = 0x000063ef;                                                       // Disable any Ethernet controller interrupt generation
  ETHCON1SET = _ETHCON1_ON_MASK;                                                // Enable the Ethernet controller by setting the ON bit 0x00008000
  IFS1CLR = 0x10000000;                                                         // Clear Ethernet interrupt flag
  ETHIEN = 0;                                                                   // Disable any Ethernet controller interrupt generation
  ETHIRQ = 0;                                                                   // Disable any Ethernet controller interrupt generation
  ETHTXST = 0;                                                                  // Clear the TX and RX start addresses
  ETHRXST = 0;                                                                  // Clear the TX and RX start addresses
  // ETHIENSET = 0x0000400c;                                                     Were what i had before  // enable RXACT interrupt
  // IPC12CLR = 0x0000001f;
  // IPC12SET = 0x000016;
  // IEC1SET = 0x10000000;                                                      // RE_Enable Ethernet MAC interrupts
  SPI1CONbits.ON= FALSE;                                                        // disable SPI for TCPIP interface
  WDTCONbits.ON = FALSE;                                                        // watchdog off
  
  EMAC1CFG1SET = 0x00008000;                                                    // Reset the MAC using SOFTRESET
  EMAC1CFG1CLR = 0x00008000;                                                    // Reset the MAC using SOFTRESET
  EMAC1SUPPSET = 0x00000800;                                                    // Reset the RMII module
  EMAC1SUPPCLR = 0x00000800;                                                    // Reset the RMII module

  EMAC1MCFGSET = 0x00008000;                                                    // Issue an MIIM block reset by setting the RESETMGMT bit
  EMAC1MCFGCLR = 0x00008000;                                                    // Issue an MIIM block reset by setting the RESETMGMT bit
  
  EMAC1MCFG = _EMAC1MCFG_CLKSEL_DIV40;                                          // Select the proper divider for the MDC clock (8<<0x00000002)

  Net_Ethernet_Intern_Set_Default_PHY();                                        // Not sure if we need this here or at start

  ETHHT0 = 0;                                                                   // Initialize hash table
  ETHHT1 = 0;

  ETHRXFC =  _ETHRXFC_HTEN_MASK | _ETHRXFC_CRCOKEN_MASK  |  _ETHRXFC_RUNTEN_MASK | _ETHRXFC_UCEN_MASK |  _ETHRXFC_BCEN_MASK;    // Configure the receive filter

  Net_Ethernet_Intern_Init(This_Node.MacAddr, This_Node.IpAddr, Net_Eth_Int_AUTO_NEGOTIATION & Net_Eth_Int_DEFAULT_MAC & Net_Eth_Int_SPEED_100 & Net_Eth_Int_FULLDUPLEX ); // init ethernet board as My
  Net_Ethernet_Intern_confNetwork(ipMask, gwIpAddr, dnsIpAddr);
  Net_Ethernet_Intern_Enable( _Net_Ethernet_Intern_UNICAST | _Net_Ethernet_Intern_BROADCAST | _Net_Ethernet_Intern_CRC); // enable Unicast traffic
  EMAC1CFG1 = 0x00000001;                                                       // Disable flow control RX_ENAB (MAC)
  EMAC1CFG2 = 0x00000020 | 0x00000010;                                          // Automatic padding and CRC generation This is youre own Intern_Enable
  EMAC1MAXF = 1518;                                                             // Set the maximum frame length
  Net_Ethernet_Intern_Disable( _Net_Ethernet_Intern_MULTICAST );                // disable Multicast traffic
  // Net_Ethernet_Intern_Disable(_Net_Ethernet_Intern_BROADCAST);               // disable Broadcast traffic  (we accept e.g. gratuotous ARP)

  ETHCON2 = PIC32MX_ETH_RX_BUFFER_SIZE;                                         // Set receive buffer size
  ETHCON1SET = 0x00008100;                                                      // Enable the reception by setting the EN AND RXEN bit
  
  //  ETHTXST = (uint32_t) KVA_TO_PA(&txBufferDesc[0]);
  
}

/*******************************************************************************
* Function Name: ARP
********************************************************************************
* Summary:
*  Perform ARP request to remote node
*
* Parameters:
*   unsigned char *NodeIPAddr
*
* Return:
*   char  FALSE = error TRUE = okay
*
*******************************************************************************/
char ARP(unsigned char *NodeIPAddr)
 {   
     unsigned int LC1;
     unsigned int LC2;
     
     if (!CheckTimout())
     {
       LC1 = 0;
       EFinish = 1;
       Ether_Link_Present = FALSE;                                              // Set the link down
          // Flush the ARP Cache
       Net_Ethernet_Intern_arpCache[0].valid = 0;
       Net_Ethernet_Intern_arpCache[1].valid = 0;
       Net_Ethernet_Intern_arpCache[2].valid = 0;
       // Do an ARP request sequence for NMS1
       //Delay_ms(1000);
       while ( (LC1++ < 3) && (EFinish == 1))
       {
          ptr_ru = Net_Ethernet_Intern_arpResolve(NodeIPAddr, 1);               // get MAC address behind the above IP address, wait up to 1 secs for the response
          //Net_Ethernet_Intern_arpResolve(NodeIPAddr, 1);
          Delay_ms(100);                                                        // ok as doing nothing else until resolved (does nothing else until ARP complete anyway
          for ( LC2 = 0; LC2 < ARPCACHESIZE_Intern; ++LC2 )
          {
              if ( Net_Ethernet_Intern_arpCache[LC2].valid == 1 )
              {

               if
               (
                  (Net_Ethernet_Intern_arpCache[LC2].ip[0] == NodeIpAddr[0]) && \
                  (Net_Ethernet_Intern_arpCache[LC2].ip[1] == NodeIpAddr[1]) && \
                  (Net_Ethernet_Intern_arpCache[LC2].ip[2] == NodeIpAddr[2]) && \
                  (Net_Ethernet_Intern_arpCache[LC2].ip[3] == NodeIpAddr[3]) )
                  {
                      Ethernet_Fault = FALSE;
                      Ether_Link_Present = TRUE;                                // Ethernet is present
                      LC2 = 3;                                                  // set to exit for loop
                      EFinish = 0;                                              // set to exit the for loop

                      if (Ether_Link_Present != Ether_Link_Present_WAS)
                      {
                        Ether_Link_Present_WAS = Ether_Link_Present;
                        Status(ETHER_LINK_STATE,Ether_Link_Present);
                      }
                      if (!Ether_Link_Present){StartTimeout();}
                        return TRUE;                                            // return success
                   }
                   else                                                         // Not the ip address requested
                   {
                      Ethernet_Fault = TRUE;
                      Ether_Link_Present = FALSE;                               // Ethernet is NOT present
                      Heart.Time = Net_Ethernet_Intern_userTimerSec;
                      Heart.Enabled = TRUE;
                      if (Ether_Link_Present != Ether_Link_Present_WAS)
                      {
                          Ether_Link_Present_WAS = Ether_Link_Present;
                          Status(ETHER_LINK_STATE,Ether_Link_Present);
                      }
                     if (!Ether_Link_Present){StartTimeout();}
                     //Uart2_write_text("ETH_ARP_FAILED\n\r");                  // record the event
                       return FALSE;                                            // return an error
                   }
               }
            }

         }
      }
 }

/*******************************************************************************
* Function Name: boot_arp()
********************************************************************************
* Summary:
*  Call the ARP at boot-up or on reset (also called on exceeding consequtive bad sends)
*
* Parameters:
*   none
*
* Return:
*   none
*
*******************************************************************************/
void boot_arp()
{
   while(Ether_Link_Present==FALSE)
   {
      WDTCONSET = 0x01;
      // Uart5_write_text("ARP\n\r");
      if (Node==AIR)  {MasGs.Link_Established = ARP(MasGs.IPAddr);              // MASTER GS
                       FirGs.Link_Established = ARP(FirGs.IPAddr);}             // FIRE GS
      else if (Node==MASGS){Air.Link_Established = ARP(Air.IpAddr);}            // COM PIC AIR
      else if (Node==FIRGS){Air.Link_Established = ARP(Air.IpAddr);}            // COM PIC AIR
      else if (Node==GIMJOY){Air.Link_Established = ARP(Air.IpAddr);}           // COM PIC (gimbal / xstream)
      else if (Node==COMGS){Air.Link_Established = ARP(Air.IpAddr);}            // COM PIC (gimbal / xstream)
      // Uart5_write_text("ARP While @boot\n\r");
      asm nop;                                                                  // wait a tick
   }
}

/*******************************************************************************
* Function Name: StartTimeout()
********************************************************************************
* Summary:
*  Collect time ran by ARP function
*
* Parameters:
*   none
*
* Return:
*   none
*
*******************************************************************************/
void StartTimeout() 
{
  TimoutRunning = TRUE;                                                         // set timeout as running
  EthTimePrevCounter = Net_Ethernet_Intern_userTimerSec;                        // save present time
}

// --[ Function ] -----------------------------------------------------------------------------------------------------
// Function Name:               CheckTimout
// Description:                 This function checks if a ruuning TCP timeout has expired and returns a 1 if yes
// Input Parameters:            None
// Output Parameters:           None
// Input/Output Parameters:     None
// Return:                      TRUE if timeout else FALSE
// Notes:                       Nil
// Side Effects:                Nil
// Status:                      Active
// --------------------------------------------------------------------------------------------------------------------
unsigned char CheckTimout() 
{
  if ( TimoutRunning == TRUE )                                                  // set timeout as running
  {
    if ( (Net_Ethernet_Intern_userTimerSec - EthTimePrevCounter) > 4 ) 
    {
      TimoutRunning = FALSE;                                                    // stop timout from running
      return TRUE;                                                              // a timeout occurred
    } 
    else
      return FALSE;                                                              // else no timeout yet
  } 
  return FALSE;                                                                 // a timeout was not running anyway
}

// --[ Function ] -----------------------------------------------------------------------------------------------------
// Function Name:               StopTimer1
// Description:                 This function is used to stop timer 1 so that the timer can be restarted
// Input Parameters:            None
// Output Parameters:           None
// Input/Output Parameters:     None
// Return:                      None
// Notes:                       Nil
// Side Effects:                Nil
// Status:                      Active
// --------------------------------------------------------------------------------------------------------------------
void StopTimer1() 
{
  T1IF_bit = 0;             // clear interrupt flag - Added in by DNT for restarting on the fly purposes
  TMR1 = 0;                 // reset timer value to zero
  T1IE_bit = 0;             // Enable Timer1 Interrupt
  ON__T1CON_bit = 0;        // Disable Timer1
  asm nop;                  // delay to allow settlement
}


// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

// --[ Function ] -----------------------------------------------------------------------------------------------------
// Function Name:             ProcessTcpMsg()
// Description:               This function uses the function Net_Ethernet_Intern_startSendTCP() in the
//                            Net_Ethernet_Internal library to send unsolicited TCP messages to the Remote PIC.
//                            The Tx message buffer should be loaded within Net_Ethernet_Intern_UserTCP() or elsewhere
//                            before transmission is required. It does the unsolicited Tx by sending a dummy ACK to the
//                            Remote which responds. That response causes the the calling of
//                            Net_Ethernet_Intern_UserTCP() which provides the opportunity to tx a message out. Without
//                            that, the library provides NO OTHER WAY to send unsolicited messages, and assumes that you
//                            are always writing code from the Remote perspective. An assumption that is not always
//                            true. It can also be used to process received messages within
//                            Net_Ethernet_Intern_UserTCP(). The function does not hold the cpu until completion so it
//                            must be called contineously from within main().
//                            This routine can be called in different modes:
//                            1) Open the Socket if closed and send a message.
//                            2) Close the socket.
// Input Parameters:          EtherConnState must be set to the entry condition defined in "globaldefs.h" in enum Ethstate
//                            typically starting at OPEN_SOCKET or CLOSE_SOCKET. If you do not want the function to
//                            Close the socket after sending the message, set the flag CloseTcpSocket = 0.
//                            if  EtherConnState = OPEN_SOCKET && CloseTcpSocket = 1;
//                                The TCP Socket will be Auto OPENED, a message SENT, and the socket CLOSED
//                            if  EtherConnState = OPEN_SOCKET && CloseTcpSocket = 0;
//                                The TCP Socket will be "Auto OPENED", a message SENT, and the functions exits without
//                                closing the socket. ("Auto OPENED" means that if the socket is closed, the code will
//                                try and open it. If it is Open , it will nothing and the socket will stay open)
//                            if  EtherConnState = CLOSE_SOCKET && CloseTcpSocket = 0|1;
//                                The TCP Socket will be CLOSED, and the functions exits
//                            if  EtherConnState = UNDEFINED;
//                                The functionality of the routine is disabled and enters without doing anything
//                            StartEthComms must be set 1 at the start of a transmission and further transmissions
//                            must not be started until it has been cleared by this function
// Output Parameters:         none
// Input/Output Parameters:   none
// Return:                    none
// Notes:                     The entire process took about 40mS to 50mS with worst case about 55mS and typical case
//                            about 42mS on a 13 byte send message with nio close socket at the end.
//                            TESTING:
//                            When testing the routine for a fully bi-directional scenario using "TCP/IP Builder", Set
//                            the Destination and Remote port, both to 502. Enter a test string such as "Hi", and use
//                            the "Send" button to send it.
// Side Effects:              None
// Status:                    Active
// --------------------------------------------------------------------------------------------------------------------
 // --------------------------------------------------------------------------------------------------------------------
void ProcessTcpMsg() {
 //Modec to ProcessTcpMsg.txt for now
}

//#if  defined(PARROT_SEQ_CAM) || defined(__Yi_c)
//
// To use TCP/IP This should be called after Net_Ethernet_Intern_confNetwork
//
/*******************************************************************************
* Function Name: StartTCPCommunication()
********************************************************************************
* Summary:
* Initialises TCP/IP Stack for use
*
* Parameters:
* none
*
* Return:
* none
*
*******************************************************************************/
void StartTCPCommunication()
{
   Net_Ethernet_Intern_stackInitTCP();                                          // Initialise TCP Stack
}

#if defined(YI_CAM_USED)
/*******************************************************************************
* Function Name: XY_OpenTCPSocketRawJson
********************************************************************************
* Summary:
*  Open the socket for the Yi Cam RAW to the JSON port
*
* Parameters:
*   SOCKET_Intern_Dsc **sock1
*
* Return:
* return if success is 1
* ======================================
* 0 - no successful transmit SYN segment.
* 1 - successful transmit SYN segment.
* 2 - no available socket.
* 3 - no ARP resolve
*
*******************************************************************************/
uint8_t XY_OpenTCPSocketRawJson( SOCKET_Intern_Dsc **sock1 )
{
  return(Net_Ethernet_Intern_connectTCP(XY_USE_IP, XY_DEF_PORT, XY_DEF_PORT, sock1));     // open the socket
}

/*******************************************************************************
* Function Name: XY_CloseTCPSocketRawJson
********************************************************************************
* Summary:
*  Close the socket for the Yi Cam RAW to the JSON port
*
* Parameters:
*   SOCKET_Intern_Dsc **sock1
*
* Return:
* return if success is 1
* ======================================
* 0 - no successful transmit SYN segment.
* 1 - successful transmit SYN segment.
* 2 - no available socket.
* 3 - no ARP resolve
*
*******************************************************************************/
uint8_t XY_CloseTCPSocketRawJson( SOCKET_Intern_Dsc *sock1 )
{
  return(Net_Ethernet_Intern_disconnectTCP(sock1));                             // close the socket
}

/*******************************************************************************
* Function Name: TCPStateCheck
********************************************************************************
* Summary:
*  TCPStateCheck function performs following functions:
*   1. Checks the state of the socket passed to the function
*   2. Attempts to keep the connection alive
*
* Parameters:
*  SOCKET_Intern_Dsc *used_tcp_socket - The TCP socket.
*
* Return:
*  None.
*
*******************************************************************************/
void TCPStateCheck( SOCKET_Intern_Dsc *used_tcp_socket)
{
  uint8_t XY_retTCP;                                                            // Socket open command return code
     
  switch(used_tcp_socket->state)                                                // for the states of the Yi Cam TCP socket
  {
    case TCP_STATE_CLOSED:                                                      // We dont have a connection
    XY_retTCP=XY_OpenTCPSocketRawJson( &used_tcp_socket );                      // try to open a socket
    switch(XY_retTCP)
    {
        case TCP_NO_SYN_SEND:                                                   // didnt transmit SYN
        XY_retTCP=XY_OpenTCPSocketRawJson( &used_tcp_socket );                  // try again to open a socket
        if (XY_retTCP==TCP_SYN_SUCCESS)
          YiCam.Link_Alive   = TRUE;
        else
          YiCam.Link_Alive   = FALSE;
        break;

        case TCP_SYN_SUCCESS:                                                   // continue the SYN was sent
        YiCam.Link_Alive   = TRUE;
        break;

        case TCP_NO_SOCK:                                                       // no socket
        YiCam.Link_Alive   = FALSE;
        break;

        case TCP_NO_ARP:                                                        // no arp
        YiCam.Link_Alive   = FALSE;
        break;

        default:                                                                // invalid return code
        break;
    }
    YiTimer.tcpClosedTm=++YiTimer.tcpClosedTm % UINT32_MAX;                     // Count the time in closed
    break;

    case TCP_STATE_LISTEN:                                                      // Server is listening for a SYN_ACK from the client in response to the SYN sent
    YiTimer.tcpListenTm=++YiTimer.tcpListenTm % UINT32_MAX;                     // Count the time in listen
    if (YiTimer.tcpListenTm > XY_TCP_TIMEOUT)
      if (Net_Ethernet_Intern_startSendTCP( used_tcp_socket )==0)               // 1 - error occur. 0 - routine sent dummy ACK to remote host. If remote host answer, Net_Ethernet_Intern_UserTCP routine will be called, and than We can put our data in TCP Tx buffer
         YiCam_DATA.State = ETH_ACK_RESENT;                                     // Dummy ACK was sent
    break;

    case TCP_STATE_ESTABLISHED:                                                 // Socket ready to send so dont do anything here
    break;

    case TCP_STATE_SYN_SENT:                                                    // You need to look for a SYN
    YiTimer.tcpSynSentTm=++YiTimer.tcpSynSentTm % UINT32_MAX;                   // Count the time in SYN sent
    if (YiTimer.tcpSynSentTm > XY_TCP_TIMEOUT)
       XY_retTCP=XY_OpenTCPSocketRawJson( &used_tcp_socket );                   // try again to open the socket by sending another SYN
    break;

    case TCP_STATE_FIN_WAIT_1:                                                  // You sent a FIN
    YiTimer.tcpSynSentTm=++YiTimer.tcpSynSentTm % UINT32_MAX;                   // Count the time in fin wait 1
    if (YiTimer.tcpFinWait1Tm > XY_TCP_TIMEOUT)
       XY_retTCP=XY_CloseTCPSocketRawJson( used_tcp_socket );                   // try again to close the socket
    break;

    case TCP_STATE_FIN_WAIT_2:                                                  // You wait for a remote FIN
    YiTimer.tcpFinWait2Tm=++YiTimer.tcpFinWait2Tm % UINT32_MAX;                 // Count the time in fin wait 2
    if (YiTimer.tcpFinWait2Tm > XY_TCP_TIMEOUT)
       XY_retTCP=XY_CloseTCPSocketRawJson( used_tcp_socket );                   // try again to close the socket
    break;

    case TCP_STATE_RETRANSMIT:                                                  // Asked to re-transmit
    if (Net_Ethernet_Intern_startSendTCP( used_tcp_socket )==0)                 // 1 - error occur. 0 - routine sent dummy ACK to remote host. If remote host answer, Net_Ethernet_Intern_UserTCP routine will be called, and than We can put our data in TCP Tx buffer
    {
       YiCam_DATA.State = ETH_ACK_OLD_DATA_REQ;                                 // Dummy ACK was sent
    }
    YiTimer.tcpRetransTm=++YiTimer.tcpRetransTm % UINT32_MAX;                   // Count the time in retransmit
    break;
  }
}
/*******************************************************************************
* Function Name: resetXYActions()
********************************************************************************
* Summary:
*  Reset of the HMI state request from the TCP state engine
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void resetXYActions()
{
// iterative action make the command hmiReqActive a uint64_t if you want
// otherwise case is faster although long to look @
//
//for (counter=1; counter < 32u ; counter++)
//{
//     if (XYRequest == counter)
//     {
//        hmiReqActive1 = hmiReqActive1 ^ (uint64_t) pow(2,((double) counter)); // remove the request bit for the case if we are in that step of the sequence
//        hmiReqActive = (COMGS_YiOptActive_t) hmiReqActive1;                   // set equal will not type cast must make the COMGS_YiOptActive_t a uint64_t if you want this method
//        break;
//     }
//}
   switch (XYRequest)
   {
      case 0u:
      hmiReqActive.batteryLeft=0U;
      break;

      case 1u:
      hmiReqActive.startEvent=0U;
      break;

      case 2u:
      hmiReqActive.tkPhoto=0U;
      break;

      case 3u:
      hmiReqActive.setPIV=0U;
      break;

      case 4u:
      hmiReqActive.getRecTime=0U;
      break;

      case 5u:
      hmiReqActive.startRecord=0U;
      break;

      case 6u:
      hmiReqActive.stopRecord=0U;
      break;

      case 7u:
      hmiReqActive.ccsStop=0U;
      break;

      case 8u:
      hmiReqActive.quickRecord=0U;
      break;

      case 9u:
      hmiReqActive.quickRecPause=0U;
      break;

      case 10u:
      hmiReqActive.quickRecResume=0U;
      break;

      case 11u:
      hmiReqActive.resetWifi=0U;
      break;

      case 12u:
      hmiReqActive.videoStd=0U;
      break;

      case 13u:
      hmiReqActive.videoLoopRes=0U;
      break;

      case 14u:
      hmiReqActive.videoPhoRes=0U;
      break;

      case 15u:
      hmiReqActive.videoLapseRes=0U;
      break;

      case 16u:
      hmiReqActive.videoRes=0U;
      break;

      case 17u:
      hmiReqActive.photoSize=0U;
      break;

      case 18u:
      hmiReqActive.idleMode=0U;
      break;

      case 19u:
      hmiReqActive.wakeUpMode=0U;
      break;

      case 20u:
      hmiReqActive.logStart=0U;
      break;

      case 21u:
      hmiReqActive.resetStream=0U;
      break;

      case 22u:
      hmiReqActive.stopStream=0U;
      break;

      case 23u:
      hmiReqActive.bindBlue=0U;
      break;

      case 24u:
      hmiReqActive.unBindBlue=0U;
      break;

      case 25u:
      hmiReqActive.lapseVidTime=0U;
      break;

      case 26u:
      hmiReqActive.lapseVidOpt=0U;
      break;

      case 27u:
      hmiReqActive.slowMotionRt=0U;
      break;

      case 28u:
      hmiReqActive.burstCap=0U;
      break;

      case 29u:
      hmiReqActive.preciseContTim=0U;
      break;

      case 30u:
      hmiReqActive.recPhoTime=0U;
      break;

      case 31u:
      hmiReqActive.loopRecDur=0U;
      break;

      case 32u:
      hmiReqActive.videoQ=0U;
      break;

      case 33u:
      hmiReqActive.photoQ=0U;
      break;

      case 34u:
      hmiReqActive.vidStampVal=0U;
      break;

      case 35u:
      hmiReqActive.phoStampVal=0U;
      break;

      case 36u:
      hmiReqActive.onOffOpt=0U;
      break;

      case 37u:
      hmiReqActive.vidOutDev=0U;
      break;

      case 38u:
      hmiReqActive.meterOpt=0U;
      break;

      case 39u:
      hmiReqActive.ledMode=0U;
      break;

      case 40u:
      hmiReqActive.buzzerOpt=0U;
      break;

      case 41u:
      hmiReqActive.captureMode=0U;
      break;

      case 42u:
      hmiReqActive.fastZoom=0U;
      break;

      case 43u:
      hmiReqActive.bitRate=0U;
      break;

      case 44u:
      hmiReqActive.delFile=0U;
      break;

      case 45u:
      hmiReqActive.format=0U;
      break;

      case 46u:
      hmiReqActive.dir=0U;
      break;

      case 47u:
      hmiReqActive.sndGetFile=0U;
      break;

      case 48u:
      hmiReqActive.getMedia=0U;
      break;

      case 49u:
      hmiReqActive.thumbNail=0U;
      break;

      case 50u:
      hmiReqActive.enabScript=0U;
      break;

      case 51u:
      hmiReqActive.getLastPho=0U;
      break;

      case 52u:
      hmiReqActive.getFreeSD=0U;
      break;
      
      case 53u:
      hmiReqActive.getTotalSD=0U;
      break;
      
      default:
      break;
   }
   
}
#endif                                                                          // ======================== eNd TCP/IP YiCam included ====================
#if defined(SEQ_CAM_USED)
/*******************************************************************************
* Function Name: SEQ_OpenTCPSocketHttpJson
********************************************************************************
* Summary:
*  Open the socket for the http put and get requests for the parrot sequioa cam
*  the replies and configuration requests are JSON objects
*
* Parameters:
*   SOCKET_Intern_Dsc **sock1
*
* Return:
* return if success is 1
* ======================================
* 0 - no successful transmit SYN segment.
* 1 - successful transmit SYN segment.
* 2 - no available socket.
* 3 - no ARP resolve
*
*******************************************************************************/
uint8_t SEQ_OpenTCPSocketHttpJson( SOCKET_Intern_Dsc **sock1 )
{
  return(Net_Ethernet_Intern_connectTCP(SEQ_USE_IP, SEQ_DEF_PORT,SEQ_DEF_PORT, sock1));     // open the socket
}

/*******************************************************************************
* Function Name: SEQ_CloseTCPSocketHttpJson
********************************************************************************
* Summary:
*  Close the socket for the http put and get requests for the parrot sequioa cam
*  the replies and configuration requests are JSON objects from the http API
*
* Parameters:
*   SOCKET_Intern_Dsc **sock1
*
* Return:
* return if success is 1
* ======================================
* 0 - no successful transmit SYN segment.
* 1 - successful transmit SYN segment.
* 2 - no available socket.
* 3 - no ARP resolve
*
*******************************************************************************/
uint8_t SEQ_CloseTCPSocketHttpJson( SOCKET_Intern_Dsc *sock2 )
{
  return(Net_Ethernet_Intern_disconnectTCP(sock2));                             // close the socket
}
#endif                                                                          // ================== eNd Parrot Sequoia camera ============================
/*******************************************************************************
* Function Name: Net_Ethernet_Intern_UserTCP
********************************************************************************
* Summary:
*  Net_Ethernet_Intern_UserTCP function performs following functions:
*   1. Checks the state of the socket passed to the function
*   2. Reads data sent from the TCP Serve and places into a read buffer
*
* Parameters:
*  SOCKET_Intern_Dsc *used_tcp_socket - The TCP socket.
*
* Return:
*  None.
*
*******************************************************************************/
void Net_Ethernet_Intern_UserTCP(SOCKET_Intern_Dsc *used_tcp_socket)
 {
#if ((((defined(YI_CAM_USED) || defined(PENTAX_CAM_USED)) || defined(SEQ_CAM_USED)) || defined(CBOR_COMS_USED)) || defined(JSON_COMS_USED))
#if defined(SEQ_CAM_USED)
     unsigned char rcvIpAddr[17u];                                              // String to hold the incoming ip address
     uint8_t SEQ_retTCP;                                                        // Socket open command return code
#endif
#ifdef YI_CAM_USED
     unsigned char rcvIpAddr[17u];                                              // String to hold the incoming ip address
     uint8_t XY_retTCP;                                                         // Socket open command return code
     uint8_t dat_pos=0U;                                                        // writing postion in the array
     uint8_t counter;
     uint64_t val;
     uint64_t hmiReqActive1;
     
     if ((YiCam_DATA.State != ETH_ACK_OLD_DATA_REQ) && (g_YiCamReqState == 5u)) // We have not been asked to re-transmit old data and have new data to send
     {
         memcpy(xyTcpSendBuff,g_XYtcpBuffer,sizeof(g_XYtcpBuffer));             // Copy the new message to the send buffer
     }
     
     if (used_tcp_socket->open==0)                                              // Socket is not busy
     {
        switch(used_tcp_socket->state)                                          // for the states of the sockets
        {
           case TCP_STATE_CLOSED:                                               // We dont have a connection
           XY_retTCP=XY_OpenTCPSocketRawJson( &used_tcp_socket );               // try to open a socket
           switch(XY_retTCP)
           {
              case TCP_NO_SYN_SEND:                                             // didnt transmit SYN
              XY_retTCP=XY_OpenTCPSocketRawJson( &used_tcp_socket );            // try again to open a socket
              break;

              case TCP_SYN_SUCCESS:                                             // continue the SYN was sent
              break;

              case TCP_NO_SOCK:                                                 // no socket
              YiCam.Link_Alive   = FALSE;
              break;

              case TCP_NO_ARP:                                                  // no arp
              YiCam.Link_Alive   = FALSE;
              break;

              default:                                                          // invalid return code
              break;
           }
           break;

           case TCP_STATE_LISTEN:                                               // Server is listening for a SYN or ACK from the client
           break;

           case TCP_STATE_ESTABLISHED:                                          // Socket ready to send or receive
           //
           // ===================== Now Send Any Data =============================
           //
           if ((YiCam_DATA.State == ETH_ACK_OLD_DATA_REQ) && ((g_YiCamReqState == 5u) || (g_YiCamReqState == 6u)))
           {
//=========================================================================================================================================================
//  while(dat_pos <= strlen(xyTcpSendBuff))                                 // If dat_pos < string, try to put bytes in TCP Tx buffer.
//  {
//     if(Net_Ethernet_Intern_putByteTCP(xyTcpSendBuff[dat_pos++], used_tcp_socket) == 0)   // Overflow occur, decrement dat_pos, because last byte not written
//     {
//       dat_pos--;
//       break;
//     }
//  }
//==========================================================================================================================================================
               if((Net_Ethernet_Intern_putBytesTCP(xyTcpSendBuff, strlen(xyTcpSendBuff), used_tcp_socket))==strlen(xyTcpSendBuff))  // may be faster to put all bytes
               {
                  switch(g_YiCamReqState)
                  {
                     case 5u:                                                   // We sent a request for a start token
                     g_YiCamReqState == 1u;                                     // wait for a response message to the stream start token request
                     // hmiReqActive.getToken=0U;                                  not sure this is needed
                     break;
                 
                     case 6u:                                                   // we sent a message to do a camera action
                     g_YiCamReqState == 3u;                                     // wait for a response message to request made
                     resetXYActions();                                          // Reset the stimulus as the message was sent
                     break;

                     case 7u:                                                   // we sent a message to do a camera action
                     g_YiCamReqState == 11u;                                    // wait for a response message to request made
                     break;
                  
                     default:                                                   // request was re-send old data
                     YiCam_DATA.State == ETH_OLD_DATA_SENT;                     // mark the port state as we have sent the old data
                     break;
                  }
               }
//
//              ================== Disconnect ? =====================================
//
//             if( Net_Ethernet_Intern_bufferEmptyTCP(used_tcp_socket)&&(dat_pos >= strlen(xyTcpSendBuff)) ) // If all bytes written, and TCP Tx buffer empty, We can close connection.
//             {
//                  Net_Ethernet_Intern_disconnectTCP(used_tcp_socket);                // Not sure we should disconnect the socket every send ?
//             }
           }
           sprintf(rcvIpAddr,"%d.%d.%d.%d",used_tcp_socket->remoteIP[0],used_tcp_socket->remoteIP[1],used_tcp_socket->remoteIP[2],used_tcp_socket->remoteIP[3]);
           if(!strcmp(rcvIpAddr,XY_USE_IP))                                     // Check if we have received from the Xiong Yi Cam
           {
               YiCam.Link_Alive   = TRUE;
               YiCam.Link_Alive_Time=Heart.time;
               if(YiCam.Link_Alive_Was != YiCam.Link_Alive)
               {
                   YiCam.Link_Alive_Was = YiCam.Link_Alive;                     // set link alive status
                //Status(ETHER_LINK_STATE,AIR_LINK_UP);
               }

               switch(used_tcp_socket->destPort)                                // Test the destination port for the received TCP packet
               {
                // ======================  JSON message ============================== From Yi Cam
                  case XY_DEF_PORT:                                             // The JSON port of the Yi Action Camera
                  YiCam_DATA.UDP_Index =0U;                                     // Reset buffer index
                  YiCam_DATA.UDP_Buffer_Len = used_tcp_socket->dataLength;
                  while(used_tcp_socket->dataLength--)                          // Keep filling YiCam_DATA.UDP_Buffer until TCP receive buffer is empty (@@strut name should change)
                  {
                      YiCam_DATA.UDP_Buffer[YiCam_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
                      YiCam_DATA.UDP_Index=++YiCam_DATA.UDP_Index % UINT8_MAX;
                      if ( XY_MSG_MAX_LEN <= YiCam_DATA.UDP_Index )             // counted past the end of the TCP receive buffer
                      {
                      // ----- Want to flush this ???
                      break;                                                    // break from the loop
                      }
                  }
                  YiCam_DATA.UDP_Index--;                                       // Number of bytes received is minus one
                  YiCam_DATA.State = ETH_WAIT_ACK_CRC;                          // Set the port to received a packet and tell to send a dummy ack
                  break;                                                        // Message was sent to us from the Yi Action Cam Server on JSON port

                  case TELNET_PORT:                                             // The the Yi Action Camera connected via telnet session
                  YiCam_DATA.UDP_Index =0U;                                     // Reset buffer index
                  YiCam_DATA.UDP_Buffer_Len = used_tcp_socket->dataLength;
                  while(used_tcp_socket->dataLength--)                          // Keep filling YiCam_DATA.UDP_Buffer until TCP receive buffer is empty (@@strut name should change)
                  {
                     YiCam_DATA.UDP_Buffer[YiCam_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
                     YiCam_DATA.UDP_Index=++YiCam_DATA.UDP_Index % UINT8_MAX;
                     if ( XY_MSG_MAX_LEN <= YiCam_DATA.UDP_Index )              // counted past the end of the TCP receive buffer
                     {
                     // ------ Want to flush this ?
                     break;                                                     // return with no reply message was longer than max expected for Yi Action Cam
                     }
                  }
                  YiCam_DATA.UDP_Index--;                                       // Number of bytes received is minus one
                  YiCam_DATA.State = ETH_WAIT_ACK_CRC;                          // Set the port to received a packet and tell to send a dummy ack
                  break;                                                        // Message was sent to us from the Yi Action Cam Server on JSON port

                  default:                                                      // An unknown TCP message
                  break;
               }
           }
           break;

           case TCP_STATE_SYN_SENT:                                             // You need to look for a SYN
           break;

           case TCP_STATE_FIN_WAIT_1:                                           // You sent a FIN
           break;

           case TCP_STATE_FIN_WAIT_2:                                           // You wait for a remote FIN
           break;

           case TCP_STATE_RETRANSMIT:                                           // Asked to re-transmit
           break;
        }                                                                       // END CASE SOCKET STATE
     }
     else                                                                       // Socket is busy
     {
         // wait for the socket to become free or exit with an error
     }
#endif                                                                          // ===================== eNd use Yi Cam ===================================

#if defined(SEQ_CAM_USED)
     if (used_tcp_socket->open==0)                                              // Socket is not busy
     {
        switch(used_tcp_socket->state)                                          // for the states of the sockets
        {
           case TCP_STATE_CLOSED:                                               // We dont have a connection
           SEQ_retTCP=SEQ_OpenTCPSockethttpJson( &used_tcp_socket );            // try to open a socket
           switch(SEQ_retTCP)
           {
              case TCP_NO_SYN_SEND:                                             // didnt transmit SYN
              SEQ_retTCP=SEQ_OpenTCPSockethttpJson( &used_tcp_socket );         // try again to open a socket
              break;

              case TCP_SYN_SUCCESS:                                             // continue the SYN was sent
              break;

              case TCP_NO_SOCK:                                                 // no socket
              ParrotSeq.Link_Alive   = FALSE;
              break;

              case TCP_NO_ARP:                                                  // no arp
              ParrotSeq.Link_Alive   = FALSE;
              break;

              default:                                                          // invalid return code
              break;
           }
           break;

           case TCP_STATE_LISTEN:                                               // Server is listening for a SYN or ACK from the client
           break;

           case TCP_STATE_ESTABLISHED:
           // if((Net_Ethernet_Intern_putBytesTCP(xyTcpSendBuff, strlen(xyTcpSendBuff), used_tcp_socket))==strlen(xyTcpSendBuff))
           sprintf(rcvIpAddr,"%d.%d.%d.%d",used_tcp_socket->remoteIP[0u],used_tcp_socket->remoteIP[1u],used_tcp_socket->remoteIP[2u],used_tcp_socket->remoteIP[3u]);
           if(!strcmp(rcvIpAddr,SEQ_USE_IP))                                    // Check if we have received from the Parrot Seq Cam
           {
               ParrotSeq.Link_Alive = true;
               ParrotSeq.Link_Alive_Time=Heart.time;
               if(ParrotSeq.Link_Alive_Was != ParrotSeq.Link_Alive)
               {
                  ParrotSeq.Link_Alive_Was = ParrotSeq.Link_Alive;              // set link alive status
               }

               switch(used_tcp_socket->destPort)                                // Test the destination port for the received TCP packet
               {
                  // ======================  JSON message ============================== From http get request
                  case SEQ_DEF_PORT:                                            // The http port for the Parrot Sequoia Camera
                  SeqCam_DATA.UDP_Index =0U;                                    // Reset buffer index
                  SeqCam_DATA.UDP_Buffer_Len = used_tcp_socket->dataLength;
                  while(used_tcp_socket->dataLength--)                          // Keep filling SeqCam_DATA.UDP_Buffer until TCP receive buffer is empty (@@strut name should change)
                  {
                      SeqCam_DATA.UDP_Buffer[SeqCam_DATA.UDP_Index] = Net_Ethernet_Intern_getByte();   // get each byte and put it in the buffer
                      SeqCam_DATA.UDP_Index=++SeqCam_DATA.UDP_Index % UINT8_MAX;
                      if ( SEQ_MSG_MAX_LEN <= SeqCam_DATA.UDP_Index )           // counted past the end of the TCP receive buffer
                      {
                      // ----- Want to flush this ???
                      break;                                                    // break from the loop
                      }
                  }
                  SeqCam_DATA.UDP_Index--;                                      // Number of bytes received is minus one
                  SeqCam_DATA.State = ETH_WAIT_ACK_CRC;                         // Set the port to received a packet and tell to send a dummy ack
                  break;
                }
           }
           break;
       
           case TCP_STATE_SYN_SENT:                                             // You need to look for a SYN
           break;

           case TCP_STATE_FIN_WAIT_1:                                           // You sent a FIN
           break;

           case TCP_STATE_FIN_WAIT_2:                                           // You wait for a remote FIN
           break;

           case TCP_STATE_RETRANSMIT:                                           // Asked to re-transmit
           break;
       }                                                                        // END CASE SOCKET STATE
     }
     else                                                                       // Socket is busy
     {
         // wait for the socket to become free or exit with an error
     }
#endif                                                                          // ===================== eNd use Sequoia Cam ==============================
#if defined(PENTAX_CAM_USED)
#endif                                                                          // ===================== eNd use Pentax Cam ==============================
#if defined(CBOR_COMS_USED)
#endif                                                                          // ===================== eNd use CBOR iOt communication  =================
#if defined(JSON_COMS_USED)
#endif                                                                          // ===================== eNd use JSON webservice  ========================

#ifdef CAN_UDP_USED
  uint16_t Can2_TCP_RX_Packet[8];
  unsigned int reqLength =0;
  int counter=0;

  Uart5_write_text("ETH_TCP_EVENT\n\r");
  // test to see if you can send Ethernet messages
  /*TCP_RXBUF_pnt = 0;                // point to start of buffer
  Packet_Len = 0;                   // init to zero testtest
  // If its a TCP message on the right port, Read in the buffer.
  if ( used_socket->destPort == 502 ) {         // MODBUS TCP is on this Remote machines port 502
    Packet_Len = used_socket->dataLength;         // Get packet length testtest
    while(Packet_Len--) {
      TCP_RXBuffer[TCP_RXBUF_pnt++] = Net_Ethernet_Intern_getByte();
    }
  }*/
  // do we have data to send?
      if (SendTCPMsg)
      {
         Uart5_write_text("Send TCP\n\r");
         Uart5_write(U2BuffInd);
         Uart5_write_text("Loading TCP buffer\n\r");
         U2BuffInd++;
         Net_Ethernet_Intern_putBytesTCP(UART2.Buffer,UART2.Index, used_socket);

         SendTCPMsg =0;
         U2BuffInd =0;
         CloseTcpSocket =0;
         Uart5_write_text("Sent TCP packet\n\r");
       }


  if (used_socket->destPort==UART2.Dest_Port)
  {
    UART2_Write(Net_Ethernet_Intern_getByte());
  }
 // Process TCP packets for UART5
  // If UDP packet comes from destination port (UART5RXPort) then sent packet out UART5
  if (used_socket->destPort==UART5destPort)
  {
          UART5_Write(Net_Ethernet_Intern_getByte());
  }

 if (used_socket->destPort==Can2destPort)                                       // Process TCP packets for CAN
 {
    Lo(Can2_TX_MSG_ID)=Net_Ethernet_Intern_getByte();                           // Load first two bytes into message ID
    Hi(Can2_TX_MSG_ID)=Net_Ethernet_Intern_getByte();
    // load the remaining of the message into Can2_UDP_RX_Packet EXLUDING the MASK which is the last byte in the UDP Packet.
    reqLength = used_socket->dataLength;
    while(counter < reqLength)
    {
       Can2_TCP_RX_Packet[counter] = Net_Ethernet_Intern_getByte();
       counter++;
    }
    CAN2Write(Can2_TX_MSG_ID, Can2_TCP_RX_Packet, reqLength, Can2_Send_Flags);  // Sent message out CAN2
   }
 #endif                                                                         // ======================== eNd use Can over UDP  =======================
#endif                                                                          // ======================== eNd TCP/IP Stack used =======================
 }

/*-----------------------------------------------------------------------------
 *      Init_MCU():  Initialize the MCU (CPU) and RTC for the application
 *
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_MCU(){
  
  // RTCC registers unlock procedure
  SYSKEY = 0xAA996655UL;                                                        // Write first unlock key to SYSKEY
  SYSKEY = 0x556699AAUL;                                                        // Write second unlock key to SYSKEY
  RTCWREN_bit = 1U;                                                             // RTC Value registers can be written and read.
  ON__RTCCON_bit= 0U;                                                           // Turn off the RTCC module
  Delay_100ms;
  while(RTCCLKON_bit)                                                           // Wait for clock to be turned on
  //  ;
  RTCTIME = 0x0U;                                                               // Reset uptime
  RTCDATE = 0x0U;                                                               // Reset update
  
  //while(ALRMSYNC_bit)       // Wait for clock to be turned off
  //  ;
    
  RTSECSEL_bit = 0U;                                                            // Use RTC interrupt
  ON__RTCCON_bit= 1U;                                                           // Turn on the RTCC module
  //while(!RTCCLKON_bit)      // Wait for clock to be turned on
}

#ifdef TIMER1_NEEDED
/*-----------------------------------------------------------------------------
 *      Init_Timer1():  Initialize interrupt timer no. 1
 *
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_Timer1() {

  T1CON     = 0x8030U;                                                          // Timer 1 prescale
  T1IP0_bit = 1U;                                                               // set interrupt
  T1IP1_bit = 1U;                                                               // priority
  T1IP2_bit = 1U;                                                               // to 7

  TCKPS0_bit = 1U;                                                              // Set Timer Input Clock
  TCKPS1_bit = 1U;                                                              // Prescale value to 1:256
  T1IF_bit   = 0U;

  T1IE_bit = 1U;                                                                // Enable Timer1 Interrupt
  PR1 = 62500UL;                                                                // Load period register with 200 ms (timer duration)
  TMR1 = 0U;                                                                    // set to start from 0
  ON__T1CON_bit = 1U;                                                           // Enable Timer1

}
#endif

#ifdef TIMER2_NEEDED
/*-----------------------------------------------------------------------------
 *      Init_Timer2():  Initialize interrupt timer no. 2
 *      combine Timer2 and 3 for a 400 ms timeout for UART2
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_Timer2(){
  T2CON         = 0x70U;
  PR2           = 62500UL;
  IPC2SET       = 0b11001;                                                      // Set Priority 6 and sub 1 so it is below UART2
  IPC2CLR       = 0b00110;                                                      // Set Priority 6 and sub 1 so it is below UART2
  T2IF_bit      = 0U;                                                           // Clear If
  ON__T2CON_bit = 0U;                                                           // Disable Timer2
  T2IE_bit      = 1U;                                                           // Enable IF
  TMR2          = 0U;

}
/*-----------------------------------------------------------------------------
 *      Init_Timer2_3():  Initialize interrupt timer no. 3
 *      combine Timer2 and 3 for a 400 ms timeout for UART2
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_Timer2_3(){
  T2CON                 = 0x08U;
  T3CON                 = 0x0U;
  TMR2                  = 0u;
  TMR3                  = 0u;
  T3IS0_bit             = 0u;
  T3IS1_bit             = 1u;
  T3IP0_bit             = 0u;
  T3IP1_bit             = 1u;
  T3IP2_bit             = 1u;
  T3IF_bit              = 0u;
  T3IE_bit              = 1u;
  PR2                   = 18432UL;
  PR3                   = 488UL;
}
#endif

/*-----------------------------------------------------------------------------
 *      Init_Timer5():  Initialize interrupt timer no. 5
 *      This timer is used as an interrupt driven clock tick
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_Timer5()
{                                                                               // Initialise timer 5 to 200 ms - change PR5= 62500 if you want 200 ms
  T5CON         = 0x8070UL;                                                     // timer set point from timer tool
  T5IP0_bit     = 1u;
  T5IP1_bit     = 1u;
  T5IP2_bit     = 1u;                                                           // set priority to 0b111 (7)
  T5IF_bit      = 0u;                                                           // remove pending
  T5IE_bit       = 1u;                                                          // enable interrupt
  PR5           = 31250UL;                                                      // 100 ms timer interrupt for a global counter
  TMR5           = 0u;
}

/*-----------------------------------------------------------------------------
 *      Init_PHYPins():  Initialise the pins from MCU to PHY chip
 *
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_PHYPins()
{
  TRISD11_bit = 0u;                                                             // ETH_ALT_MDC_BIT
  TRISD8_bit  = 1u;                                                             // ETH_ALT_MDIO_BIT

  TRISD6_bit  = 0u;                                                             // ETH_ALT_TXEN_BIT
  TRISF1_bit  = 0u;                                                             // ETH_ALT_TXD0_BIT
  TRISF0_bit  = 0u;                                                             // ETH_ALT_TXD1_BIT

  TRISG9_bit  = 1u;                                                             // ETH_ALT_RXCLK_BIT
  TRISG8_bit  = 1u;                                                             // ETH_ALT_RXDV_BIT
  TRISB12_bit = 1u;                                                             // ETH_ALT_RXD0_BIT
  TRISB13_bit = 1u;                                                             // ETH_ALT_RXD1_BIT
  TRISB11_bit = 1u;                                                             // ETH_ALT_RXERR_BIT
  //TrisB0_bit  = 0;                                                              // LED 1
  //TrisB1_Bit  = 0;                                                              // LED 2
  //TrisB2_Bit  = 0;                                                              // LED 3
}

/*-----------------------------------------------------------------------------
 *      Init_UART:  Initialise the required serial UART ports
 *  Parameters: uint16_t parityValue for SimpleBGC port
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_UART( uint16_t parityValue )
{
  unsigned int pb_divisor;                                                      // depends on PBDIV0 and 1.
  uint32_t periph_clock;                                                        // required for advanced function
  
#ifdef UART2_INTERUPT                                                           // SimpleBGC is connected on serial direct
  if ((PBDIV1_bit == 1u) && (PBDIV0_bit == 1u)) pb_divisor = 8u;
  else if ((PBDIV1_bit == 1u) && (PBDIV0_bit == 0u)) pb_divisor = 4u;
  else if ((PBDIV1_bit == 0u) && (PBDIV0_bit == 1u)) pb_divisor = 2u;
  else pb_divisor = 1u;
  periph_clock = Get_Fosc_kHz()/pb_divisor;                                     // calculated clock speed   you may have to set to 20000 hardcoded (try)
  
  UART2.Baud = 115200UL;                                                        // Set the Baud for SimpleBGC
  
  if (parityValue == _UART_8BIT_NOPARITY)
  {
     UART2_Init(UART2.Baud);                                                    // Set the Baud Rate for UART2
     UART2_Init_Advanced(UART2.Baud, periph_clock, _UART_HIGH_SPEED, _UART_8BIT_NOPARITY, _UART_ONE_STOPBIT);
  }
  else if (parityValue == _UART_8BIT_EVENPARITY)
  {
     UART2_Init_Advanced(UART2.Baud, periph_clock, _UART_HIGH_SPEED, _UART_8BIT_EVENPARITY, _UART_ONE_STOPBIT);
  }                                                                             // 9 bit or odd not supported by gimbal so ignore.
  
  U2IP0_bit = 0u;                                                               // Set UART2 interrupt
  U2IP1_bit = 1u;                                                               // Set interrupt priorities
  U2IP2_bit = 1u;                                                               // Set UART2 interrupt to level 6
  U2EIF_bit = 0u;                                                               // Clear IF
  U2RXIF_bit = 0u;                                                              // Clear RXIF
  U2EIE_bit = 0u;                                                               // Enable UART2 error interupts  (could also use   IEC1BITS.U2EIE =1)
  U2RXIE_bit =0u;                                                               // Enable UART2 RX IF
  UTXISEL0_U2STA_bit = 0u;                                                      // Set UART IF to trigger when buffer is 3/4 full
  UTXISEL1_U2STA_bit = 1u;                                                      // "                                           "
  URXEN_U2STA_bit = 0u;                                                         // Disable UART 2 receiver to prevent craching uppon boot
  UART2.Index=1u;                                                               // Set read index to 1
#endif
#if defined(GPS_INCLUDED) || defined(GPS_INCLUDED2)                             // A GPS option has been selected on port 3

  UART3_Init(GPS_BAUD_RATE);                                                    // Set the Baud Rate for UART3
  UART_Set_Active(&UART3_Read, &UART3_Write, &UART3_Data_Ready, &UART3_Tx_Idle);// Initialise the handlers for UART3

  U3IP0_bit = 1u;                                                                // Set UART3 interrupt
  U3IP1_bit = 1u;                                                                // Set interrupt priorities
  U3IP2_bit = 0u;                                                                // Set UART3 interrupt to level 3
  U3EIF_bit = 0u;                                                                // Clear IF
  U3RXIF_bit = 0u;                                                               // Clear RXIF
  U3EIE_bit = 0u;                                                                // Enable UART3 error interupts  (could also use   IEC1BITS.U2EIE =1)
  U3RXIE_bit =0u;                                                                // Enable UART3 RX IF not 3/4 full as below
  UTXISEL0_U3STA_bit = 0u;                                                       // Set UART IF to trigger when buffer is 3/4 full
  UTXISEL1_U3STA_bit = 1u;                                                       // "                                           "
  URXEN_U3STA_bit = 0u;                                                          // Disable UART 3 receiver to prevent craching uppon boot
#endif
#ifdef UART4_INTERUPT                                                           // GPS requested

  UART4_Init(GPS_BAUD_RATE);                                                    // Set the Baud Rate for UART4
  UART_Set_Active(&UART4_Read, &UART4_Write, &UART4_Data_Ready, &UART4_Tx_Idle);// Initialise the handlers for UART4

  U4IP0_bit = 0u;                                                               // Set UART4 interrupt
  U4IP1_bit = 0u;                                                               // Set interrupt priorities
  U4IP2_bit = 1u;                                                               // Set UART4 interrupt to level 4
  U4EIF_bit = 0u;                                                               // Clear IF
  U4RXIF_bit = 0u;                                                              // Clear RXIF
  U4EIE_bit = 0u;                                                               // Enable UART4 error interupts  (could also use   IEC1BITS.U2EIE =1)
  U4RXIE_bit =0u;                                                               // Enable UART4 RX IF not 3/4 full as below
  UTXISEL0_U4STA_bit = 0u;                                                      // Set UART IF to trigger when buffer is 3/4 full
  UTXISEL1_U4STA_bit = 1u;                                                      // "                                           "
  URXEN_U4STA_bit = 0u;                                                         // Disable UART 4 receiver to prevent craching uppon boot
#endif
#ifdef UART6_INTERUPT                                                           // Liddar is requested

  UART6_Init(LIDDAR_BAUD_RATE);                                                 // Set the Baud Rate for UART6 to that of the Liddar
  UART_Set_Active(&UART6_Read, &UART6_Write, &UART6_Data_Ready, &UART6_Tx_Idle);// Initialise the handlers for UART6

  U6IP0_bit = 0u;                                                               // Set UART6 interrupt
  U6IP1_bit = 0u;                                                               // Set interrupt priorities
  U6IP2_bit = 1u;                                                               // Set UART6 interrupt to level 4
  U6EIF_bit = 0u;                                                               // Clear IF
  U6RXIF_bit = 0u;                                                              // Clear RXIF
  U6EIE_bit = 0u;                                                               // Enable UART6 error interupts  (could also use   IEC1BITS.U2EIE =1)
  U6RXIE_bit =0u;                                                               // Enable UART6 RX IF not 3/4 full as below
  UTXISEL0_U6STA_bit = 0u;                                                      // Set UART IF to trigger when buffer is 3/4 full
  UTXISEL1_U6STA_bit = 1u;                                                      // "                                           "
  URXEN_U6STA_bit = 0u;                                                         // Disable UART 6 receiver to prevent craching uppon boot
#endif
#ifdef ROBOT_HELPER                                                             // Odrive is requested

  UART1_Init(OD_BAUD_RATE);                                                     // Set the Baud Rate for UART1 to that of the Run Cam
  UART_Set_Active(&UART1_Read, &UART1_Write, &UART1_Data_Ready, &UART1_Tx_Idle);// Initialise the handlers for UART1

  U1IP0_bit = 0u;                                                               // Set UART1 interrupt
  U1IP1_bit = 0u;                                                               // Set interrupt priorities
  U1IP2_bit = 1u;                                                               // Set UART1 interrupt to level 4
  U1EIF_bit = 0u;                                                               // Clear IF
  U1RXIF_bit = 0u;                                                              // Clear RXIF
  U1EIE_bit = 0u;                                                               // Enable UART1 error interupts  (could also use   IEC1BITS.U2EIE =1)
  U1RXIE_bit = 0u;                                                              // Enable UART1 RX IF not 3/4 full as below
  //UTXISEL0_U1STA_bit = 0;                                                       // Set UART IF to trigger when buffer is 3/4 full
  //UTXISEL1_U1STA_bit = 1;                                                       // "                                           "
  //URXEN_U1STA_bit = 0;                                                          // Disable UART 1 receiver to prevent craching uppon boot
#endif
#ifdef RUN_CAM_USED                                                             // Run Cam is requested

  UART5_Init(RC_BAUD_RATE);                                                     // Set the Baud Rate for UART1 to that of the Run Cam
  UART_Set_Active(&UART5_Read, &UART5_Write, &UART5_Data_Ready, &UART5_Tx_Idle);// Initialise the handlers for UART1

  U5IP0_bit = 0u;                                                               // Set UART1 interrupt
  U5IP1_bit = 0u;                                                               // Set interrupt priorities
  U5IP2_bit = 1u;                                                               // Set UART interrupt to level 4
  U5EIF_bit = 0u;                                                               // Clear IF
  U5RXIF_bit = 0u;                                                              // Clear RXIF
  U5EIE_bit = 0u;                                                               // Enable UART error interupts  (could also use   IEC5BITS.U5EIE =1)
  U5RXIE_bit = 0u;                                                              // Enable UART RX IF not 3/4 full as below
  //UTXISEL0_U1STA_bit = 0;                                                       // Set UART IF to trigger when buffer is 3/4 full
  //UTXISEL1_U1STA_bit = 1;                                                       // "                                           "
  //URXEN_U1STA_bit = 0;                                                          // Disable UART 1 receiver to prevent craching uppon boot
#endif
}

#ifdef USE_CAN
/*-----------------------------------------------------------------------------
 *      Init_Can():  Initialize CAN open interface
 *
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Init_Can()
{
  Can2_Init_Flags = 0u;                                                         //
  //Can2_Send_Flags = 0;                                                        // clear flags
  Can2_Rcv_Flags  = 0u;                                                         //

  CAN2Initialize(SJW, BRP, PHSEG1, PHSEG2, PROPSEG, CAN_CONFIG_FLAGS);

  CAN2SetOperationMode(_CAN_MODE_CONFIG,0xFFu);                                 // set CONFIGURATION mode

  CAN2AssignBuffer(FIFObuffers);                                                //assign the buffers
  //configure rx fifo
  CAN2ConfigureFIFO(0u, 8u,_CAN_FIFO_RX & _CAN_FULL_MESSAGE);                   //RX buffer 8 messages deep
  //configure tx fifo
  CAN2ConfigureFIFO(1u, 8u,_CAN_FIFO_TX & _CAN_TX_PRIORITY_3 & _CAN_TX_NO_RTR_FRAME);// TX buffer 8 messages deep

  //set filter 1
  CAN2SetFilter(_CAN_FILTER_31, Can2_MSG_ID, _CAN_MASK_0, _CAN_BUFFER_0, _CAN_CONFIG_XTD_MSG);// set id of filter1 to 2nd node ID
  // set NORMAL mode
  CAN2SetOperationMode(_CAN_MODE_NORMAL,0xFFu);
}
#endif

/*-----------------------------------------------------------------------------
 *      Status:  Print debug Status messages
 *
 *  Parameters: int16_t address, int16_t state
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Status(int16_t address, int16_t state)
 {

#ifdef PRINT_DEBUG_MESSAGES                                                     // Print messages to UART5 when enabled

   char txt[7u];
   char str1[100u]={"No known state"};

   int secondsLow;
   int secondsHigh;
   int secondsActual;
   int minutesLow;
   int minutesHigh;
   int minutesActual;
   int hoursLow;
   int hoursHigh;
   int hoursActual;

   // Grab the RTC to show the timestamp
   secondsLow = (RTCTIME & 0xF00ul)>>8u;
   //secondsLow = RTCTIME;
   secondsHigh = (RTCTIME & 0x7000ul)>>12u;
   secondsActual = (secondsHigh * 10u) + secondsLow;
   minutesLow = (RTCTIME & 0xF0000ul)>>16u;
   minutesHigh = (RTCTIME & 0x700000ul)>>20u;
   minutesActual = (minutesHigh * 10u) + minutesLow;
   hoursLow = (RTCTIME & 0xF000000ul)>>24u;
   hoursHigh = (RTCTIME & 0x30000000ul)>>28u;
   hoursActual = (hoursHigh * 10u) + hoursLow;

   IntToStr(Status_Count++,txt);
   Uart5_write_text(txt);
   Uart5_write_text("-");

   switch(address)
  {
     case SOCKET_STATE :
    {
      switch(state)
      {
      //      state  0 - connection closed
      //             1 - remote SYN segment received, our SYN segment sent, and We wait for ACK (Remote mode)
      //             3 - connection established
      //             4 - our SYN segment sent, and We wait for SYN response (My mode)
      //             5 - FIN segment sent, wait for ACK.
      //             6 - Received ACK on our FIN, wait for remote FIN.
      //             7 - Expired ACK wait time. We retransmit last sent packet, and again set Wait-Timer. If this happen again
      //                 connection close.
       case 0u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - connection closed %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        // Uart5_write_text("ETH_SOCKET_STATE: - connection closed %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 1u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - remote SYN segment received, our SYN segment sent, and We wait for ACK (Remote mode) %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        // Uart5_write_text("ETH_SOCKET_STATE: - remote SYN segment received, our SYN segment sent, and We wait for ACK (Remote mode) %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 3u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - connection established %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        // Uart5_write_text("ETH_SOCKET_STATE: - connection established %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 4u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - our SYN segment sent, and We wait for SYN response (My mode) %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_SOCKET_STATE: - our SYN segment sent, and We wait for SYN response (My mode) %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 5u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - FIN segment sent, wait for ACK %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_SOCKET_STATE: - FIN segment sent, wait for ACK %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 6u :
       {
        sprintf(str1,"ETH_SOCKET_STATE: - Received ACK on our FIN, wait for remote FIN %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
       // Uart5_write_text("ETH_SOCKET_STATE: - Received ACK on our FIN, wait for remote FIN %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
       case 7u :
       {
         sprintf(str1,"ETH_SOCKET_STATE: - Expired ACK wait time. We retransmit last sent packet, and again set Wait-Timer. If this happen again connection close%d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_SOCKET_STATE: - Expired ACK wait time. We retransmit last sent packet, and again set Wait-Timer. If this happen again connection close %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        break;
        }
        break; //switch(state)
       }
       break;//case SOCKET_STATE
     }
     
     case Eth_Progress :
     {
      //  Net_Ethernet_Intern_doPacket() Return
      //0 - upon successful packet processing (zero packets received or received packet processed successfully).
      //1 - upon reception Status or receive buffer corruption. internal Ethernet controller needs to be restarted.
      //2 - received packet was not sent to us (not our IP, nor IP broadcast address).
      //3 - received IP packet was not IPv4.
      //4 - received packet was of type unknown to the library.*

        switch(state)
       {
        case 0u :
        {
         sprintf(str1,"ETH_PROGRESS_STATE: - upon successful packet processing (zero packets received or received packet processed successfully)%d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_PROGRESS_STATE: - upon successful packet processing (zero packets received or received packet processed successfully)\n\r");
        break;
        }
        case 1u :
        {
        sprintf(str1,"ETH_PROGRESS_STATE: - upon reception Status or receive buffer corruption. internal Ethernet controller needs to be restarted)%d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_PROGRESS_STATE: - upon reception Status or receive buffer corruption. internal Ethernet controller needs to be restarted)\n\r");
        break;
        }
        case 2u :
        {
        sprintf(str1,"ETH_PROGRESS_STATE: - received packet was not sent to us (not our IP, nor IP broadcast address)%d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
        //Uart5_write_text("ETH_PROGRESS_STATE: - received packet was not sent to us (not our IP, nor IP broadcast address)\n\r");
        break;
        }
        case 3u :
        {
        sprintf(str1,"ETH_PROGRESS_STATE: - received IP packet was not IPv4 %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);

        //Uart5_write_text("ETH_PROGRESS_STATE: - received IP packet was not IPv4\n\r");
        break;
        }
        case 4u :
        {
         sprintf(str1,"ETH_PROGRESS_STATE: - received packet was of type unknown to the stack %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
       // Uart5_write_text("ETH_PROGRESS_STATE: - received packet was of type unknown to the stack\n\r");
        break;
        }

       }//switch(state) case Eth_Progress
       break;
      }//case Eth_Progress
      
      case(ETHER_LINK_STATE):
      {
         switch(State)
         {
           case TRUE :
            {
               sprintf(str1,"ETH_Present_STATE: - TRUE %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - TRUE\n\r");
             break;
            }
           case FALSE :
           {
             sprintf(str1,"ETH_Present_STATE: - FALSE %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - FALSE\n\r");
             break;
            }
            break;
            
            case AIR_LINK_UP  :
           {
             sprintf(str1,"ETH_Present_STATE: - AIR_LINK_UP %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - AIR_LINK_UP\n\r");
             break;
           }
            case AIR_LINK_DOWN  :
           {
             sprintf(str1,"ETH_Present_STATE: - AIR_LINK_DOWN %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - AIR_LINK_DOWN\n\r");
             break;
           }
            case MASTER_LINK_UP  :
           {
             sprintf(str1,"ETH_Present_STATE: - MASTER_LINK_UP %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
            // Uart5_write_text("ETH_Present_STATE: - MASTER_LINK_UP\n\r");
             break;
           }
            case MASTER_LINK_DOWN  :
           {
            sprintf(str1,"ETH_Present_STATE: - MASTER_LINK_DOWN %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
            // Uart5_write_text("ETH_Present_STATE: - MASTER_LINK_DOWN\n\r");
             break;
           }
            case FIRE_LINK_UP  :
           {
            sprintf(str1,"ETH_Present_STATE: - FIRE_LINK_UP %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - FIRE_LINK_UP\n\r");
             break;
           }
            case FIRE_LINK_DOWN  :
           {
             sprintf(str1,"ETH_Present_STATE: - FIRE_LINK_DOWN %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - FIRE_LINK_DOWN\n\r");
             break;
           }
            case LINK_DOWN  :
           {
             sprintf(str1,"ETH_Present_STATE: - LINK_DOWN_PACKET_NOT_SENT %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("ETH_Present_STATE: - FIRE_LINK_DOWN\n\r");
             break;
           }

            break;
         }// case ETHER_LINK_STATE

      
        break;
      }//case (ETHER_LINK_STATE):
      
      case(U2STA_Add):
      {
         switch(State)
         {
           case U2STA_B2TO :
            {
             sprintf(str1,"UART2_STATE: - Buffer underflow %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UART2_STATE: - Buffer underflow\n\r");
             break;//case U2STA_B2TO
            }
           case UART2_BUFFER_OVERFLOW :
            {
             sprintf(str1,"UDP_STATE: -UART2 Buffer Overrun %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
            // Uart5_write_text("UDP_STATE: -UART2 Buffer Overrun\n\r");
             UART2.State = CLEAR;
             break;//case UART2_SEND_UDP_PACKET_FAIL

            }
            case UART2_BAD_CRC :
            {
             sprintf(str1,"UDP_STATE: -UART2 BAD CRC %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
            // Uart5_write_text("UDP_STATE: -UART2 Buffer Overrun\n\r");
             UART2.State = CLEAR;
             break;//case UART2_SEND_UDP_PACKET_FAIL

            }
          break;//switch(State)
         }

      break;//case (U2STA_Add)
      }
      case(UDPSTA_Add):
      {
         switch(State)
         {
           case UDP_PACKET_SEND_FAIL :
            {
             sprintf(str1,"UDP_STATE: -Packet Send Failed :%d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             bad_ether++ % UINT8_MAX;

             //Uart5_write_text("UDP_STATE: -Paket Send Failed\n\r");
             break;//case UDP_PACKET_SEND_FAIL
            } 
           case UDP_PACKET_SEND_GOOD :
            {
             sprintf(str1,"UDP_STATE: -Packet Sent %d : %d : %d\n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket Sent\n\r");
             break;//case UDP_PACKET_SEND_GOOD
            }

           case UDP_UART2_PACKET_ACK_TIMEOUT  :
            {
             sprintf(str1,"UDP_STATE: -Packet UART2 ACK TIMEOUT %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket UART2 ACK TIMEOUT\n\r");
             break;//case UDP_PACKER_ACK_TIMEOUT
            }
           case UDP_UART2_SEND_PACKET_FAIL  :
            {
             sprintf(str1,"UDP_STATE: -Packet UART2 NO ACK RECEIVED %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket UART2 NO ACK RECEIVED\n\r");
             break;//case UART2_SEND_UDP_PACKET_FAIL
            }
           case UDP_PACKET_ACK_RECEIVED  :
            {
             sprintf(str1,"UDP_STATE: -Packet UART2 GOOD ACK RECEIVED %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket UART2 GOOD ACK RECEIVED\n\r");
             break;//case UART2_SEND_UDP_PACKET_FAIL
            }
            case UDP_UART2_Packet_RECEIVED  :
            {
             sprintf(str1,"UDP_STATE: -UDP_UART2_Packet_RECEIVED %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket UART2 GOOD ACK RECEIVED\n\r");
             break;//case UART2_SEND_UDP_PACKET_FAIL
            }
           case UDP_UART2_Packet_BAD_CRC  :
            {
             sprintf(str1,"UDP_STATE: -UDP_UART2_Packet_BAD_CRC %d : %d : %d \n\r",hoursActual,minutesActual,secondsActual);
             //Uart5_write_text("UDP_STATE: -Paket UART2 GOOD ACK RECEIVED\n\r");
             break;//case UART2_SEND_UDP_PACKET_FAIL
            }
           break;//switch(State)
         }

        break;
      }//case(UDPSTA_Add)
   }//switch(address)
     Uart5_write_text(&str1);
#endif
 }
/*-----------------------------------------------------------------------------
 *      CRC16_Calc:  CRC16 calc
 *
 *  Parameters: unsigned char *Packet,unsigned char Packet_length
 *  Return:     uint16_t = CRC16
 *----------------------------------------------------------------------------*/
uint16_t CRC16_Calc( unsigned char *Packet,unsigned char Packet_length)
{
  unsigned int dat=0u,
   cnt,
   nibblemax,
   reg,
   look,
   pos,
   crc;
  const unsigned int CRC16_TBL[16] = {0x0000u,0xDA44u,0x3081u,0xEAC5u,0x6102u,
  0xBB46u,0x5183u,0x8BC7u,0xC204u,0x1840u,
  0xF285u,0x28C1u,0xA306u, 0x7942u,0x9387u,
  0x49C3u};
  /**************************************************************
  * Initialize
  **************************************************************/
   crc=0u;
   dat=0u;
   cnt=0u;
   nibblemax=0u;
   pos=0u;
   reg=0u;
   look=0u;
   reg = 0u;
  nibblemax = Packet_length << 1u; /* 2 nibbles per byte */
  /**************************************************************
  * Run over whole byte vector
  **************************************************************/
  for( cnt = 0u; cnt < nibblemax; cnt++ )
  {
  pos = cnt >> 1u; /* byte counter */
  dat = Packet[pos];
  pos = ( cnt & 0x0001u ) << 2u; /* lower/upper nibble */
  dat >>= pos; /* shift by 4 or by 0 */
  dat &= 0x000Fu;
  /***********************************************************
  * xor register with LSB input data nibble
  * lookup intermediate value
  * shift register one nibble right
  * xor register with intermediate value
  ***********************************************************/
  reg ^= dat;
  pos = reg & 0x000Fu;
  look = CRC16_TBL[pos];
  reg >>= 4u;
  reg ^= look;
  }
  /* end for( cnt < nibblemax ) */
  crc = reg;
  return (crc);
}

/*-----------------------------------------------------------------------------
 *      Init_Node():  Initialise the Node with ip address and remote (send) ip address
 *
 *  Parameters: none
 *  Return:     none
 *----------------------------------------------------------------------------*/
void Init_Node()
 {
   
   if (Node==GIMJOY)                                                            // Set-up determines Gimbal Joystick PIC32 or Commander FT900
   {
     This_Node.Is=GIMJOY;
     This_Node.IPAddr[0u]= GimJoy_IpAddr[0u];
     This_Node.IPAddr[1u]= GimJoy_IpAddr[1u];
     This_Node.IPAddr[2u]= GimJoy_IpAddr[2u];
     This_Node.IPAddr[3u]= GimJoy_IpAddr[3u];

#ifdef ETHER_USE_DEFINED_MAC                                                    // Otherwise use the physical MAC Address of the Card
     This_Node.MacAddr[0u]=GimJoy_MACaddr[0u];                                  // What was hardcoded in the definition file
     This_Node.MacAddr[1u]=GimJoy_MACaddr[1u];
     This_Node.MacAddr[2u]=GimJoy_MACaddr[2u];
     This_Node.MacAddr[3u]=GimJoy_MACaddr[3u];
     This_Node.MacAddr[4u]=GimJoy_MACaddr[4u];
     This_Node.MacAddr[5u]=GimJoy_MACaddr[5u];
#else
     This_Node.MacAddr[0u]=(EMAC1SA2 & 0xFFu);                                  // Read what is physically there and use that
     This_Node.MacAddr[1u]=((EMAC1SA2>>8u) & 0xFFu);
     This_Node.MacAddr[2u]=(EMAC1SA1 & 0xFFu);
     This_Node.MacAddr[3u]=((EMAC1SA1>>8u) & 0xFFu);
     This_Node.MacAddr[4u]=(EMAC1SA0 & 0xFFu);
     This_Node.MacAddr[5u]=((EMAC1SA0>>8u) & 0xFFu);
#endif

     // Set-Up Remote IP (of the corresponding serial decoder) for each protocol
#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
     SBGC_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; SBGC_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; SBGC_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];SBGC_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];    // Air COM PIC
#endif
#if (CAMERA_TYPE == XS_CAM)
     XS_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; XS_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; XS_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];XS_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];
#endif
#if defined(RUN_CAM_USED)
     RunC_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; RunC_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; RunC_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];RunC_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];  // Air COM PIC
#endif
   }

   if (Node==COMGS)                                                             // Set-up determines Gimbal Joystick PIC32 or Commander FT900
   {
     This_Node.Is=COMGS;
     This_Node.IPAddr[0u]= Com1Gs_IpAddr[0u];
     This_Node.IPAddr[1u]= Com1Gs_IpAddr[1u];
     This_Node.IPAddr[2u]= Com1Gs_IpAddr[2u];
     This_Node.IPAddr[3u]= Com1Gs_IpAddr[3u];

#ifdef ETHER_USE_DEFINED_MAC                                                    // Otherwise use the physical MAC Address of the Card
     This_Node.MacAddr[0u]=Com1Gs_MACaddr[0u];
     This_Node.MacAddr[1u]=Com1Gs_MACaddr[1u];
     This_Node.MacAddr[2u]=Com1Gs_MACaddr[2u];
     This_Node.MacAddr[3u]=Com1Gs_MACaddr[3u];
     This_Node.MacAddr[4u]=Com1Gs_MACaddr[4u];
     This_Node.MacAddr[5u]=Com1Gs_MACaddr[5u];
#else
     This_Node.MacAddr[0u]=(EMAC1SA2 & 0xFFu);                                  // What is physically there read from the PHY chip
     This_Node.MacAddr[1u]=((EMAC1SA2>>8u) & 0xFFu);
     This_Node.MacAddr[2u]=(EMAC1SA1 & 0xFFu);
     This_Node.MacAddr[3u]=((EMAC1SA1>>8u) & 0xFFu);
     This_Node.MacAddr[4u]=(EMAC1SA0 & 0xFFu);
     This_Node.MacAddr[5u]=((EMAC1SA0>>8u) & 0xFFu);
#endif

     // Set-Up Remote IP (of the corresponding serial decoder) for each protocol
#if defined(SBGC_GIMBAL_HMI) || defined(SBGC_GIMBAL_JOY)
     SBGC_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; SBGC_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; SBGC_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];SBGC_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];
#endif
#if (CAMERA_TYPE == XS_CAM)
     XS_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; XS_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; XS_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];XS_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];  // Air COM PIC
#endif
#if defined(RUN_CAM_USED)
     RunC_DATA.RemoteIpAddr[0u] = Air_IpAddr[0u]; RunC_DATA.RemoteIpAddr[1u] = Air_IpAddr[1u]; RunC_DATA.RemoteIpAddr[2u] =Air_IpAddr[2u];RunC_DATA.RemoteIpAddr[3u] = Air_IpAddr[3u];  // Air COM PIC
#endif
   }
 
 }

/*-----------------------------------------------------------------------------
 *      checksum:  Function to calculate a checksum for old version SimpleBGC message
 *
 *  Parameters: unsigned char *fdata, unsigned int sz
 *  Return:     unsigned char (8bit checksum)
 *----------------------------------------------------------------------------*/
unsigned char checksum(unsigned char *fdata, unsigned int sz)                   // A Function to calculate a checksum for old version of simpleBGC
{
  int i;
  unsigned char checksum;
  for(i=0u, checksum=0u; i<sz; i++)                                             // initialise checksum to zero and iterate through each char in the message
    checksum+=fdata[i];                                                         // Add them together
  return checksum;
}

/*-----------------------------------------------------------------------------
 *      DataChangeDetected:  Return true if a datachange occurred
 *
 *  Parameters: uint16_t dataVal, uint16_t dataLast, uint8_t delta
 *  Return:     uint8_t true if the new value is greater than the old by more than delta
 *----------------------------------------------------------------------------*/
uint8_t DataChangeDetected( uint16_t dataVal, uint16_t dataLast, uint8_t delta) // Function to detect a data change by more than X
{
   return (uint8_t) (abs(dataVal - dataLast)>delta);                            // 1 for change 0 for no-change
}

/*-----------------------------------------------------------------------------
 *      Scale_Raw_Value:  Scale a raw float value
 *
 *  Parameters: uint16_t raw_min, uint16_t raw_max, float scale_min, float scale_max, uint16_t raw_value
 *  Return:     float32_t : scaled value
 *----------------------------------------------------------------------------*/
float32_t Scale_Raw_Value( int16_t raw_min, int16_t raw_max, float32_t scale_min, float32_t scale_max, int16_t raw_value)
{
    return  ((( (float) (scale_max - scale_min) / (float) (raw_max - raw_min)) * raw_value) + (float) scale_min);
}