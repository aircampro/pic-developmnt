#ifndef LwNx_Defs_H                                                                  // if not defined then define (include once)
#define LwNx_Defs_H
#include"definitions.h"

#define PACKET_START_BYTE 0xAAU                                                 // define start byte
#define PACKET_TIMEOUT 200U                                                     // timeout on reply message
#define PACKET_RETRIES 4U                                                       // number of retries

#define LWNX_CMD_PRODUCT_NM 0U                                                  // (Command 0: Product name)
#define LWNX_CMD_HW_VER 1U                                                      // (Command 1: Hardware version)
#define LWNX_CMD_FW_VER 2U                                                      // (Command 2: Firmware version)
#define LWNX_CMD_SERIAL_NO 3U                                                   // (Command 3: Serial number)
#define LWNX_CMD_TXT 7U                                                         // UTF8 text message Human readable text message
#define LWNX_CMD_STORE_USER_DATA 9U                                             // User data 16 byte store for user data RW 16 16 Y
#define LWNX_CMD_TOKEN 10U                                                      // Token Next usable safety token R 2 -
#define LWNX_CMD_SAVE_PARAM 12U                                                 // Save parameters Store persistable parameters W - 2
#define LWNX_CMD_RESET 14U                                                      // Reset Restart the unit W - 2
#define LWNX_CMD_UPLOAD_FIRM 16U                                                // Stage firmware Upload firmware file pages RW 4 130
#define LWNX_CMD_COMMIT 17U                                                     // Commit firmware Apply staged firmware RW 4 0
#define LWNX_CMD_VOLT 20U                                                       // Incoming voltage Measured incoming voltage R 4 -
#define LWNX_CMD_STREAM 30U                                                     // (Command 30: Stream)
#define LWNX_CMD_DIST_OBJ 48U                                                   // Command 48: Distance output. Streaming distance data
#define LWNX_CMD_LASER_STATE 50U                                                // Laser firing Laser firing state RW 1 1 N
#define LWNX_CMD_TEMP 55U                                                       // Temperature Measured temperature R 4 -
#define LWNX_CMD_BAUD 90U                                                       // Baud rate Serial baud rate RW 1 1 Y
#define LWNX_CMD_DIST 105U                                                      // Distance Single direction distance RW 12 6 N
#define LWNX_CMD_MOTOR_STATE 106U                                               // Motor state Motor state R 1 -
#define LWNX_CMD_MTR_VOLT 107U                                                  // Motor voltage Measured motor voltage R 2 -
#define LWNX_CMD_STREAM_RATE 108U                                               // Output rate Streaming point output rate RW 1 1 Y
#define LWNX_CMD_FORWARD_OFF 109U                                               // Forward offset Forward orientation offset RW 2 2 Y
#define LWMX_CMD_REVOL 110U                                                     // Revolutions Number of revolutions R 4 -
#define LWNX_CMD_ALARM_STATE 111U                                               // Alarm state Triggered state of alarms R 1 -
#define LWNX_CMD_ALARM1 112U                                                    // Alarm 1 configuration RW
#define LWNX_CMD_ALARM2 113U                                                    // Alarm 2 configuration RW
#define LWNX_CMD_ALARM3 114U                                                    // Alarm 3 configuration RW
#define LWNX_CMD_ALARM4 115U                                                    // Alarm 4 configuration RW
#define LWNX_CMD_ALARM5 116U                                                    // Alarm 5 configuration RW
#define LWNX_CMD_ALARM6 117U                                                    // Alarm 6 configuration RW
#define LWNX_CMD_ALARM7 118U                                                    // Alarm 7 configuration RW

// define the serial port here ---------------
#define LWNX_PORT 1U                                                            // Use UART 1  You have to set-up the corresponding interrupt

// maxiumum number of point distances that can be returned
#define LWNX_MAX_POINT_DIST 210U

// error codes returned from a firmware upload
#define LWNX_FIRM_UPL_PAGE_ERR -1                                               //Page length is invalid
#define LWNX_FIRM_UPL_IDX_ERR -2                                                //Page index is out of range
#define LWNX_FIRM_UPL_ERASE_ERR -3                                              //Flash failed to erase
#define LWNX_FIRM_UPL_FILE_ERR -4                                               //Firmware file has invalid header
#define LWNX_FIRM_UPL_WRITE_ERR -5                                              //Flash failed to write
#define LWNX_FIRM_UPL_WRONG_HW -6                                               // Firmware is for a different hardware version
#define LWNX_FIRM_UPL_WRONG_PROD -7                                             //Firmware is for a different product

// error codes from commit
#define LWNX_COMMIT_FAIL -1                                                     //Firmware integrity check failed
#define LWNX_COMMIT_OK 1                                                        // Firmware integrity check passed and firmware committed

// conversion of volts
#define LWNX_VOLTS(counts) (( counts / 4095.0f) * 2.048f * 5.7f)                // volts calculated from counts returned by command 20

// write stream state
#define LWNX_WRITE_STREAM_OFF 0U                                                // disable stream when write with cmd 30
#define LWNX_WRITE_STREAM_ON 3U                                                 // enable stream and read back distance objects with cmd 48

// laser fire
#define LWNX_LASER_ON 1U                                                        // laser enable with cmd 50
#define LWNX_LASER_OFF 0U                                                       // laser disable with cmd 50
#define LWNX_RETRY_TICKS 10000U                                                 // retry ticks

// temp conversion
#define LWNX_TEMP(ret) (((float32_t) ret) / 100.0f)                            // temperture calculation in degree from returned count in 100th degree cmd 55

// set baud (only 115200 allowed in PIC32)
#define LWNX_BAUD_115200 4U                                                     // 115200 baud
#define LWNX_BAUD_230400 5U                                                     // 230400 baud
#define LWNX_BAUD_460800 6U                                                     // 460800 baud
#define LWNX_BAUD_921600 7U                                                     // 921600 baud

// motor state command 106
#define LWNX_MOTOR_STARTING 1U                                                  // Motor is preparing for start-up.
#define LWNX_MOTOR_WAIT_REVOL 2U                                                // Motor is waiting for first 5 revolutions to occur.
#define LWNX_MOTOR_RUN 3U                                                       // Motor is running normally.
#define LWNX_MOTOR_COMM_FAIL 4U                                                 // Motor has failed to communicate.

// stream rates in points / second with command 108
#define LWNX_STREAM_20010 0U                                                    // 20010 points / second
#define LWNX_STREAM_10005 1U                                                    // 10005 points / second
#define LWNX_STREAM_6670 2U                                                     // 6670 points / second
#define LWNX_STREAM_2001 3U                                                     // 2001 points / second

// alarm status word cmd 111
#define LWNX_ALARM1 (1U<<0U)
#define LWNX_ALARM2 (1U<<1U)
#define LWNX_ALARM3 (1U<<2U)
#define LWNX_ALARM4 (1U<<3U)
#define LWNX_ALARM5 (1U<<4U)
#define LWNX_ALARM6 (1U<<5U)
#define LWNX_ALARM7 (1U<<6U)
#define LWNX_ALARM8 (1U<<7U)

#define LWNX_MAX_WAIT 10000U                                                    // number of ticks to wait for a stream point reply or reset after no resposne

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define LWNXPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define LWNXPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif


#ifdef lwserial
class lwResponsePacket {
        public:
                uint8_t data[1024];
                int32_t size;
                int32_t payloadSize;
                uint8_t parseState;
                lwResponsePacket();
};
#else
LWNXPACKED(
typedef struct {
                uint8_t dataVal[1024U];                                         // payload segment
                int32_t size;                                                   // size
                int32_t payloadSize;                                            // payload size in bytes
                uint8_t parseState;                                             // state of message collection
}) lwResponsePacket;                                                            // response packet structure
LWNXPACKED(
typedef struct {
               char modelName[16U];                                             // returned with (Command 0: Product name)
               uint32_t hardwareVersion;                                        // returned with Read the hardware version. (Command 1: Hardware version)
               uint32_t firmwareVersion;                                        // returned with // Read the firmware version. (Command 2: Firmware version)
               char serialNumber[16U];                                          // Read the serial number. (Command 3: Serial number)
}) lwHardware_t;
LWNXPACKED(
typedef struct {
                int16_t pointDistance[LWNX_MAX_POINT_DIST];                     // maximum number of point distances that could be returned
                float32_t angle[LWNX_MAX_POINT_DIST];                           // angle calculated for point
}) lwPoint_t;
LWNXPACKED(
typedef struct {
                uint8_t alarmState;                                             // State of each alarm as described in Alarm state
                uint16_t pointsPerSecond;                                       // Number of points per second.
                int16_t forwardOffset;                                          // Orientation offset as described in Forward offset
                int16_t motorVoltage;                                           // motor voltage
                uint8_t revolutionIndex;                                        // Increments as each new revolution begins. Note that this value wraps to 0 after 255
                uint16_t pointTotal;                                            // Total number of points this revolution
                uint16_t pointCount;                                            // Number of points in this packet
                uint16_t pointStartIndex;                                       // Point start index Index of the first point in this packet
                lwPoint_t pointDistAngle[];                                     // Array of distances [cm] for each point
}) lwOutputData_t;
LWNXPACKED(
typedef struct {
                uint16_t pageIndex;                                             // page index
                uint8_t pageData[128U];                                         // each page 128 byte chunks
}) lwFirmwareUpload_t;                                                          // firware upgrade structure Pages are created by dividing the firmware upgrade file into multiple 128 byte chunks.
LWNXPACKED(
typedef struct {
                int16_t avg_dist;                                               // Average distance [cm]
                int16_t closest_dist;                                           // Closest distance [cm]
                int16_t furthest_dist;                                          // Furthest distance [cm]
                int16_t ang_close_dist;                                         // Angle to closest distance [10ths of a degree]
                uint32_t calcTime;                                              // Calculation time [us]
}) lwDistanceResp_t;                                                            // distance type read with command 105
LWNXPACKED(
typedef struct {
                int16_t Direction;                                              // Direction [degrees]
                int16_t angular_width;                                          // Angular width [degrees]
                int16_t min_dist;                                               // Minimum distance [cm]
}) lwDistanceReq_t;                                                             // distance type request with command 105
LWNXPACKED(
typedef struct {
                uint8_t Enabled;                                                // Enabled 1 is enabled, 0 is disabled.
                int16_t Direction;                                              // Primary direction in degrees
                int16_t Width;                                                  // Angular width in degrees around the primary direction.
                int16_t AlmDistance;                                            // Distance at which alarm is triggered.
}) lwAlarm1_t;                                                                  // command 112

#endif

#endif