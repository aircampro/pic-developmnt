#ifndef Struts_H
#define Struts_H
#include "SBGC_Command.h"
#include "inttypes.h"
#include "stdint.h"

//#define MAX_BUFFER 38
#include "definitions.h"
#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define STRUTPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define STRUTPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

STRUTPACKED (
typedef struct{
  unsigned int    Baud;                                                         //UART port Baud rate
  unsigned int    From_Port;                                                    //Ethernet port packets will come from
  unsigned int    Dest_Port;                                                    //Etherner port packets will be sent to
  unsigned int    ACK_Port;                                                     //Ethernet Packat acknowledgment port
  //unsigned char   Packet_Ready;  //Colpleate packet in the UART Buff TRUE or FALSE
  unsigned char   Enable;                                                       //Enable UART
  unsigned char   Buffer[SBGC_CMD_MAX_BYTES];                                   //Buffer the Packet
  unsigned int    Index;                                                        //Packet Index
  unsigned int    Index_Was;                                                    //The last index was
  unsigned char   Len;                                                          //Length of UART packet
  unsigned char   UDP_Buffer[SBGC_CMD_MAX_BYTES];                               //Buffer for incoming UDP packet
  unsigned char   UDP_Index;                                                    //Index for incoming UDP packet  unsigned char   Max_Length;    //Mexumam length of package we expect
  unsigned char   UDP_Buffer_Len;                                               //Length of UDP Packet
  unsigned char   RemoteIpAddr[4];
  unsigned char   UDP_CRC[5];                                                   //ACK CRC packet
  unsigned char   UDP_CRC_Index;
  unsigned char   UDP_Send_Trys;                                                //How many times we tried to send the UDP packet
  unsigned char   UDP_Send_Slow_ACK;                                            // How many times did we slow ACK when a UDP packet was sent?
  unsigned char   UDP_Send_Slow_ACK_Timeout;
  unsigned char   UDP_Send_Fail;                                                // How many times did we fail to send a UDP packet?
  unsigned char   UDP_Send_Retry;                                               //How many times we has to retry sending a packet
  unsigned int    UDP_Packets_Sent;                                             //how many packets we have sent.
  unsigned int    UDP_Byte_Sent;                                                //how many Bytes we have sent.
  unsigned int    Bytes_In;
  unsigned int    Packets_In;
  unsigned char   Buffer_Overrun;                                               // How many timer we had a buffer overrun
  unsigned char   Buffer_Underrun;                                              // How many timer we has a buffer underrun
  unsigned char   State;                                                        //Status of the Packet (See Definitions)
  unsigned char   Timeout_Enable;                                               //Enable UART Timeoute
  unsigned char   Msg_id_recv;                                                  // Message Id received.
 }) UART;

 STRUTPACKED (
typedef struct{
  unsigned int    Baud;                                                         //UART port Baud rate
  unsigned char   Enable;                                                       //Enable UART
  unsigned char   Buffer[SBGC_CMD_MAX_BYTES];                                   //Buffer the Packet
  unsigned int    Index;                                                        //Packet Index
  unsigned int    Index_Was;                                                    //The last index was
  unsigned char   Len;                                                          //Length of UART packet
  unsigned int    Bytes_In;
  unsigned int    Packets_In;
  unsigned char   Buffer_Overrun;                                               // How many timer we had a buffer overrun
  unsigned char   Buffer_Underrun;                                              // How many timer we has a buffer underrun
  unsigned char   State;                                                        //Status of the Packet (See Definitions)
  unsigned char   Timeout_Enable;                                               //Enable UART Timeoute
  unsigned char   Msg_id_recv;                                                  // Message Id received.
 }) SerialObject_t;

STRUTPACKED (
typedef struct{
  unsigned int    From_Port;                                                    //Ethernet port packets will come from
  unsigned int    Dest_Port;                                                    //Etherner port packets will be sent to
  unsigned int    ACK_Port;                                                     //Ethernet Packat acknowledgment port
  unsigned char   Enable;                                                       //Enable UART
  unsigned int    Index;                                                        //Packet Index
  unsigned int    Index_Was;                                                    //The last index was
  unsigned char   Len;                                                          //Length of UART packet
  unsigned char   UDP_Buffer[SBGC_CMD_MAX_BYTES];                               //Buffer for incoming UDP packet
  unsigned char   UDP_Index;                                                    //Index for incoming UDP packet  unsigned char   Max_Length;    //Mexumam length of package we expect
  unsigned char   UDP_Buffer_Len;                                               //Length of UDP Packet
  unsigned char   RemoteIpAddr[4];
  unsigned char   UDP_CRC[5];                                                   // ACK CRC packet
  unsigned char   UDP_CRC_Index;
  unsigned char   UDP_Send_Trys;                                                //How many times we tried to send the UDP packet
  unsigned char   UDP_Send_Slow_ACK;                                            // How many times did we slow ACK when a UDP packet was sent?
  unsigned char   UDP_Send_Slow_ACK_Timeout;
  unsigned char   UDP_Send_Fail;                                                // How many times did we fail to send a UDP packet?
  unsigned char   UDP_Send_Retry;                                               //How many times we has to retry sending a packet
  unsigned int    UDP_Packets_Sent;                                             //how many packets we have sent.
  unsigned int    UDP_Byte_Sent;                                                //how many Bytes we have sent.
  unsigned int    Bytes_In;
  unsigned int    Packets_In;
  unsigned char   Buffer_Overrun;                                               // How many timer we had a buffer overrun
  unsigned char   Buffer_Underrun;                                              // How many timer we has a buffer underrun
  unsigned char   State;                                                        //Status of the Packet (See Definitions)
  unsigned char   Timeout_Enable;                                               //Enable UART Timeoute
  unsigned char   Msg_id_recv;                                                  // Message Id received.
 }) EthUDPObject_t;
 
 STRUTPACKED (
 typedef struct{
  unsigned char   Is;
  unsigned char   MacAddr[6];
  unsigned char   IpAddr[4];
  unsigned char   Link_Established;
  unsigned char   Link_Alive;
  unsigned char   Link_Alive_Was;
  unsigned int    Link_Alive_Time;
  
})Node;

 STRUTPACKED (
 typedef struct{
 unsigned char Enabled;     // enable heart beat
 unsigned char Count; //Clear the heart Beat counter
 unsigned char Step;        //Process step
 unsigned char Max_Time; // Maximum time for timeout
 unsigned char Air;  //Clear heart Beat for air Node
 unsigned char MasGs;  //Clear heart Beat for Master ground station Node
 unsigned char FirGs;  //Clear heart Beat for Fire ground station Node
 unsigned int Time;   //Clear the heart beat timer beat
 unsigned char Air_Time;  //Clear heart Beat for air Node
 unsigned char MasGs_Time;  //Clear heart Beat for Master ground station Node
 unsigned char FirGs_Time;  //Clear heart Beat for Fire ground station Node
 })H_B;

 STRUTPACKED (                                                                  // GPS message object
typedef struct {

      uint32_t timestamp;                                                       // time from GPS
      uint8_t numOfSatel;                                                       // number of satelites used in solution
      float32_t lat;                                                            // latitude
      float32_t longd;                                                          // longditude
      float32_t altRef;                                                         // Altitude above user datum ellipsoid
      float32_t hAcc;                                                           // Horizontal accuracy estimate.
      float32_t vAcc;                                                           // Vertical accuracy estimate
      float32_t SOG;                                                            // speed over ground
      float32_t COG;                                                            // true course
      float32_t vVel;                                                           // Vertical velocity (positive downwards)
      float32_t SealevelAltitude;                                               // Sea level altitude meters
      float32_t GeoidalHt;                                                      // Geoidal height

}) GPS_Info_t;

 #endif /*Struts_H*/