#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define CAMPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define CAMPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif
// =============================================================================
// ------------------------ Xiaomi Yi Action Camera ----------------------------
// =============================================================================
#define XY_HOTSPOT_NAME(X) "YDXJ_" #X                                           // macro for making default hotspot name  X corresponds to the last 7 digits of the battery compartment at the camera's back side
#define XY_PASS_DEFAULT "1234567890"                                            // default password wifi
#define XY_CUSTOM_VLC_PATH "."                                                  // default path
//#define XY_CUSTOM_IP "192.168.1.254"                                          // Custom ip for the Yi Action Server  default (for SJ4000 WIFI used)
#if defined(XY_CUSTOM_IP)
   #define XY_USE_IP XY_CUSTOM_IP                                               // Use the custom ip specified
#else
  #define XY_USE_IP "192.168.42.1"                                              // Use the default ip
#endif

#define XY_STATE_PRE_MAX_TIM 10000U                                             // Maximum time to wait for an AJAX reply from the server

// Telnet server info  (May be needed, otherwise try raw tcp open on JSON port)
// Set up configuration parameter defaults if not overridden in
// You need to create a file shown here "cd /Volumes/16GB_CLASS4 "touch enable_info_display.script" in the root directory of the SD card,
// that triggers the main OS (the one that boots before Linux) to tell Linux to run telnetd once Linux comes up.
// edit /etc/init.d/S50service to uncomment the invocation of telnetd
// Create an autoexec.ash file on the root of the SD card with content "lu_util exec telnetd -l/bin/sh"
#if defined(XY_TELNET_PORT)
   #define TELNET_PORT  23U                                                     // Unsecured Telnet port  (check cam is there nc -vvn <ip.ip.ip.ip> 23
#endif
#if !defined(XY_TELNETS_PORT)
   #define TELNETS_PORT        992U                                             // SSL Secured Telnet port (ignored if STACK_USE_SSL_SERVER is undefined)
#endif
#if !defined(XY_MAX_TELNET_CONNECTIONS)
   #define MAX_TELNET_CONNECTIONS (3u)                                          // Maximum number of Telnet connections
#endif
#if !defined(XY_TELNET_USERNAME)
   #define TELNET_USERNAME "root"                                               // Default Telnet user name is root
#endif
#if !defined(XY_TELNET_PASSWORD)
   #define TELNET_PASSWORD ""                                                   // Default Telnet password is no password
#endif

// command replies in rval which is parsed using JSMN
#define XY_RESP_OK 0                                                            // return values in rval
#define XY_INVALID_JSON_OBJ -7                                                  // invalid JSON object
#define XY_INVALID_CMD -9                                                       // input object is not a valid command
#define XY_RESP_CRASH -101                                                      // camera crash
#define XY_RESP_INVALID -15                                                     // invalid response
#define XY_RESP_REQ_NEW_TOKEN -4                                                // request a new token (input object is empty)
#define XY_RESP_NO_SD_CARD -27                                                  // No SD Card was present
// maximum send and response bytes
#define XY_MSG_MAX_SND_LEN 100U                                                 // Maximum size request
#define XY_MSG_MAX_LEN 2000U                                                    // Maximum size reply

// Define how the jasmine parser reads the reply message (from logic related to the msg_id or top level)
#define XY_READ_ALL  0U                                                         //  read all the params and types
#define XY_READ_SPECIFIC 1U                                                     // reads the specific keys requested
//==============================================================================================================================================================
// Folder Defs         self.FileTypes = {"/":"Folder", ".ash":"Script", ".bmp":"Image", ".ico":"Image", ".jpg":"Image", ".mka":"Audio", ".mkv":"Video", "mp3":"Audio", ".mp4":"Video", ".mpg":"Video", ".png":"Image", ".txt":"Text", "wav":"Audio"}
//                self.ChunkSizes = [0.5,1,2,4,8,16,32,64,128,256]

// To initialte requests you must get the token
// Access token
// Request:
// {"msg_id":257,"token":0}
// Answer:
// { "rval": 0, "msg_id": 257, "param": 1 }
// param=1, 1 is a TOKEN, it could be any other INT (or string? or something else?)
//
// Via telnet you can test as :- (you need cyclone tcp for tcp/ip stack in pic32)
// tcp 0 0 0.0.0.0:7878 0.0.0.0:* LISTEN 745/network_message
// this lien tells you, that 7878 is open. So you have to be able to connect to it from another device. thus from windows
// telnet 192.168.42.1 7878
// and send this
// {"msg_id":257,"token":0}
#define XY_START_SESS_CMD 257U                                                  // Start Session "param"  '{"msg_id":257,"token":0}') call before 259 to start session
#define API_XY_INIT_SESS_STR ""msg_id":257,"token":0"
#define API_XY_INIT_SESSION(X)  { sprintf(X,"{\"msg_id\":267,\"token\":0, \"heartbeat\": 1}\r"); } // {"msg_id":257,"token":0}
#define API_XY_INIT_FAST "{\"msg_id\": 267,\"token\":0}"

#define XY_STOP_SESS_CMD 258U                                                   // Stop Session "param"
#define API_XY_STOP_SESSION(X,token)  { sprintf(X,"{\"msg_id\":258,\"token\":%d}\r",token); } // {"msg_id":258,"token":<id>}
#define API_XY_STOPDEF_FAST "{\"msg_id\": 258,\"token\":0}"                     // makes the string
#define API_XY_STOP_FAST(token) "{\"msg_id\": 258,\"token\":" #token "}"        // makes the string

// ======= AJAX messages to the JSON server ====================================
#define XY_FILE_CMD 1287U                                                       // sendCancelGetFile and sendCancelPutFile (String)  param sent_size
#define XY_API_FILE_CANCEL(X,token)  { sprintf(X,"{\"msg_id\":1287,\"token\":%d}\r",token); }
#define XY_API_FILE_CANCEL_FAST(token) "{\"msg_id\": 1287,\"token\":" #token "}"   // makes the string

#define XY_BITRATE_CMD 16U                                                      // ChangeBitRate (int16)
#define XY_API_BITRATE(X,token,bitr)  { sprintf(X,"{\"msg_id\":16,\"token\":%d,\"param\":%d}\r",token,bitr); }
#define XY_API_BITRATE_FAST(token,bitr) "{\"msg_id\": 16,\"token\":" #token ",\"param\":" #bitr "}"   // makes the string

#define XY_DIR_CMD 1283U                                                        // sendChangeDirectory (String)   "param"
#define XY_API_DIR(X,token,para)  { sprintf(X,"{\"msg_id\":1283,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_DIR_FAST(token,para) "{\"msg_id\": 1283,\"token\":" #token ",\"param\":" #para "}"   // makes the string

#define XY_CCS_CMD 770U                                                         // sendContinueCaptureStop (Null)
#define XY_API_CCS(X,token)  { sprintf(X,"{\"msg_id\":770,\"token\":%d}\r",token); }
#define XY_API_CCS_FAST(token) "{\"msg_id\": 770,\"token\":" #token "}"    // makes the string

#define XY_DEL_FILE_CMD 1281U                                                   // sendDeleteFile (string) "param"
#define XY_API_DEL_FILE(X,token,para)  { sprintf(X,"{\"msg_id\":1281,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_DEL_FAST(token,para) "{\"msg_id\": 1281,\"token\":" #token ",\"param\":" #para "}"   // makes the string

#define XY_RESET_CMD 259U                                                       // sendForceResetVF
#define XY_API_RESET(X,token)  { sprintf(X,"{\"msg_id\":259,\"token\":%d}\r",token); }
#define XY_API_RESET_FAST(token) "{\"msg_id\": 259,\"token\":" #token "}"    // makes the string

#define XY_SND_FMT_CMD 4U                                                       // sendFormat(String s) "param"
#define XY_API_SND_FMT(X,token,para)  { sprintf(X,"{\"msg_id\":4,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_FMT_FAST(token,para) "{\"msg_id\": 4,\"token\":" #token ",\"param\": \"" #para "\"}"   // makes the string

// start event for status updates
#define XY_STATUS_CMD 7U                                                        // statusUpdate() of various items may be sent automatically
#define XY_API_STATUS(X,token,type)  { sprintf(X,"{\"msg_id\":7,\"token\":%d,\"type\": \"%s\" }\r",token,type); }
#define XY_API_AUTOSTAT(X,token)  { sprintf(X,"{\"msg_id\":7,\"token\":%d }\r",token); }
#define XY_API_AUTOSTAT_FAST(token) "{\"msg_id\": 7,\"token\":" #token "}"      // without the type asks for status update event to be turned on
#define XY_API_STATUS_FAST(token,type) "{\"msg_id\": 7,\"token\":" #token ",\"type\": \"" #type "\"}"    // makes the string "type" : "battery"

// Camera config
// Request:
// {"msg_id":3,"token":1}
// Answer:
// { "rval": 0, "msg_id": 3, "param": [ { "camera_clock": "2015-04-07 02:32:29" }, { "video_standard": "NTSC" }, { "app_status": "idle" }, { "video_resolution": "1920x1080 60P 16:9" }, { "video_stamp": "off" }, { "video_quality": "S.Fine" }, { "timelapse_video": "off" }, { "capture_mode": "precise quality" }, { "photo_size": "16M (4608x3456 4:3)" }, { "photo_stamp": "off" }, { "photo_quality": "S.Fine" }, { "timelapse_photo": "60" }, { "preview_status": "on" }, { "buzzer_volume": "mute" }, { "buzzer_ring": "off" }, { "capture_default_mode": "precise quality" }, { "precise_cont_time": "60.0 sec" }, { "burst_capture_number": "7 p / s" }, { "restore_factory_settings": "on" }, { "led_mode": "all enable" }, { "dev_reboot": "on" }, { "meter_mode": "center" }, { "sd_card_status": "insert" }, { "video_output_dev_type": "tv" }, { "sw_version": "YDXJv22_1.0.7_build-20150330113749_b690_i446_s699" }, { "hw_version": "YDXJ_v22" }, { "dual_stream_status": "on" }, { "streaming_status": "off" }, { "precise_cont_capturing": "off" }, { "piv_enable": "off" }, { "auto_low_light": "on" }, { "loop_record": "off" }, { "warp_enable": "off" }, { "support_auto_low_light": "on" }, { "precise_selftime": "5s" }, { "precise_self_running": "off" }, { "auto_power_off": "5 minutes" }, { "serial_number": "xxxxx" }, { "system_mode": "capture" }, { "system_default_mode": "capture" }, { "start_wifi_while_booted": "off" }, { "quick_record_time": "0" }, { "precise_self_remain_time": "0" }, { "sdcard_need_format": "no-need" }, { "video_rotate": "off" } ] }
#define XY_GET_SET_CMD 3U                                                       // sendGetAllCurrentSetting() sendGetAllSettingOption(String s) "param"
#define XY_API_GET_SET(X,token,para)  { sprintf(X,"{\"msg_id\":3,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_GET_SET_FAST(token,para) "{\"msg_id\": 3,\"token\":" #token ",\"param\":" #para "}"   // makes the string
#define XY_API_GET_SET_ALL(X,token)  { sprintf(X,"{\"msg_id\":3,\"token\":%d}\r",token); }
#define XY_API_GET_SET_ALL_FAST(token) "{\"msg_id\": 3,\"token\":" #token "}"   // makes the string "msg_id":3,"token":1

#define XY_BAT_LVL_CMD 13U                                                      // sendGetBatteryLevel()
#define XY_API_BAT_LVL(X,token)  { sprintf(X,"{\"msg_id\":%13,\"token\":%d}\r",token); }
#define XY_API_BAT_LVL_FAST(token) "{\"msg_id\": 13,\"token\":" #token "}"    // makes the string

#define XY_ZOOM_INFO_CMD 15U                                                    // sendGetDigitalZoomInfo(String s)  "type"
#define XY_API_ZOOM_INFO(X,token,para)  { sprintf(X,"{\"msg_id\":15,\"token\":%d,\"type\":%s}\r",token,para); }
#define XY_API_ZOOM_INFO_FAST(token,para) "{\"msg_id\": 15 ,\"token\":" #token ",\"param\":" #para "}"   // makes the string

#define XY_SND_GET_FIL_CMD 1285U                                                // sendGetFile(String s, long l, long l1)  "param" "offset" "fetch_size"
#define XY_API_GET_FIL(X,token,para,off,fet)  { sprintf(X,"{\"msg_id\":1285\"token\":%d,\"param\":%s,\"offset\": %d,\"fetch_size\":%d}\r",token,para,off,fet); }
#define XY_API_GET_FIL_FAST(token,para,off,fet) "{\"msg_id\": 1285,\"token\":" #token ",\"param\":" #para ",\"offset\":" #off ",\"fetch_size\":" #fet "}"  // makes the string

#define XY_SND_GET_MDA_CMD 1026U                                                // sendGetMediaInfo(String s) "param"
#define XY_API_GET_MDA(X,token,para)  { sprintf(X,"{\"msg_id\":1026,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_GET_MDA_FAST(token,para) "{\"msg_id\": 1026 ,\"token\":" #token ",\"param\":" #para "}"   // makes the string

#define XY_SND_GET_RCT_CMD 515U                                                 // sendGetRecordTime()
#define XY_API_GET_RCT(X,token)  { sprintf(X,"{\"msg_id\":515,\"token\":%d}\r",token); }
#define XY_API_GET_RCT_FAST(token) "{\"msg_id\": 515 ,\"token\":" #token "}"   // makes the string

// gets the single setting {"msg_id":1,"type":"video_resolution","token":1}
#define XY_SND_GETSET_CMD 1U                                                    // sendGetSetting(String s) "type"
#define XY_API_SND_GETSET(X,token,type)  { sprintf(X,"{\"msg_id\":1,\"token\":%d,\"type\":%s}\r",token,type); }
#define XY_API_GETSET_FAST(token,para,type) "{\"msg_id\": 1 ,\"token\":" #token ",\"param\":" #para ",\"type\":" #type "}"  // makes the string

// shows available choices for an option e.g {"msg_id":9,"param":"video_resolution","token":1}
#define XY_SND_GET_SETO_CMD 9U                                                  // sendGetSingleSettingOptions(String s) "param" (+ "options")
#define XY_API_GET_SETO(X,token,para)  { sprintf(X,"{\"msg_id\":9,\"token\":%d,\"param\":%s}\r",token,para); }
#define XY_API_SET_OPTION(X,token,para,opt)  { sprintf(X,"{\"msg_id\":9,\"token\":%d, \"permission\": \"settable\" , \"param\":%s, \"options\": %s}\r",token,para,opt); }
#define XY_API_SET_GET_SETO_FAST(token,para) "{\"msg_id\": 9 ,\"token\":" #token ",\"param\":" #para "}" // makes the string
#define XY_API_SET_OPTION_FAST(token,para,opt) "{\"msg_id\": 9 ,\"token\":" #token ", \"permission\" : \"settable\" , \"param\":" #para ", \"options\": " #opt "}" // makes the string

#define XY_SND_GET_SPC_CMD 5U                                                   // sendGetSpace(String s) "type"
#define XY_API_GET_SPC_TOT(X,token)  { sprintf(X,"{\"msg_id\":5,\"token\":%d,\"type\": \"total\"}\r",token); }      // type = "total" or "free"
#define XY_API_GET_SPC_FREE(X,token)  { sprintf(X,"{\"msg_id\":5,\"token\":%d,\"type\": \"free\"}\r",token); }      // type = "total" or "free"
#define XY_API_GET_SPC_FAST(token,type) "{\"msg_id\": 5,\"token\":" #token ",\"type\":" #type "}"  // makes the string

#define XY_SND_GET_TH_CMD 1025U                                                 // sendGetThumb(String s, String s1)  "type" "param"
#define XY_API_SND_GET_TH(X,token,type,param)  { sprintf(X,"{\"msg_id\":1025,\"token\":%d,\"type\":%s,\"param\" %s}\r",token,type,param); }
#define XY_API_GET_TH_FAST(token,type,para) "{\"msg_id\": 1025 ,\"token\":" #token ",\"type\":" #type ",\"param\":" #para "}"  // makes the string

#define XY_SND_STATE_IDLE_CMD 0x100000dUL                                        // sendIntoIdleStateMode()
#define XY_API_SND_STATE_IDLE(X,token)  { sprintf(X,"{\"msg_id\":16777229,\"token\":%d}\r",token); }
#define XY_API_SND_STATE_IDLE_FAST(token) "{\"msg_id\": 16777229 ,\"token\":" #token "}"    // makes the string

#define XY_BBD_CMD 0x100000fUL                                                   // sendIsBindedBluetoothDevs()
#define XY_API_BBD(X,token)  { sprintf(X,"{\"msg_id\":16777231,\"token\":%d}\r",token); }
#define XY_API_BBD_FAST(token) "{\"msg_id\":16777231 ,\"token\":" #token "}"    // makes the string

#define XY_SND_LIST_CMD 1282U                                                   // sendListing() sendListingWithOption(String s)  option  (-D -S)  "param"
#define XY_API_SND_LIST(X,token)  { sprintf(X,"{\"msg_id\":1282,\"token\":%d}\r",token); }
#define XY_API_SND_LIST_FAST(token) "{\"msg_id\": 1282 ,\"token\":" #token "}"   // makes the string
#define XY_API_SND_LIST_OPT(X,token,para)  { sprintf(X,"{\"msg_id\":1282,\"token\":%d, -S -D "param" %s}\r",token,para); }
#define XY_API_SND_LIST_OPT_FAST(token,para) "{\"msg_id\": 1282 ,\"token\":" #token ", -S -D \"param\":" #para "}"   // makes the string

#define XY_SND_STATE_WAKE_CMD 0x100000eUL                                        // sendOutIdleStateMode()
#define XY_API_SND_STATE_WAKE(X,token)  { sprintf(X,"{\"msg_id\":16777220,\"token\":%d}\r",token); }
#define XY_API_SND_STATE_WAKE_FAST(token) "{\"msg_id\": 16777220,\"token\":" #token "}"    // makes the string

#define XY_SND_PIV_CMD 32U                                                      // sendPIV()  0x100000b
#define XY_API_SND_PIV(X,token)  { sprintf(X,"\"msg_id\":32,\"token\":%d\r",token); }
#define XY_API_SND_PIV_FAST(token) "{\"msg_id\": 32 ,\"token\":" #token "}"    // makes the string

#define XY_SND_PUT_FILE_CMD 1286U                                               // sendPutFIle(File file, String s)  "md5sum" "param" "size" "offset"=0U the param can enable a script //{"msg_id":1286,"token":%s,"param":"enable_info_display.script", "offset":0, "size":0, "md5sum":"d41d8cd98f00b204e9800998ecf8427e"}}
#define XY_API_SND_PUT_FILE(X,token,md,para,sz)  { sprintf(X,"{\"msg_id\":1286,\"token\":%d, \"md5sum\" %s, \"param\":%s, \"size\" %d, \"offset\"=0}\r",token,md,para,sz); }  // mds file contains settings created and used by TestComplete, an application that allows users to test web, desktop, and mobile programs. It stores project settings that are common for testers, such as the names of project items and the scripting language.

#define XY_SND_QCK_REC_PAU_CMD 0x1000006UL                                       // sendQuickRecordPause()
#define XY_API_SND_QCK_REC_PAU(X,token)  { sprintf(X,"{\"msg_id\":16777222,\"token\":%d}\r",token); }
#define XY_API_SND_QCK_REC_PAU_FAST(token) "{\"msg_id\": 16777222 ,\"token\":" #token "}"    // makes the string

#define XY_SND_QCK_REC_RES_CMD 0x1000007UL                                       // sendQuickRecordResume()
#define XY_API_SND_QCK_REC_RES(X,token)  { sprintf(X,"{\"msg_id\":16777223,\"token\":%d}\r",token); }
#define XY_API_SND_QCK_REC_RES_FAST(token) "{\"msg_id\": 16777223 ,\"token\":" #token "}"    // makes the string

#define XY_SND_QCK_REC_STRT_CMD 0x1000005UL                                      // sendQuickRecordStart()
#define XY_API_SND_QCK_REC_STRT(X,token)  { sprintf(X,"{\"msg_id\":16777221,\"token\":%d}\r",token); }
#define XY_API_SND_QCK_REC_STRT_FAST(token) "{\"msg_id\": 16777221 ,\"token\":" #token "}"    // makes the string

#define XY_SND_RST_WIFI_CMD 0x1000008UL                                          // sendRestartWiFi()
#define XY_API_SND_RST_WIFI(X,token)  { sprintf(X,"{\"msg_id\":16777224,\"token\":%d}\r",token); }   //    example --- const char * const xy_rst_msg[32]; API_XY_SND_RST_WIFI((char *) &xy_rst_msg,20,XY_SND_RST_VF_CMD);
#define XY_API_SND_RST_WIFI_FAST(token) "{\"msg_id\": 16777224,\"token\":" #token "}"

#define XY_SND_CAP_MDE_CMD 0x100000cUL                                          // sendSetCaptureMode(String s) "param"
#define XY_API_SND_CAP_MDE(X,token,para)  { sprintf(X,"{\"msg_id\":1677228,\"token\":%d, "param" %s}\r",token,para); }
#define XY_API_SND_CAP_MDE_FAST(token) "{\"msg_id\": 1677228 ,\"token\":" #token "}"    // makes the string

#define XY_SND_SET_ZOOM_CMD 14U                                                 // sendSetDigitalZoom(String s, String s1) "type" "param"
#define XY_API_SND_SET_ZOOM(X,token,typ,para)  { sprintf(X,"{\"msg_id\":14,\"token\":%d, \"type\" %s, \"param\" %s}\r",token,typ,para); }
#define XY_API_SND_SET_FAST_ZOOM(X,token,para)  { sprintf(X,"{\"msg_id\":14,\"token\":%d, \"type\" : \"fast\" , \"param\" %s}\r",token,para); }
#define XY_API_SND_SET_ZOOM_FAST(token,typ,para) "{\"msg_id\": 14 ,\"token\":" #token ",\"type\": # typ ",\"param\":" #para "}"   // makes the string

#define XY_SND_SET_SET_CMD 2U                                                   // sendSetSetting(String s, String s1) "type" "param"
// #define XY_API_SND_SET(X,token,typ,para)  { sprintf(X,"{\"msg_id\":2,\"token\":%d, \"type\" %s, \"param\" %s}\r",token,typ,para); }
// might be needed on some systems ---- #define XY_API_SND_SET(X,token,typ,para)  { sprintf(X," \{ \"msg_id\":2\, \"token\":%d\, \"type\"\:%s\, \"param\"\:%s \} \r",token,typ,para); }
#define XY_API_SND_SET(X,token,typ,para)  { sprintf(X," { \"msg_id\":2 , \"token\":%d , \"type\":%s , \"param\":%s } \r",token,typ,para); }
#define XY_API_CONFIG_DEFAULT(X,token)  { sprintf(X," { \"msg_id\":2 , \"token\":%d , \"type\":\"restore_factory_settings\" , \"param\":\"on\"} \r",token); }
#define XY_API_SND_ON(X,token,typ)  { sprintf(X," { \"msg_id\":2 , \"token\":%d , \"type\":%s , \"param\":\"on\" } \r",token,typ); }
#define XY_API_SND_OFF(X,token,typ)  { sprintf(X," { \"msg_id\":2 , \"token\":%d , \"type\":%s , \"param\":\"off\" } \r",token,typ); }
#define XY_API_LOG_START(X,token)  { sprintf(X,"{\"msg_id\":2,\"token\":%d, \"type\" \"save_log\", \"param\" \"on\"}\r",token); }
#define XY_API_SND_SET_FAST(token,typ,para) "{\"msg_id\": 2 ,\"token\":" #token ",\"type\":" #typ ",\"param\":" #para "}"
#define XY_API_LOG_START_FAST(token) "{\"msg_id\":2,\"type\":\"save_log\",\"param\":\"on\",\"" #token"\":1}"  // {"msg_id":2,"type":"save_log","param":"on","token":1} enable log files to look you tail -f /tmp/fuse_a/firmware.avtive.log

#define XY_SND_STRT_REC_CMD 513U                                                // sendStartRecord() '{"msg_id":513,"token":%s}' %token}
#define XY_API_START_REC(X,token)  { sprintf(X,"{\"msg_id\":513\"token\":%d}\r",token); }
#define XY_API_START_REC_FAST(token) "{\"msg_id\": 513 ,\"token\":" #token "}"    // makes the string

#define XY_SND_STOP_REC_CMD 514U                                                // sendStopRecord() "msg_id":514,"token":%
#define XY_API_STOP_REC(X,token)  { sprintf(X,"{\"msg_id\":514,\"token\":%d}\r",token); }
#define XY_API_STOP_REC_FAST(token) "{\"msg_id\": 514 ,\"token\":" #token "}"    // makes the string

#define XY_SND_RST_VF_CMD 259U                                                  // sendResetVF( none_force ) '{"msg_id":259,"token":%s,"param":"none_force"}'
#define XY_API_SND_RST_VF(X,token)  { sprintf(X,"{\"msg_id\":259,\"token\":%d,\"param\":\"none_force\"}\r",token); }
#define XY_API_SND_RST_VF_FAST(token) "{\"msg_id\": 259 ,\"token\":" #token "}"   // makes the string
#define XY_API_RESET_STREAM(X,token)   { sprintf(X,"{\"msg_id\":2,\"token\":%d, \"param\" \"vf_start\"}\r",token); }
#define XY_API_RESET_STREAM_FAST(token) "{\"msg_id\": 259 ,\"token\":" #token "\"type\": \"vf_start\"}"   // makes the string

#define XY_SND_STOP_VF_CMD 260U                                                 //  sendStopVF()  Stop the stream Syncro Life  "type vf_stop ?"
#define XY_API_STOP_VF(X,token)  { sprintf(X,"{\"msg_id\":260,\"token\":%d}\r",token); }  // msg id and token
#define XY_API_STOP_VF_FAST(token) "{\"msg_id\": 260 ,\"token\":" #token "}"   // makes the string
#define XY_API_STOP_STREAM(X,token) { sprintf(X,"{\"msg_id\":260,\"token\":%d , \"type\" : \"vf_stop\" }\r",token); }   // makes the string
#define XY_API_STOP_STREAM_FAST(token) "{\"msg_id\": 260 ,\"token\":" #token "\"type\": \"vf_stop\"}"   // makes the string

// Photo capture
// Request:
// {"msg_id":769,"token":1}
// Answers:
// { "msg_id": 7, "type": "start_photo_capture" ,"param":"precise quality;off"}
// { "msg_id": 7, "type": "photo_taken" ,"param":"/tmp/fuse_d/DCIM/100MEDIA/YDXJ0047.jpg"}
// photo will be stored on SD card as DCIM/100MEDIA/YDXJ0047.jpg

#define XY_SND_TK_PHO_CMD 769U                                                  // sendTakePhoto() {"msg_id":769,"token":%s}' %token}
#define XY_API_TK_PHO(X,token)  { sprintf(X,"{\"msg_id\":769,\"token\":%d}\r",token); }
#define XY_API_TK_PHO_FAST(token) "{\"msg_id\": 769 ,\"token\":" #token "}"    // makes the string

#define XY_SND_TK_PHOMD_CMD 0x1000004UL                                          // sendTakePhotoWithMode(String s) "param"
#define XY_API_SND_TK_PHOMD(X,token,para)  { sprintf(X,"{\"msg_id\":16777220,\"token\":%d, \"param\" %s}\r",token,para); }
#define XY_API_XY_SND_TK_PHOMD_FAST(token,para) "{\"msg_id\": 16777220 ,\"token\":" #token ",\"param\":" #para "}"   // makes the string

#define XY_SND_UBIND_BLUE_CMD 0x1000010UL                                        // sendUnbindBluetoothDevs()
#define XY_API_UBIND_BLUE(X,token)  { sprintf(X,"{\"msg_id\":16777232,\"token\":%d}\r",token); }
#define XY_API_UBIND_BLUE_FAST(token) "{\"msg_id\": 16777232 ,\"token\":" #token "}"    // makes the string

#define XY_SND_UPGRD_CMD  0x1000003UL                                            // sendUpgrade(String s) "param"
#define XY_API_SND_UPGRD(X,token,para)  { sprintf(X,"{\"msg_id\":16777219,\"token\":%d, "param" %s}\r",token,para); }
#define XY_API_SND_UPGRD_FAST(token,para) "{\"msg_id\": 16777219 ,\"token\":" #token "\"param\":" #para "}"   // makes the string

#define XY_SET_IPADDR_CMD 261U                                                  // setIPAddress() "param" dotted ip example "192.11.1.5"
#define XY_API_SET_IPADDR(X,para,token)  { sprintf(X,"{\"msg_id\":261, "param" %s, \"token\":%d}\r",para,token); }
#define XY_API_SET_IPADDR_FAST(token,para) "{\"msg_id\": 261, \"param\":" #para ",\"token\":" #token "}"   // makes the string

#define XY_UNKNOWN_CMD 1027U                                                    // param": "/tmp/fuse_d/DCIM/129MEDIA/YDXJ2007.jpg", "type": 100

#define XY_GET_LAST_PHO_CMD 16777236UL
#define XY_API_GET_LAST_PHO(X,token)  { sprintf(X,"{\"msg_id\":16777236,\"token\":%d}\r",token); }  // getLastPhoto() - return the last taken picture

#define XY_DUMP_FWARE 16777242UL
#define XY_API_DUMP_FWARE(X,token)  { sprintf(X,"{\"msg_id\":16777242,\"token\":%d, \"param\":'/DCIM/firmware.log' }\r",token); }  // dump firmware

// {"msg_id":3,"token":%s,"param":"%s"}
// {"msg_id":2,"type":"dev_reboot","param":"on"}

CAMPACKED(
typedef struct {

        int16_t token;                                                          // token value returned (this is the handle for further communication with the webserver)

        int16_t rval;                                                           // return value from the command
        
        int32_t msg_id;                                                         // the message id of the message it is replying to
        
        uint32_t size;                                                          // size returned from enquiry
        
        unsigned char resolution[10];                                           // resolution string returned

        unsigned char param_str[XY_MSG_MAX_LEN];                                // container to hold all the params returned as a string
        
        unsigned char type_str[XY_MSG_MAX_LEN];                                 // container to hold all the types returned as a string

        unsigned char video_std[5];                                             // video_standard NTSC or PAL
        
        unsigned char video_res[25];                                            // video resolution returned

        unsigned char photo_sz[15];                                             // photo size returned

        unsigned char burst_cap[15];                                            // burst capture size returned
        
        unsigned char md5sum[MD5_MAX_LEN];                                      // returned from md5sum tag in reply string

}) XY_reply_t;                                                                  // typical JSON reply container

CAMPACKED(
typedef struct {                                                                // Typicsl replies shown below
        unsigned char camera_clock[25];                                         // "2015-04-07 02:32:29"
        unsigned char video_standard[6];                                        // "NTSC"
        unsigned char app_status[15];                                           // "idle"
        unsigned char video_resolution[25];                                     // "1920x1080 60P 16:9"
        unsigned char video_stamp[11];                                          // "off,
        unsigned char video_quality[10];                                        // "S.Fine
        unsigned char timelapse_video[10];                                      // "off
        unsigned char capture_mode[25];                                         // "precise quality
        unsigned char photo_size[25];                                           // "16M (4608x3456 4:3
        unsigned char photo_stamp[11];                                          // "off
        unsigned char photo_quality[10];                                        // "S.Fine
        unsigned char timelapse_photo[5];                                       // "60
        unsigned char preview_status[5];                                        // "on
        unsigned char buzzer_volume[10];                                        // "mute
        unsigned char buzzer_ring[5];                                           // "off
        unsigned char capture_default_mode[25];                                 // "precise quality
        unsigned char precise_cont_time[10];                                    // "60.0 sec
        unsigned char burst_capture_number[15];                                 // "7 p / s
        unsigned char restore_factory_settings[5];                              // "on
        unsigned char led_mode[20];                                             // "all enable
        unsigned char dev_reboot[5];                                            // "on
        unsigned char meter_mode[10];                                           // "center
        unsigned char sd_card_status[10];                                       // "insert
        unsigned char video_output_dev_type[10];                                // "tv
        unsigned char sw_version[60];                                           // "YDXJv22_1.0.7_build-20150330113749_b690_i446_s699
        unsigned char hw_version[32];                                           // "YDXJ_v22
        unsigned char dual_stream_status[10];                                   // "on
        unsigned char streaming_status[10];                                     // "off
        unsigned char precise_cont_capturing[10];                               // "off
        unsigned char piv_enable[10];                                           // "off
        unsigned char auto_low_light[10];                                       // "on
        unsigned char loop_record[10];                                          // "off
        unsigned char warp_enable[10];                                          // "off
        unsigned char support_auto_low_light[10];                               // "on
        unsigned char precise_selftime[10];                                     // "5s
        unsigned char precise_self_running[10];                                 // "off
        unsigned char auto_power_off[10];                                       // "5 minutes
        unsigned char serial_number[10];                                        // "xxxxx
        unsigned char system_mode[15];                                          // "capture
        unsigned char system_default_mode[15];                                  //  "capture
        unsigned char start_wifi_while_booted[15];                              // "off
        unsigned char quick_record_time[10];                                    // "0
        unsigned char precise_self_remain_time[10];                             // "0
        unsigned char sdcard_need_format[10];                                   // "no-need
        unsigned char video_rotate[10];                                         // "off
}) XY_config_t;                                                                 // Config JSON reply container, This is a readback of configuration readback from param field in response to {"msg_id":3, "token":1}

// defines a word showing the JSON parser message tags found in the reply or the msb to show an error or zero (nothing found)
#define _XY_TOKEN (1U<<0U)
#define _XY_RVAL (1U<<1U)
#define _XY_MSG_ID (1U<<2U)
#define _XY_PARAM (1U<<3U)
#define _XY_TYPE (1U<<4U)
#define  _XY_VIDEO_STD (1U<<5U)
#define _XY_VIDEO_RES (1U<<6U)
#define _XY_PHOTO_SZ (1U<<7U)
#define _XY_BURST_CAP (1U<<8U)
#define _XY_UNEXPECT (1U<<9U)
#define _XY_MD5SUM (1U<<10U)
#define _XY_SIZE (1U<<11U)
#define _XY_RESOL (1U<<12U)
#define _XY_ERROR (1U<<15U)

CAMPACKED(
typedef struct  {

        uint16_t f_token  : 1;                                                  // found token
        uint16_t f_rval  : 1;                                                   // found rval
        uint16_t f_msg_id  : 1;                                                 // found msg id
        uint16_t f_param  : 1;                                                  // found param
        uint16_t f_type  : 1;                                                   // found type
        uint16_t f_vid_std  : 1;                                                // found video standard
        uint16_t f_vid_res  : 1;                                                // found video resolution
        uint16_t f_photo_sz : 1;                                                // found photo size
        uint16_t f_burst_cap  : 1;                                              // found burst cap
        uint16_t f_unex  : 1;                                                   // found unexpected key
        uint16_t f_md5sum : 1;                                                  // found a md5sum key
        uint16_t f_size : 1;                                                    // found a size key
        uint16_t f_resol : 1;                                                   // found a resolution key
        uint16_t spare : 2;                                                     // unused bits
        uint16_t f_error  : 1;                                                  // error occurred

}) XY_JSparse_t;                                                                // Struct to the status of the json parse

CAMPACKED(
typedef struct  {

        uint16_t CableConnection  : 1;                                            // Cable is connected
        uint16_t VidOrPho  : 1;                                                  // video mode = 0 or photo mode = 1
        uint16_t SDCardStatus : 1;                                               // status of the SD Card
        uint16_t spare : 4;

        uint16_t Battpercent;                                                    // percent remaining in battery

        // uint32_t SDcardSpace;                                                   // SD Card space remaining

        uint16_t Token;                                                          // Token
        
        uint32_t size;                                                          // file size

        int16_t rval;                                                            // function return code
        
        uint32_t free_space;                                                    // SD Card free space bytes
        
        uint32_t total_space;                                                   // SD Card total space bytes
        
        unsigned char LastPicName[60];                                          // full path and last picture name
        
        unsigned char md5sum[MD5_MAX_LEN];                                      // the mds info for the given file
        
        unsigned char resolution[10];                                           // resolution string returned

}) COMGS_YiDig_t;                                                               // Struct to hold the information read back

CAMPACKED(
typedef struct  {

   uint16_t VideoRes : 4;                                                        // 4 bit selection for xy_video_res
   uint16_t PhotoSz : 3;                                                         // 2 bit selection for xy_photo_sz
   uint16_t OnOffState : 1;                                                      // On or Off for the On_Off Type Selection
   uint16_t TimeLapOpt : 4;                                                      // time lapse option
   uint16_t TimeLapDuration : 4;                                                 // Time Lapse duration
   uint16_t SlowMotion : 4;                                                      // Slow motion option
   uint16_t BurstCap : 4;                                                        // Burst Capture setting
   uint16_t PreContTim : 4;                                                      // Precise cont time setting
   uint16_t RecPhoTim : 4;                                                       // Record or photo time
   uint16_t LoopRecDur : 4;                                                      // Loop record duration
   uint16_t VidPhoQ : 4;                                                         // Video photo quality
   uint16_t VidPhoStamp : 4;                                                     // Video photo stamp
   uint16_t VideoStd: 3;                                                         // Video Standard
   uint16_t OnOffTypSel : 8;                                                     // On_Off Type Selection
   uint16_t VidOutDev : 4;                                                       // Video output device type selection
   uint16_t MeterOpt : 4;                                                        // meter options
   uint16_t LedMode : 4;                                                         // led mode
   uint16_t BuzzVol : 4;                                                         // buzzer volume
   uint16_t CapMode : 4;                                                         // capture modes
   uint16_t spare : 4;

}) COMGS_YiOptSelect_t;                                                         // Struct to hold the various options for YiCam configurations