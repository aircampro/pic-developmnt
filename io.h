// io.h - Define the input and output
//
#ifndef  __io__

#define  __io__

// Set up the IO PORTS for analogue input (AIN) digital input (DIN) and LED output (DOT)

#define DINPORT1 PORTA                                                          // We will assign PORTA to DIN
#define AINPORT1 PORTB                                                          // We will assing PORTB to the AIN
#define DOTPORT1 PORTC                                                          // We will assign PORTC to the 1st DOT bank
#define DOTPORT2 PORTD                                                          // We Wiill assign PORTD to 2nd DOT bank
#define IOCONFIG 0xFF0F                                                         // Banks 1,3,4 digital bank 2 analogue

#define DINTRIS1 TRISA                                                          // We will assign TRISA to DIN
#define AINTRIS1 TRISB                                                          // We will assing TRISB to the AIN
#define DOTTRIS1 TRISC                                                          // We will assign TRISC to the 1st DOT bank
#define DOTTRIS2 TRISD                                                          // We Wiill assign TRISD to 2nd DOT bank

// Define the digital input switches
#define di1SpeedAngle PORTA.B0                                                  // Flag TRUE=Speed  FALSE=Angle
#define di2PosHalt PORTA.B1                                                     // Flag command FALSE=active or TRUE=HALT
#define di3Reset PORTA.B2                                                       // Issue a Reset to the device.
#define di4RCMode PORTA.B3                                                      // Select RC Mode (for speed)
#define di5VCMode PORTA.B4                                                      // Select VC Mode  (for speed).
#define di6FastSelect PORTA.B5                                                  // Select Fast mode for ANGLE or SPEED.

// State Engine Reported to the DOT LED on DOTPORT1
#define JoyReset 0xE1                                                           // AND to Set bit 1
#define JoyAngle 0xE2                                                           // AND to set bit 2
#define JoySpeed 0xE4                                                           // AND to set bit 3
#define JoyOff 0xE8                                                             // AND to set bit 4
#define JoySend 0xF0                                                            // AND to set bit 5
#define GSCMotorSend 32
#define GSCPIDAutotune 64
#define JoyClear 0xE0                                                           // AND to Reset the bits 1-5 of the Status word (Joystick)

// State Engine poll requests for data DOT LED on DOTPORT2
#define GetExtAngles 0x2                                                        // get angles LED DOTPORT2 0
#define GetAngles 0x1                                                           // get angles ext LED DOTPORT2 1
#define GetRealtime4 0x4                                                        // get realtime data 4 LED DOTPORT2 2
#define Clear3LED 0xF8                                                          // clear the 1st 3 LED's on DOTPORT2
#define GSCMenuSend 0x8                                                         // Menu Command exceute LED
#define GSCAdjvar 0x10                                                          // Adjust variables execute LED
#define GSCGetvar 0x20                                                          // Get variables request LED
#define GSCImu 0x40                                                             // Select IMU request LED
#define GSCProSet 0x80                                                          // Select Profile and load / save / erase it LED
#define GSCConSet 0x80                                                          // Share the same LED as above we ran out of spare

#define BTN_STATE_RELEASED 0                                                    // Push button States
#define BTN_STATE_PRESSED 1
#define BTN_BOUNCE_THRESHOLD_MS 3                                               // interval for button de-bouncer threshold (in multiples of 100ms)

#define IO_AD_ENABLE (1<<15)                                                    // bit to set in AD1CON1 for enable
#define IO_SIDL_ENABLE (1<<13)                                                  // enable stop on IDLE mode for the ADC
#define IO_FORM_SF16 (3<<8)
#define IO_FORM_F16 (2<<8)
#define IO_FORM_SI16 (1<<8)
#define IO_FORM_I16 0
#define IO_FORM_SF32 (7<<8)
#define IO_FORM_F32 (6<<8)
#define IO_FORM_SI32 (5<<8)
#define IO_FORM_I32 (4<<8)
#define IO_FORM_CLRASAM (1<<4)
#define IO_FORM_ASAM (1<<2)
#define IO_FORM_SAMP (1<<1)

#define IO_CH0SB_15 (0b1111<<24)                                                // bits to set in AD1CHS for enable positive input select MUXB
#define IO_CH0SB_14 (0b1110<<24)                                                // bits to set in AD1CHS for enable
#define IO_CH0SB_13 (0b1101<<24)                                                // bits to set in AD1CHS for enable
#define IO_CH0SB_12 (0b1100<<24)                                                // bits to set in AD1CHS for enable
#define IO_CH0SB_11 (0b1011<<24)                                                // bits to set in AD1CHS for enable
#define IO_CH0SB_10 (0b1010<<24)                                                // bits to set in AD1CHS for enable
#define IO_CH0SB_9 (0b1001<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_8 (0b1000<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_7 (0b0111<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_6 (0b0110<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_5 (0b0101<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_4 (0b0100<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_3 (0b0011<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_2 (0b0010<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_1 (0b1001<<24)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SB_0 0                                                            // bits to set in AD1CHS for enable

#define IO_CH0SA_15 (0b1111<<16)                                                // bits to set in AD1CHS for enable positive input select MUXA
#define IO_CH0SA_14 (0b1110<<16)                                                // bits to set in AD1CHS for enable
#define IO_CH0SA_13 (0b1101<<16)                                                // bits to set in AD1CHS for enable
#define IO_CH0SA_12 (0b1100<<16)                                                // bits to set in AD1CHS for enable
#define IO_CH0SA_11 (0b1011<<16)                                                // bits to set in AD1CHS for enable
#define IO_CH0SA_10 (0b1010<<16)                                                // bits to set in AD1CHS for enable
#define IO_CH0SA_9 (0b1001<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_8 (0b1000<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_7 (0b0111<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_6 (0b0110<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_5 (0b0101<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_4 (0b0100<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_3 (0b0011<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_2 (0b0010<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_1 (0b1001<<16)                                                 // bits to set in AD1CHS for enable
#define IO_CH0SA_0 0                                                            // bits to set in AD1CHS for enable

// Choose Oscillator
#define IO_FRC_OSC 0                                                            // internal fast RC oscillator
#define IO_FRCPLL_OSC 0b1                                                       // FRC PLL OSC
#define IO_PO_OSC 0b01                                                          // PO OSC
#define IO_POPLL_OSC 0b11                                                       // PO PLL OSC
#define IO_SOSC_OSC 0b100                                                       // SO OSC
#define IO_LPRC_OSC 0b101                                                       // LPRC OSC
#define IO_FRC16_OSC 0b110                                                      // FRC div by 16 OSC
#define IO_FRCDIV_OSC 0b111                                                     // FRC div by FRCDIV bits

#define RAW_MAX 1024                                                            // 10 bit ADC maxiumum raw counts

typedef struct {

        uint8_t  state;                                                         // current state

        uint8_t trigger_state;                                                  // de-bounced state
        
        uint64_t last_time_ms;                                                  // Time out of the latch

} SBGC_btn_state_t;                                                             // This is a debounced button type for a DI key press

uint8_t debounce_button( SBGC_btn_state_t *btn, uint8_t new_state );
extern void calculateTimeDiff( uint64_t TimeDuration, uint64_t TimeDurationLast );

// De-bounce button: it should keep its state for a given period of time (30ms)
// Returns 1 if btn.trigger_state is changed.

uint8_t debounce_button( SBGC_btn_state_t *btn, uint8_t new_state )
{

   uint16_t cur_time_ms = g_timer5_counter;                                     // Take the global tick counter and store (interrupt timer 5)
   uint16_t diff_time;

   if ((cur_time_ms - btn->last_time_ms) < 0)                                   // We rolled over
   {
       diff_time = (cur_time_ms - btn->last_time_ms) + UINT_LEAST64_MAX;        // Correct the diff by adding Uint_MAX
   }
    else
    {
       diff_time = (cur_time_ms - btn->last_time_ms);
    }
    if(new_state != btn->state)                                                 // The I/O State is different from last known
    {
        btn->state = new_state;                                                 // Set the new button state
        btn->last_time_ms = cur_time_ms;                                        // Remeber last tick counter
    }
    else if (btn->trigger_state != btn->state && (uint16_t)(diff_time) > BTN_BOUNCE_THRESHOLD_MS)
    {
         btn->trigger_state = btn->state;                                       // We persisted in this state for more than 30 ticks set the trigger
         return 1;
    }
    return 0;
}

//
//  Simple function to give a time difference between two tick counters
//  TimeDuration set to -ve means initialise
//
//
void calculateTimeDiff( int64_t TimeDuration, uint64_t TimeDurationLast )
{
  if (TimeDuration<=-1)                                                         // Value to initialise
  {
    TimeDurationLast=g_timer5_counter;                                          // Remember the current counter value
    TimeDuration==0;                                                            // first time differnce after init is zero
  }
  else
  {
    if ( TimeDurationLast > g_timer5_counter )                                  // Overrun
    {
       TimeDuration = (UINT64_MAX - TimeDurationLast) + g_timer5_counter;       // Calculate difference in time
    }
    else
    {
       TimeDuration = (g_timer5_counter - TimeDurationLast);                    // Calculate difference in time
    }
    TimeDurationLast = g_timer5_counter;                                        // Remember the last one
  }
}

#endif