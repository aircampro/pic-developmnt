#ifndef Pid_h
#define Pid_h

#include "stdint.h"                                                             // Interger type defines
#include "definitions.h"
//#include "io.h"

typedef uint32_t time_t;

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define PIDPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define PIDPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

#define PID_NONE    0U
#define PID_POSITION  1U
#define PID_SPEED    2U

//#define INT32_MAX (long)0x7fffffff
//#define INT32_MIN (long)0x80000000

#define PID_TUNE_STATE_IDLE   0U                                                /* PID tuning state machine */
#define PID_TUNE_STATE_START  1U
#define PID_TUNE_STATE_WAIT   2U
#define PID_TUNE_STATE_LOOP   3U
#define PID_TUNE_STATE_END    4U

// auto tune methods  (they are specified in the tuner object as parameters to znMode attribute)
#define ZNModeBasicPID 1U
#define ZNModeLessOvershoot 2U
#define ZNModeNoOvershoot 3U

PIDPACKED(
typedef struct {

      uint8_t mode_man : 1;                                                     // bit for manual mode
      uint8_t mode_rem : 1;                                                     // remote setpoint mode
      uint8_t mode_trk : 1;                                                     // track mode
      uint8_t mode_hld : 1;                                                     // hold mode
      uint8_t mode_init : 1;                                                    // mode initialise
      uint8_t mode_bmp : 1;                                                     // remote mode bumpless
      uint8_t mode_began : 1;                                                   // first run of the PID (time initialise)
      uint8_t mode_spare : 1;                                                   // spare bits
      
      int32_t setpoint;                                                         // local setpoint
      
      int32_t rem_setpoint;                                                     // remote setpoint

      int32_t feedback;                                                         // feedback (measurement)

      float32_t kp;                                                             // proportional band

      float32_t ki;                                                             // gain

      float32_t kd;                                                             // derivative

      int32_t lastError;                                                        // last error

      uint64_t lasttime;                                                        // time time it ran

      uint32_t output;                                                          // calculated pid output
      
      uint32_t initVal;                                                         // feedback for output to start at when init flag is set on

}) pidBlock_t;

PIDPACKED(
typedef struct {

     uint8_t init : 1;                                                          // init = false, init done = true
     uint8_t znMode : 7;                                                        // tuner object mode
     
     uint8_t cycles;                                                            // number of iterative cycles to do
     
     float32_t targetInputValue;                                                // target value

}) pidTuner_t;

    //float32_t kp;
    //float32_t ki;
    //float32_t kd;
    uint8_t PIDtype;
    int16_t scale;
    int32_t integral;
    int32_t lastError;
    int32_t integralMax;
    int32_t integralMin;
    int32_t iTermMax;
    int32_t iTermMin;
    int32_t pwmPeriod;
    time_t sampleLastTime;
    int8_t lastDir;
    int32_t lastWidth;

    /* tuning parameter */
    uint8_t tuneState;
    int16_t tuneInput;
    int16_t lastTuneInput;
    uint16_t tuneBaseInput;
    uint16_t tuneStep;
    int32_t tuneSetpoint;
    int32_t tuneLastFeedback;
    int8_t tuneDirChange;                                                       //0: initial, 1: go up, -1 go down
    int32_t tuneMaxSum;
    int32_t tuneMinSum;
    uint8_t tuneMaxCount;
    uint8_t tuneMinCount;
    uint8_t tuneNoiseband;
    time_t tuneStartTime;
    time_t tuneEndTime;
    time_t tuneWaitTime;

//    uint32_t compute(uint32_t setpoint, uint32_t feedback);
    uint8_t computePID( pidBlock_t *pidObj );                                   // the PID routine
    
    //DcMotorPID(uint8_t sid, uint8_t port);
    //void setGain(float pGain, float iGain, float dGain);
    //void setPeriod(long period);
    //void setScaleFactor(float scaleFactor);
    //void setIntegralLimit(long min, long max);
    //void setIntegralTermLimit(long min, long max);
    //long getSpeed(void);
    //float getKp(void);
    //float getKi(void);
    //float getKd(void);

    //void begin(uint8_t pidType);
    //void beginTune(void);
    //bool loopTune(void);
    //long loop(long setpoint);

#endif