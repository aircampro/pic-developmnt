// SMPTE interface library.

#ifndef SMPTE_LIB
#define SMPTE_LIB

// via udp message http://www.orangepi-dmx.org/orange-pi-smpte-timecode-ltc-reader-converter
//
#define SMPTE_UDP_PORT 0x5443U                                                  // UDP Port for SMPTE

#define SMPTE_START(A) { strcpy(A,"ltc!start"); }                               // Starts the TimeCode increments from start_*  (see configuration)
#define SMPTE_STOP(A) { strcpy(A,"ltc!stop"); }                                 // Stops the TimeCode increments. The TimeCode is still outputting.
#define SMPTE_RESUME(A) { strcpy(A,"ltc!resume"); }                             // Continue the TimeCode increments from the 'stop' command.

#define SMPTE_TIMECODE_START(A,B,C,D,E) { if (((((B>=0U) && (B<=24U)) && ((C>=0) && (C<=59U))) && ((D>=0U) && (D<=59U))) && ((E>=0U) && (E<=29U))) \
                                         sprintf(A,"ltc!start#%d:%d:%d.%d\r\n",B,C,D,E }; }    // Sets the start TimeCode.

#define SMPTE_TIMECODE_STOP(A,B,C,D,E) { if (((((B>=0U) && (B<=24U)) && ((C>=0) && (C<=59U))) && ((D>=0U) && (D<=59U))) && ((E>=0U) && (E<=29U))) \
                                         sprintf(A,"ltc!stop#%d:%d:%d.%d\r\n",B,C,D,E }; }    // Sets the stop TimeCode.

#define SMPTE_RATE(A,B) {  if(((B>=24U) && (B<=25U)) || ((B>=29U) && (B<=30U)))  \
                           sprintf(A, "ltc!rate#%d\r\n"); }                     //  Sets the rate of the TimeCode. Valid values: 24, 25, 29 and 30.

#define SMPTE_TIMECODE_RUNNING(A,B,C,D,E) { if (((((B>=0U) && (B<=24U)) && ((C>=0) && (C<=59U))) && ((D>=0U) && (D<=59U))) && ((E>=0U) && (E<=29U)))  \
                                         sprintf(A,"ltc!start!%d:%d:%d.%d\r\n",B,C,D,E }; }    // Sets the running TimeCode.

#define SMPTE_TIMECODE_JUMP(A,B,C,D,E) { if (((((B>=0U) && (B<=24U)) && ((C>=0) && (C<=59U))) && ((D>=0U) && (D<=59U))) && ((E>=0U) && (E<=29U)))  \
                                         sprintf(A,"ltc!start@%d:%d:%d.%d\r\n",B,C,D,E }; }    // Set and jump to the start TimeCode, but stop the TimeCode incrementing/decrementing

#define SMPTE_DIRECTION(A) { sprintf(A,"ltc!direction%d\r\n"); }                // Sets the direction. Valid values: forward and backward

#define SMPTE_PITCH {  if ((B>=100) && (B<=100))                               \
                           sprintf(A, "ltc!pitch#%d\r\n"); }                    // Sets the pitch. Valid values are -100 <= ppp >= 100 (integers).

// MIDI Time Code (MTC)
// A supplement to the MIDI specification defines a standard whereby the timing information
// in hours, minutes, seconds, and, frames) found in SMPTE timecode can carried by a MIDI connection. 
// In this way, a suitably equipped MIDI sequencer may be synchronised to an absolute timing reference. 
// This standard is called MIDI Time Code or MTC.
#define MIDI_STX 0xF0U                                                          // start transmission char
#define MIDI_ETX 0xF7U                                                          // end transmission char
#define MIDI_MANUF_ID 0x7FU                                                     // manufacturer is set to real time universal
#define MIDI_CHAN 0x7FU                                                         // channel is set to global braodcast
#define MIDI_CMD_MTC 0x01U                                                      // ccommand for MTC

#define MIDI_MTC_BYTE1_24 0U                                                    // 24 fps
#define MIDI_MTC_BYTE1_25 1U                                                    // 25 fps
#define MIDI_MTC_BYTE1_29 2U                                                    // 29 fps
#define MIDI_MTC_BYTE1_30 3U                                                    // 30 fps
typedef struct {                                                                // a value of 1 means active 0 inactive 2 unknown
      uint8_t stx;
      uint8_t manu;
      uint8_t chan;
      uint8_t cmd;
      uint8_t byte1;
      uint8_t hrs;
      uint8_t mins;
      uint8_t secs;
      uint8_t frame;
      uint8_t etx;
} SMPTE_midi_mtc_t;
// Message is fixed as below you need to set the smpteMTCMsg.hrs etc.....
SMPTE_midi_mtc_t smpteMTCMsg = { MIDI_STX, MIDI_MANUF_ID, MIDI_CHAN, MIDI_CMD_MTC, MIDI_MTC_BYTE1_30, 0U, 0U, 0U, 0U, MIDI_ETX };

#endif