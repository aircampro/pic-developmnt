// ComGS Commander Ground Station Definitions and structures
//
// Defines the structures used between ComGs (data display) and Gimbal / Camera
// Also provides a library of functions which do calculations and conversions manipulation on this data
//
//
#ifndef __comgs_h

#define __comgs_h

#include "definitions.h"
#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define COMGSPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define COMGSPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

extern struct COMGS_cmd_get_angles_t commangles;
extern struct XS_Hmi_Slider_t hmiTimeCalcTotal;                                 // Calculated from the Time Entered on the HMI
extern struct XS_Hmi_Slider_t hmiDateCalcTotal;                                 // Calculated from the Date Entered on the HMI
extern struct XS_Hmi_Slider_t  hmiRawContrast[4];                               // encoder Contrast request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawHue[4];                                    // encoder Hue request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawSaturation[4];                             // encoder Saturation request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawClipsz[4];                                 // encoder Clip size request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawCliplen[4];                                // encoder Clip length request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawBright[4];                                 // encoder Brightness request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawFramert[4];                                // encoder Frame Rate request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawFramescl[4];                               // encoder Frame Scale request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawFrameint[4];                               // encoder Frame Interval request state (per channel)
extern struct XS_Hmi_Slider_t  hmiRawBitrate[4];                                // encoder Bit rate request state

// #define USE_CAL_UNLOCK                                                       uncomment to : Unlock calibration and allow other parallel action during calibration of joystick

#define MAX_ZERO_ACCS 25                                                        // zero velocity if we exceed this many accelerometer readings at zero

// defines for AHRS quarternion functions
#define deltat 0.001f                                                           // sampling period in seconds (shown as 1 ms)
#define gyroMeasError 3.14159265358979f * (5.0f / 180.0f)                       // gyroscope measurement error in rad/s (shown as 5 deg/s)
#define beta sqrt(3.0f / 4.0f) * gyroMeasError                                  // compute beta

#define gyroMeasDrift 3.14159265358979 * (0.2f / 180.0f)                        // gyroscope measurement error in rad/s/s (shown as 0.2f deg/s/s)
#define beta sqrt(3.0f / 4.0f) * gyroMeasError                                  // compute beta
#define zeta sqrt(3.0f / 4.0f) * gyroMeasDrift                                  // compute zeta

#define white_space(c) ((c) == ' ' || (c) == '\t')                              // Check for whitespace
#define valid_digit2(c) ((c) >= '0' && (c) <= '9')                              // Check for valid digits

// COMGS_quarternion quartCoOrd;                                                // quarternion co-ordinates
// for calibrate
#include "io.h"

#include "pid.h"                                                                // This is for flight controller robot helper
#include "robot.h"                                                              // This is for robot helper functions

// includes for Mahony
#include "MahonyAHRS.h"
#include <math.h>

// for alarms
#include "AlarmDefinitions.h"

// includes for madgwick
#include "Madgwick.h"

//---------------------------------------------------------------------------------------------------
// Definitions for Mahony

// #define sampleFreq 512.0f                                                    // sample frequency in Hz (in our case we calculate this from the iteration)
#define twoKpDef (2.0f * 0.5f)                                                  // 2 * proportional gain
#define twoKiDef (2.0f * 0.0f)                                                  // 2 * integral gain

//---------------------------------------------------------------------------------------------------
// Definitions for Madgewick

// #define sampleFreq 512.0f                                                       // sample frequency in Hz (in our case we calculate this from the iteration)
#define betaDef 0.1f                                                            // 2 * proportional gain

//---------------------------------------------------------------------------------------------------
// variable declarations for Mahony algorythm (global volatiles)

volatile float32_t twoKp = twoKpDef;                                            // 2 * proportional gain (Kp)
volatile float32_t twoKi = twoKiDef;                                            // 2 * integral gain (Ki)
volatile float32_t q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;                  // quaternion of sensor frame relative to auxiliary frame
volatile float32_t integralFBx = 0.0f,  integralFBy = 0.0f, integralFBz = 0.0f; // integral error terms scaled by Ki

//---------------------------------------------------------------------------------------------------
// Variable definitions for Madgwick (global)

volatile float32_t betaMadg = betaDef;                                                // 2 * proportional gain (Kp)
// volatile float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;                        ignore same as mahony quaternion of sensor frame relative to auxiliary frame

// The data from a Get_Angles readback SBGC message.
COMGSPACKED(
typedef struct {

        float imu_angle_roll;

        float target_angle_roll;

        float target_speed_roll;

        float imu_angle_pitch;

        float target_angle_pitch;

        float target_speed_pitch;

        float imu_angle_yaw;

        float target_angle_yaw;

        float target_speed_yaw;
        
        float stator_rotor_ang_yaw;

        float stator_rotor_ang_roll;

        float stator_rotor_ang_pitch;

}) COMGS_cmd_get_angles_t;

COMGSPACKED(
typedef struct {

        int32_t gyro_roll;                                                      // raw data from gyro

        int32_t acc_roll;                                                       // raw data from accelerometer

        int32_t gyro_pitch;

        int32_t acc_pitch;

        int32_t gyro_yaw;

        int32_t acc_yaw;
        
        int32_t mavg_acc_roll;                                                    // moving averages
        
        int32_t mavg_acc_pitch;
        
        int32_t mavg_acc_yaw;
        
        int32_t mavg_acc_rolls[2];                                              // last known averages first and second
        
        int32_t mavg_acc_pitchs[2];                                             // last known averages first and second
        
        int32_t mavg_acc_yaws[2];                                               // last known averages first and second

        uint8_t g_count_sam;                                                    // number of samples taken
        
        uint8_t g_count_acc;                                                    // first or second sample chosen
        
        uint8_t zero_acc_cnt_roll;                                              // count the number of zero accelerations
        
        uint8_t zero_acc_cnt_pitch;                                             // count the number of zero accelerations
        
        uint8_t zero_acc_cnt_yaw;                                               // count the number of zero accelerations
        
        int32_t vel_roll[2];                                                    // calculated velocity
        
        int32_t pos_roll[2];                                                    // calculated position
        
        int32_t vel_pitch[2];                                                   // calculated velocity

        int32_t pos_pitch[2];                                                   // calculated position
        
        int32_t vel_yaw[2];                                                     // calculated velocity

        int32_t pos_yaw[2];                                                     // calculated position
        
        float32_t theta_yaw;
        
        float32_t theta_roll;
        
        float32_t theta_pitch;

}) COMGS_gyro_acc_data_t;

COMGSPACKED(
typedef struct {

        float IMU_ANGLE[3];

        float FRAME_IMU_ANGLE[3];

        float TARGET_ANGLE[3];

        float BAT_LEVEL;

        uint8_t MOTORS_STATE;

        uint8_t CUR_IMU;
        
        uint8_t CUR_PROFILE;

}) COMGS_real_data_3_t;

COMGSPACKED(
typedef struct {

      float imu_temp;
      
      float frame_imu_temp;
      
}) COMGS_temp_t;

COMGSPACKED(
typedef struct {

      float mag_data_roll;

      float mag_data_pitch;
      
      float mag_data_yaw;

}) COMGS_magnet_t;

COMGSPACKED(
typedef struct {

      float motor_power_roll;

      float motor_power_pitch;

      float motor_power_yaw;
      
      float CURRENT;

}) COMGS_motor_power_t;

COMGSPACKED(
typedef struct {

      float v1;                                                                 // Configurable data got with a CMD_GET_ADJ_VARS_VAL command
      
      float v2;
      
      float v3;
      
      float v4;
      
      float v5;
      
      float v6;
      
      float v7;
      
}) COMGS_get_vars;

COMGSPACKED(
typedef struct {

      uint8_t AlarmWdCurrent;                                                  // Current Data associated with an ERROR Alarm

      uint8_t AlarmWdAck;                                                      // Acknowledgement state of the Alarm Word

      uint8_t AlarmWdState;                                                    // Alarm Status word to ComGs

      uint8_t AlarmWdAcked;                                                    // Re-trig for Ack (Ack button removed after timer T) DON (T) of ACK
      
      uint8_t AlarmWdAckLastScan;                                               // Last Scan of Alarm ACK
      
      uint16_t AlarmTmRun : 1;                                                  // Turn the timer on/off
      
      uint16_t AlarmTm : 15;                                                    // Timer Tick for DON

}) COMGS_error_alarm;

// -------- HMI Command numbers ----------
//
#define COMGS_OFF_NORMAL 4U                                                      // The possible states of the command  for motor from the commGS
#define COMGS_ON 1U
#define COMGS_OFF_SAFE 3U
#define COMGS_OFF_BREAK 2U
#define SEND_MOTOR_OFF 5U
#define SEND_MOTOR_ON 6U

#define COMGS_PID_STOP 0U                                                        // The possible states of the command for the PID tune frm commGS
#define COMGS_PID_ROLL 1U
#define COMGS_PID_PITCH 2U
#define COMGS_PID_YAW 4U
#define COMGS_PID_GUI 8U
#define COMGS_PID_KEEP 16U
#define COMGS_PID_LPF 32U
#define COMGS_PID2_ACTION_START 40U
#define COMGS_PID2_ACTION_START_SAVE 41U
#define COMGS_PID2_ACTION_SAVE 42U
#define COMGS_PID2_ACTION_STOP 43U
#define COMGS_PID2_ACTION_READ 44U

#define COMGS_PROFILE3 1U                                                       // define the request values for the menu_execute remote word from Com GS GUI
#define COMGS_CALIB_ACC 2U
#define COMGS_CALIB_GYRO 3U
#define COMGS_MOTOR_TOGGLE 4U
#define COMGS_HOME_POS 5U
#define COMGS_CAM_EV 6U
#define COMGS_PH_EV 7U
#define COMGS_CALIB_MAG 8U

#define COMGS_PROFILE_SET_ACTION_SAVE 1U                                        // define the request values for the profile_set remote word from Com GS GUI
#define COMGS_PROFILE_SET_ACTION_CLEAR 2U
#define COMGS_PROFILE_SET_ACTION_LOAD 3U

#define COMGS_EXPO_RATE_MODE_RC 1U                                              // define the request values for the configure control remote word from Com GS GUI
#define COMGS_ANGLE_LPF 2U
#define COMGS_SPEED_LPF 3U
#define COMGS_RC_LPF 4U

// Command Adj_var_val
#define COMGS_SPEED100 1U                                                       // Set Roll Pitch and Yaw speeds to 100%
#define COMGS_FOLLOW60 2U                                                       // Set Roll Pitch and Yaw follow speed to 60%
#define COMGS_PIDGAIN 3U                                                        // Set all PID Gain to value
                                                                                // Command IMU
#define COMGS_IMU_MAIN 1U                                                       // Request Main IMU
#define COMGS_GET7_1 1U                                                         // get the 1st set of 7 values
#define COMGS_IMU_FRAME 2U                                                      // Request Frame IMU

#define COMGS_ALARM_ACK_TIME 200U                                               // Time for alarm ACK to be active

#define CMD_NOCMD 0U                                                            // No Command Active (test initial state)
#define CMD_SEND 1U                                                             // States of each of the SBGC send requests from ComGS.
#define CMD_SENT 2U                                                             // We sent a UDP frame with the simpleBGC request
#define CMD_CONFIRMED 3U                                                        // We got a confirm for the message sent from the remote device
#define CMD_COMPLETE 4U                                                         // We completed the message
#define CMD_RESEND 5U                                                           // We re-triggered the send due to timeout on recieving the confirm message back
#define CMD_SENT_2 6U                                                           // poll period is being set after event registration

#define COMGS_RUN_NEW_SCRIPT 1U                                                 // Request from COM GS to Write to EEPROM slot 0 a new script and run it
#define COMGS_RUN_A_SCRIPT 2U                                                   // Request from COM GS to run new script @ slot 0
#define COMGS_STOP_SCRIPT 3U                                                    // Request from COM GS to stop new script @ slot 0

// State engine for scripting on gimbal host
#define CMD_SSEND 1U                                                            // send a script
#define CMD_WSEND 2U                                                            // write to eeprom
#define CMD_SSENT 3U                                                            // sent a script start wait for confirm
#define CMD_WSENT 4U                                                            // written to eeprom wait for confirm
#define CMD_WRITTEN 5U                                                          // eeprom write confirmation
#define CMD_WCONFIRMED 6U                                                       // script start confirmed
#define CMD_WCOMPLETE 7U                                                        // command completed
#define CMD_SSTOP 8U                                                            // send a stop to the gimbal to end script execution

// valid values for the event logger set-up
#define COMGS_CMD_REALTIME_DATA_3 1U
#define COMGS_CMD_REALTIME_DATA_4 2U
#define COMGS_CMD_REALTIME_DATA_CUSTOM 3U
#define COMGS_CMD_AHRS_HELPER 4U
#define COMGS_CMD_EVENT 5U

#define COMGS_EVENT_ID_MENU_BUTTON_OFF 1U
#define COMGS_EVENT_ID_MENU_BUTTON_ON 2U
#define COMGS_EVENT_ID_MENU_BUTTON_HOLD 3U
#define COMGS_EVENT_ID_MOTOR_STATE_OFF 4U
#define COMGS_EVENT_ID_MOTOR_STATE_ON 5U
#define COMGS_EVENT_ID_EMERGENCY_STOP_OFF 6U
#define COMGS_EVENT_ID_EMERGENCY_STOP_ON 7U
#define COMGS_EVENT_ID_CAMERA_REC_PHO 8U
#define COMGS_EVENT_ID_CAMERA_PHO 9U
#define COMGS_EVENT_ID_SCRIPT_OFF 10U
#define COMGS_EVENT_ID_SCRIPT_ON 11U


static const uint16_t Udays[4][12] =                                            // Used for calculating time stamp since 1970

{

    {   0U,  31U,     60U,     91U,     121U,    152U,    182U,    213U,    244U,    274U,    305U,    335U},

    { 366U,  397U,    425U,    456U,    486U,    517U,    547U,    578U,    609U,    639U,    670U,    700U},

    { 731U,  762U,    790U,    821U,    851U,    882U,    912U,    943U,    974U,    1004U,   1035U,   1065U},

    {1096U,  1127U,   1155U,   1186U,   1216U,   1247U,   1277U,   1308U,   1339U,   1369U,   1400U,   1430U},

};

COMGSPACKED(
typedef struct {

      uint8_t mode_began : 1;                                                   // initialise integrator first time get the 100ms tick and store in lastitme
      uint8_t spare : 7;
      
      float32_t SEq_1;                                                          // estimated orientation quaternion elements
      
      float32_t SEq_2;
      
      float32_t SEq_3;
      
      float32_t SEq_4;

      float32_t b_x;                                                            // reference direction of flux in earth frame
      
      float32_t b_z;
      
      float32_t w_bx;                                                           // estimate gyroscope biases error
      
      float32_t w_by;
      
      float32_t w_bz;
      
      float32_t hx;
      
      float32_t hy;

      uint64_t lasttime;                                                        // last known 100ms tick counter value from the interrupt

}) COMGS_quarternion;

COMGSPACKED(
typedef struct {

      float32_t vx;

      float32_t vy;
      
      float32_t vz;

      float32_t wx;

      float32_t wy;

      float32_t wz;


}) COMGS_magcorr_t;

COMGSPACKED(
typedef struct {

      float32_t angle;                                                          // axis angle object

      float32_t x;

      float32_t y;

      float32_t z;

}) COMGS_axis_angle;

COMGSPACKED(
typedef struct {

      float32_t heading;                                                        // euler angle object

      float32_t attitude;

      float32_t bank;

}) COMGS_euler_angle;

COMGSPACKED(
typedef struct {

   uint8_t ProfileId;                                                           // The profile in question 0-4 or 255 = current

   uint8_t P[3];                                                                // Proportional bands (pitch,roll,yaw)
   
   uint8_t I[3];                                                                // Integral band (pitch,roll,yaw)
   
   uint8_t D[3];                                                                // Derivative bands (pitch,roll,yaw)
   
   uint8_t power[3];                                                            // power
   
   uint8_t invert[3];                                                           // reverse or forward direction for PID
   
   uint8_t poles[3];                                                            // number of poles in motor

   uint8_t rc_mode;                                                             // speed or angle
   
   uint8_t pwm_freq;                                                            // speed of pwm outputs (PID.out)
   
}) COMGS_pid_object;

COMGSPACKED(
typedef struct {

      float32_t z_real;                                                         // Ideal position: the ideal value we wish to measure

      float32_t z_measured;                                                     // Measured position:  the 'noisy' value we measured

      float32_t x_est;                                                          // Kalman position:

      float32_t sum_error_measure;                                              // Total error using measurement raw

      float32_t sum_error_kalman;                                               // Total error using kalman

      float32_t Diff_err;                                                       // Reduction in error with kalman


}) COMGS_kalman;                                                                // Kalman filter structure

COMGSPACKED(
typedef struct {

      uint8_t mode_began : 1;                                                   // flag for initialise
      uint8_t spare : 7;
      
      float32_t eInt[3];                                                        // Integral error

      float32_t Ki;                                                             // Integral band

      float32_t Kp;                                                             // Proportional band

      float32_t  Quaternion[4];                                                 // Output
      
      uint64_t lasttime;                                                        // last time that we got the gloabal 100 ms interrupt tick
      

}) COMGS_QControl_t;

#ifndef USE_CAL_UNLOCK                                                          // The define does not wait for calibration to complete
COMGSPACKED(
typedef struct {

     uint16_t centerValx;                                                       // X center

     uint16_t centerValy;                                                       // Y center

     uint16_t centerValz;                                                       // Z center

     uint16_t maxValx;                                                          // X max

     uint16_t maxValy;                                                          // Y max

     uint16_t maxValz;                                                          // Z max

     uint16_t minValx;                                                          // X min

     uint16_t minValy;                                                          // Y min

     uint16_t minValz;                                                          // Z min

     uint8_t butState;                                                          // The tick boxes for completion

}) COMGS_joy_calib_t;                                                           // Joystick calibration correction structure
#else
COMGSPACKED(
typedef struct {

     uint16_t centerValx;                                                       // X center

     uint16_t centerValy;                                                       // Y center

     uint16_t centerValz;                                                       // Z center

     uint16_t maxValx;                                                          // X max

     uint16_t maxValy;                                                          // Y max

     uint16_t maxValz;                                                          // Z max

     uint16_t minValx;                                                          // X min

     uint16_t minValy;                                                          // Y min

     uint16_t minValz;                                                          // Z min

     uint8_t butState;                                                          // The tick boxes for completion

     uint8_t seqState;                                                          // sequence state if non waiting

     uint64_t lasttime;                                                         // stored time for non waiitng
     
     uint8_t counter1;                                                          // internal sequence step for time calculation

}) COMGS_joy_calib_t;                                                           // Joystick calibration correction structure
#endif

COMGSPACKED(
typedef struct {

      uint8_t angleReq : 1;                                                     // Option flags
      uint8_t useDefault: 1;                                                    // use the default script parameters
      uint8_t swapPitchRoll : 1;                                                // swaps the pitch for roll
      uint8_t swapRollYaw : 1;                                                  // swap the roll for yaw
      uint8_t ScriptNumber : 4;                                                 // The valid script number to select (atm 0 ir 1)
      
      float32_t PA;                                                             // Pitch Angle

      float32_t YA;                                                             // Yaw Angle

      float32_t YS;                                                             // Yaw Speed

      float32_t PS;                                                             // Pitch Speed

      float32_t RA;                                                             // Roll Angle

      float32_t RS;                                                             // Roll Speed


}) COMGS_Script_t;                                                              // Time lapse script type structure

COMGSPACKED(
typedef struct {

      uint8_t ReqState : 1;                                                     // Requested state of the motor
      uint8_t LastReq: 1;                                                       // Last requested state
      uint8_t State: 1;                                                         // Motor state event
      uint8_t ESD: 1;                                                           // Motor ESD Event
      uint8_t FTS : 1;                                                          // Fail to stop/start alarm
      uint8_t Hold : 1;                                                         // Motor in Hold state
      uint8_t EcalErr: 1;                                                       // Encoder calibration error
      uint8_t Changing : 1;                                                     // Change of state requested

      float32_t TimeMax;                                                        // Maximum time to wait for a change of state

      uint64_t timeElapsed;                                                     // time elapsed since last motion
      uint64_t lasttime;                                                        // Last known ms counter (counted in 100ms slices)

}) COMGS_Motor_t;                                                               // Motor object

COMGSPACKED(
typedef struct {

      uint16_t Year;                                                            // 4 digit year
      uint8_t Month;                                                            // Month
      uint8_t Day;                                                              // Day
      uint8_t Hour;                                                             // Motor ESD Event
      uint8_t Minute;                                                           // Fail to stop/start alarm
      uint8_t Second;                                                           // Motor in Hold state

}) COMGS_DateTime_t;                                                            // Date and Time object

COMGSPACKED(
typedef struct {

      uint32_t hmiCommitRequest : 1;                                            // Requested to do an encoder commit
      uint32_t hmiShutdRequest : 1;                                             // Requested to do an encoder shutdown
      uint32_t hmiRebootRequest : 1;                                            // Requested to do an encoder reboot
      uint32_t hmiRecordRequest : 1;                                            // Requested to do an encoder record to disk for all channels
      uint32_t hmiStopRequest : 1;                                              // Requested to do an encoder recording stop for all channels
      uint32_t hmiMarkRequest : 1;                                              // Requested to do an encoder mark with a text the disk record for all channels
      uint32_t hmiRecordRequestCh1 : 1;                                         // Requested to do an encoder record to disk for channel 1
      uint32_t hmiStopRequestCh1 : 1;                                           // Requested to do an encoder recording stop for channel 1
      uint32_t hmiMarkRequestCh1 : 1;                                           // Requested to do an encoder mark with a text the disk record for channel 1
      uint32_t hmiRecordRequestCh2 : 1;                                         // Requested to do an encoder record to disk for channel 2
      uint32_t hmiStopRequestCh2 : 1;                                           // Requested to do an encoder recording stop for channel 2
      uint32_t hmiMarkRequestCh2 : 1;                                           // Requested to do an encoder mark with a text the disk record for channel 2
      uint32_t hmiRecordRequestCh3 : 1;                                         // Requested to do an encoder record to disk for channel 3
      uint32_t hmiStopRequestCh3 : 1;                                           // Requested to do an encoder recording stop for channel 3
      uint32_t hmiMarkRequestCh3 : 1;                                           // Requested to do an encoder mark with a text the disk record for channel 3
      uint32_t hmiRecordRequestCh4 : 1;                                         // Requested to do an encoder record to disk for channel 4
      uint32_t hmiStopRequestCh4 : 1;                                           // Requested to do an encoder recording stop for channel 4
      uint32_t hmiMarkRequestCh4 : 1;                                           // Requested to do an encoder mark with a text the disk record for channel 4
      uint32_t hmiEraseRequest : 1;                                             // Request to erase on all channels
      uint32_t hmiEraseRequestCh1 : 1;                                          // Request to erase on channel 1
      uint32_t hmiEraseRequestCh2 : 1;                                          // Request to erase on channel 2
      uint32_t hmiEraseRequestCh3 : 1;                                          // Request to erase on channel 3
      uint32_t hmiEraseRequestCh4 : 1;                                          // Request to erase on channel 4
      uint32_t sparebits : 9;

}) COMGS_Request_t;                                                             // HMI flag request object

// Structure for a camera object on the GUI
COMGSPACKED(
typedef struct {

       uint8_t   delta;                                                         // delta change value, must change by this to send a message
       uint32_t  value;                                                         // Current value store (largest value bit rate > 16 bits)
       uint32_t  last;                                                          // Last Stored value

}) XS_Hmi_Slider_t;                                                             // A slider object on the HMI requesting a change message to the encoder

COMGSPACKED(
typedef struct {

       uint8_t wifi : 1;                                                        // toggle wifi state
       uint8_t record : 1;                                                      // record to disk
       uint8_t stop : 1;                                                        // stop recording to disk
       uint8_t up : 1;                                                          // up keypad
       uint8_t down : 1;                                                        // down keypad
       uint8_t right : 1;                                                       // right keypad
       uint8_t left : 1;                                                        // left keypad
       uint8_t confirm : 1;                                                     // confirm keypad
       
       uint8_t ntsc : 1;                                                        // 1=ntsc 0=pal
       uint8_t writetxt : 1;                                                    // text write request to screen
       uint8_t mode : 2;                                                        // mode video photo osd
       uint8_t time : 1;                                                        // change the time of the camera
       uint8_t resol : 1;                                                       // change the resolution
       uint8_t charset : 1;                                                     // select the charset
       uint8_t spare : 1;

}) RC_Hmi_Request_t;                                                            // Run Cam HMI Pushbuttons

COMGSPACKED(
typedef struct  {

        uint8_t CableConnection  : 1;                                            // Cable is connected
        uint8_t VidOrPho  : 1;                                                  // video mode = 0 or photo mode = 1
        uint8_t SDCardStatus : 1;                                               // status of the SD Card
        uint8_t spare : 4;

        uint8_t Battpercent;                                                    // percent remaining in battery

        // uint32_t SDcardSpace;                                                   // SD Card space remaining

        uint8_t Token;                                                          // Token
        
        uint32_t size;                                                          // file size

        int8_t rval;                                                            // function return code
        
        uint32_t free_space;                                                    // SD Card free space bytes
        
        uint32_t total_space;                                                   // SD Card total space bytes
        
        unsigned char LastPicName[60];                                          // full path and last picture name
        
        unsigned char md5sum[MD5_MAX_LEN];                                      // the mds info for the given file
        
        unsigned char resolution[10];                                           // resolution string returned

}) COMGS_YiDig_t;                                                               // Struct to hold the information read back

COMGSPACKED(
typedef struct  {

   uint8_t VideoRes : 4;                                                        // 4 bit selection for xy_video_res
   uint8_t PhotoSz : 3;                                                         // 2 bit selection for xy_photo_sz
   uint8_t OnOffState : 1;                                                      // On or Off for the On_Off Type Selection
   uint8_t TimeLapOpt : 4;                                                      // time lapse option
   uint8_t TimeLapDuration : 4;                                                 // Time Lapse duration
   uint8_t SlowMotion : 4;                                                      // Slow motion option
   uint8_t BurstCap : 4;                                                        // Burst Capture setting
   uint8_t PreContTim : 4;                                                      // Precise cont time setting
   uint8_t RecPhoTim : 4;                                                       // Record or photo time
   uint8_t LoopRecDur : 4;                                                      // Loop record duration
   uint8_t VidPhoQ : 4;                                                         // Video photo quality
   uint8_t VidPhoStamp : 4;                                                     // Video photo stamp
   uint8_t VideoStd: 3;                                                         // Video Standard
   uint8_t OnOffTypSel : 8;                                                     // On_Off Type Selection
   uint8_t VidOutDev : 4;                                                       // Video output device type selection
   uint8_t MeterOpt : 4;                                                        // meter options
   uint8_t LedMode : 4;                                                         // led mode
   uint8_t BuzzVol : 4;                                                         // buzzer volume
   uint8_t CapMode : 4;                                                         // capture modes
   uint8_t spare : 4;

}) COMGS_YiOptSelect_t;                                                         // Struct to hold the various options for YiCam configurations

COMGSPACKED(
typedef struct  {
   uint8_t laserStateOn : 1U;
   uint8_t laserStateOff : 1U;
   uint8_t getTemp : 1U;
   uint8_t getVolts : 1U;
   uint8_t getAlarm : 1U;                                                       // timed pulse to retrieve alarms
   uint8_t setAlarm : 1U;                                                       // request to set an alarm
   uint8_t spare : 2U;
}) COMGS_SF40OptSelect_t;                                                       // Struct to hold the various options for SF40 liddar configurations

#ifdef ROBOT_HELPER
COMGSPACKED(
typedef struct  {
   float32_t sp_theta;
   float32_t sp_gamma;
}) COMGS_Odrive_t;                                                              // Struct to hold odrive settings
COMGSPACKED(
typedef struct  {
 uint16_t kFrontLeft_val;
 uint16_t kFrontRight_val;
 uint16_t kRearLeft_val;
 uint16_t kRearRight_val;
}) roboMtr_t;
#endif

#define JOY_CENTER_MIN 100                                                      // worst case center calib value accepted

// define the alarm word for calibration
#define JCAL_SUCCESS 0                                                          // calibration success
#define JCAL_CEN_FLT 1<<0                                                       // not at center
#define JCAL_XDIR_FLT 1<<1                                                      // xdir calibration fault
#define JCAL_YDIR_FLT 1<<2                                                      // ydir calibration fault
#define JCAL_ZDIR_FLT 1<<3                                                      // zdir calibration fault
#define JCAL_STEP_FLT 1<<4                                                      // not all step completed calibration fault
#define JCAL_OOR_FLT 1<<5                                                       // calibration out of range fault
#define JCAL_TIME_FLT 1<<6                                                      // calibration out of range fault

//
//
//  --------------------  Function Declarations --------------------------------
//
//
float32_t invSqrt(float32_t x);
void IMU_filterUpdate(float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, COMGS_quarternion *quartCalculated );
void MARG_AHRS_filterUpdate(float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, float32_t m_x, float32_t m_y, float32_t m_z, COMGS_quarternion *quartCalculated);
float32_t frand_func();
int16_t anglefromMag( int16_t magX, int16_t magY );
void Number2String(float32_t x, unsigned char *str, unsigned char precision);
void MadgwickAHRSupdate(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, float32_t mx, float32_t my, float32_t mz, COMGS_quarternion *quartCalculated);
void MadgwickAHRSupdateIMU(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, COMGS_quarternion *quartCalculated);
void MahonyAHRSupdate(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, float32_t mx, float32_t my, float32_t mz, COMGS_quarternion *quartCalculated);
void MahonyAHRSupdateIMU(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, COMGS_quarternion *quartCalculated);
void JoyStick2BearSteer( int16_t nJoyX, int16_t nJoyY, int16_t nMotMixL, int16_t nMotMixR);
void JoyStick2DiffSteer( int16_t nJoyX, int16_t nJoyY, int16_t nMotMixL, int16_t nMotMixR);
void moving_average( COMGS_gyro_acc_data_t *accStruct );
void calculate_velocity_position( COMGS_gyro_acc_data_t *accStruct );
void calculate_theta(COMGS_gyro_acc_data_t *accStruct);
void Process_Alarm_Ack( COMGS_error_alarm *almWord, AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc );
void Update_Alm_Line( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, AlarmLineObject_t *almLineObj );
void pushAlmToStack( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, uint8_t positionInobj );
void popAlmOffStack( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, uint8_t indexInAlmDefObj );
void kalman_filter( COMGS_kalman *kalmanData );
uint8_t CalibrateCenter( COMGS_joy_calib_t *calibObj );
uint8_t CalibrateSpan( COMGS_joy_calib_t *calibObj );
void Process_Motor_State( COMGS_Motor_t *mtrBlk );
int16_t is_valid_ip(unsigned char *ip_str);
void compute_time(uint32_t time, COMGS_DateTime_t *hmiTm);
void compute_secs(XS_Hmi_Slider_t *CalcTime, COMGS_DateTime_t *hmiTm);
uint8_t check_datetime(COMGS_DateTime_t *hmiTm);
void compute_date(XS_Hmi_Slider_t *CalcTime, COMGS_DateTime_t *hmiTm);
float32_t fastA2F(const char *p);
uint16_t fastA2I(const char *s);
int16_t a2d(char ch);
uint32_t fastA2UL(const char *p);
float32_t fastA2F(const char *p);
uint32_t bswap_32(uint32_t num2ByteSwap);
uint16_t bswap_16(uint16_t x);
uint32_t dateTimeToRtcTime(COMGS_DateTime_t *dt);
float32_t farenhFromcelsius( float32_t celsius );
int16_t is_valid_ip(unsigned char *ip_str);
int16_t valid_digit(unsigned char *ip_str);
void Calc_Cursor_LL(signed int lat, signed int lon, char *lt_Str, char *lng_Str, unsigned int color);
#ifdef GPS_INCLUDED                                                             // Ducati GPS version wanted
void ReadGPSData();
#endif
#ifdef ROBOT_HELPER                                                             // Robot leg library wanted
void hop(GaitParams params, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4);
void TransitionToDance();
void gait(GaitParams params, float leg0_offset, float leg1_offset, float leg2_offset, float leg3_offset, LegGain *gains, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4);
void CoupledMoveLeg( float32_t t,GaitParams params, float32_t gait_offset, float32_t leg_direction, LegGain *gains, float32_t theta, float32_t gamma);
void UpdateStateGaitParams(RobStates curr_state);
uint8_t IsValidLegGain(LegGain gains);
uint8_t IsValidGaitParams(GaitParams params);
void SinTrajectory (float32_t t, GaitParams params, float32_t gaitOffset, float32_t x, float32_t y);
void CartesianToThetaGamma(float32_t x, float32_t y, float32_t leg_direction, float32_t theta, float32_t gamma);
void CartesianToLegParams(float32_t x, float32_t y, float32_t leg_direction, float32_t L, float32_t theta);
void LegParamsToCartesian(float32_t L, float32_t theta, float32_t leg_direction, float32_t x, float32_t y);
void GetGamma(float32_t L, float32_t theta, float32_t gamma);
void CommandAllLegs(float32_t theta, float32_t gamma, LegGain *gains, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4);
void SetPositionUsingGains( LegGain *gains, char* sndBuf );
void SetCoupledPosition(float32_t sp_theta, float32_t sp_gamma, LegGain *gains, char *sndBuf);
uint8_t XorShort(int16_t val);
int16_t ParseDualPosition(char* msg, int16_t len, float32_t  *th, float32_t *ga);
void SetThetaGamma(float32_t theta, float32_t gamma);
int16_t ParseMtrStateReply(char* msg);
void precomputed_jacobian( float32_t joints[4], float32_t jacobian[3][4] );
void manual_forward_kinematics( float32_t joints[4], float32_t array[3] );
void automatic_forward_kinematics(float32_t joints[4], float32_t t10[4][4], float32_t t21[4][4], float32_t t32[4][4], float32_t t43[4][4]  );
void transform_matrix(float32_t alpha,float32_t a, float32_t d, float32_t theta, float32_t array[4][4]);
#endif
#ifdef PID_LOOPS                                                                // PID loop library wanted
uint8_t tunePID( pidBlock_t *pidObj, pidTuner_t *pidTuner );
void DcMotorPIDloop(pidBlock_t *pidObj, uint16_t pwm_period, uint16_t channel);
void PWMSetDuty( uint16_t pid_out, uint16_t pwm_period, uint16_t channel );
void PWMStop( uint16_t channel );
void PWMStart( uint16_t channel );
uint16_t PWMinit(uint32_t freq_hz, uint16_t enable_channel_x, uint16_t timer_prescale, uint16_t use_timer_x);
uint8_t computePID( pidBlock_t *pidObj );
#endif

//==============================================================================
//                                 Functions
//
//==============================================================================
// Orientation filter for IMU
// Filter for Accelerometer and Gyroscope data
// w_x w_y w_z = gyroscope readings in rad/s
// a_x a_y a_z = accelorometer readings
//
/*-----------------------------------------------------------------------------
 *      IMU_filterUpdate :  Filter for Accelerometer and Gyroscope data
 *  Parameters: float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, 
 *  COMGS_quarternion *quartCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void IMU_filterUpdate(float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, COMGS_quarternion *quartCalculated )
{
  
  // Local system variables
  float32_t norm;                                                               // vector norm
  float32_t SEqDot_omega_1, SEqDot_omega_2, SEqDot_omega_3, SEqDot_omega_4;     // quaternion derrivative from gyroscopes elements
  float32_t f_1, f_2, f_3;                                                      // objective function elements
  float32_t J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33;                 // objective function Jacobian elements
  float32_t SEqHatDot_1, SEqHatDot_2, SEqHatDot_3, SEqHatDot_4;                 // estimated direction of the gyroscope error

  // Axulirary variables to avoid reapeated calcualtions
  float32_t halfSEq_1 = 0.5f * quartCalculated->SEq_1;
  float32_t halfSEq_2 = 0.5f * quartCalculated->SEq_2;
  float32_t halfSEq_3 = 0.5f * quartCalculated->SEq_3;
  float32_t halfSEq_4 = 0.5f * quartCalculated->SEq_4;
  float32_t twoSEq_1 = 2.0f * quartCalculated->SEq_1;
  float32_t twoSEq_2 = 2.0f * quartCalculated->SEq_2;
  float32_t twoSEq_3 = 2.0f * quartCalculated->SEq_3;
  
  // Initialisation of values (not sure if needed everytime or only on creation)
  quartCalculated->SEq_1=0;                                                     // estimated orientation quaternion elements with initial conditions
  quartCalculated->SEq_2=0;
  quartCalculated->SEq_3=0;
  quartCalculated->SEq_4=0;
  
  // Normalise the accelerometer measurement
  norm = sqrt(a_x * a_x + a_y * a_y + a_z * a_z);
  a_x /= norm;
  a_y /= norm;
  a_z /= norm;
  // Compute the objective function and Jacobian
  f_1 = twoSEq_2 * quartCalculated->SEq_4 - twoSEq_1 * quartCalculated->SEq_3 - a_x;
  f_2 = twoSEq_1 * quartCalculated->SEq_2 + twoSEq_3 * quartCalculated->SEq_4 - a_y;
  f_3 = 1.0f - twoSEq_2 * quartCalculated->SEq_2 - twoSEq_3 * quartCalculated->SEq_3 - a_z;
  J_11or24 = twoSEq_3;
  // J_11 negated in matrix multiplication
  J_12or23 = 2.0f * quartCalculated->SEq_4;
  J_13or22 = twoSEq_1;                                                          // J_12 negated in matrix multiplication
  J_14or21 = twoSEq_2; J_32 = 2.0f * J_14or21;                                  // negated in matrix multiplication
  J_33 = 2.0f * J_11or24;                                                       // negated in matrix multiplication
  // Compute the gradient (matrix multiplication)
  SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1;
  SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3;
  SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1;
  SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2;
  // Normalise the gradient
  norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
  SEqHatDot_1 /= norm;
  SEqHatDot_2 /= norm;
  SEqHatDot_3 /= norm;
  SEqHatDot_4 /= norm;
  // Compute the quaternion derrivative measured by gyroscopes
  SEqDot_omega_1 = -halfSEq_2 * w_x - halfSEq_3 * w_y - halfSEq_4 * w_z;
  SEqDot_omega_2 = halfSEq_1 * w_x + halfSEq_3 * w_z - halfSEq_4 * w_y;
  SEqDot_omega_3 = halfSEq_1 * w_y - halfSEq_2 * w_z + halfSEq_4 * w_x;
  SEqDot_omega_4 = halfSEq_1 * w_z + halfSEq_2 * w_y - halfSEq_3 * w_x;
  // Compute then integrate the estimated quaternion derrivative
  quartCalculated->SEq_1 +=  SEqDot_omega_1;
  quartCalculated->SEq_1 += (SEqDot_omega_1 - (beta * SEqHatDot_1)) * deltat;
  quartCalculated->SEq_2 += (SEqDot_omega_2 - (beta * SEqHatDot_2)) * deltat;
  quartCalculated->SEq_3 += (SEqDot_omega_3 - (beta * SEqHatDot_3)) * deltat;
  quartCalculated->SEq_4 += (SEqDot_omega_4 - (beta * SEqHatDot_4)) * deltat;
  // Normalise quaternion
  norm = sqrt(pow(quartCalculated->SEq_1,2) + pow(quartCalculated->SEq_2,2) + pow(quartCalculated->SEq_3,2) + pow(quartCalculated->SEq_4,2));
  quartCalculated->SEq_1 /= norm;
  quartCalculated->SEq_2 /= norm;
  quartCalculated->SEq_3 /= norm;
  quartCalculated->SEq_4 /= norm;
}

//
// w_(x) y z = gyro data
// a_(x) y z = accelerometer data
// m_(x) y z = magnetometer data
/*-----------------------------------------------------------------------------
 *      MARG_AHRS_filterUpdate :  Marginal AHRS Filter
 *  Parameters: float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, float32_t m_x, float32_t m_y, float32_t m_z,
 *  COMGS_quarternion *quartCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void MARG_AHRS_filterUpdate(float32_t w_x, float32_t w_y, float32_t w_z, float32_t a_x, float32_t a_y, float32_t a_z, float32_t m_x, float32_t m_y, float32_t m_z, COMGS_quarternion *quartCalculated)
{

   // local system variables
  float32_t norm; // vector norm
  float32_t SEqDot_omega_1, SEqDot_omega_2, SEqDot_omega_3, SEqDot_omega_4;         // quaternion rate from gyroscopes elements
  float32_t f_1, f_2, f_3, f_4, f_5, f_6;                                           // objective function elements
  float32_t J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33, J_41, J_42, J_43, J_44, J_51, J_52, J_53, J_54, J_61, J_62, J_63, J_64;  // objective function Jacobian elements
  float32_t SEqHatDot_1, SEqHatDot_2, SEqHatDot_3, SEqHatDot_4;                     // estimated direction of the gyroscope error
  float32_t w_err_x, w_err_y, w_err_z;                                              // estimated direction of the gyroscope error (angular)
  float32_t h_x, h_y, h_z;                                                          // computed flux in the earth frame
 
  // axulirary variables to avoid reapeated calcualtions
  float32_t halfSEq_1 = 0.5f * quartCalculated->SEq_1;
  float32_t halfSEq_2 = 0.5f * quartCalculated->SEq_2;
  float32_t halfSEq_3 = 0.5f * quartCalculated->SEq_3;
  float32_t halfSEq_4 = 0.5f * quartCalculated->SEq_4;
  float32_t twoSEq_1 = 2.0f * quartCalculated->SEq_1;
  float32_t twoSEq_2 = 2.0f * quartCalculated->SEq_2;
  float32_t twoSEq_3 = 2.0f * quartCalculated->SEq_3;
  float32_t twoSEq_4 = 2.0f * quartCalculated->SEq_4;
  float32_t twob_x = 2.0f * quartCalculated->b_x;
  float32_t twob_z = 2.0f * quartCalculated->b_z;
  float32_t twob_xSEq_1 = 2.0f * quartCalculated->b_x * quartCalculated->SEq_1;
  float32_t twob_xSEq_2 = 2.0f * quartCalculated->b_x * quartCalculated->SEq_2;
  float32_t twob_xSEq_3 = 2.0f * quartCalculated->b_x * quartCalculated->SEq_3;
  float32_t twob_xSEq_4 = 2.0f * quartCalculated->b_x * quartCalculated->SEq_4;
  float32_t twob_zSEq_1 = 2.0f * quartCalculated->b_z * quartCalculated->SEq_1;
  float32_t twob_zSEq_2 = 2.0f * quartCalculated->b_z * quartCalculated->SEq_2;
  float32_t twob_zSEq_3 = 2.0f * quartCalculated->b_z * quartCalculated->SEq_3;
  float32_t twob_zSEq_4 = 2.0f * quartCalculated->b_z * quartCalculated->SEq_4;
  float32_t SEq_1SEq_2;
  float32_t SEq_1SEq_3 = quartCalculated->SEq_1 * quartCalculated->SEq_3;
  float32_t SEq_1SEq_4;
  float32_t SEq_2SEq_3;
  float32_t SEq_2SEq_4 = quartCalculated->SEq_2 * quartCalculated->SEq_4;
  float32_t SEq_3SEq_4;
  float32_t twom_x = 2.0f * m_x;
  float32_t twom_y = 2.0f * m_y;
  float32_t twom_z = 2.0f * m_z;
  
  // Initialisation of values (not sure if needed everytime or on creation)
  quartCalculated->SEq_1 = 1, quartCalculated->SEq_2 = 0, quartCalculated->SEq_3 = 0, quartCalculated->SEq_4 = 0;        // estimated orientation quaternion elements with initial conditions
  quartCalculated->b_x = 1, quartCalculated->b_z = 0;                           // reference direction of flux in earth frame
  quartCalculated->w_bx = 0, quartCalculated->w_by = 0, quartCalculated->w_bz = 0;        // estimate gyroscope biases error

  // normalise the accelerometer measurement
  norm = sqrt(a_x * a_x + a_y * a_y + a_z * a_z);
  a_x /= norm;
  a_y /= norm;
  a_z /= norm;
  // normalise the magnetometer measurement
  norm = sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
  m_x /= norm;
  m_y /= norm;
  m_z /= norm;

  // compute the objective function and Jacobian
  f_1 = twoSEq_2 * quartCalculated->SEq_4 - twoSEq_1 * quartCalculated->SEq_3 - a_x;
  f_2 = twoSEq_1 * quartCalculated->SEq_2 + twoSEq_3 * quartCalculated->SEq_4 - a_y;
  f_3 = 1.0f - twoSEq_2 * quartCalculated->SEq_2 - twoSEq_3 * quartCalculated->SEq_3 - a_z;
  f_4 = twob_x * (0.5f - pow(quartCalculated->SEq_3,2) - pow(quartCalculated->SEq_4,2)) + twob_z * (SEq_2SEq_4 - SEq_1SEq_3) - m_x; 
  f_5 = twob_x * (quartCalculated->SEq_2 * quartCalculated->SEq_3 - quartCalculated->SEq_1 * quartCalculated->SEq_4) + twob_z * (quartCalculated->SEq_1 * quartCalculated->SEq_2 + quartCalculated->SEq_3 * quartCalculated->SEq_4) - m_y; 
  f_6 = twob_x * (SEq_1SEq_3 + SEq_2SEq_4) + twob_z * (0.5f - pow(quartCalculated->SEq_2,2) - pow(quartCalculated->SEq_3,2)) - m_z;
  J_11or24 = twoSEq_3;
  // J_11 negated in matrix multiplication
  J_12or23 = 2.0f * quartCalculated->SEq_4;
  J_13or22 = twoSEq_1;
  // J_12 negated in matrix multiplication
  J_14or21 = twoSEq_2;
  J_32 = 2.0f * J_14or21;
  // negated in matrix multiplication
  J_33 = 2.0f * J_11or24;
  // negated in matrix multiplication
  J_41 = twob_zSEq_3;
  // negated in matrix multiplication
  J_42 = twob_zSEq_4;
  J_43 = 2.0f * twob_xSEq_3 + twob_zSEq_1;
  // negated in matrix multiplication
  J_44 = 2.0f * twob_xSEq_4 - twob_zSEq_2;
  // negated in matrix multiplication
  J_51 = twob_xSEq_4 - twob_zSEq_2;
  // negated in matrix multiplication
  J_52 = twob_xSEq_3 + twob_zSEq_1;
  J_53 = twob_xSEq_2 + twob_zSEq_4;
  J_54 = twob_xSEq_1 - twob_zSEq_3;
  // negated in matrix multiplication
  J_61 = twob_xSEq_3;
  J_62 = twob_xSEq_4 - 2.0f * twob_zSEq_2;
  J_63 = twob_xSEq_1 - 2.0f * twob_zSEq_3;
  J_64 = twob_xSEq_2;

  // compute the gradient (matrix multiplication)
  SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1 - J_41 * f_4 - J_51 * f_5 + J_61 * f_6;
  SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3 + J_42 * f_4 + J_52 * f_5 + J_62 * f_6; SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1 - J_43 * f_4 + J_53 * f_5 + J_63 * f_6; SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2 - J_44 * f_4 - J_54 * f_5 + J_64 * f_6;
  // normalise the gradient to estimate direction of the gyroscope error
  norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
  SEqHatDot_1 = SEqHatDot_1 / norm;
  SEqHatDot_2 = SEqHatDot_2 / norm;
  SEqHatDot_3 = SEqHatDot_3 / norm;
  SEqHatDot_4 = SEqHatDot_4 / norm;
  // compute angular estimated direction of the gyroscope error
  w_err_x = twoSEq_1 * SEqHatDot_2 - twoSEq_2 * SEqHatDot_1 - twoSEq_3 * SEqHatDot_4 + twoSEq_4 * SEqHatDot_3;
  w_err_y = twoSEq_1 * SEqHatDot_3 + twoSEq_2 * SEqHatDot_4 - twoSEq_3 * SEqHatDot_1 - twoSEq_4 * SEqHatDot_2;
  w_err_z = twoSEq_1 * SEqHatDot_4 - twoSEq_2 * SEqHatDot_3 + twoSEq_3 * SEqHatDot_2 - twoSEq_4 * SEqHatDot_1;
  // compute and remove the gyroscope baises
  quartCalculated->w_bx += w_err_x * deltat * zeta;
  quartCalculated->w_by += w_err_y * deltat * zeta;
  quartCalculated->w_bz += w_err_z * deltat * zeta;
  w_x -= quartCalculated->w_bx;
  w_y -= quartCalculated->w_by;
  w_z -= quartCalculated->w_bz;
  // compute the quaternion rate measured by gyroscopes
  SEqDot_omega_1 = -halfSEq_2 * w_x - halfSEq_3 * w_y - halfSEq_4 * w_z;
  SEqDot_omega_2 = halfSEq_1 * w_x + halfSEq_3 * w_z - halfSEq_4 * w_y;
  SEqDot_omega_3 = halfSEq_1 * w_y - halfSEq_2 * w_z + halfSEq_4 * w_x;
  SEqDot_omega_4 = halfSEq_1 * w_z + halfSEq_2 * w_y - halfSEq_3 * w_x;
  // compute then integrate the estimated quaternion rate
  quartCalculated->SEq_1 += (SEqDot_omega_1 - (beta * SEqHatDot_1)) * deltat;
  quartCalculated->SEq_2 += (SEqDot_omega_2 - (beta * SEqHatDot_2)) * deltat;
  quartCalculated->SEq_3 += (SEqDot_omega_3 - (beta * SEqHatDot_3)) * deltat;
  quartCalculated->SEq_4 += (SEqDot_omega_4 - (beta * SEqHatDot_4)) * deltat;
  // normalise quaternion
  norm = sqrt(pow(quartCalculated->SEq_1,2) + pow(quartCalculated->SEq_2,2) + pow(quartCalculated->SEq_3,2) + pow(quartCalculated->SEq_4,2));
  quartCalculated->SEq_1 /= norm;
  quartCalculated->SEq_2 /= norm;
  quartCalculated->SEq_3 /= norm;
  quartCalculated->SEq_4 /= norm;
  // compute flux in the earth frame
  SEq_1SEq_2 = quartCalculated->SEq_1 * quartCalculated->SEq_2;
  // recompute axulirary variables
  SEq_1SEq_3 = quartCalculated->SEq_1 * quartCalculated->SEq_3;
  SEq_1SEq_4 = quartCalculated->SEq_1 * quartCalculated->SEq_4;
  SEq_3SEq_4 = quartCalculated->SEq_3 * quartCalculated->SEq_4;
  SEq_2SEq_3 = quartCalculated->SEq_2 * quartCalculated->SEq_3;
  SEq_2SEq_4 = quartCalculated->SEq_2 * quartCalculated->SEq_4;
  h_x = twom_x * (0.5f - quartCalculated->SEq_3 * quartCalculated->SEq_3 - quartCalculated->SEq_4 * quartCalculated->SEq_4) + twom_y * (SEq_2SEq_3 - SEq_1SEq_4) + twom_z * (SEq_2SEq_4 + SEq_1SEq_3);
  h_y = twom_x * (SEq_2SEq_3 + SEq_1SEq_4) + twom_y * (0.5f - quartCalculated->SEq_2 * quartCalculated->SEq_2 - quartCalculated->SEq_4 * quartCalculated->SEq_4) + twom_z * (SEq_3SEq_4 - SEq_1SEq_2);
  h_z = twom_x * (SEq_2SEq_4 - SEq_1SEq_3) + twom_y * (SEq_3SEq_4 + SEq_1SEq_2) + twom_z * (0.5f - quartCalculated->SEq_2 * quartCalculated->SEq_2 - quartCalculated->SEq_3 * quartCalculated->SEq_3);
  // normalise the flux vector to have only components in the x and z
  quartCalculated->b_x = sqrt((h_x * h_x) + (h_y * h_y));
  quartCalculated->b_z = h_z;
  
}

/*-----------------------------------------------------------------------------
 *      frand_func :  floating point random number generator
 *  Parameters: none
 *  Return:
 *         float32_t random number
 *----------------------------------------------------------------------------*/
#define RAND_MAX 2147483647                                                     /*implementation defined for GNU C consider 32767 if not work */
float32_t frand_func()
{
    //return (rand());
    //return ( (rand() / (double) RAND_MAX) );
    return (2*((rand()/(double)RAND_MAX) - 0.5));

}

/*-----------------------------------------------------------------------------
 *      anglefromMag :  Calculate the angle from the magnetrometer data (X Y co-ordinates)
 *  Parameters: int16_t magX, int16_t magY
 *  Return:
 *         int16_t angle
 *----------------------------------------------------------------------------*/
int16_t anglefromMag( int16_t magX, int16_t magY )                              // Calculate Angle from magnetrometer data
{
    int16_t Angle;
    
    Angle = (atan2(magY, magX) * 180) / (double) (4.0f * atan(1.0f));
    if (Angle < 0)  Angle += 360;
    return (Angle);
}

/*-----------------------------------------------------------------------------
 *      NormaliseAccMag :  Normalise function for Magnetrometer or Accelorometer
 *  Parameters: float32_t ax, float32_t ay, float32_t az
 *  Return:
 *         uint8_t return 0=error 1=success
 *----------------------------------------------------------------------------*/
uint8_t NormaliseAccMag( float32_t ax, float32_t ay, float32_t az )
{
  float32_t norm;
  norm = (float32_t) Sqrt(ax * ax + ay * ay + az * az);
  if (norm == 0f) return(0);                                                    // handle NaN
  norm = 1 / norm;                                                              // use reciprocal for division
  ax *= norm;
  ay *= norm;
  az *= norm;
  return(1);
}

/*-----------------------------------------------------------------------------
 *      RefDirEarthMag :  Reference direction of Earth's magnetic field and estimated direction of gravity and magnetic field
 *  Parameters: float32_t mx, float32_t my, float32_t mz, COMGS_quarternion *quartCalculated, COMGS_magcorr_t *magCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void RefDirEarthMag(float32_t mx, float32_t my, float32_t mz, COMGS_quarternion *quartCalculated, COMGS_magcorr_t *magCalculated)
{
    // Auxiliary variables to avoid repeated arithmetic
    float32_t q1q1 = quartCalculated->SEq_1 * quartCalculated->SEq_1;
    float32_t q1q2 = quartCalculated->SEq_1 * quartCalculated->SEq_2;
    float32_t q1q3 = quartCalculated->SEq_1 * quartCalculated->SEq_3;
    float32_t q1q4 = quartCalculated->SEq_1 * quartCalculated->SEq_4;
    float32_t q2q2 = quartCalculated->SEq_2 * quartCalculated->SEq_2;
    float32_t q2q3 = quartCalculated->SEq_2 * quartCalculated->SEq_3;
    float32_t q2q4 = quartCalculated->SEq_2 * quartCalculated->SEq_4;
    float32_t q3q3 = quartCalculated->SEq_3 * quartCalculated->SEq_3;
    float32_t q3q4 = quartCalculated->SEq_3 * quartCalculated->SEq_4;
    float32_t q4q4 = quartCalculated->SEq_4 * quartCalculated->SEq_4;
    
    // Reference direction of Earth's magnetic field
    quartCalculated->hx = 2f * mx * (0.5f - q3q3 - q4q4) + 2f * my * (q2q3 - q1q4) + 2f * mz * (q2q4 + q1q3);
    quartCalculated->hy = 2f * mx * (q2q3 + q1q4) + 2f * my * (0.5f - q2q2 - q4q4) + 2f * mz * (q3q4 - q1q2);
    quartCalculated->b_x = (float32_t) Sqrt((quartCalculated->hx * quartCalculated->hx) + (quartCalculated->hy * quartCalculated->hy));
    quartCalculated->b_z = 2f * mx * (q2q4 - q1q3) + 2f * my * (q3q4 + q1q2) + 2f * mz * (0.5f - q2q2 - q3q3);
    
    // Estimated direction of gravity and magnetic field
    magCalculated->vx = 2f * (q2q4 - q1q3);
    magCalculated->vy = 2f * (q1q2 + q3q4);
    magCalculated->vz = q1q1 - q2q2 - q3q3 + q4q4;
    magCalculated->wx = 2f * quartCalculated->b_x * (0.5f - q3q3 - q4q4) + 2f * quartCalculated->b_z * (q2q4 - q1q3);
    magCalculated->wy = 2f * quartCalculated->b_x * (q2q3 - q1q4) + 2f * quartCalculated->b_z * (q1q2 + q3q4);
    magCalculated->wz = 2f * quartCalculated->b_x * (q1q3 + q2q4) + 2f * quartCalculated->b_z * (0.5f - q2q2 - q3q3);
}

/*-----------------------------------------------------------------------------
 *      AHRS2quartAGM :
 * PI Control giving quarternion output
 * reads accelorometer magnetrometer and gyro values as well as quartObject and magcorr objects
 * Algorithm AHRS update method.(another variant of maghony)
 *
 *  Parameters: COMGS_quarternion *quartCalculated, COMGS_magcorr_t *magCalculated, float32_t ax, float32_t ay, float32_t az, float32_t gx, float32_t gy, 
 *  float32_t gz, float32_t mx, float32_t my, float32_t mz, COMGS_QControl_t *QContObj
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void AHRS2quartAGM(COMGS_quarternion *quartCalculated, COMGS_magcorr_t *magCalculated, float32_t ax, float32_t ay, float32_t az, float32_t gx, float32_t gy, float32_t gz, float32_t mx, float32_t my, float32_t mz, COMGS_QControl_t *QContObj )
{
            float32_t ex,ey,ez;                                                 // errors calculated
            float32_t pa,pb,pc;
            float32_t norm;                                                     // normalised quartenion
            int32_t samplePeriod;                                               // sample period calculated from the 100ms interrupt tick
            
             if (QContObj->mode_began == false)                                 // time initialisation request
             {
                  samplePeriod = -1;                                            // initialise the timer if its the first time we called this function
                  QContObj->mode_began = true;                                  // initialisation of time complete
             }

            calculateTimeDiff(samplePeriod, QContObj->lasttime);                // time in 100 ms counts from the interrupt
            samplePeriod *= 10;                                                 // convert to seconds
            
            // Error is cross product between estimated direction and measured direction of gravity
            ex = (ay * magCalculated->vz - az * magCalculated->vy) + (my * magCalculated->wz - mz * magCalculated->wy);
            ey = (az * magCalculated->vx - ax * magCalculated->vz) + (mz * magCalculated->wx - mx * magCalculated->wz);
            ez = (ax * magCalculated->vy - ay * magCalculated->vx) + (mx * magCalculated->wy - my * magCalculated->wx);

            if (QContObj->Ki > 0f)                                              // integral parameter of q control object
            {
                QContObj->eInt[0] += ex;                                        // accumulate integral error
                QContObj->eInt[1] += ey;
                QContObj->eInt[2] += ez;
            }
            else
            {
                QContObj->eInt[0] = 0.0f;                                       // prevent integral wind up
                QContObj->eInt[1] = 0.0f;
                QContObj->eInt[2] = 0.0f;
            }

            gx = gx + QContObj->Kp * ex + QContObj->Ki * QContObj->eInt[0];     // Apply feedback terms
            gy = gy + QContObj->Kp * ey + QContObj->Ki * QContObj->eInt[1];
            gz = gz + QContObj->Kp * ez + QContObj->Ki * QContObj->eInt[2];

            pa = quartCalculated->SEq_2;                                        // Integrate rate of change of quaternion
            pb = quartCalculated->SEq_3;
            pc = quartCalculated->SEq_4;
            quartCalculated->SEq_1 = quartCalculated->SEq_1 + (-quartCalculated->SEq_2 * gx - quartCalculated->SEq_3 * gy - quartCalculated->SEq_4 * gz) * (0.5f * SamplePeriod);
            quartCalculated->SEq_2 = pa + (quartCalculated->SEq_1 * gx + pb * gz - pc * gy) * (0.5f * SamplePeriod);
            quartCalculated->SEq_3 = pb + (quartCalculated->SEq_1 * gy - pa * gz + pc * gx) * (0.5f * SamplePeriod);
            quartCalculated->SEq_4 = pc + (quartCalculated->SEq_1 * gz + pa * gy - pb * gx) * (0.5f * SamplePeriod);

            // Normalise quaternion
            norm = (float32_t) Sqrt(quartCalculated->SEq_1 * quartCalculated->SEq_1 + quartCalculated->SEq_2 * quartCalculated->SEq_2 + quartCalculated->SEq_3 * quartCalculated->SEq_3 + quartCalculated->SEq_4 * quartCalculated->SEq_4);
            norm = 1.0f / norm;

            QContObj->Quaternion[0] = quartCalculated->SEq_1 * norm;
            QContObj->Quaternion[1] = quartCalculated->SEq_2 * norm;
            QContObj->Quaternion[2] = quartCalculated->SEq_3 * norm;
            QContObj->Quaternion[3] = quartCalculated->SEq_4 * norm;

}

/*-----------------------------------------------------------------------------
 *      AHRS2quartAG :
 * PI Control giving quarternion output
 * reads accelorometer magnetrometer and gyro values as well as quartObject and magcorr objects
 * Algorithm AHRS update method.(another variant of maghony)
 *
 *  Parameters: COMGS_quarternion *quartCalculated, float32_t ax, float32_t ay, float32_t az, float32_t gx, float32_t gy, float32_t gz, 
 *  COMGS_magcorr_t *magCalculated, COMGS_QControl_t *QContObj
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void AHRS2quartAG(COMGS_quarternion *quartCalculated, float32_t ax, float32_t ay, float32_t az, float32_t gx, float32_t gy, float32_t gz, COMGS_magcorr_t *magCalculated, COMGS_QControl_t *QContObj )
{
            float32_t ex,ey,ez;                                                 // errors calculated
            float32_t pa,pb,pc;
            float32_t norm;                                                     // normalised quartenion
            int32_t samplePeriod;                                               // sample period calculated from the 100ms interrupt tick

             if (QContObj->mode_began == false)                                 // time initialisation request
             {
                  samplePeriod = -1;                                            // initialise the timer if its the first time we called this function
                  QContObj->mode_began = true;                                  // initialisation of time complete
             }

            calculateTimeDiff(samplePeriod, QContObj->lasttime);                // time in 100 ms counts from the interrupt
            samplePeriod *= 10;
            if (NormaliseAccMag(ax,ay,az))                                      // Normalise acc data
            {
               // Estimated direction of gravity
               magCalculated->vx = 2.0f * (quartCalculated->SEq_2 * quartCalculated->SEq_4 - quartCalculated->SEq_1 * quartCalculated->SEq_3);
               magCalculated->vy = 2.0f * (quartCalculated->SEq_1 * quartCalculated->SEq_2 + quartCalculated->SEq_3 * quartCalculated->SEq_4);
               magCalculated->vz = quartCalculated->SEq_1 * quartCalculated->SEq_1 - quartCalculated->SEq_2 * quartCalculated->SEq_2 - quartCalculated->SEq_3 * quartCalculated->SEq_3 + quartCalculated->SEq_4 * quartCalculated->SEq_4;

              // Error is cross product between estimated direction and measured direction of gravity
              ex = (ay * magCalculated->vz - az * magCalculated->vy);
              ey = (az * magCalculated->vx - ax * magCalculated->vz);
              ez = (ax * magCalculated->vy - ay * magCalculated->vx);

              if (QContObj->Ki > 0f)
              {
                QContObj->eInt[0] += ex;                                        // accumulate integral error
                QContObj->eInt[1] += ey;
                QContObj->eInt[2] += ez;
              }
              else
              {
                QContObj->eInt[0] = 0.0f;                                       // prevent integral wind up
                QContObj->eInt[1] = 0.0f;
                QContObj->eInt[2] = 0.0f;
              }

              gx = gx + QContObj->Kp * ex + QContObj->Ki * QContObj->eInt[0];   // Apply feedback terms
              gy = gy + QContObj->Kp * ey + QContObj->Ki * QContObj->eInt[1];
              gz = gz + QContObj->Kp * ez + QContObj->Ki * QContObj->eInt[2];

              // Integrate rate of change of quaternion
              pa = quartCalculated->SEq_2;
              pb = quartCalculated->SEq_3;
              pc = quartCalculated->SEq_4;
              quartCalculated->SEq_1 = quartCalculated->SEq_1 + (-quartCalculated->SEq_2 * gx - quartCalculated->SEq_3 * gy - quartCalculated->SEq_4 * gz) * (0.5f * SamplePeriod);
              quartCalculated->SEq_2 = pa + (quartCalculated->SEq_1 * gx + pb * gz - pc * gy) * (0.5f * SamplePeriod);
              quartCalculated->SEq_3 = pb + (quartCalculated->SEq_1 * gy - pa * gz + pc * gx) * (0.5f * SamplePeriod);
              quartCalculated->SEq_4 = pc + (quartCalculated->SEq_1 * gz + pa * gy - pb * gx) * (0.5f * SamplePeriod);

              // Normalise quaternion
              norm = (float32_t) Sqrt(quartCalculated->SEq_1 * quartCalculated->SEq_1 + quartCalculated->SEq_2 * quartCalculated->SEq_2 + quartCalculated->SEq_3 * quartCalculated->SEq_3 + quartCalculated->SEq_4 * quartCalculated->SEq_4);
              norm = 1.0f / norm;
              QContObj->Quaternion[0] = quartCalculated->SEq_1 * norm;
              QContObj->Quaternion[1] = quartCalculated->SEq_2 * norm;
              QContObj->Quaternion[2] = quartCalculated->SEq_3 * norm;
              QContObj->Quaternion[3] = quartCalculated->SEq_4 * norm;
           }
}

/*-----------------------------------------------------------------------------
 *      Number2String : Convert a number to a string (should be less overhead than sprintf - test performance)
 *
 *  Parameters: float32_t x, unsigned char *str, unsigned char precision
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void Number2String(float32_t x, unsigned char *str, unsigned char precision)    // Convert a number to an ascii string
{
   int ie, i, k, ndig;
   double y;
   if (precision>=7) precision=7;  else precision++;                            // max digits is 7
   ndig =   precision;
   ie = 0;
   if ( x < 0.0000f)                                                             // negative
   {
     *str++ = '-';
     x = -x;
   }
   if (x > 0.00000f) while (x < 1.00000f)                                         // less than 1
   {
     x *= 10.000f;
     ie--;
   }
   while (x >= 10.000f)                                                          // bigger than 10
   {
     x = x/10.000f;
     ie=++ie % UINT16_MAX;
   }
   ndig += ie;
   for (y = i = 1; i < ndig; i++)
   y = y/10.000f;
   x += y/2.000f;
   if (x >= 10.000f) {x = 1.000f; ie++;}                                          // between 10 and 1
   if (ie<0)
   {
     *str++ = '0'; *str++ = '.';
     if (ndig < 0) ie = ie-ndig;
     for (i = -1; i > ie; i--)  *str++ = '0';
   }
   for (i=0; i < ndig; i++)
   {
     k = x;
     *str++ = k + '0';
     if (i ==  ie ) *str++ = '.';
     x -= (y=k);
     x *= 10.000f;
   }
   *str = '\0';
}

/*-----------------------------------------------------------------------------
 *      MahonyAHRSupdate : AHRS algorithm update
 *
 *  Parameters: float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, 
 *  float32_t mx, float32_t my, float32_t mz,  COMGS_quarternion *quartCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void MahonyAHRSupdate(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, float32_t mx, float32_t my, float32_t mz,  COMGS_quarternion *quartCalculated)
{
        float32_t recipNorm;
        float32_t q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;
        float32_t hx, hy, bx, bz;
        float32_t halfvx, halfvy, halfvz, halfwx, halfwy, halfwz;
        float32_t halfex, halfey, halfez;
        float32_t qa, qb, qc;
        float32_t sampleFreq;                                                   // calculated sample frequency
        int64_t samplePeriod;

        // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
        if((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f)) {
                MahonyAHRSupdateIMU(gx, gy, gz, ax, ay, az, quartCalculated);
                return;
        }

        if (quartCalculated->mode_began == false)                                // time initialisation request
        {
          samplePeriod = -1;                                                    // initialise the timer if its the first time we called this function
          quartCalculated->mode_began = true;                                   // initialisation of time complete
        }

        calculateTimeDiff(samplePeriod, quartCalculated->lasttime);             // time in 100 ms counts from the interrupt
        samplePeriod *= 10;                                                     // in seconds
        sampleFreq = 1.0f / (float32_t) samplePeriod;                           // convert to hertz
        
        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) 
        {

           // Normalise accelerometer measurement
           recipNorm = invSqrt(ax * ax + ay * ay + az * az);
           ax *= recipNorm;
           ay *= recipNorm;
           az *= recipNorm;

           // Normalise magnetometer measurement
           recipNorm = invSqrt(mx * mx + my * my + mz * mz);
           mx *= recipNorm;
           my *= recipNorm;
           mz *= recipNorm;

           // Auxiliary variables to avoid repeated arithmetic
           q0q0 = q0 * q0;
           q0q1 = q0 * q1;
           q0q2 = q0 * q2;
           q0q3 = q0 * q3;
           q1q1 = q1 * q1;
           q1q2 = q1 * q2;
           q1q3 = q1 * q3;
           q2q2 = q2 * q2;
           q2q3 = q2 * q3;
           q3q3 = q3 * q3;

           // Reference direction of Earth's magnetic field
           hx = 2.0f * (mx * (0.5f - q2q2 - q3q3) + my * (q1q2 - q0q3) + mz * (q1q3 + q0q2));
           hy = 2.0f * (mx * (q1q2 + q0q3) + my * (0.5f - q1q1 - q3q3) + mz * (q2q3 - q0q1));
           bx = sqrt(hx * hx + hy * hy);
           bz = 2.0f * (mx * (q1q3 - q0q2) + my * (q2q3 + q0q1) + mz * (0.5f - q1q1 - q2q2));

           // Estimated direction of gravity and magnetic field
          halfvx = q1q3 - q0q2;
          halfvy = q0q1 + q2q3;
          halfvz = q0q0 - 0.5f + q3q3;
          halfwx = bx * (0.5f - q2q2 - q3q3) + bz * (q1q3 - q0q2);
          halfwy = bx * (q1q2 - q0q3) + bz * (q0q1 + q2q3);
          halfwz = bx * (q0q2 + q1q3) + bz * (0.5f - q1q1 - q2q2);
          
          // Error is sum of cross product between estimated direction and measured direction of field vectors
          halfex = (ay * halfvz - az * halfvy) + (my * halfwz - mz * halfwy);
          halfey = (az * halfvx - ax * halfvz) + (mz * halfwx - mx * halfwz);
          halfez = (ax * halfvy - ay * halfvx) + (mx * halfwy - my * halfwx);
          
          // Compute and apply integral feedback if enabled
          if(twoKi > 0.0f)
          {
             integralFBx += twoKi * halfex * (1.0f / sampleFreq);                 // integral error scaled by Ki
             integralFBy += twoKi * halfey * (1.0f / sampleFreq);
             integralFBz += twoKi * halfez * (1.0f / sampleFreq);
             gx += integralFBx;                                                   // apply integral feedback
             gy += integralFBy;
             gz += integralFBz;
          }
          else
          {
             integralFBx = 0.0f;                                                 // prevent integral windup
             integralFBy = 0.0f;
             integralFBz = 0.0f;
          }
          
          // Apply proportional feedback
          gx += twoKp * halfex;
          gy += twoKp * halfey;
          gz += twoKp * halfez;
       }
       // Integrate rate of change of quaternion
       gx *= (0.5f * (1.0f / sampleFreq));                                         // pre-multiply common factors
       gy *= (0.5f * (1.0f / sampleFreq));
       gz *= (0.5f * (1.0f / sampleFreq));
       qa = q0;
       qb = q1;
       qc = q2;
       q0 += (-qb * gx - qc * gy - q3 * gz);
       q1 += (qa * gx + qc * gz - q3 * gy);
       q2 += (qa * gy - qb * gz + q3 * gx);
       q3 += (qa * gz + qb * gy - qc * gx);

       // Normalise quaternion and write back to the structure
       recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
       q0 *= recipNorm;
       q1 *= recipNorm;
       q2 *= recipNorm;
       q3 *= recipNorm;
       quartCalculated->SEq_1 = q0;
       quartCalculated->SEq_2 = q1;
       quartCalculated->SEq_3 = q2;
       quartCalculated->SEq_4 = q3;
}

/*-----------------------------------------------------------------------------
 *      MahonyAHRSupdateIMU : AHRS algorithm update
 *
 *  Parameters: float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az,
 *  COMGS_quarternion *quartCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void MahonyAHRSupdateIMU(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, COMGS_quarternion *quartCalculated )
{
        float32_t recipNorm;
        float32_t halfvx, halfvy, halfvz;
        float32_t halfex, halfey, halfez;
        float32_t qa, qb, qc;
        int32_t samplePeriod;                                                   // calculated sample period from 100 ms interrupt timer
        float32_t sampleFreq;                                                   // calcualted frquency of iteration

        if (quartCalculated->mode_began == false)                                // time initialisation request
        {
          samplePeriod = -1;                                                    // initialise the timer if its the first time we called this function
          quartCalculated->mode_began = true;                                   // initialisation of time complete
        }

        calculateTimeDiff(samplePeriod, quartCalculated->lasttime);             // time in 100 ms counts from the interrupt
        samplePeriod *= 10;                                                     // in seconds
        sampleFreq = 1.0f / (float32_t) samplePeriod;                           // convert to hertz

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

                // Normalise accelerometer measurement
                recipNorm = invSqrt(ax * ax + ay * ay + az * az);
                ax *= recipNorm;
                ay *= recipNorm;
                az *= recipNorm;

                // Estimated direction of gravity and vector perpendicular to magnetic flux
                halfvx = q1 * q3 - q0 * q2;
                halfvy = q0 * q1 + q2 * q3;
                halfvz = q0 * q0 - 0.5f + q3 * q3;

                // Error is sum of cross product between estimated and measured direction of gravity
                halfex = (ay * halfvz - az * halfvy);
                halfey = (az * halfvx - ax * halfvz);
                halfez = (ax * halfvy - ay * halfvx);

                // Compute and apply integral feedback if enabled
                if(twoKi > 0.0f) {
                        integralFBx += twoKi * halfex * (1.0f / sampleFreq);        // integral error scaled by Ki
                        integralFBy += twoKi * halfey * (1.0f / sampleFreq);
                        integralFBz += twoKi * halfez * (1.0f / sampleFreq);
                        gx += integralFBx;        // apply integral feedback
                        gy += integralFBy;
                        gz += integralFBz;
                }
                else {
                        integralFBx = 0.0f;                                        // prevent integral windup
                        integralFBy = 0.0f;
                        integralFBz = 0.0f;
                }

                // Apply proportional feedback
                gx += twoKp * halfex;
                gy += twoKp * halfey;
                gz += twoKp * halfez;
        }

        // Integrate rate of change of quaternion
        gx *= (0.5f * (1.0f / sampleFreq));                                        // pre-multiply common factors
        gy *= (0.5f * (1.0f / sampleFreq));
        gz *= (0.5f * (1.0f / sampleFreq));
        qa = q0;
        qb = q1;
        qc = q2;
        q0 += (-qb * gx - qc * gy - q3 * gz);
        q1 += (qa * gx + qc * gz - q3 * gy);
        q2 += (qa * gy - qb * gz + q3 * gx);
        q3 += (qa * gz + qb * gy - qc * gx);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;
        quartCalculated->SEq_1 = q0;
        quartCalculated->SEq_2 = q1;
        quartCalculated->SEq_3 = q2;
        quartCalculated->SEq_4 = q3;
}

/*-----------------------------------------------------------------------------
 *      MadgwickAHRSupdateIMU : IMU algorithm update Madgwick
 *
 *  Parameters: float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az,
 *  COMGS_quarternion *quartCalculated
 *  Return:
 *         none
 *----------------------------------------------------------------------------*/
void MadgwickAHRSupdateIMU(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, COMGS_quarternion *quartCalculated)
{
        float32_t recipNorm;
        float32_t s0, s1, s2, s3;
        float32_t qDot1, qDot2, qDot3, qDot4;
        float32_t _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;
        int32_t samplePeriod;                                                   // calculated sample period from 100 ms interrupt timer
        float32_t sampleFreq;                                                   // calcualted frquency of iteration

        if (quartCalculated->mode_began == false)                                // time initialisation request
        {
          samplePeriod = -1;                                                    // initialise the timer if its the first time we called this function
          quartCalculated->mode_began = true;                                   // initialisation of time complete
        }

        calculateTimeDiff(samplePeriod, quartCalculated->lasttime);             // time in 100 ms counts from the interrupt
        samplePeriod *= 10;                                                     // in seconds
        sampleFreq = 1.0f / (float32_t) samplePeriod;                           // convert to hertz
        
        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) 
        {

           // Normalise accelerometer measurement
           recipNorm = invSqrt(ax * ax + ay * ay + az * az);
           ax *= recipNorm;
           ay *= recipNorm;
           az *= recipNorm;

            // Auxiliary variables to avoid repeated arithmetic
            _2q0 = 2.0f * q0;
            _2q1 = 2.0f * q1;
            _2q2 = 2.0f * q2;
            _2q3 = 2.0f * q3;
            _4q0 = 4.0f * q0;
            _4q1 = 4.0f * q1;
            _4q2 = 4.0f * q2;
            _8q1 = 8.0f * q1;
            _8q2 = 8.0f * q2;
            q0q0 = q0 * q0;
            q1q1 = q1 * q1;
            q2q2 = q2 * q2;
            q3q3 = q3 * q3;

            // Gradient decent algorithm corrective step
            s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
            s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
            s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
            s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
            recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3);     // normalise step magnitude
            s0 *= recipNorm;
            s1 *= recipNorm;
            s2 *= recipNorm;
            s3 *= recipNorm;

            // Apply feedback step
            qDot1 -= betaMadg * s0;
            qDot2 -= betaMadg * s1;
            qDot3 -= betaMadg * s2;
            qDot4 -= betaMadg * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;
        quartCalculated->SEq_1 = q0;                                            // set the values in the external structure
        quartCalculated->SEq_2 = q1;
        quartCalculated->SEq_3 = q2;
        quartCalculated->SEq_4 = q3;
}

//---------------------------------------------------------------------------------------------------
// AHRS algorithm update Madgwick

void MadgwickAHRSupdate(float32_t gx, float32_t gy, float32_t gz, float32_t ax, float32_t ay, float32_t az, float32_t mx, float32_t my, float32_t mz, COMGS_quarternion *quartCalculated)
{
        float32_t recipNorm;
        float32_t s0, s1, s2, s3;
        float32_t qDot1, qDot2, qDot3, qDot4;
        float32_t hx, hy;
        float32_t _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;
        int32_t samplePeriod;                                                   // calculated sample period from 100 ms interrupt timer
        float32_t sampleFreq;                                                   // calcualted frquency of iteration

        if (quartCalculated->mode_began == false)                                // time initialisation request
        {
          samplePeriod = -1;                                                    // initialise the timer if its the first time we called this function
          quartCalculated->mode_began = true;                                   // initialisation of time complete
        }

        calculateTimeDiff(samplePeriod, quartCalculated->lasttime);             // time in 100 ms counts from the interrupt
        samplePeriod *= 10;                                                     // in seconds
        sampleFreq = 1.0f / (float32_t) samplePeriod;                           // convert to hertz

        // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
        if((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f)) 
        {
            MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az, quartCalculated);
            return;
        }

        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) 
        {

           // Normalise accelerometer measurement
           recipNorm = invSqrt(ax * ax + ay * ay + az * az);
           ax *= recipNorm;
           ay *= recipNorm;
           az *= recipNorm;

           // Normalise magnetometer measurement
           recipNorm = invSqrt(mx * mx + my * my + mz * mz);
           mx *= recipNorm;
           my *= recipNorm;
           mz *= recipNorm;

          // Auxiliary variables to avoid repeated arithmetic
          _2q0mx = 2.0f * q0 * mx;
          _2q0my = 2.0f * q0 * my;
          _2q0mz = 2.0f * q0 * mz;
          _2q1mx = 2.0f * q1 * mx;
          _2q0 = 2.0f * q0;
          _2q1 = 2.0f * q1;
          _2q2 = 2.0f * q2;
          _2q3 = 2.0f * q3;
          _2q0q2 = 2.0f * q0 * q2;
          _2q2q3 = 2.0f * q2 * q3;
          q0q0 = q0 * q0;
          q0q1 = q0 * q1;
          q0q2 = q0 * q2;
          q0q3 = q0 * q3;
          q1q1 = q1 * q1;
          q1q2 = q1 * q2;
          q1q3 = q1 * q3;
          q2q2 = q2 * q2;
          q2q3 = q2 * q3;
          q3q3 = q3 * q3;

          // Reference direction of Earth's magnetic field
          hx = mx * q0q0 - _2q0my * q3 + _2q0mz * q2 + mx * q1q1 + _2q1 * my * q2 + _2q1 * mz * q3 - mx * q2q2 - mx * q3q3;
          hy = _2q0mx * q3 + my * q0q0 - _2q0mz * q1 + _2q1mx * q2 - my * q1q1 + my * q2q2 + _2q2 * mz * q3 - my * q3q3;
         _2bx = sqrt(hx * hx + hy * hy);
         _2bz = -_2q0mx * q2 + _2q0my * q1 + mz * q0q0 + _2q1mx * q3 - mz * q1q1 + _2q2 * my * q3 - mz * q2q2 + mz * q3q3;
         _4bx = 2.0f * _2bx;
         _4bz = 2.0f * _2bz;

          // Gradient decent algorithm corrective step
          s0 = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) - _2bz * q2 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q3 + _2bz * q1) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q2 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
          s1 = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q1 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + _2bz * q3 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q2 + _2bz * q0) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q3 - _4bz * q1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
          s2 = -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q2 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + (-_4bx * q2 - _2bz * q0) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q1 + _2bz * q3) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q0 - _4bz * q2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
          s3 = _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) + (-_4bx * q3 + _2bz * q1) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q0 + _2bz * q2) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q1 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
          recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3);     // normalise step magnitude
          s0 *= recipNorm;
          s1 *= recipNorm;
          s2 *= recipNorm;
          s3 *= recipNorm;

          // Apply feedback step
          qDot1 -= betaMadg * s0;
          qDot2 -= betaMadg * s1;
          qDot3 -= betaMadg * s2;
          qDot4 -= betaMadg * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;
        quartCalculated->SEq_1 = q0;                                            // set the values in the external structure
        quartCalculated->SEq_2 = q1;
        quartCalculated->SEq_3 = q2;
        quartCalculated->SEq_4 = q3;
}

//---------------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root

float invSqrt(float32_t x)
{
        float32_t halfx = 0.5f * x;
        float32_t y = x;
        uint32_t i = *(uint32_t*)&y;
        i = 0x5f3759df - (i>>1);
        y = *(float*)&i;
        y = y * (1.5f - (halfx * y * y));
        return y;
}

//
// Converting a quartenion to an Axix Angle
//
void Quarternion_to_AxisAngle( COMGS_quarternion *quartCalculated, COMGS_axis_angle *axisAngle )
{
  axisAngle->angle = 2 * acos(quartCalculated->SEq_4);
  axisAngle->x = quartCalculated->SEq_1 / (sqrt(1-quartCalculated->SEq_4));
  axisAngle->y = quartCalculated->SEq_2 / (sqrt(1-quartCalculated->SEq_4));
  axisAngle->z = quartCalculated->SEq_3 / (sqrt(1-quartCalculated->SEq_4));
}

//
// Converting an Axix Angle to quartenion
//
void AxisAngle_to_Quarternion( COMGS_quarternion *quartCalculated, COMGS_axis_angle *axisAngle )
{
   quartCalculated->SEq_1= axisAngle->x * sin(axisAngle->angle/2);
   quartCalculated->SEq_2 = axisAngle->y * sin(axisAngle->angle/2);
   quartCalculated->SEq_3 = axisAngle->z * sin(axisAngle->angle/2);
   quartCalculated->SEq_4 = cos(axisAngle->angle/2);
}

//
// Convert the quartenion to euler co-ordinates
//
void Quarternion_to_euler( COMGS_quarternion *quartCalculated, COMGS_euler_angle *eulerCoord )
{
   if (((quartCalculated->SEq_1 * quartCalculated->SEq_2) + (quartCalculated->SEq_3 * quartCalculated->SEq_4)) == 0.5)         // north pole
   {
      eulerCoord->heading = 2 * atan2(quartCalculated->SEq_1,quartCalculated->SEq_4);
      eulerCoord->bank = 0;
      eulerCoord->attitude = asin(2*quartCalculated->SEq_1*quartCalculated->SEq_2 + 2*quartCalculated->SEq_3*quartCalculated->SEq_4);
   } 
   else if (((quartCalculated->SEq_1 * quartCalculated->SEq_2) + (quartCalculated->SEq_3 * quartCalculated->SEq_4)) == -0.5)     // south pole
   {
      eulerCoord->heading = 0 - (2 * atan2(quartCalculated->SEq_1,quartCalculated->SEq_4));
      eulerCoord->bank = 0;
      eulerCoord->attitude = asin(2*quartCalculated->SEq_1*quartCalculated->SEq_2 + 2*quartCalculated->SEq_3*quartCalculated->SEq_4);
   }
   else
   {
     eulerCoord->heading = atan2(2*quartCalculated->SEq_2*quartCalculated->SEq_4-2*quartCalculated->SEq_1*quartCalculated->SEq_3 , 1 - 2*pow(quartCalculated->SEq_2,2) - 2*pow(quartCalculated->SEq_3,2));
     eulerCoord->attitude = asin(2*quartCalculated->SEq_1*quartCalculated->SEq_2 + 2*quartCalculated->SEq_3*quartCalculated->SEq_4);
     eulerCoord->bank = atan2(2*quartCalculated->SEq_1*quartCalculated->SEq_4-2*quartCalculated->SEq_2*quartCalculated->SEq_3 , 1 - 2*pow(quartCalculated->SEq_1,2) - 2*pow(quartCalculated->SEq_3,2));
   }

}

//**************************************************************************************************
//
// Euclidian: https://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm
//
//  Euler to quarternion and not via madgwick, mahony etc
//
void euler_to_Quarternion(COMGS_quarternion *quartCalculated, COMGS_euler_angle *eulerCoord)

{

  float32_t c1 = cos(eulerCoord->heading/2);                                    // The Euler angles should be in radians.
  float32_t s1 = sin(eulerCoord->heading/2);
  float32_t c2 = cos(eulerCoord->attitude/2);
  float32_t s2 = sin(eulerCoord->attitude/2);
  float32_t c3 = cos(eulerCoord->bank/2);
  float32_t s3 = sin(eulerCoord->bank/2);
  float32_t c1c2 = c1*c2;
  float32_t s1s2 = s1*s2;

  float32_t w = c1c2*c3 - s1s2*s3;
  float32_t x = c1c2*s3 + s1s2*c3;
  float32_t y = s1*c2*c3 + c1*s2*s3;
  float32_t z = c1*s2*c3 - s1*c2*s3;

  quartCalculated->SEq_4 = w;                                                   // quarternion calculated
  quartCalculated->SEq_1 = x;
  quartCalculated->SEq_2 = y;
  quartCalculated->SEq_3 = z;

}

/* detect "overvoltage" if over 4.6 V and "undervoltage" if under 0.4 V,
   detection points set outside pot range to account for resistor tolerance
   and change of value over time/temperature -- this produces "fudge zones" between 401 and 499 mV
   and between 4501 and 4599 mV that are set to PotMin and 100% respectively 
   
   Function not needed ????
   
   */
   

//                    Differential Steering Joystick Algorithm
// =============================================================================
//                           by Calvin Hass
//   https://www.impulseadventure.com/elec/
//
// Converts a single dual-axis joystick into a differential
// drive motor control, with support for both drive, turn
// and pivot operations.
//
//
// INPUTS
//int     nJoyX;              // Joystick X input                     (-128..+127)
//int     nJoyY;              // Joystick Y input                     (-128..+127)
//
// OUTPUTS
//int     nMotMixL;           // Motor (left)  mixed output           (-128..+127)
//int     nMotMixR;           // Motor (right) mixed output           (-128..+127)

void JoyStick2DiffSteer( int16_t nJoyX, int16_t nJoyY, int16_t nMotMixL, int16_t nMotMixR)
{
  // CONFIG
  // - fPivYLimt  : The threshold at which the pivot action starts
  //                This threshold is measured in units on the Y-axis
  //                away from the X-axis (Y=0). A greater value will assign
  //                more of the joystick's range to pivot actions.
  //                Allowable range: (0..+127)
  float32_t fPivYLimit = 32.0;

  // TEMP VARIABLES
  float32_t   nMotPremixL;                                                      // Motor (left)  premixed output        (-128..+127)
  float32_t   nMotPremixR;                                                      // Motor (right) premixed output        (-128..+127)
  int16_t     nPivSpeed;                                                        // Pivot Speed                          (-128..+127)
  float32_t   fPivScale;                                                        // Balance scale b/w drive and pivot    (   0..1   )

  if (nJoyY >= 0)                                                               // Calculate Drive Turn output due to Joystick X input
  {
    nMotPremixL = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);                          // Forward
    nMotPremixR = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;
  }
  else
  {
    nMotPremixL = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;                          // Reverse
    nMotPremixR = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);
  }

  nMotPremixL = nMotPremixL * nJoyY/128.0;                                      // Scale Drive output due to Joystick Y input (throttle)
  nMotPremixR = nMotPremixR * nJoyY/128.0;
                                                                                // Now calculate pivot amount
  nPivSpeed = nJoyX;                                                            // - Strength of pivot (nPivSpeed) based on Joystick X input
  fPivScale = (abs(nJoyY)>fPivYLimit)? 0.0 : (1.0 - abs(nJoyY)/fPivYLimit);     // - Blending of pivot vs drive (fPivScale) based on Joystick Y input

  nMotMixL = (1.0-fPivScale)*nMotPremixL + fPivScale*( nPivSpeed);              // Calculate final mix of Drive and Pivot
  nMotMixR = (1.0-fPivScale)*nMotPremixR + fPivScale*(-nPivSpeed);

}

// Bearing based Steering Joystick Algorithm
// ========================================
//   by Calvin Hass
//   https://www.impulseadventure.com/elec/
//
// Converts a single dual-axis joystick into a Bearing
// drive motor control, with support for both drive, turn
// and pivot operations.
//
//
// INPUTS
//int     nJoyX;              // Joystick X input                     (-128..+127)
//int     nJoyY;              // Joystick Y input                     (-128..+127)
//
// OUTPUTS
//int     nMotMixL;           // Motor (left)  mixed output           (-128..+127)
//int     nMotMixR;           // Motor (right) mixed output           (-128..+127)

void JoyStick2BearSteer( int16_t nJoyX, int16_t nJoyY, int16_t nMotMixL, int16_t nMotMixR)
{
  // CONFIG
  float32_t fPivBearLimit = 75.0;                                               // Bearing threshold for pivot action (degrees)

  // TEMP VARIABLES
  float32_t nMotPremixL;                                                        // Motor (left)  premixed output        (-128..+127)
  float32_t nMotPremixR;                                                        // Motor (right) premixed output        (-128..+127)
  int16_t nPivSpeed;                                                            // Pivot Speed                          (-128..+127)
  float32_t fPivScale;                                                          // Balance scale b/w drive and pivot    (   0..1   )
  float32_t fBearMag;

  if (nJoyY >= 0)                                                               // Calculate Drive Turn output due to Joystick X input
  {
    nMotPremixL = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);                          // Forward
    nMotPremixR = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;
  }
  else
  {
    nMotPremixL = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;                          // Reverse
    nMotPremixR = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);
  }

  nMotPremixL = nMotPremixL * nJoyY/128.0;                                      // Scale Drive output due to Joystick Y input (throttle)
  nMotPremixR = nMotPremixR * nJoyY/128.0;

  if (nJoyY == 0)
  {
    if (nJoyX == 0)                                                             // Handle special case of Y-axis=0
    {
      fBearMag = 0;
    }
    else
    {
      fBearMag = 90;
    }
  }
  else
  {
    // Bearing (magnitude) away from the Y-axis is calculated based on the
    // Joystick X & Y input. The arc-tangent angle is then converted
    // from radians to degrees.
    fBearMag = atan( (float)abs(nJoyX) / (float)abs(nJoyY) ) *90.0/(3.14159/2.0);
  }

  // Now calculate pivot amount
  // Blending of pivot vs drive (fPivScale) based on Joystick bearing
  nPivSpeed = nJoyX;
  fPivScale = (fBearMag<fPivBearLimit)? 0.0 : (fBearMag-fPivBearLimit)/(90.0-fPivBearLimit);

  nMotMixL = (1.0-fPivScale)*nMotPremixL + fPivScale*( nPivSpeed);              // Calculate final mix of Drive and Pivot
  nMotMixR = (1.0-fPivScale)*nMotPremixR + fPivScale*(-nPivSpeed);

}

//
// Another set of speed corrections from L Robbins for speed values passed to a controller from a joystick
// these can also be tested during the test phase
//
int16_t JoyStick2Speed( int16_t nJoyX, uint8_t choice )
{
    switch (choice)
    {
      case 1:
        return ((nJoyX + pow(nJoyX,2))/2);                                      // option 1
      case 2:
        return (((2*nJoyX) + pow(nJoyX,2))/3);                                  // option 2
      case 3:
        return ((nJoyX+(2*pow(nJoyX,2)))/3);                                    // option 3
      case 4:
         return (pow(nJoyX,2));                                                 // option 4
      default:
         return(nJoyX);                                                         // option not specified then return it back
    }
}

// moving averages on acceleromter data
//
void moving_average( COMGS_gyro_acc_data_t *accStruct )
{
  if ((accStruct->acc_roll <=3) && (accStruct->acc_roll >= -3))                 // test for near zero and eliminate for each axis
  {
     accStruct->acc_roll = 0;
     accStruct->zero_acc_cnt_roll++ % UINT8_MAX;
  }
  if ((accStruct->acc_yaw <=3) && (accStruct->acc_yaw >= -3))
  {
     accStruct->acc_yaw = 0;
     accStruct->zero_acc_cnt_yaw++ % UINT8_MAX;
  }
  if ((accStruct->acc_pitch <=3) && (accStruct->acc_pitch >= -3))
  {
     accStruct->acc_pitch = 0;
     accStruct->zero_acc_cnt_pitch++ % UINT8_MAX;
  }

  if (accStruct->g_count_sam!=0x40)                                             // iterate this function 64 times before writing result and reseting  (moving average filter)
  {
    accStruct->mavg_acc_roll=accStruct->mavg_acc_roll + accStruct->acc_roll;
    accStruct->mavg_acc_pitch=accStruct->mavg_acc_pitch + accStruct->acc_pitch;
    accStruct->mavg_acc_yaw=accStruct->mavg_acc_yaw + accStruct->acc_yaw;
    accStruct->g_count_sam++ %UINT8_MAX;                                        // count number of samples
  }
  else
  {
     accStruct->mavg_acc_roll = ( ((uint32_t) accStruct->mavg_acc_roll) >> 6);  // divide by 64 the sum
     accStruct->mavg_acc_pitch = ( ((uint32_t) accStruct->mavg_acc_pitch) >> 6);
     accStruct->mavg_acc_yaw = ( ((uint32_t) accStruct->mavg_acc_yaw) >> 6);

     accStruct->mavg_acc_rolls[1] = accStruct->mavg_acc_roll;                   // copy current moving average to actual moving average values stored as 1 and 2
     accStruct->mavg_acc_pitchs[1] = accStruct->mavg_acc_pitch;
     accStruct->mavg_acc_yaws[1] = accStruct->mavg_acc_yaw;
     accStruct->g_count_sam=0;                                                  // reset and start collecting another 64 samples
     accStruct->g_count_acc++ % UINT8_MAX;                                      // Rolling Counter (msg id)
  }

}

//
//  Function to calculate velocity position
//
void calculate_velocity_position( COMGS_gyro_acc_data_t *accStruct )
{

   if (accStruct->zero_acc_cnt_roll >= MAX_ZERO_ACCS)
   {
      accStruct->vel_roll[0] = 0;
      accStruct->vel_roll[1] = 0;
   }
   if (accStruct->zero_acc_cnt_pitch >= MAX_ZERO_ACCS)
   {
      accStruct->vel_pitch[0] = 0;
      accStruct->vel_pitch[1] = 0;
   }
   if (accStruct->zero_acc_cnt_yaw >= MAX_ZERO_ACCS)
   {
      accStruct->vel_yaw[0] = 0;
      accStruct->vel_yaw[1] = 0;
   }

   // 1st iteration
   accStruct->vel_roll[1] = accStruct->vel_roll[0] + accStruct->mavg_acc_rolls[0] + ((accStruct->mavg_acc_rolls[1] - accStruct->mavg_acc_rolls[0])>>1);
   accStruct->vel_pitch[1] = accStruct->vel_pitch[0] + accStruct->mavg_acc_pitchs[0] + ((accStruct->mavg_acc_pitchs[1] - accStruct->mavg_acc_pitchs[0])>>1);
   accStruct->vel_yaw[1] = accStruct->vel_yaw[0] + accStruct->mavg_acc_yaws[0] + ((accStruct->mavg_acc_yaws[1] - accStruct->mavg_acc_yaws[0])>>1);

   // 2nd iteration
   accStruct->pos_roll[1] = accStruct->pos_roll[0] + accStruct->vel_roll[0] + ((accStruct->vel_roll[1] - accStruct->vel_roll[0])>>1);
   accStruct->pos_pitch[1] = accStruct->pos_pitch[0] + accStruct->vel_pitch[0] + ((accStruct->vel_pitch[1] - accStruct->vel_pitch[0])>>1);
   accStruct->pos_yaw[1] = accStruct->pos_yaw[0] + accStruct->vel_yaw[0] + ((accStruct->vel_yaw[1] - accStruct->vel_yaw[0])>>1);

  // Copy current velocity and acceleration from previous
  accStruct->vel_roll[0] = accStruct->vel_roll[1];
  accStruct->vel_pitch[0] = accStruct->vel_pitch[1];
  accStruct->vel_yaw[0] = accStruct->vel_yaw[1];
  accStruct->mavg_acc_rolls[0] = accStruct->mavg_acc_rolls[1];
  accStruct->mavg_acc_pitchs[0] = accStruct->mavg_acc_pitchs[1];
  accStruct->mavg_acc_yaws[0] = accStruct->mavg_acc_yaws[1];
  accStruct->pos_roll[0] =  accStruct->pos_roll[1];
  accStruct->pos_pitch[0] = accStruct->pos_pitch[1];
  accStruct->pos_yaw[0] = accStruct->pos_yaw[1];

}

// Function to calculate theta
// theta (pitch)=atan2(Rx/sqrt(Ry^2+Rz^2)
// theta (roll)= atan2(Ry/sqrt(Rx^2+Rz^2)
// theta (yaw)= atan2(Rz/sqrt(Rx^2+Ry^2)
//
void calculate_theta(COMGS_gyro_acc_data_t *accStruct)
{
    accStruct->theta_yaw = atan2( (double) accStruct->mavg_acc_yaws[1], sqrt( pow((double)accStruct->mavg_acc_pitchs[1],2)+ pow((double)accStruct->mavg_acc_rolls[1],2) ) );
    accStruct->theta_roll = atan2( (double) accStruct->mavg_acc_rolls[1], sqrt( pow((double)accStruct->mavg_acc_pitchs[1],2)+ pow((double)accStruct->mavg_acc_yaws[1],2) ) );
    accStruct->theta_pitch = atan2( (double) accStruct->mavg_acc_pitchs[1], sqrt( pow((double)accStruct->mavg_acc_yaws[1],2)+ pow((double)accStruct->mavg_acc_rolls[1],2) ) );
}
// The first and third Euler angles in the sequence (phi and psi) become
// unreliable when the middle angles of the sequence (theta) approaches ±90
// degrees. This problem commonly referred to as Gimbal Lock.
// See: http://en.wikipedia.org/wiki/Gimbal_lock    -- possibly need to monitor ?

//
// remove alarm from alarm stack (called from Process_Alarm_Ack) not globally defined
//
void popAlmOffStack( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, uint8_t indexInAlmDefObj )
{
    uint8_t elementNo,elementNo2;                                               // indexs in the arrays

    if ((indexInAlmDefObj >= 9) || (indexInAlmDefObj <= 0))                     // invalid structure position for the tag number has been passed
    {
       return;                                                                  // return function use invalid
    }
    
    for(elementNo=1;elementNo<=alarmStack->numObjects;elementNo++)              // for stack start to the end, check if its in the stack
    {
      if (alarmStack->AlarmStack[elementNo] == almDesc->objectNo[indexInAlmDefObj+8]) // match was found actual id found in AlarmDefinitionObject_t offset by 8
      {
         for(elementNo2=elementNo+1;elementNo2<=alarmStack->numObjects;elementNo2++)   // from the next position down to the end of the stack
         {
           alarmStack->AlarmStack[elementNo2-1] = alarmStack->AlarmStack[elementNo2];   // remove and shift the remainder of the stack upwards
         }
         return;                                                                // excercise is complete
      }
    }
    
}

//
// Add a new alarm to the display stack  returns without a code if already in stack or invalid index is passed  (called from Process_Alarm_Ack) not globally defined
//
void pushAlmToStack( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, uint8_t positionInobj )
{
    uint8_t elementNo;                                                          // index in the array
    
    if ((positionInObj >= 9) || (positionInObj <= 0))                           // invalid structure position for the tag number has been passed
    {
       return;                                                                  // return function use invalid
    }
    
    if (alarmStack->numObjects >= 0)                                            // not first element in stack
    {
       for(elementNo=1;elementNo<=alarmStack->numObjects;elementNo++)           // for stack to the end
       {
           if (alarmStack->AlarmStack[elementNo] == almDesc->objectNo[positionInObj+8])   // check if its already in the stack if so do nothing, object id starts after descriptions (+8)
           {
              return;                                                           // so return
           }
       }
      alarmStack->AlarmStack[alarmStack->numObjects+1] = almDesc->objectNo[positionInObj+8];    // place at next position in the queue stack, object id starts after descriptions (+8)
      if (alarmStack->numObjects <= TOTAL_NO_OF_ALARMS ) alarmStack->numObjects++ % UINT8_MAX; // increment the alarm stack index
    }
    else
    {
       alarmStack->AlarmStack[1] = almDesc->objectNo[positionInObj+8];          // place at the head of the queue, object id starts after descriptions (+8)
       if (alarmStack->numObjects <= TOTAL_NO_OF_ALARMS ) alarmStack->numObjects++ %UINT8_MAX;  // increment the alarm stack index
    }
}

//
//   Display the Alarm Stack to a banner defined by the AlarmLineObject
//
void Update_Alm_Line( AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, AlarmLineObject_t *almLineObj )
{

   if (alarmStack->numObjects <= 0)                                             // nothing to display
   {
      strcpy(&almLineObj->Caption,"          \n");                              // blank display
      almLineObj->CapVisible = false;
      almLineObj->AckVisible = false;
   }
   else
   {
     almLineObj->CapVisible = true;                                             // we have an alarm make it have caption and visible acknowledge button
     almLineObj->AckVisible = true;
     almLineObj->AckActive = true;
     if ((alarmStack->AlarmStack[1] >= almDesc->objectNo[1]) && (alarmStack->AlarmStack[1] <= almDesc->objectNo[8]))
     {                                                                          // Alarm at top of stack is in range of this Alarm Definition Object passed
        if ((alarmStack->AlarmStack[1]%8) == 0)                                 // its element 8 of a definition object thats at position 1 in the stack
        {
          strcpy(&almLineObj->Caption,&almDesc->Caption[8]);                    // take description 8 as its the last one
        }
        else
        {
          strcpy(&almLineObj->Caption,&almDesc->Caption[(alarmStack->AlarmStack[1]%8)]);   // take description of index mod 8 (elements 1-7)
        }
     }
   }
}

//
//   Process the Alarm word and HMI Acknowledge button to cancel it  (called from Process_Alarm_Ack) not globally defined
//
void Process_Alarm_Ack( COMGS_error_alarm *almWord, AlarmStack_t *alarmStack, AlarmDefinitionObject_t *almDesc, AlarmLineObject_t *almLineObj )
{
   uint8_t counterAlm,ackBit;                                                   // counter to iterate each bit in the word
   
   if ( almWord->AlarmWdAck != almWord->AlarmWdAckLastScan )                    // Ack was pressed on the HMI (one-shot)
   {
     almWord->AlarmWdAckLastScan = almWord->AlarmWdAck;                         // Make the condition one shot
     almWord->AlarmTmRun = true;                                                // Start delay off (DOFF) timer
     for(ackBit=1;ackBit<=8;ackBit++)                                           // for all the bits in the word
     {                                                                          // if the ack is on call to remove from the stack
        if (almWord->AlarmWdAck & ((uint8_t) pow(2,((float64_t) ackBit))))      // the ack bit was ON
        {
           popAlmOffStack(alarmStack, almDesc, ((uint8_t) pow(2,((float64_t) ackBit)))); // remove the alarm that was acknowledged from the stack and GUI
        }
     }
   }
   if ( almWord->AlarmTmRun == true )                                           // You had an ACK pressed
   {
      almWord->AlarmTm=++almWord->AlarmTm % UINT16_MAX;                                          // Increment ACK timer
   }
   if ( almWord->AlarmTm > COMGS_ALARM_ACK_TIME )                               // ACK timer expired then clear the ACK request
   {
       almWord->AlarmWdAcked = almWord->AlarmWdAck;                             // Set the ACKED to ACK
       almWord->AlarmTm=0;                                                      // Reset the timer
       almWord->AlarmTmRun = false;                                             // Stop the timer
    }
    else
    {
        almWord->AlarmWdAcked = almWord->AlarmWdAcked&almWord->AlarmWdAck;
    }
    almWord->AlarmWdAck = almWord->AlarmWdAck^almWord->AlarmWdAcked;            // After DOFF (COMS_ALARM_ACK_TIME) remove the GUI Ack
    almWord->AlarmWdCurrent = almWord->AlarmWdCurrent^almWord->AlarmWdAck;      // Alarm is removed when Acknowledged on GUI
    
    for(counterAlm=0; counterAlm<=7; counterAlm++)                              // Now process the alarm line display and stack
    {
       if ((almWord->AlarmWdCurrent & ((uint8_t)(pow(2,((float64_t) counterAlm)))))!=0)                    // if the bit is true We have an alarm
       {
          pushAlmToStack(alarmStack,almDesc,((uint8_t) pow(2,((float64_t) counterAlm))));   // place the alarm on the stack
       }
    }
   
   Update_Alm_Line( alarmStack, almDesc, almLineObj );                          // update the chosen banner with the alarm message if its now the top of the stack
}

//
//   Check Motor States and alarm if neccessary and use HMI Acknowledge button to cancel it
//
void Process_Motor_State( COMGS_Motor_t *mtrBlk )
{
   if ( mtrBlk->ReqState != mtrBlk->LastReq )                                   // Requested change of state has been seen
   {
      mtrBlk->Changing = true;                                                  // request change of state of the motor
      mtrBlk->timeElapsed=-1;                                                   // initialise the timer
   }
   if (( mtrBlk->ReqState != mtrBlk->State ) &&  ( mtrBlk->Changing==true ))    // state mismatch and a change of state was requested
   {
       calculateTimeDiff(mtrBlk->timeElapsed, mtrBlk->lasttime);                // Calculate time difference since last function call
       mtrBlk->FTS = ((((float32_t) mtrBlk->timeElapsed)/10.0f) >= mtrBlk->TimeMax);     // Alarm if time greater than requested
   }
   else if (( mtrBlk->ReqState != mtrBlk->State ) &&  ( mtrBlk->Changing==false ))    // state mismatch and no requested change (ESTOP or TRIP FAULT or other request not us)
   {
       mtrBlk->FTS = true;                                                      // Alarm without question (immediately)
   }
   else                                                                         // motor at commanded state
   {
      mtrBlk->FTS = false;                                                      // no fail to start or stop alarm
      mtrBlk->Changing = false;                                                 // no request to change
   }
   mtrBlk->LastReq = mtrBlk->ReqState;                                          // remember last known state in order to detect a state change on HMI
}

//
// Simple Kalman function
//
void kalman_filter( COMGS_kalman *kalmanData )
{
    float32_t x_est_last = 0f;                                                  //initial values for the kalman filter
    float32_t P_last = 0f;
    float32_t Q = 0.022f;                                                       //the noise in the system
    float32_t R = 0.617f;

    float32_t K;
    float32_t P;
    float32_t P_temp;
    float32_t x_temp_est;
    float32_t x_est;
    float32_t z_measured;
    float32_t z_real = 0.5;                                                     // default for desired value (meas and desired should be passed in struct)
    float32_t sum_error_kalman = 0f;
    float32_t sum_error_measure = 0f;
    uint8_t i;
    
    z_measured = kalmanData->z_measured;                                        // the 'noisy' value we measured
    z_real = kalmanData->z_real;                                                // the ideal value we wish to measure
    
    srand(0);                                                                   // initialise random number generator

    //initialize with a measurement
    x_est_last = z_real + frand_func()*0.09f;                                   // last estimation plus random noise

    for (i=0;i<30;i++)                                                          // Repeat for 30 iterations
    {

        x_temp_est = x_est_last;                                                //do a prediction
        P_temp = P_last + Q;

        K = P_temp * (1.0/(P_temp + R));                                        //calculate the Kalman gain

        z_measured = z_real + frand_func()*0.09;                                // the real measurement plus noise

        x_est = x_temp_est + K * (z_measured - x_temp_est);                     // correction
        P = (1- K) * P_temp;

        //we have our new system
        KalmanData->x_est = x_est;                                              // write the kalman position
        sum_error_kalman += fabs(z_real - x_est);                               // sum of errors using kalman
        sum_error_measure += fabs(z_real-z_measured);                           // sum of errors in real value

        P_last = P;                                                             // update our last's
        x_est_last = x_est;

    }
    KalmanData->sum_error_measure = sum_error_measure;                          // Total error if using raw measured:
    KalmanData->sum_error_kalman = sum_error_kalman;                            // Total error if using kalman filter
    KalmanData->Diff_err = 100-(int)((sum_error_kalman/sum_error_measure)*100); // Reduction in error:

}

//
//  Calibrate (change to get a calibration object and set it back)
//  triggered by a 3xDIN being pressed simultaneously                           ((di5VCMode==1) && (di2PosHalt==1) && (di4RCMode==1))
//
//  returns 1 if too far out, returns 0 if at center
//
uint8_t CalibrateCenter( COMGS_joy_calib_t *calibObj )
{
  uint8_t counter1=0;                                                           // intialise counter 1
  uint64_t centerValx=0, centerValy=0, centerValz=0;                            // initial values set to zero (reset total for samples)

  calibObj->butState &= !0x38;                                                  // REmove the tick boxes for center from the HMI
  
  while( counter1 != 0x0400 )                                                   // sample 1024 times quicky at start of calibration (should be at center position)
  {
      centerValx =+ ADC1_Get_Sample(1);                                         // Get Raw ADC input 1 in units 0-1023
      centerValy =+ ADC1_Get_Sample(2);                                         // Get Raw ADC input 1 in units 0-1023
      centerValz =+ ADC1_Get_Sample(3);                                         // Get Raw ADC input 1 in units 0-1023
      asm nop;                                                                  // Wait for a tick
  }
  centerValx = centerValx >> 10;                                                // divide by 1024
  centerValy = centerValy >> 10;                                                // divide by 1024
  centerValz = centerValz >> 10;                                                // divide by 1024
  
  if (abs((RAW_MAX>>1)-centerValx) <= JOY_CENTER_MIN)                           // set the calibration object with the new center positions read so long as okay
  {
     calibObj->centerValx = centerValx;                                         // center position for split range if required
     calibObj->butState |= 8;                                                   // tick box for calibration state on GUI
     if (abs((RAW_MAX>>1)-centerValy) <= JOY_CENTER_MIN)
     {
        calibObj->centerValy = centerValy;                                      // center position for split range if required
        calibObj->butState |= 16;                                               // tick box for calibration state on GUI
        if (abs((RAW_MAX>>1)-centerValz) <= JOY_CENTER_MIN)
        {
           calibObj->centerValz = centerValz;                                   // center position for split range if required
           calibObj->butState |= 32;                                            // tick box for calibration state on GUI
           return(JCAL_SUCCESS);                                                // return 0 (success)
        }
     }
  }
  return(JCAL_CEN_FLT);                                                         // return an error not close enough centered
}

//
//  Calibrate MAX and ( MIN to do) for each direction
//  triggered by a 3xDIN being pressed simultaneously                           ((di5VCMode==1) && (di2PosHalt==1) && (di4RCMode==1))
//  and then pressing DIN to be at direction each time
//
//  The routine uses a while to lock the joystick controller in calibrate mode during
//  the process, use USE_CAL_UNLOCK, should you want to send updates to GUI
//  or send other messages types during calibration 
//  then iterate this function within the main body (it then no longer waits for calibration)
//  it will perform this as a paralell task
//
//  define USE_CAL_UNLOCK to unlock and allow other activity as described above
//
uint8_t CalibrateSpan( COMGS_joy_calib_t *calibObj )
{
  uint64_t counter1=0;
  uint16_t maxValx=0, maxValy=0, maxValz=0;                                     // Reset the values so they will collect new ones (when in CAL_LOCK)
  uint16_t minValx=0, minValy=0, minValz=0;
  uint16_t Valx=0, Valy=0, Valz=0;                                              // actual raw ADC 10 bit counter
  
  calibObj->butState &= 0x38;                                                   // reset tick boxes except the center calibration ones already done

#ifndef USE_CAL_UNLOCK
  while (di3Reset==1)                                                           // press button 3 to engage
  {
      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (Valx > maxValx) maxValx = Valx;                                       // take max if its higher
      if (Valx < minValx) minValx = Valx;                                       // take min if its lower
      if (abs((RAW_MAX>>1)-Valz) <= JOY_CENTER_MIN) return(JCAL_XDIR_FLT);      // return fail if we wernt in only x direction
      if (abs((RAW_MAX>>1)-Valy) <= JOY_CENTER_MIN) return(JCAL_XDIR_FLT);
      asm nop;                                                                  // Wait for a tick
      calibObj->butState |= 1;                                                  // indicate the step in calibration
  }
#else
  if ((di3Reset==1) || (calibObj->seqState == 1))                               // press button 3 the 1st time
  {

      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (abs((RAW_MAX>>1)-Valz) <= JOY_CENTER_MIN) return(JCAL_XDIR_FLT);      // return fail if we wernt in only x direction
      if (abs((RAW_MAX>>1)-Valy) <= JOY_CENTER_MIN) return(JCAL_XDIR_FLT);
      if (Valx > calibObj->maxValx) calibObj->maxValx = Valx;                   // take max if its higher
      if (Valx < calibObj->minValx) calibObj->minValx = Valx;                   // take min if its lower
      calibObj->butState |= 1;                                                  // indicate the step in calibration
      calibObj->seqState = 1;                                                   // indicate the step in calibration
      if ((calibObj->seqState == 1) && (di3Reset==0))                           // ready to change sequence step
      {
        calibObj->seqState = 2;
      }
  }
#endif

#ifndef USE_CAL_UNLOCK
  while ((di3Reset==0) && (counter1 <= 80000000))                               // wait for button or timeout  ( 1sec at 80 MHz)
  {
     counter1++ %UINT64_MAX;
     asm nop;
  }
#else
  if ((di3Reset==0) || (calibObj->seqState == 2))                               // wait for button or timeout  ( 1sec at 80 MHz)
  {
     if (calibObj->counter1 <= 0)                                               // first time in sequence
     {
        calculateTimeDiff(counter1, calibObj->lasttime);                        // store the time from global counter in obj->lasttime
        calibObj->counter1=1;                                                   // next step for initialise (wait for next press)
     }
     else
     {
        calculateTimeDiff(counter1, calibObj->lasttime);                        // calculate time difference from the interrrupt
        if (counter1 > (200))                                                   // 100 ms counts have exceeded the timeout
        {
            calibObj->counter1=0;
            return(JCAL_OOR_FLT);                                               // timeout error alarm
        }
        if ((calibObj->seqState == 2) && (di3Reset==1))                         // ready to change sequence step
        {
           calibObj->seqState = 3;
        }
     }
  }
#endif

#ifndef USE_CAL_UNLOCK
  while (di3Reset==1)                                                           // press button 3 to engage
  {
      counter1=0;
      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (Valy > maxValy) maxValy = Valy;                                       // take max if its higher
      if (Valy < minValy) minValy = Valy;                                       // take min if its lower
      if (abs((RAW_MAX>>1)-Valz) <= JOY_CENTER_MIN) return(JCAL_YDIR_FLT);      // return fail if we wernt in only y direction
      if (abs((RAW_MAX>>1)-Valx) <= JOY_CENTER_MIN) return(JCAL_YDIR_FLT);
      asm nop;                                                                  // Wait for a tick
      calibObj->butState |= 2;                                                  // indicate the step in calibration
  }
#else
  if ((di3Reset==1) || (calibObj->seqState == 3))                               // button 3 pressed and in step 3 of calibration
  {
      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (abs((RAW_MAX>>1)-Valz) <= JOY_CENTER_MIN) return(JCAL_YDIR_FLT);      // return fail if we wernt in only x direction
      if (abs((RAW_MAX>>1)-Valx) <= JOY_CENTER_MIN) return(JCAL_YDIR_FLT);
      if (Valy > calibObj->maxValy) calibObj->maxValy = Valy;                   // take max if its higher
      if (Valy < calibObj->minValy) calibObj->minValy = Valy;                   // take min if its lower
      calibObj->butState |= 2;                                                  // indicate the step in calibration
      if ((calibObj->seqState == 3) && (di3Reset==0))                           // ready to change sequence step
      {
        calibObj->seqState = 4;                                                 // advance to next step in calibration
      }
  }
#endif

#ifndef USE_CAL_UNLOCK
  while ((di3Reset==0) && (counter1 <= 80000000))                               // wait for button or timeout
  {
     counter1=++counter1 % UINT64_MAX;
     asm nop;
  }
#else
  if ((di3Reset==0) || (calibObj->seqState == 4))                               // wait for button or timeout  ( 1sec at 80 MHz)
  {
     if (calibObj->counter1 == 1)                                               // second time to check the global 100ms timer
     {
        calculateTimeDiff(counter1, calibObj->lasttime);                        // store the time from global counter in obj->lasttime
        calibObj->counter1=0;
     }
     else
     {
        calculateTimeDiff(counter1, calibObj->lasttime);                        // calculate time difference from the interrrupt
        if (counter1 > (200))                                                   // 100 ms counts have exceeded the timeout
        {
            calibObj->counter1=0;
            return(JCAL_TIME_FLT);                                              // timeout error alarm
        }
        if ((calibObj->seqState == 4) && (di3Reset==1))                         // ready to change sequence step
        {
           calibObj->seqState = 5;                                              // advance to step 5 of the calibration
        }
     }
  }
#endif

#ifndef USE_CAL_UNLOCK
  while (di3Reset==1)                                                           // press button 3 to engage
  {
      counter1=0;
      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (Valz > maxValz) maxValz = Valz;                                       // take max if its higher
      if (Valz < minValz) minValz = Valz;                                       // take min if its lower
      if (abs((RAW_MAX>>1)-Valy) <= JOY_CENTER_MIN) return(JCAL_ZDIR_FLT);      // return fail if we wernt in only z direction
      if (abs((RAW_MAX>>1)-Valx) <= JOY_CENTER_MIN) return(JCAL_ZDIR_FLT);
      asm nop;                                                                  // Wait for a tick
      calibObj->butState |= 4;                                                  // indicate the step in calibration
  }
  
  if (abs(RAW_MAX - maxValx) > 400) return(JCAL_OOR_FLT);                       // out of expected range
  if (abs(RAW_MAX - maxValy) > 400) return(JCAL_OOR_FLT);                       // out of expected range
  if (abs(RAW_MAX - maxValz) > 400) return(JCAL_OOR_FLT);                       // out of expected range
  if (abs(RAW_MAX - minValx) > 400) return(JCAL_OOR_FLT);                       // out of expected range
  if (abs(RAW_MAX - minValy) > 400) return(JCAL_OOR_FLT);                       // out of expected range
  if (abs(RAW_MAX - minValz) > 400) return(JCAL_OOR_FLT);                       // out of expected range

  if ((calibObj->butState&7) && (di3Reset==0) )                                 // saw all steps in the calibration sequence and finished
  {
      calibObj->maxValx = maxValx;                                              // set the values to be used in calibration to the cal object
      calibObj->maxValy = maxValy;
      calibObj->maxValz = maxValz;
      calibObj->minValx = minValx;
      calibObj->minValy = minValy;
      calibObj->minValz = minValz;
      return(JCAL_SUCCESS);                                                     // success
  }
  else
  {
      return(JCAL_STEP_FLT);                                                    // incomplete steps
  }
#else
  if ((di3Reset==1) || (calibObj->seqState == 5))                               // button pressed and in step 5 of calibration
  {
      Valx = ADC1_Get_Sample(1);                                                // Get Raw ADC input 1 in units 0-1024
      Valy = ADC1_Get_Sample(2);                                                // Get Raw ADC input 2 in units 0-1024
      Valz = ADC1_Get_Sample(3);                                                // Get Raw ADC input 2 in units 0-1024
      if (abs((RAW_MAX>>1)-Valy) <= JOY_CENTER_MIN) return(JCAL_ZDIR_FLT);      // return fail if we wernt in only x direction
      if (abs((RAW_MAX>>1)-Valx) <= JOY_CENTER_MIN) return(JCAL_ZDIR_FLT);
      if (Valz > calibObj->maxValz) calibObj->maxValz = Valz;                   // take max if its higher
      if (Valz < calibObj->minValz) calibObj->minValz = Valz;                   // take min if its lower
      calibObj->butState |= 4;                                                  // indicate the step in calibration
      if ((calibObj->seqState == 5) && (di3Reset==0))                           // ready to change sequence step
      {
        calibObj->seqState = 6;                                                 // advance to next step in calibration
        if (calibObj->butState&7)                                               // saw all steps in the calibration sequence
        {
           return(JCAL_SUCCESS);                                                // success
        }
        else
        {
           return(JCAL_STEP_FLT);                                               // incomplete steps
        }
      }
  }
#endif

}

#ifdef PID_LOOPS                                                                // PID Loop library included
//
//
// PID Block :: Returns 1 on success 0 on error (no integral time found or manual mode set)
//
//
uint8_t computePID( pidBlock_t *pidObj )                                        //compute(int32_t setpoint, int32_t feedback)  kp ki kd  lastError   lasttime output
{

  uint64_t sampleTime=0;
  int32_t curError;
  float32_t pTerm;
  float32_t iTerm;
  float32_t dTerm;

  if (pidObj->mode_began == false)                                              // time initialisation request
  {
     sampleTime = -1;                                                           // initialise the timer
     pidObj->mode_began = true;                                                 // initialisation of time complete
  }
  
  //   curTime = micros();                                                      get the current time in micro seconds from the interrupt counter !!!
  calculateTimeDiff(sampleTime, pidObj->lasttime);                              // time in 100 ms counts from the interrupt

  if ((sampleTime <= 0) || (pidObj->mode_hld==true))                            // no time difference or hold then exit
  {
     return(0);                                                                 // exit function its the first time or interrupt not working  would give divide by zero error (exit)
  }

  sampleTime /= 10;                                                             // time in second interrupt is 100 ms counts
  if (pidObj->mode_rem == false)                                                // in local
  {
    curError = pidObj->setpoint - pidObj->feedback;                             // find the error between measurement feedback and requested value
  }
  else                                                                          // in remote
  {
    curError = pidObj->rem_setpoint - pidObj->feedback;                         // find the error between measurement feedback and requested value
    if (pidObj->mode_bmp == true)                                               // if bumpless on let local spt track remote
    {
       pidObj->setpoint =  pidObj->rem_setpoint;
    }
  }
  pTerm = pidObj->kp * curError;                                                /* proportional term computation */

  if(pidObj->ki == 0.0)                                                         /* integral term computation */
    integral = 0;
  else {
    int32_t lastIntegral;
    iTerm = pidObj->ki * curError * sampleTime;                                 // calculate itegral

    if(iTerm > 0)
      iTerm  = ceil(iTerm);                                                     // round up
    else
      iTerm  = floor(iTerm);                                                    // round down

    if(iTermMax != 0 &&(iTerm > iTermMax))                                      /* Check iTerm bound */
      iTerm = iTermMax;
    if(iTermMin != 0 &&(iTerm < iTermMin))
      iTerm = iTermMin;
    lastIntegral = integral;
    integral += iTerm;

    if(integral > 0 && iTerm < 0 && lastIntegral < 0)                           /* Check  integral overflow  */
      integral = INT32_MIN;
    else if(integral < 0 && iTerm > 0 && lastIntegral > 0)
      integral = INT32_MAX;

    if(integralMax != 0 &&(integral > integralMax))                             /* Check integral bound */
      integral = integralMax;

    if(integralMin != 0 &&(integral < integralMin))
      integral = integralMin;

    lastIntegral = integral;                                                    // remember last
    integral += iTerm;                                                          // integrate
  }

  dTerm = pidObj->kd * (curError - pidObj->lastError) / sampleTime;             /* differential term computation */

  pidObj->lastError = curError;                                                 // save last error calculation

  if (((pidObj->mode_man==false) && (pidObj->mode_trk==false)) && (pidObj->mode_init==false))
  {                                                                             // dont write output in manual state or track is on
    pidObj->output = ROUND((pTerm + integral + dTerm),0);                       // return the output for the PID loop
    return (1);                                                                 // return success
  }
  else if (pidObj->mode_trk==true)                                              // track means set output to measurement
  {
    pidObj->output = pidObj->feedback;
  }
  else if (pidObj->mode_init==true)                                             // initialise sets output to specified value or 0 (reset function)
  {
      pidObj->output = pidObj.initVal;                                          // can be set to the downstream output when in remote or to zero for reset function
  }
}

//
//  PWMinit : initialize pwm  (returns pwm_period for set function)
//
uint16_t PWMinit(uint32_t freq_hz, uint16_t enable_channel_x, uint16_t timer_prescale, uint16_t use_timer_x)
{
     CHECON = 0x32;                                                             // cache control sfr : 2 wait states & enable predictive prefetch for cacheable and non-cacheable
     // set up your i/o
     //   AD1PCFG = 0xFFFF;                          // Configure AN pins as digital I/O
     //TRISB = 0xFFFF;                            // configure PORTB pins as input
     //PORTD = 0;                                 // set PORTD to 0
     //TRISD = 0;                                 // designate PORTD pins as output
     return( PWM_Init(freq_hz , enable_channel_x, timer_prescale, use_timer_x) );  // return the calculated timer period or 0xFFFF for error
}
//
// start pwm
//
void PWMStart( uint16_t channel )
{
   PWMStart(channel);
}
//
// stop pwm
//
void PWMStop( uint16_t channel )
{
   PWMStop(channel);
}
//
// set pwm from PID output  takes pidObj.output and pwmperiod from the Init function and the channel for output
//
void PWMSetDuty( uint16_t pid_out, uint16_t pwm_period, uint16_t channel )
{
   if (pid_out > pwm_period)                                                    // if we increase current_duty greater then possible pwm_period1 value
   {
      pid_out = 0;                                                              // reset current_duty value to zero
   }
   if (pwm_period != 0xFFFF)
   {
      PWM_Set_Duty(pid_out, channel);                                           // set newly acquired duty ratio
   }
}
//
// DC Servo Motor Application using above PID  can be speed or angle/position
//
void DcMotorPIDloop(pidBlock_t *pidObj, uint16_t pwm_period, uint16_t channel)
{
  uint32_t feedback;

  switch (channel)                                                              // Depending on output channel decide which loop it is and choose measurement
  {
     case 1:
     //feedback = getEncoderPosition();                                          as you read it
     break;
     case 2:
     //feedback = getSpeed();
     break;
  }
  pidObj->feedback = feedback;                                                  // make feedback what you measured
  while(!computePID(pidObj));                                                   // do the PID loop more than once and calculate PWM width

  if(pidObj->output >= 0)                                                       // computed pid output is pwm
    PWMSetDuty( pidObj->output, pwm_period, channel );

}
//
// tunePID : Auto a PID using ziegler nicholls method
//
uint8_t tunePID( pidBlock_t *pidObj, pidTuner_t *pidTuner )
{
  // Useful information on the algorithm used (Ziegler-Nichols method/Relay method)
  // http://www.processcontrolstuff.net/wp-content/uploads/2015/02/relay_autot-2.pdf
  // https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method
  // https://www.cds.caltech.edu/~murray/courses/cds101/fa04/caltech/am04_ch8-3nov04.pdf

  // Basic explanation of how this works:
  //  * Turn on the output of the PID controller to full power
  //  * Wait for the output of the system being tuned to reach the target input value
  //      and then turn the controller output off
  //  * Wait for the output of the system being tuned to decrease below the target input
  //      value and turn the controller output back on
  //  * Do this a lot
  //  * Calculate the ultimate gain using the amplitude of the controller output and
  //      system output
  //  * Use this and the period of oscillation to calculate PID gains using the
  //      Ziegler-Nichols method

  uint64_t microseconds=0,t1,t2;
  float32_t ku;
  float32_t tu;
  float32_t tHigh;
  float32_t tLow;
  float32_t max=0,maxOutput=100;
  float32_t min=100,minOutput=0;
  float32_t kpConstant, tiConstant, tdConstant;
  float32_t pAverage=0, iAverage=0, dAverage=0;
  uint8_t i=0;
  float32_t loopInterval;

  float32_t deltaT;

  if (pidTuner->init == false)                                                  // first use of the tuner (reset from top level)
  {
     i = 0;                                                                     // Cycle counter
     pidObj->mode_man = false;                                                  // Control state is manual set the output to max
     pidObj->output = 100;                                                      // set output to start at maximum
     t1 = t2 = g_timer5_counter;                                                // Times used for calculating period
     tHigh = tLow = 0;
     max = 0;                                                                   // Max input
     min = 100;                                                                 // Min input
     pAverage = iAverage = dAverage = 0;
     pidTuner->init = true;
     pidObj->mode_hld = false;                                                  // Control state is not hold
     return(0);                                                                 // Return without completion
  }
  else
  {
     pidObj->mode_man = true;                                                   // Put the PID back to auto after setting it to max in the first iteration
  }
  
  calculateTimeDiff(microseconds, pidObj->lasttime);                            // calculate a time difference from last time
  if (microseconds >= 0)
  {
    deltaT = microseconds;
    loopinterval = deltaT;                                                      // make loop interval the time since last time
  }
  else
  {
     return(0);                                                                 // first time initialise wait til next iteration
  }

  if (max <= pidObj->output)                                                    // Calculate max and min
          max = pidObj->output;
  else if (min >= pidObj->output)
          min = pidObj->output;

  minOutput - min;
  maxOutput = max;

  if (!(pidObj->mode_hld) && (pidObj->output > pidTuner->targetInputValue))     // Output is on and input signal has risen to target
  {
    // Turn output off, record current time as t1, calculate tHigh, and reset maximum
    pidObj->mode_hld = true;
    pidObj->output = minOutput;                                                 // pid output
    t1 = g_timer5_counter;
    if ( t2 > t1 )                                                              // Overrun of iterrrupt 100 ms counts
    {
       tHigh = (UINT64_MAX - t2) + t1;                                          // Calculate difference in time
    }
    else
    {
       tHigh = (t1 - t2);                                                       // Calculate difference in time
    }
    // tHigh = t1 - t2;
    max = pidTuner->targetInputValue;
  }

  // Output is off and input signal has dropped to target
  if ((pidObj->mode_hld==true) && pidObj->output < pidTuner->targetInputValue)
  {
    pidObj->mode_hld = false;                                                   // Turn output on (donthold), record current time as t2, calculate tLow
    pidObj->output = maxOutput;
    t2 = g_timer5_counter;
        if ( t1 > t2 )                                                          // Overrun
    {
       tLow = (UINT64_MAX - t1) + t2;                                           // Calculate difference in time
    }
    else
    {
       tLow = (t2 - t1);                                                        // Calculate difference in time
    }
    //tLow = t2 - t1;

    // Calculate Ku (ultimate gain)
    // Formula given is Ku = 4d / pa
    // d is the amplitude of the output signal
    // a is the amplitude of the input signal
    ku = (4.0 * ((maxOutput - minOutput) / 2.0)) / (PI * (max - min) / 2.0);    // calculated gain

    // Calculate Tu (period of output oscillations)
    tu = (tLow * 100) + (tHigh * 100);                                          // calculated in milliseconds

    // How gains are calculated
    // PID control algorithm needs Kp, Ki, and Kd
    // Ziegler-Nichols tuning method gives Kp, Ti, and Td
    //
    // Kp = 0.6Ku = Kc
    // Ti = 0.5Tu = Kc/Ki
    // Td = 0.125Tu = Kd/Kc
    //
    // Solving these equations for Kp, Ki, and Kd gives this:
    //
    // Kp = 0.6Ku
    // Ki = Kp / (0.5Tu)
    // Kd = 0.125 * Kp * Tu

    // Constants
    // https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method


    if (pidTuner->znMode == ZNModeBasicPID)                                     // modes of tune set in the tune object
    {
      kpConstant = 0.6;
      tiConstant = 0.5;
      tdConstant = 0.125;
    } else if (pidTuner->znMode == ZNModeLessOvershoot)
    {
      kpConstant = 0.33;
      tiConstant = 0.5;
      tdConstant = 0.33;
    } else if (pidTuner->znMode == ZNModeNoOvershoot)
    {
      kpConstant = 0.2;
      tiConstant = 0.5;
      tdConstant = 0.33;
    }

    pidObj->kp = kpConstant * ku;                                               // Calculate gains
    pidObj->ki = (pidObj->kp / (tiConstant * tu)) * loopInterval;
    pidObj->kd = (tdConstant * pidObj->kp * tu) / loopInterval;

    if (i > 1) {
      pAverage += pidObj->kp;                                                   // Average all gains after the first two cycles
      iAverage += pidObj->ki;
      dAverage += pidObj->kd;
    }

    min = pidTuner->targetInputValue;                                           // Reset minimum

    i ++ % UINT8_MAX;                                                           // Increment cycle count
  }

  // If loop is done, disable output and calculate averages
  if (i >= pidTuner->cycles)
  {
     pidObj->mode_hld = true;
     pidObj->output = minOutput;
     pidObj->kp = pAverage / (i - 1);
     pidObj->ki = iAverage / (i - 1);
     pidObj->kd = dAverage / (i - 1);
     return (1);                                                                // return complete state
  }
  return(0);                                                                    // wait until all cycles done iteration is from top loop

}
#endif                                                                          // end of PID loops library

#ifdef ROBOT_HELPER                                                             // include the robot helper
/////////------------------- Robotics helpers ----------------------------------
//////// Refer open source project https://github.com/Nate711/Doggo/blob/7f2044c0d9b43046c2e013c68de10a85d67f346c/src/position_control.cpp
///////  NATE 711 Doggo

/**
* Takes the leg parameters and returns the gamma angle (rad) of the legs
*/
#include "odrive.h"                                                             // communication helper library

void readJoyStickInput( uint16_t xIn, uint16_t yIn, uint16_t rotation, roboMtr_t *MotorType )
{

  MotorType->kFrontLeft_val = xIn + yIn + rotation;
  MotorType->kFrontRight_val = -xIn + yIn - rotation;
  MotorType->kRearLeft_val = -xIn + yIn + rotation;
  MotorType->kRearRight_val = xIn + yIn - rotation;

}

/**
 * XOR operation on first 8 bits and last 8 bits of a short
 * @param  val  The short to work on
 * @return      Byte resulting from xoring the first 8 and last 8 bits.
 */
uint8_t XorShort(int16_t val) 
{
    uint8_t v0 = val & 0xFFU;
    uint8_t v1 = (val >> 8U) & 0xFFU;
    return v0 ^ v1;
}
/**
* Parses the encoder position message and stores positions as counts
* Assumes the message is in format "<1><6><'P'><short1><short2><checksum>"
* This would greatly improve the noise on the Kd term!!
* @param msg    String: Message to parse
* @param th     float&: Output parameter for theta reading
* @param ga     float&: Output parameter for gamma reading
* @return       int:    1 if success, -1 if failed to find get full message or checksum failed
*/

int16_t ParseDualPosition(char* msg, int16_t len, float32_t  *th, float32_t *ga) 
{
    uint8_t rcvdCheckSum;
    uint16_t th_16;
    uint16_t ga_16;
    uint8_t checkSum;
    
    // check if 1 byte for "P", 4 bytes holding encoder data, and 1 checksum byte were received
    if (len != 6U) 
    {
        return -1; // return -1 to indicate that the message length was wrong
    } 
    else if (msg[0U] == 'P' )
    {
        // retrieve short from byte stream
        // remember that the first character is 'P'
        th_16 = (msg[2U] << 8U) | msg[1U];
        ga_16 = (msg[4] << 8) | msg[3];
        rcvdCheckSum = msg[5U];

        // compute checksum
        checkSum = 0U;
        checkSum ^= msg[0U];                                                    // letter 'P'
        checkSum ^= msg[1U];
        checkSum ^= msg[2U];
        checkSum ^= msg[3U];
        checkSum ^= msg[4U];

        // if the computed and received check sums match then update the motor position variables
        if (checkSum == rcvdCheckSum) 
        {
            *th = ((float32_t)th_16)/1000.0f;
            *ga = ((float32_t)ga_16)/1000.0f;
            return 1;
        } 
        else 
        {
            // return -1 to indicate that the checksums didn't match
            return -1;
        }
    }
    else
    {
       return -1;
    }
    return 1;
}

int16_t ParseMtrStateReply(char* msg)
{
    char * pch = NULL;
    uint8_t numOfStr = 0U;                                                      // number of strings found
    uint8_t axisState;                                                          // axis state readback
    
    if (!strncmp(msg,"r axis",6U))                                              // message is r axis
    {
       pch = strtok (msg," ");                                                  // split it by space
       while (pch != NULL)
       {
          numOfStr = ++numOfStr % UINT8_MAX;
          if (numOfStr==3U)                                                     // 3rd value on the line
          {
              axisState=atoi(pch);
              if ((axisState<=AXIS_STATE_UNDEFINED) && (axisState>=AXIS_STATE_CLOSED_LOOP_CONTROL))
                  return axisState;
              else
                  return -1;
              pch = strtok (NULL, " ");                                         // get the next string in the line
          }
       }
    }
    return -1;
}

void SetPositionUsingGains( LegGain *gains, char* sndBuf )
{
    const int POS_MULTIPLIER = 1000U;
    const int GAIN_MULTIPLIER = 100U;
    ODRIVE_SndPkt_t odrive;
    uint8_t checkSum;

    int16_t kp_theta_16;
    int16_t kd_theta_16;
    int16_t kp_gamma_16;
    int16_t kd_gamma_16;

    kp_theta_16 = (gains->kp_theta * GAIN_MULTIPLIER);
    kd_theta_16 = (gains->kd_theta * GAIN_MULTIPLIER);
    kp_gamma_16 = (gains->kp_gamma * GAIN_MULTIPLIER);
    kd_gamma_16 = (gains->kd_gamma * GAIN_MULTIPLIER);

    // Calculate the checksum based on the 2 current value shorts
    checkSum = 'S';                                                             // what it was spec says a crc8 ? of CRC8 of bytes 0 and 1
    checkSum ^= XorShort(kp_theta_16);
    checkSum ^= XorShort(kd_theta_16);
    checkSum ^= XorShort(kp_gamma_16);
    checkSum ^= XorShort(kd_gamma_16);

    // Command in Stream format
    odrive.Stx = 0xAAU;                                                         // send start byte 0xAA
    odrive.Length=14U;                                                          // payload length
    odrive.CRC8='S';                                                            // dual current command
    odrive.dataV[0U]=kp_theta_16;
    odrive.dataV[1U]=kd_theta_16;
    odrive.dataV[2U]=kp_gamma_16;
    odrive.dataV[3U]=kd_gamma_16;
    odrive.checksum=checkSum;

    memcpy(sndBuf, &odrive, sizeof(odrive));
    memset(sndBuf+(odrive.Length+1U),(void *) '\0',sizeof(char));
}

void SetThetaGamma(float32_t theta, float32_t gamma, char *sndBuf)
{
    const int MULTIPLIER = 1000;
    ODRIVE_SndPkt_t odrive;
    int16_t theta_16;
    int16_t gamma_16;
    uint8_t checkSum;

    // Calculate the checksum based on the 2 current value shorts
    checkSum = 'P';
    theta_16 = (theta * MULTIPLIER);
    gamma_16 = (gamma * MULTIPLIER);
    checkSum ^= XorShort(theta_16);
    checkSum ^= XorShort(gamma_16);

    // Send off bytes
    odrive.Stx = 0xAAU;                                                         // send start byte 0xAA
    odrive.Length=14U;                                                          // payload length
    odrive.CRC8='S';                                                            // dual current command
    odrive.dataV[0U]=theta_16;
    odrive.dataV[1U]=gamma_16;
    odrive.checksum=checkSum;

    memcpy(sndBuf, &odrive, sizeof(odrive));
    memset(sndBuf+(odrive.Length+1U),(void *) '\0',sizeof(char));
}


void SetCoupledPosition(float32_t sp_theta, float32_t sp_gamma, LegGain *gains, char *sndBuf)
{

    const int POS_MULTIPLIER = 1000U;
    const int GAIN_MULTIPLIER = 100U;
    uint8_t checkSum;
    int16_t sp_theta_16;
    int16_t kp_theta_16;
    int16_t kd_theta_16;
    int16_t sp_gamma_16;
    int16_t kp_gamma_16;
    int16_t kd_gamma_16;
    ODRIVE_SndPkt_t odrive;

    sp_theta_16 = (sp_theta * POS_MULTIPLIER);
    kp_theta_16 = (gains->kp_theta * GAIN_MULTIPLIER);
    kd_theta_16 = (gains->kd_theta * GAIN_MULTIPLIER);

    sp_gamma_16 = (sp_gamma * POS_MULTIPLIER);
    kp_gamma_16 = (gains->kp_gamma * GAIN_MULTIPLIER);
    kd_gamma_16 = (gains->kd_gamma * GAIN_MULTIPLIER);

    // Calculate the checksum based on the 2 current value shorts
    checkSum = 'S';
    checkSum ^= XorShort(sp_theta_16);
    checkSum ^= XorShort(kp_theta_16);
    checkSum ^= XorShort(kd_theta_16);
    checkSum ^= XorShort(sp_gamma_16);
    checkSum ^= XorShort(kp_gamma_16);
    checkSum ^= XorShort(kd_gamma_16);

    // Send off bytes
    odrive.Stx = 0xAAU;                                                         // send start byte 0xAA
    odrive.Length=14U;                                                          // payload length
    odrive.CRC8='S';                                                            // dual current command
    odrive.dataV[0U]=sp_theta_16;
    odrive.dataV[1U]=kp_theta_16;
    odrive.dataV[2U]=kd_theta_16;
    odrive.dataV[3U]=sp_gamma_16;
    odrive.dataV[4U]=kp_gamma_16;
    odrive.dataV[5U]=kd_gamma_16;
    odrive.checksum=checkSum;

    memcpy(sndBuf, &odrive, sizeof(odrive));
    memset(sndBuf+(odrive.Length+1U),(void *) '\0',sizeof(char));
}

void GetGamma(float32_t L, float32_t theta, float32_t gamma) 
{
    float32_t L1 = 0.09f;                                                       // upper leg length (m)

    float32_t L2 = 0.162f;                                                      // lower leg length (m)

    float32_t cos_param = (pow(L1,2.0f) + pow(L,2.0f) - pow(L2,2.0f)) / (2.0f*L1*L);

    if (cos_param < -1.0f)
    {
        gamma = PI;
    } 
    else if (cos_param > 1.0f)
    {
        gamma = 0f;
    }
    else
    {
        gamma = acos(cos_param);
    }

}

/**
* Converts the leg params L, gamma to cartesian coordinates x, y (in m)
* Set x_direction to 1.0 or -1.0 to change which direction the leg walks
*/
void LegParamsToCartesian(float32_t L, float32_t theta, float32_t leg_direction, float32_t x, float32_t y)
{
    x = leg_direction * L * cos(theta);
    y = L * sin(theta);
}

/**
* Converts the cartesian coords x, y (m) to leg params L (m), theta (rad)
*/
void CartesianToLegParams(float32_t x, float32_t y, float32_t leg_direction, float32_t L, float32_t theta)
{
    L = pow((pow(x,2.0f) + pow(y,2.0f)), 0.5f);
    theta = atan2(leg_direction * x, y);
}

void CartesianToThetaGamma(float32_t x, float32_t y, float32_t leg_direction, float32_t theta, float32_t gamma)
{
    float32_t L = 0.0f;

    CartesianToLegParams(x, y, leg_direction, L, theta);
    GetGamma(L, theta, gamma);
}


/**
* Sinusoidal trajectory generator function with flexibility from parameters described below. Can do 4-beat, 2-beat, trotting, etc with this.
*/
void SinTrajectory (float32_t t, GaitParams params, float32_t gaitOffset, float32_t x, float32_t y)
{
    static float32_t p = 0;
    static float32_t prev_t = 0;
    float32_t gp;

    float32_t stanceHeight = params.stance_height;
    float32_t downAMP = params.down_amp;
    float32_t upAMP = params.up_amp;
    float32_t flightPercent = params.flight_percent;
    float32_t stepLength = params.step_length;
    float32_t FREQ = params.freq;
    float32_t powof;

    p += FREQ * (t - prev_t < 0.5 ? t - prev_t : 0);                            // should reduce the lurching when starting a new gait
    prev_t = t;

    //float gp = fmod((p+gaitOffset),1.0);                                      // mod(a,m) returns remainder division of a by m
    powof=1.0f;
    gp = modf((p+gaitOffset),&powof);
    
    if (gp <= flightPercent) 
    {
        x = (gp/flightPercent)*stepLength - stepLength/2.0f;
        y = -upAMP*sin(PI*gp/flightPercent) + stanceHeight;
    }
    else 
    {
        float percentBack = (gp-flightPercent)/(1.0f-flightPercent);
        x = -percentBack*stepLength + stepLength/2.0f;
        y = downAMP*sin(PI*percentBack) + stanceHeight;
    }
}

uint8_t IsValidGaitParams(GaitParams params)
{

    const float32_t maxL = 0.25f;
    const float32_t minL = 0.08f;

    float32_t stanceHeight = params.stance_height;
    float32_t downAMP = params.down_amp;
    float32_t upAMP = params.up_amp;
    float32_t flightPercent = params.flight_percent;
    float32_t stepLength = params.step_length;
    float32_t FREQ = params.freq;

    if (stanceHeight + downAMP > maxL || sqrt(pow(stanceHeight, 2) + pow(stepLength / 2.0, 2)) > maxL) 
    {
        return false;
    }
    if (stanceHeight - upAMP < minL) 
    {
        return false;
    }
    if (flightPercent <= 0 || flightPercent > 1.0) 
    {
        return false;
    }
    if (FREQ < 0) 
    {
        return false;
    }
    if (FREQ > 10.0)
    {
        return false;
    }
    return true;
}


uint8_t IsValidLegGain(LegGain gains)
{
    uint8_t bad =  gains.kp_theta < 0 || gains.kd_theta < 0 ||                  // check for unstable gains
                gains.kp_gamma < 0 || gains.kd_gamma < 0;

    if (bad) {
        return false;
    }

    bad = bad || gains.kp_theta > 320 || gains.kd_theta > 10 ||                 // check for instability / sensor noise amplification
                 gains.kp_gamma > 320 || gains.kd_gamma > 10;

    if (bad) {                                                                  // too high
        return false;
    }

    bad = bad || (gains.kp_theta > 200 && gains.kd_theta < 0.1);                // check for underdamping -> instability
    bad = bad || (gains.kp_gamma > 200 && gains.kd_gamma < 0.1);

    if (bad) {                                                                  // underdamped
        return false;
    }
    return true;
}

void CommandAllLegs(float theta, float32_t gamma, LegGain *gains, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4) 
{

    char * odriveMsg = NULL;
    //odrv0Interface.SetCoupledPosition(theta, gamma, gains);
    //ODRIVE_API_SetPositionStr(odriveMsg, 0U, theta, gamma, 0U )                 // for motor number 0
    SetCoupledPosition(theta, gamma, gains, odriveMsg);
    UART1_write_text(odriveMsg);                                                // write the message to port 1
    //odrv1Interface.SetCoupledPosition(theta, gamma, gains);
    //ODRIVE_API_SetPositionStr(odriveMsg, 1U, theta, gamma, 0U )                 // for motor number 1
    SetCoupledPosition(theta, gamma, gains, odriveMsg);
    UART1_write_text(odriveMsg);                                                // write the message to port 2
    //odrv2Interface.SetCoupledPosition(theta, gamma, gains);
    //ODRIVE_API_SetPositionStr(odriveMsg, 2U, theta, gamma, 0U )                 // for motor number 2
    SetCoupledPosition(theta, gamma, gains, odriveMsg);
    UART1_write_text(odriveMsg);                                                // write the message to port 3
    //odrv3Interface.SetCoupledPosition(theta, gamma, gains);
    //ODRIVE_API_SetPositionStr(odriveMsg, 3U, theta, gamma, 0U )                 // for motor number 3
    SetCoupledPosition(theta, gamma, gains, odriveMsg);
    UART1_write_text(odriveMsg);                                                // write the message to port 4

    od1->sp_theta = theta;
    od1->sp_gamma = gamma;
    od2->sp_theta = theta;
    od2->sp_gamma = gamma;
    od3->sp_theta = theta;
    od3->sp_gamma = gamma;
    od4->sp_theta = theta;
    od4->sp_gamma = gamma;
}

void UpdateStateGaitParams(RobStates curr_state)
{
    
    if (!IsaNan(state_gait_params[RobSTOP].stance_height)) 
    {
        state_gait_params[curr_state].stance_height = state_gait_params[RobSTOP].stance_height;
        state_gait_params[RobSTOP].stance_height = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].down_amp)) 
    {
        state_gait_params[curr_state].down_amp = state_gait_params[RobSTOP].down_amp;
        state_gait_params[RobSTOP].down_amp = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].up_amp)) 
    {
        state_gait_params[curr_state].up_amp = state_gait_params[RobSTOP].up_amp;
        state_gait_params[RobSTOP].up_amp = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].flight_percent)) 
    {
        state_gait_params[curr_state].flight_percent = state_gait_params[RobSTOP].flight_percent;
        state_gait_params[RobSTOP].flight_percent = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].step_length)) 
    {
        state_gait_params[curr_state].step_length = state_gait_params[RobSTOP].step_length;
        state_gait_params[RobSTOP].step_length = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].freq)) 
    {
        state_gait_params[curr_state].freq = state_gait_params[RobSTOP].freq;
        state_gait_params[RobSTOP].freq = NaN;
    }

    if (!IsaNan(state_gait_params[RobSTOP].step_diff)) 
    {
        state_gait_params[curr_state].step_diff = state_gait_params[RobSTOP].step_diff;
        state_gait_params[RobSTOP].step_diff = NaN;
    }

}

void CoupledMoveLeg( float32_t t, GaitParams params, float32_t gait_offset, float32_t leg_direction, LegGain *gains, float32_t theta, float32_t gamma)
{                                                                               //  ODriveArduino& odrive,  interface missing here
    float x=0;                                                                  // float x for leg 0 to be set by the sin trajectory
    float y=0;
    char * Xbuf = NULL;                                                         // the buffer you are sending through the serial port connected

    SinTrajectory(t, params, gait_offset, x, y);
    CartesianToThetaGamma(x, y, leg_direction, theta, gamma);
   // odrive.SetCoupledPosition(theta, gamma, gains);                           // This is arduino   comms to odrive pic is below
   // ODRIVE_API_SetPositionStr(Xbuf, 0U, theta, gamma, 0U );
   // UART1_write_text(Xbuf);
   SetCoupledPosition( theta, gamma, gains, Xbuf );
   UART1_write_text(Xbuf);
}

void gait(GaitParams params, float leg0_offset, float leg1_offset, float leg2_offset, float leg3_offset, LegGain *gains, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4)
{

    GaitParams paramsR;
    GaitParams paramsL;
    float32_t t;
    float32_t leg0_direction, leg1_direction, leg2_direction, leg3_direction;

    paramsR.step_length -= params.step_diff;
    paramsL.step_length += params.step_diff;

    if (!IsValidGaitParams(paramsR) || !IsValidGaitParams(paramsL) || !IsValidLegGain(*gains)) {
        return;
    }

    t = ((float32_t) g_timer5_counter)/100.0f;

    leg0_direction = -1.0;
    // Drive the odrive motor with the parameters
    //
    CoupledMoveLeg( t, paramsL, leg0_offset, leg0_direction, gains,  od1->sp_theta, od1->sp_gamma);

    leg1_direction = -1.0;
    CoupledMoveLeg( t, paramsL, leg1_offset, leg1_direction, gains, od2->sp_theta, od2->sp_gamma);

    leg2_direction = 1.0;
    CoupledMoveLeg(t, paramsR, leg2_offset, leg2_direction, gains, od3->sp_theta, od3->sp_gamma);

    leg3_direction = 1.0;
    CoupledMoveLeg( t, paramsR, leg3_offset, leg3_direction, gains, od4->sp_theta, od4->sp_gamma);

}
// example of how to drive state engine
//
void TransitionToDance() 
{
    LegGain gait_gains;
    RobStates state;
    
    gait_gains.kp_theta = 50.0f;
    gait_gains.kd_theta = 0.5f;
    gait_gains.kp_gamma = 30.0f;
    gait_gains.kd_gamma = 0.5f;

    state = RobDANCE;
    UpdateStateGaitParams(state);
}

void hop(GaitParams params, COMGS_Odrive_t *od1, COMGS_Odrive_t *od2, COMGS_Odrive_t *od3, COMGS_Odrive_t *od4)
{
    float32_t freq = params.freq;
    LegGain hop_gains;
    LegGain land_gains;
    float32_t theta=0, gamma=0;                                                 // theta and gamma are calculated but this initialises to stop warning

    hop_gains.kp_theta = 120.0f;                                                // kp_theta
    hop_gains.kd_theta = 1.0f;                                                  // kd_theta
    hop_gains.kp_gamma = 80.0f;                                                 // kp_gamma
    hop_gains.kd_gamma = 1.0f;                                                  // kd_gamma

    land_gains.kp_theta = 120.0f;                                               // kp_theta
    land_gains.kd_theta = 2.0f;                                                 // kd_theta
    land_gains.kp_gamma = 20.0f;                                                // kp_gamma
    land_gains.kd_gamma = 2.0f;                                                 // kd_gamma

    CartesianToThetaGamma(0, params.stance_height - params.up_amp, 1, theta, gamma);
    CommandAllLegs(theta, gamma, &land_gains, od1, od2, od3, od4);
    Delay_ms(50U);

    CartesianToThetaGamma(0, params.stance_height + params.down_amp, 1, theta, gamma);
    CommandAllLegs(theta, gamma, &hop_gains, od1, od2, od3, od4);
    Delay_ms(50U);

    CartesianToThetaGamma(0, params.stance_height, 1, theta, gamma);
    CommandAllLegs(theta, gamma, &land_gains, od1, od2, od3, od4);
    Delay_ms(50U);
}
// kinematics functions
// port from python https://github.com/theodor1289/sphere-follower-robot
//
void precomputed_jacobian( float32_t joints[4], float32_t jacobian[3][4] )
{

    float32_t s1,c1,s2,c2,s3,c3,s4,c4;

    s1 = sin(joints[0]);
    c1 = cos(joints[0]);
    s2 = sin(joints[1]);
    c2 = cos(joints[1]);
    s3 = sin(joints[2]);
    c3 = cos(joints[2]);
    s4 = sin(joints[3]);
    c4 = cos(joints[3]);

    //jacobian = np.zeros((3, 4))

    jacobian[0][0] = 2 * c1 * s2 * c3 * c4 - 2 * s1 * s3 * c4 + 2 * c1 * c2 * s4 + 3 * c1 * s2 * c3 - 3 * s1 * s3;
    jacobian[0][1] = 2 * s1 * c2 * c3 * c4 - 2 * s1 * s2 * s4 + 3 * s1 * c2 * c3;
    jacobian[0][2] = - 2 * s1 * s2 * s3 * c4 + 2 * c1 * c3 * c4 - 3 * s1 * s2 * s3 + 3 * c1 * c3;
    jacobian[0][3] = - 2 * s1 * s2 * c3 * s4 - 2 * c1 * s3 * s4 + 2 * s1 * c2 * c4;
    jacobian[1][0] = 2 * s1 * s2 * c3 * c4 + 2 * c1 * s3 * c4 + 2 * s1 * c2 * s4 + 3 * s1 * s2 * c3 + 3 * c1 * s3;
    jacobian[1][1] = - 2 * c1 * c2 * c3 * c4 + 2 * c1 * s2 * s4 - 3 * c1 * c2 * c3;
    jacobian[1][2] = 2 * c1 * s2 * s3 * c4 + 2 * s1 * c3 * c4 + 3 * c1 * s2 * s3 + 3 * s1 * c3;
    jacobian[1][3] = 2 * c1 * s2 * c3 * s4 - 2 * s1 * s3 * s4 - 2 * c1 * c2 * c4;

    jacobian[2][0] = 0;
    jacobian[2][1] = - 2 * s2 * c3 * c4 - 2 * c2 * s4 - 3 * s2 * c3;
    jacobian[2][2] = - 2 * c2 * s3 * c4 - 3 * c2 * s3;
    jacobian[2][3] = - 2 * c2 * c3 * s4 - 2 * s2 * c4;

}
void transform_matrix(float32_t alpha,float32_t a, float32_t d, float32_t theta, float32_t array[4][4])
{
        array[0][0] = cos(theta);
        array[1][0] = -sin(theta) * cos(alpha);
        array[2][0] = sin(theta) * sin(alpha);
        array[3][0] = a * cos(theta);

        array[0][1] = sin(theta); 
        array[1][1] = cos(theta) * cos(alpha);
        array[2][1] = -cos(theta) * sin(alpha);
        array[3][1] = a * sin(theta);

        array[0][2] = 0;
        array[1][2] = sin(alpha);
        array[2][2] = cos(alpha);
        array[3][2] = d;

        array[0][3] = 0;
        array[1][3] = 0;
        array[2][3] = 0;
        array[3][3] = 1;
}
void automatic_forward_kinematics(float32_t joints[4], float32_t t10[4][4], float32_t t21[4][4], float32_t t32[4][4], float32_t t43[4][4]  )
{
    float32_t t10[4][4];
    float32_t t21[4][4];
    
    transform_matrix(PI / 2, 0, 2, (joints[0] + PI) / 2, t10);                  // we add pi/2 to make 0, 0, 0, 0 an
    transform_matrix(PI / 2, 0, 0, (joints[1] + PI) / 2, t21);                  // we add pi/2 to make 0, 0, 0, 0 an
    transform_matrix(-PI / 2, 3, 0, joints[2],t32);
    transform_matrix(0, 2, 0, joints[3],t43);
}
void manual_forward_kinematics( float32_t joints[4], float32_t array[3] )
{
    float32_t s1,s2,s3,s4,c1,c2,c3,c4;
    float32_t x_e,y_e,z_e;
    
    s1 = sin(joints[0]);
    c1 = cos(joints[0]);
    s2 = sin(joints[1]);
    c2 = cos(joints[1]);
    s3 = sin(joints[2]);
    c3 = cos(joints[2]);
    s4 = sin(joints[3]);
    c4 = cos(joints[3]);

    x_e = 2 * s1 * s2 * c3 * c4 + 2 * c1 * s3 * c4 + 2 * s1 * c2 * s4 + 3 * s1 * s2 * c3 + 3 * c1 * s3;
    y_e = - 2 * c1 * s2 * c3 * c4 + 2 * s1 * s3 * c4 - 2 * c1 * c2 * s4 - 3 * c1 * s2 * c3 + 3 * s1 * s3;
    z_e = 2 * c2 * c3 * c4 - 2 * s2 * s4 + 3 * c2 * c3 + 2;

    array[0]=x_e;
    array[1]=y_e;
    array[2]=z_e;
}
#ifdef NEED_2_PORT
//  ---------- This needs the gsl libraries and we need to port this
/**
 * Compute the (Moore-Penrose) pseudo-inverse of a matrix.
 *
 * If the singular value decomposition (SVD) of A = USV? then the pseudoinverse A?¹ = VS?¹U?, where ? indicates transpose and S?¹ is obtained by taking the reciprocal of each nonzero element on the diagonal, leaving zeros in place. Elements on the diagonal smaller than ``rcond`` times the largest singular value are considered zero.
 *
 * @parameter A                Input matrix. **WARNING**: the input matrix ``A`` is destroyed. However, it is still the responsibility of the caller to free it.
 * @parameter rcond                A real number specifying the singular value threshold for inclusion. NumPy default for ``rcond`` is 1E-15.
 *
 * @returns A_pinv                Matrix containing the result. ``A_pinv`` is allocated in this function and it is the responsibility of the caller to free it.
**/
gsl_matrix* moore_penrose_pinv(gsl_matrix *A, const realtype rcond) 
{
        gsl_matrix *V, *Sigma_pinv, *U, *A_pinv;
        gsl_matrix *_tmp_mat = NULL;
        gsl_vector *_tmp_vec;
        gsl_vector *u;
        realtype x, cutoff;
        size_t i, j;
        unsigned int n = A->size1;
        unsigned int m = A->size2;
        bool was_swapped = false;

        if (m > n) {
                /* libgsl SVD can only handle the case m <= n - transpose matrix */
                was_swapped = true;
                _tmp_mat = gsl_matrix_alloc(m, n);
                gsl_matrix_transpose_memcpy(_tmp_mat, A);
                A = _tmp_mat;
                i = m;
                m = n;
                n = i;
        }

        /* do SVD */
        V = gsl_matrix_alloc(m, m);
        u = gsl_vector_alloc(m);
        _tmp_vec = gsl_vector_alloc(m);
        gsl_linalg_SV_decomp(A, V, u, _tmp_vec);
        gsl_vector_free(_tmp_vec);

        /* compute S?¹ */
        Sigma_pinv = gsl_matrix_alloc(m, n);
        gsl_matrix_set_zero(Sigma_pinv);
        cutoff = rcond * gsl_vector_max(u);

        for (i = 0; i < m; ++i) {
                if (gsl_vector_get(u, i) > cutoff) {
                        x = 1. / gsl_vector_get(u, i);
                }
                else {
                        x = 0.;
                }
                gsl_matrix_set(Sigma_pinv, i, i, x);
        }

        /* libgsl SVD yields "thin" SVD - pad to full matrix by adding zeros */
        U = gsl_matrix_alloc(n, n);
        gsl_matrix_set_zero(U);

        for (i = 0; i < n; ++i) {
                for (j = 0; j < m; ++j) {
                        gsl_matrix_set(U, i, j, gsl_matrix_get(A, i, j));
                }
        }

        if (_tmp_mat != NULL) {
                gsl_matrix_free(_tmp_mat);
        }
        /* two dot products to obtain pseudoinverse */
        _tmp_mat = gsl_matrix_alloc(m, n);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., V, Sigma_pinv, 0., _tmp_mat);

        if (was_swapped) {
                A_pinv = gsl_matrix_alloc(n, m);
                gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1., U, _tmp_mat, 0., A_pinv);
        }
        else {
                A_pinv = gsl_matrix_alloc(m, n);
                gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1., _tmp_mat, U, 0., A_pinv);
        }
        gsl_matrix_free(_tmp_mat);
        gsl_matrix_free(U);
        gsl_matrix_free(Sigma_pinv);
        gsl_vector_free(u);
        gsl_matrix_free(V);

        return A_pinv;
}
#endif
#endif                                                                          // End of Robot helper

#ifdef GPS_INCLUDED                                                             // You need a GPS
//
// Function reads 2 types of GPS receiver and creeates lat long co-ordinates for a map
//
#define UBLOX                                                                   // Define you GPS here
void ReadGPSData()
{
   unsigned char txt[768];                                                                // the string that is filled with GPS receive info via interrupt
   uint16_t latitude, longitude, oldlatitude, oldlongitude;
   unsigned char *string;
   unsigned char lat_str[8];
   unsigned char long_str[9];

   string = strstr(txt,"$GPGLL");                                               // If txt array contains "$GPGLL" string we proceed...
   if(string != 0)
   {
      if(string[7] != ',')                                                    // if "$GPGLL" NMEA message have ',' sign in the 8-th
      {
#ifdef QUECTEL                                                                  // If QUECTEL GPS receiver module is used :
            latitude = (string[7]-48)*10 + (string[8]-48);
            longitude = (string[19]-48)*100 + (string[20]-48)*10 + (string[21]-48);
            if(string[17] == 'S')                                               // if the latitude is in the South direction it has minus sign
            {
              latitude = 0 - latitude;
            }

            if(string[30] == 'W')                                               // if the longitude is in the West direction it has minus sign
            {
              longitude = 0 - longitude;
            }

            if ((oldlongitude != longitude) | (oldlatitude != latitude))
            {
                 // Display_Cursor(latitude, longitude, &lat_str, &long_str, CL_WHITE);     // Display the cursor on the world map
              lat_str[0] = string[7];
              lat_str[1] = string[8];
              lat_str[2] = ',';
              lat_str[3] = string[9];
              lat_str[4] = string[10];
              lat_str[5] = ' ';
              lat_str[6] = string[17];
              lat_str[7] = 0;

              long_str[0] = string[19];
              long_str[1] = string[20];
              long_str[2] = string[21];
              long_str[3] = ',';
              long_str[4] = string[22];
              long_str[5] = string[23];
              long_str[6] = ' ';
              long_str[7] = string[30];
              long_str[8] = 0;
            }
#endif
#ifdef UBLOX                                                                    // If uBlox GPS receiver module is used :

            latitude = (string[7]-48)*10 + (string[8]-48);
            longitude = (string[20]-48)*100 + (string[21]-48)*10 + (string[22]-48);

            if(string[18] == 'S')                                               // if the latitude is in the South direction it has minus sign
            {
              latitude = 0 - latitude;
            }

            if(string[32] == 'W')                                               // if the longitude is in the West direction it has minus sign
            {
              longitude = 0 - longitude;
            }

            if ((oldlongitude != longitude) | (oldlatitude != latitude))
            {
              // Display_Cursor(latitude, longitude, &lat_str, &long_str, CL_WHITE);     // Display the cursor on the world map
              lat_str[0] = string[7];
              lat_str[1] = string[8];
              lat_str[2] = ',';
              lat_str[3] = string[9];
              lat_str[4] = string[10];
              lat_str[5] = ' ';
              lat_str[6] = string[18];
              lat_str[7] = 0;

              long_str[0] = string[20];
              long_str[1] = string[21];
              long_str[2] = string[22];
              long_str[3] = ',';
              long_str[4] = string[23];
              long_str[5] = string[24];
              long_str[6] = ' ';
              long_str[7] = string[32];
              long_str[8] = 0;
           }
#endif

            // Display_Cursor(latitude, longitude, &lat_str, &long_str, CL_RED);     // Display the cursor on the world map
      }
   }
}
//
// Scale the GPS reading for a display
//
void Calc_Cursor_LL(signed int lat, signed int lon, char *lt_Str, char *lng_Str, unsigned int color)
{

  uuint32_t latitude_y, longitude_x;
  // Latitude and Longitude scaling for 320x240 display:
  // Latitude: Input range is -90 to 90 degrees
  // Longitude: Input range is -180 to 180 degrees

  latitude_y = ((160U*(90U - lat))/180U) + 40U;
  longitude_x = (320U*(lon + 180U));
  longitude_x = longitude_x/360U - 10U;

  // Cursor drawing                                                             Do this to suit youre system
  //TFT_Set_Pen(CL_RED, 1);
  //TFT_H_Line(longitude_x - 2, longitude_x + 2, latitude_y );
  //TFT_V_Line( latitude_y - 2, latitude_y + 2, longitude_x );
  //TFT_Set_Font(&TFT_defaultFont, color, FO_HORIZONTAL);
  //TFT_Write_Text(lt_Str, 160, 200);
  //TFT_Write_Text(lng_Str, 160, 215);
}
#endif                                                                          // -----------------   END OF GPS INCLUSION

/* return 1 if string contain only digits, else return 0 */
int16_t valid_digit(unsigned char *ip_str)
{
    while (*ip_str) 
    {
        if (*ip_str >= '0' && *ip_str <= '9')
            ++ip_str;
        else
            return 0;
    }
    return 1;
}

/* return 1 if IP string is valid, else return 0 */
int16_t is_valid_ip(unsigned char *ip_str)
{
    int16_t  num, dots = 0;
    unsigned char *ptr;

    if (ip_str == NULL)
        return 0;

    ptr = strtok(ip_str, ".");                                                  // http://pubs.opengroup.org/onlinepubs/009695399/functions/strtok_r.html

    if (ptr == NULL)
        return 0;

    while (ptr) 
    {
        if (!valid_digit(ptr))                                                  // after parsing string, it must contain only digits */
            return 0;

        num = atoi(ptr);

        if (num >= 0 && num <= 255)                                             // check for valid IP */
        {
            ptr = strtok(NULL, ".");                                            // parse remaining string delimetered by .
            if (ptr != NULL)
                ++dots;
        } 
        else
           return 0;
    }

    if (dots != 3)                                                              // valid IP string must contain 3 dots */
        return 0;
    return 1;                                                                   // success
}


//
//   convert celcius to farenheit
//
float32_t farenhFromcelsius( float32_t celsius )
{
   return((celsius * 9.0f / 5.0f) + 32.0f);
}

// =================== Encoder helpers =========================================
#ifdef ENCODER_HELPER
// Helper function to xor two characters
unsigned char xor_c(unsigned char a, unsigned char b) { return (a == b) ? '0' : '1'; }

// Helper function to flip the bit
unsigned char flip(unsigned char c) { return (c == '0') ? '1' : '0'; }

// function to convert binary string
// to gray string
unsigned char binarytoGray(unsigned char binary[20])
{
    unsigned char gray[20];
    uint32_t g1;
    int8_t i;

    gray[0] = binary[0];                                                        // MSB of gray code is same as binary code

    for (i = 1; i < strlen(binary); i++)                                        // Compute remaining bits, next bit is comuted by doing XOR of previous and current in Binary
    {
        gray[i] = xor_c(binary[i - 1], binary[i]);                              // Concatenate XOR of previous bit with current bit
    }
    return (unsigned char) gray;
}

// function to convert gray code string
// to binary string
unsigned char graytoBinary(unsigned char gray[20])
{
    unsigned char binary[20];
    int8_t i;

    binary[0] = gray[0];                                                        // MSB of binary code is same as gray code

    for (i = 1; i < strlen(gray); i++)                                          // Compute remaining bits
    {
        if (gray[i] == '0')                                                     // If current bit is 0, concatenate previous bit
            binary[i] = binary[i - 1];
        else
            binary[i] = flip(binary[i - 1]);                                    // Else, concatenate invert of previous bit
    }
   return (unsigned char) binary;
}
#endif                                                                          // End of encoder helper

void compute_time(uint32_t time, COMGS_DateTime_t *hmiTm)                       // compute days hours minutes seconds from overall seconds time
{
 uint32_t days, hours, minutes, seconds;

 days = time / (24UL * 3600UL);
 time -= days * 24UL * 3600UL;

 hours = time / 3600UL;                                                         // time now contains the number of seconds in the last day */
 time -= (hours * 3600UL);

 minutes = time / 60U;                                                          // time now contains the number of seconds in the last hour */
 seconds = time - minutes * 60U;
 
 hmiTm->Day = days;                                                             // Set the values on the HMI
 hmiTm->Hour = hours;
 hmiTm->Minute = minutes;
 hmiTm->Second = seconds;
 
}

void compute_secs(XS_Hmi_Slider_t *CalcTime, COMGS_DateTime_t *hmiTm)           // compute overall seconds from a time of H:M:S on the HMI
{

 CalcTime->value = hmiTm->Second + (hmiTm->Minute * 60UL) + (hmiTm->Hour * 60UL * 24UL);

}

void compute_date(XS_Hmi_Slider_t *CalcTime, COMGS_DateTime_t *hmiTm)           // compute a 32 bit integer from the date
{

 CalcTime->value = hmiTm->Year + (hmiTm->Month << 17) + (hmiTm->Day << 22);      // make a 32 bit word that detects a change in date

}

uint8_t check_datetime(COMGS_DateTime_t *hmiTm)                                 // check for valid date and time fields entered on the HMI
{

 if (hmiTm->Year <= 2019)
 {
    if ((hmiTm->Year >= 0) && (hmiTm->Year <= 99))                              // Did he miss the 2000 part and only do the last 2 figures ?
       hmiTm->Year += 2000;                                                     // Add the 2000
    else
       return 0;
 }
 else if (hmiTm->Year >= 9999)
 {
    return 0;                                                                   // Invalid Year
 }
 
 if ((hmiTm->Month >= 1) && (hmiTm->Month <= 12))                               // Valid month found then check for a valid date combination
 {
    if (!((hmiTm->Day>=1 && hmiTm->Day<=31) && (hmiTm->Month==1 || hmiTm->Month==3 || hmiTm->Month==5 || hmiTm->Month==7 || hmiTm->Month==8 || hmiTm->Month==10 || hmiTm->Month==12)))
    {
       if (!((hmiTm->Day>=1 && hmiTm->Day<=30) && (hmiTm->Month==4 || hmiTm->Month==6 || hmiTm->Month==9 || hmiTm->Month==11)))
       {
          if (!((hmiTm->Day>=1 && hmiTm->Day<=30) && (hmiTm->Month==4 || hmiTm->Month==6 || hmiTm->Month==9 || hmiTm->Month==11)))
          {
            if (!((hmiTm->Day>=1 && hmiTm->Day<=28) && (hmiTm->Month==2)))
            {
              if (!(hmiTm->Day==29 && hmiTm->Month==2 && (hmiTm->Year%400==0 ||(hmiTm->Year%4==0 && hmiTm->Year%100!=0))))    // 29 of feb on a leap year
              {
                 return 0;                                                      // Invalid day month combination entered
              }
            }
          }
       }
    }
 }
 else
 {
    return 0;                                                                   // Invalid month
 }

 if (!((hmiTm->Hour >= 0U) && (hmiTm->Hour <= 24U)))
    return 0;                                                                   // Invalid hour
 if (!((hmiTm->Minute >= 0U) && (hmiTm->Minute <= 59U)))
    return 0;                                                                   // Invalid minute
 if (!((hmiTm->Second >= 0) && (hmiTm->Second <= 59U)))
    return 0;                                                                   // Invalid Second

 return 1;                                                                      // All were valid return success
 
}

uint32_t dateTimeToRtcTime(COMGS_DateTime_t *dt)                                // Return the unix time (seconds since 1-1-1970) can be used as aa single uint32 for time or for EGD protocol
{
    uint16_t second = dt->Second;                                               // 0-59
    uint16_t minute = dt->Minute;                                               // 0-59
    uint16_t hour = dt->Hour;                                                   // 0-23
    uint16_t day = dt->Day - 1U;                                                // 0-30
    uint16_t month = dt->Month - 1U;                                            // 0-11
    uint16_t year;                                                              // Year 4 digit (convert below or return a zero on error)
    
    if (dt->Year <= 2019U)                                                      // The year is invalid from now
    {
       if ((dt->Year >= 0U) && (dt->Year <= 99U))                               // Did he miss the 2000 part and only do the last 2 figures ?
          dt->Year += 2000U;                                                    // Add the 2000
       else
          return 0U;                                                            // Invalid year given
    }
    else if (dt->Year >= 9999U)
    {
       return 0U;                                                               // Invalid Year
    }

    year = dt->Year - UNIX_REFERENCE_YEAR;                                      // now subtract 1970 the reference year

    return ((((year / 4U * (365U * 4U + 1U) + Udays[year % 4U][month] + day) * 24U + hour) * 60U + minute) * 60U + second);
}

uint16_t bswap_16(uint16_t x)                                                   // byte swaps 16 bit
{
    return (x >> 8) | (x << 8);
}

uint32_t bswap_32(uint32_t num2ByteSwap)                                        // byte swaps 32 bit
{
    return (bswap_16(num2ByteSwap & 0xffff) << 16) | (bswap_16(num2ByteSwap >> 16));
}


// Simple and fast atof (ascii to float) function.
//
// - Executes about 5x faster than standard MSCRT library atof().
// - An attractive alternative if the number of calls is in the millions.
// - Assumes input is a proper integer, fraction, or scientific format.
// - Matches library atof() to 15 digits (except at extreme exponents).
// - Follows atof() precedent of essentially no error checking.
//
// 09-May-2009 Tom Van Baak (tvb) www.LeapSecond.com

float32_t fastA2F(const char *p)
{
    int16_t frac = 0;
    float32_t sign, value, scale;
    uint16_t expon;

    while (white_space(*p))                                                     // Skip leading white space, if any.
    {
        p += 1;
    }

    sign = 1.0f;                                                                // Get sign, if any.

    if (*p == '-') 
    {
        sign = -1.0f;
        p += 1;
    }
    else if (*p == '+')
    {
        p += 1;
    }
    value = 0.0f;                                                               // Get digits before decimal point or exponent, if any.

    while (valid_digit2(*p))
    {
        value = value * 10.0f + (*p - '0');
        p += 1;
    }

    if (*p == '.')                                                              // Get digits after decimal point, if any.
    {
        float32_t pow10 = 10.0f;
        p += 1;

        while (valid_digit2(*p))
        {
            value += (*p - '0') / pow10;
            pow10 *= 10.0f;
            p += 1;
        }
    }

    scale = 1.0f;                                                               // Handle exponent, if any.
    if ((*p == 'e') || (*p == 'E')) 
    {
        p += 1;
        frac = 0;                                                               // Get sign of exponent, if any.

        if (*p == '-') 
        {
            frac = 1;
            p += 1;
        } 
        else if (*p == '+') 
        {
            p += 1;
        }

        expon = 0;                                                              // Get digits of exponent, if any.

        while (valid_digit2(*p))
        {
            expon = expon * 10 + (*p - '0');
            p += 1;
        }

        if (expon > 308U)
            expon = 308U;

        while (expon >= 8U)                                                      // Calculate scaling factor.
        {
            scale *= 1E8f;
            expon -= 8U;
        }

        while (expon > 0U)
        {
            scale *= 10.0f;
            expon -= 1U;
        }
    }
    return sign * (frac ? (value / scale) : (value * scale));                   // Return signed and scaled floating point result.
}

uint32_t fastA2UL(const char *p)
{
    uint32_t result = 0U;
    unsigned char digit;

    while (white_space(*p)) 
    {
        p += 1;
    }

    for ( ; ; p++) 
    {
        digit = *p - '0';

        if (digit > 9U)
        {
            break;
        }
        result *= 10U;
        result += digit;
    }
    return result;
}

int16_t a2d(char ch)
{
    if (ch >= '0' && ch <= '9')
        return ch - '0';
    else if (ch >= 'a' && ch <= 'f')
        return ch - 'a' + 10;
    else if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 10;
    else
        return -1;
}

uint16_t fastA2I(const char *s)
{
    int16_t sign = 1;
    int16_t num = 0;
    int16_t digit;

    while (white_space(*s)) 
    {
        s++;
    }

    if (*s == '-') 
    {
        sign = -1;
        s++;
    }

    while ((digit = a2d(*s)) >= 0) 
    {
        if (digit > 10U)
            break;

        num = num * 10U + digit;
        s++;
    }
    return sign * num;
}
#endif