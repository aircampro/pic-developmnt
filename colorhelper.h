//
//  Color helper library
//
#ifndef __ColorHelp_h
#define __ColorHelp_h

// YUV format "The human eye is sensitive to changes in brightness, but insensitive to changes in color." 
// Thus, by suppressing chromaticity and dividing a wider band or number of bits into luminance, it is more efficient
// with less loss A format that realizes transmission and compression.
// YUV to RGB conversions
#define CLIP(x) do{if(x < 0){x = 0;} else if(x > 255){x = 255;}} while(0)
#define CONVERT_YUV_R(Y, V) ((298 * (Y - 16) + 409 * (V - 128) + 128) >> 8)
#define CONVERT_YUV_G(Y, U, V) ((298 * (Y - 16) - 100 * (U - 128) - 208 * (V - 128) + 128) >> 8)
#define CONVERT_YUV_B(Y, U) ((298 * (Y - 16) + 516 * (U - 128) + 128) >> 8)
//  RGB to CMY
#define CONVERT_RGB_C(R) (255 - R)
#define CONVERT_RGB_M(G) (255 - G)
#define CONVERT_RGB_Y(B) (255 - B)
// CMY to RGB
#define CONVERT_CMY_R(C) (255 - C)
#define CONVERT_CMY_G(M) (255 - M)
#define CONVERT_CMY_B(Y) (255 - Y)
// RGB to Y BY RY
#define CONVERT_RGB_Y1(R,G,B) (((0.300f * R) + (0.590f * G)) + (0.110f * B))
#define CONVERT_RGB_BY(R,G,B) (-0.300f * R)-(0.590f * G) + (0.890f * B)
#define CONVERT_RGB_RY(R,G,B) (0.700f * R)-(0.590f * G)- (0.110f * B)
// Y BY RY to RGB
#define CONVERT_YBYRY_R(Y,RY) (1.000f * Y) + (1.000f * RY)
#define CONVERT_YBYRY_G(Y,BY,RY) (1.000f * Y) - (0.186f * BY) - (0.508f * RY)
#define CONVERT_YBYRY_B(Y,BY) (1.000f * Y)  + (1.000f * BY)
// avery lees jfif
#define CONVERT_JFIF_R(Y,Cr) (Y + (1.402f*(Cr-128)))
#define CONVERT_JFIF_G(Y,Cb,Cr) (Y - (0.34414f * (Cb-128)) - (0.71414f * (Cr-128)))
#define CONVERT_JFIF_B(Y,Cb) (Y + (1.772f * (Cb-128)))

#define Black 0x000000ul                                                        // Color definitions to call the actual RGB color in the program
#define White 0xFFFFFFul
#define Red 0xFF0000ul
#define Lime 0x00FF00ul
#define Blue 0x0000FFul
#define Yellow 0xFFFF00ul
#define Cyan 0x00FFFFul
#define Magenta 0xFF00FFul
#define Silver 0xC0C0C0ul
#define Gray 0x808080ul
#define Maroon 0x800000ul
#define Olive 0x808000ul
#define Green 0x008000ul
#define Purple 0x800080ul
#define Teal 0x008080ul
#define Navy 0x000080ul
#define maroon 0x800000ul
#define darkred 0x8B0000ul
#define brown 0xA52A2Aul
#define firebrick 0xB22222ul
#define crimson 0xDC143Cul
#define red 0xFF0000ul
#define tomato 0xFF6347ul
#define coral 0xFF7F50ul
#define indianred 0xCD5C5Cul
#define lightcoral 0xF08080ul
#define darksalmon 0xE9967Aul
#define salmon 0xFA8072ul
#define lightsalmon 0xFFA07Aul
#define orangered 0xFF4500ul
#define darkorange 0xFF8C00ul
#define orange 0xFFA500ul
#define gold 0xFFD700ul
#define darkgoldenrod 0xB8860Bul
#define goldenrod 0xDAA520ul
#define palegoldenrod 0xEEE8AAul
#define darkkhaki 0xBDB76Bul
#define khaki 0xF0E68Cul
#define olive 0x808000ul
#define yellow 0xFFFF00ul
#define yellowgreen 0x9ACD32ul
#define darkolivegreen 0x556B2Ful
#define olivedrab 0x6B8E23ul
#define lawngreen 0x7CFC00ul
#define chartreuse 0x7FFF00ul
#define greenyellow 0xADFF2Ful
#define darkgreen 0x006400ul
#define green 0x008000ul
#define forestgreen 0x228B22ul
#define lime 0x00FF00ul
#define limegreen 0x32CD32ul
#define lightgreen 0x90EE90ul
#define palegreen 0x98FB98ul
#define darkseagreen 0x8FBC8Ful
#define mediumspringgreen 0x00FA9Aul
#define springgreen 0x00FF7Ful
#define seagreen 0x2E8B57ul
#define mediumaquamarine 0x66CDAAul
#define mediumseagreen 0x3CB371ul
#define lightseagreen 0x20B2AAul
#define darkslategray 0x2F4F4Ful
#define teal 0x008080ul
#define darkcyan 0x008B8Bul
#define aqua 0x00FFFFul
#define cyan 0x00FFFFul
#define lightcyan 0xE0FFFFul
#define darkturquoise 0x00CED1ul
#define turquoise 0x40E0D0ul
#define mediumturquoise 0x48D1CCul
#define paleturquoise 0xAFEEEEul
#define aquamarine 0x7FFFD4ul
#define powderblue 0xB0E0E6ul
#define cadetblue 0x5F9EA0ul
#define steelblue 0x4682B4ul
#define cornflowerblue 0x6495EDul
#define deepskyblue 0x00BFFFul
#define dodgerblue 0x1E90FFul
#define lightblue 0xADD8E6ul
#define skyblue 0x87CEEBul
#define lightskyblue 0x87CEFAlul
#define midnightblue 0x191970ul
#define navy 0x000080ul
#define darkblue 0x00008Bul
#define mediumblue 0x0000CDul
#define blue 0x0000FFul
#define royalblue 0x4169E1ul
#define blueviolet 0x8A2BE2ul
#define indigo 0x4B0082ul
#define darkslateblue 0x483D8Bul
#define slateblue 0x6A5ACDul
#define mediumslateblue 0x7B68EEul
#define mediumpurple 0x9370DBul
#define darkmagenta 0x8B008Bul
#define darkviolet 0x9400D3ul
#define darkorchid 0x9932CCul
#define mediumorchid 0xBA55D3ul
#define purple 0x800080ul
#define thistle 0xD8BFD8ul
#define plum 0xDDA0DDul
#define violet 0xEE82EEul
#define orchid 0xDA70D6ul
#define mediumvioletred 0xC71585ul
#define palevioletred 0xDB7093ul
#define deeppink 0xFF1493ul
#define hotpink 0xFF69B4ul
#define lightpink 0xFFB6C1ul
#define pink 0xFFC0CBul
#define antiquewhite 0xFAEBD7ul
#define beige 0xF5F5DCul
#define bisque 0xFFE4C4ul
#define blanchedalmond 0xFFEBCDul
#define wheat 0xF5DEB3ul
#define cornsilk 0xFFF8DCul
#define lemonchiffon 0xFFFACDul
#define lightgoldenrodyellow 0xFAFAD2ul
#define lightyellow 0xFFFFE0ul
#define saddlebrown 0x8B4513ul
#define sienna 0xA0522Dul
#define chocolate 0xD2691Eul
#define peru 0xCD853Ful
#define sandybrown 0xF4A460ul
#define burlywood 0xDEB887ul
#define tan 0xD2B48Cul
#define rosybrown 0xBC8F8Ful
#define moccasin 0xFFE4B5ul
#define navajowhite 0xFFDEADul
#define peachpuff 0xFFDAB9ul
#define mistyrose 0xFFE4E1ul
#define lavenderblush 0xFFF0F5ul
#define linen 0xFAF0E6ul
#define oldlace 0xFDF5E6ul
#define papayawhip 0xFFEFD5ul
#define seashell 0xFFF5EEul
#define mintcream 0xF5FFFAul
#define slategray 0x708090ul
#define lightslategray 0x778899ul
#define lightsteelblue 0xB0C4DEul
#define lavender 0xE6E6FAul
#define floralwhite 0xFFFAF0ul
#define aliceblue 0xF0F8FFul
#define ghostwhite 0xF8F8FFul
#define honeydew 0xF0FFF0ul
#define ivory 0xFFFFF0ul
#define azure 0xF0FFFFul
#define snow 0xFFFAFAul
#define black 0x000000ul
#define dimgray 0x696969ul
#define grey 0x808080ul
#define darkgrey 0xA9A9A9ul
#define silvercoco 0xC0C0C0ul
#define lightgray 0xD3D3D3ul
#define gainsboro 0xDCDCDCul
#define whitesmoke 0xF5F5F5ul
#define white 0xFFFFFFul

// structures
typedef enum
{
    RGB_RED = 0,
    RGB_GREEN,
    RGB_BLUE
} colorComponent_e;

#define RGB_COLOR_COMPONENT_COUNT (RGB_BLUE + 1)

struct rgbColor24bpp_s
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

typedef union
{
    struct rgbColor24bpp_s rgb;
    uint8_t raw[RGB_COLOR_COMPONENT_COUNT];
} rgbColor24bpp_t;

#define HSV_HUE_MAX 359U
#define HSV_SATURATION_MAX 255U
#define HSV_VALUE_MAX 255U

typedef enum
{
    HSV_HUE = 0,
    HSV_SATURATION,
    HSV_VALUE
} hsvColorComponent_e;

#define HSV_COLOR_COMPONENT_COUNT (HSV_VALUE + 1)

typedef struct hsvColor_s
{
    uint16_t h;                                                                 // 0 - 359
    uint8_t s;                                                                  // 0 - 255
    uint8_t v;                                                                  // 0 - 255
} hsvColor_t;

//
// Function defintions
//
rgbColor24bpp_t* hsvToRgb24(const hsvColor_t *c);
void RGBtoHSV(float32_t fR, float32_t fG, float32_t fB, float32_t fH, float32_t fS, float32_t fV);

//
// Source below found here: http://www.kasperkamperman.com/blog/arduino/arduino-programming-hsb-to-rgb/
//
rgbColor24bpp_t* hsvToRgb24(const hsvColor_t* c)

{

    static rgbColor24bpp_t r;
    uint16_t val = c->v;
    uint16_t sat = 255 - c->s;
    uint32_t base;
    uint16_t hue = c->h;

    if (sat == 0)
    { // Acromatic color (gray). Hue doesn't mind.

        r.rgb.r = val;
        r.rgb.g = val;
        r.rgb.b = val;
    }
    else
    {
        base = ((255 - sat) * val) >> 8;
        switch (hue / 60)
        {

            case 0:
            r.rgb.r = val;
            r.rgb.g = (((val - base) * hue) / 60) + base;
            r.rgb.b = base;
            break;

            case 1:
            r.rgb.r = (((val - base) * (60 - (hue % 60))) / 60) + base;
            r.rgb.g = val;
            r.rgb.b = base;
            break;

            case 2:
            r.rgb.r = base;
            r.rgb.g = val;
            r.rgb.b = (((val - base) * (hue % 60)) / 60) + base;
            break;

            case 3:
            r.rgb.r = base;
            r.rgb.g = (((val - base) * (60 - (hue % 60))) / 60) + base;
            r.rgb.b = val;
            break;

            case 4:
            r.rgb.r = (((val - base) * (hue % 60)) / 60) + base;
            r.rgb.g = base;
            r.rgb.b = val;
            break;

            case 5:
            r.rgb.r = val;
            r.rgb.g = base;
            r.rgb.b = (((val - base) * (60 - (hue % 60))) / 60) + base;
            break;

        }
    }
    return &r;

}
//
// Jan Winkler <winkler@cs.uni-bremen.de>
//
void RGBtoHSV(float32_t fR, float32_t fG, float32_t fB, float32_t fH, float32_t fS, float32_t fV)
{

  float32_t fCMax = max(max(fR, fG), fB);
  float32_t fCMin = min(min(fR, fG), fB);
  float32_t fDelta = fCMax - fCMin;

  if(fDelta > 0U)
  {
    if(fCMax == fR)
    {
      fH = 60U * (FMOD(((fG - fB) / fDelta), 6U));
    }
    else if(fCMax == fG)
    {
      fH = 60U * (((fB - fR) / fDelta) + 2U);
    }
    else if(fCMax == fB)
    {
      fH = 60U * (((fR - fG) / fDelta) + 4U);
    }

    if(fCMax > 0U)
    {
      fS = fDelta / fCMax;
    }
    else
    {
      fS = 0U;
    }
    fV = fCMax;
  }
  else
  {
    fH = 0U;
    fS = 0U;
    fV = fCMax;
  }
  if(fH < 0U)
  {
    fH = 360U + fH;
  }
}
// convert RGB to YUV
void convert_rgb_yuv( float32_t R, float32_t G, float32_t B, uint16_t Y, uint16_t U, uint16_t V)
{
   Y = ROUND(( (0.256788f * R) + (0.504129f * G) + (0.097906f * B)),0U) +  16f;
   U = ROUND(((-0.148223f * R) - (0.290993f * G) + (0.439216f * B)),0U) + 128f;
   V = ROUND(((0.439216f * R) - (0.367788f * G) - (0.071427f * B)),0U) + 128f;
}
// convert CMYK to RGB
void convert_cmyk_rgb( uint16_t C, uint16_t M, uint16_t Y, uint16_t K, uint16_t R, uint16_t G, uint16_t B )
{
  uint16_t R1,G1,B1;
  
  R1 = 255f - C - K;
  G1 = 255f - M - K;
  B1 = 255f - Y - K;

  R = max(0U, R1);
  G = max(0U, G1);
  B = max(0U, B1);
}
// smallest value from three integers
uint16_t GetSmallestValue(uint16_t x, uint16_t y, uint16_t z )
{
   if (y < x)
   {
      if (z < y)
        return z;
      else
        return y;
   }
   else if (z < x)
   {
      if (y < z)
        return y;
      else
        return z;
   }
   else
     return x;
}

// RGB->CMYK
void convert_rgb_cmyk( uint16_t R, uint16_t G, uint16_t B, uint16_t C, uint16_t M, uint16_t Y, uint16_t K)
{
  uint16_t K;
  K = GetSmallestValue((255U - R), (255U - G), (255U - B));
  C = 255U - R - K;
  M = 255U - G - K;
  Y = 255U - B - K;
}
// NV12 to RGB
void NV12ToRGB(uint8_t * rgbBuffer, uint8_t * yuvBuffer, int16_t width, int16_t height)
{
    uint8_t * uvStart;
    uint8_t y[2U] = { 0U, 0U };
    uint8_t u = 0U;
    uint8_t v = 0U;
    int16_t r = 0U;
    int16_t g = 0U;
    int16_t b = 0U;
    int16_t rowCnt = 0U;
    int16_t colCnt = 0U;
    int16_t cnt = 0U;
    
    uvStart = yuvBuffer + width * height;
    for (rowCnt = 0U; rowCnt < height; rowCnt++)
    {
        for (colCnt = 0U; colCnt < width; colCnt += 2U)
        {
            u = *(uvStart + colCnt + 0U);
            v = *(uvStart + colCnt + 1U);
            for (cnt = 0U; cnt < 2U; cnt++)
            {
                y[cnt] = yuvBuffer[rowCnt * width + colCnt + cnt];
                r = CONVERT_YUV_R(y[cnt], v);
                CLIP(r);
                g = CONVERT_YUV_G(y[cnt], u, v);
                CLIP(g);
                b = CONVERT_YUV_B(y[cnt], u);
                CLIP(b);
                rgbBuffer[(rowCnt * width + colCnt + cnt) * 3U + 0U] = (uint8_t)r;
                rgbBuffer[(rowCnt * width + colCnt + cnt) * 3U + 1U] = (uint8_t)g;
                rgbBuffer[(rowCnt * width + colCnt + cnt) * 3U + 2U] = (uint8_t)b;
            }
        }
        uvStart += width * (rowCnt % 2U);
    }
}
// 4:2:0 YUV to 4:2:2 YUV
int8_t convert_YUV_420_422( uint8_t *Cin, uint8_t *Cout, uint16_t Clen)
{
   uint16_t i=0U;
   
   if ((Cin==NULL) || (Cout==NULL))
      return -1;
   while (i++<=Clen)
   {
      Cout[2U*i] = Cin[i];
      Cout[2U*i+1] = min(max(((9U * (Cin[i] + Cin[i+1U]) - (Cin[i-1U] + Cin[i+2U]) + 8U) >> 4U),0U),255U);
   }
   return 0;
}
//image_src is the source image, image_dst is the converted image

void NV12_YUV420P(const unsigned char* image_src, unsigned char* image_dst, int16_t image_width, int16_t image_height)
{
        const unsigned char* pNV;
        unsigned char* pU;
        unsigned char* pV;
        int16_t i;
        
        unsigned char* p = image_dst;
        memcpy((void *) p,(void *) image_src,(int16_t) (image_width * image_height * 3 / 2));
        p[(int16_t) (image_width * image_height * 3 / 2)] = '\0';             // terminate the copy
        pNV = image_src + image_width * image_height;
        pU = p + image_width * image_height;
        pV = p + image_width * image_height + ((image_width * image_height)>>2U);
        for (i=0; i<(image_width * image_height)/2U; i++)   {
        if ((i%2)==0) *pU++ = *(pNV + i);
        else *pV++ = *(pNV + i);
        }
}
#endif