SUPER PIC CAMERA CONTROLLER
================================

Simple set of libraries for PIC32 written in C.

##Features

- Parses JSON from null-terminated string
- Easy to use tree traversal API
- Communicates with JSON server using Ajax commands 
- This communicates with Yi Action Cam and Parrot Sequoia Camera's
- Communiates on serial to AMP Encoder/Decoder
- Implements SimpleBGC protocol to communicate with camera Gimbal
- Communicates with Run Cam Camera using own protocol
- Communicates on serial tcp and udp
- Communicates on Modbus RTU ASCII TCP UDP BIN protocol
- Communicates on GEC Fanuc EGD UDP protocol
- Communicates with Leddartech (modbus) or LWNX protocol to liddar devices
- Communicates with UBLOX and FURANO GPS NMEA full support plus proprietary
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Contains PID loop and auto tune 
- Contains AHRS helper library with Madgewick and Mahony compensations moving average etc
- Contains axis movement robot helper library for gait control via UDRIVE
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Supports Joystick calibration and expo routines
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Color conversion codecs NV12_YUV420P RGB HSV YUV CMY CMYK Y,BY,RY YCbCr

## Limitations

- Nothing tested as yet

## API

TBD


#### Libraries



## Usage Example

look to each section in the main GCPIC32.c code

##License

LGPL v3

##Copyright

Copyright (c) 2020 ACP Aviation <mark@aircampro.co.uk>
