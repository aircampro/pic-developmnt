// Lightwave LW3 protocol library serial.ethernet,usb
//
// Controls lightwave devices e.g. Audio Visual switches etc
//
#ifndef LW3_INC
#define LW3_INC

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define LW3PACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define LW3PACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

#define TCP_LW3_PORT 6107U                                                      // tcp port to use

// -------------------- function tree ------------------------------------------
// Send :-
// Control command (GET/SET/CALL)
// Path of the method/property    /MEDIA/VIDEO/
// Method/property  XP:switch
// Parameters/Values (I1:O1;I2:O2)
//
// Reply :-
// Prefix of the response     mO
// Path of the method/property   /MEDIA/VIDEO/
// Method/property  XP:switch

// Control commands
#define LW3_GET(X)  { strcpy(X,"GET "); }                                       // It can be used to get the child nodes, properties, and methods of a node or the value of a property
#define LW3_SET(X) { strcpy(X,"SET "); }                                        // The setter command can be used to modify the value of a property.
#define LW3_CALL(X) { strcpy(X,"CALL "); }                                      // A method can be executed by the ‘CALL’ command

// Response prefix
#define LW3_RESP_OK "mO"                                                        // A successful method execution (‘method OK’). ¦
#define LW3_RESP_ERROR "mE"                                                     // Unsuccessful method execution (an error code is followed). ¦ an error for a method
#define LW3_RESP_RO "pR"                                                        // Read-only property (e.g. when querying a parameter).
#define LW3_RESP_FAIL "mF"                                                      // a response after a failed method execution
#define LW3_RESP_MAN "mm"                                                       // a manual for a method.
#define LW3_RESP_RW "pW"                                                        // read write property
#define LW3_RESP_NODE_ERR "nE"                                                  // error for a node
#define LW3_RESP_NODE "n-"                                                      // node
#define LW3_RESP_METHOD "m-"                                                    // method
#define LW3_RESP_CHNG "CHG"                                                     // change response after subscription e.g. CHG /MEDIA/VIDEO/QUALITY.QualityMode=video
#define LW3_RESP_SUBS "o-"                                                      // subscription response

#define LW3_RESP_ERR_CODE 1U                                                    // error codes returned
#define LW3_RESP_FAIL_CODE 2U
#define LW3_RESP_MAN_CODE 3U
#define LW3_RESP_ERR_NODE_CODE 4U
#define LW3_RESP_NODE_CODE 5U
#define LW3_RESP_METHOD_CODE 6U
#define LW3_RESP_CHG_CODE 7U
#define LW3_RESP_SUBS_CODE 8U
#define LW3_RESP_NOMATCH_CODE 9U

#define LW3_CALL_OBJ_TYP 0U                                                     // object types to parse functions
#define LW3_SET_RW_TYP 1U
#define LW3_GET_RW_TYP 2U
#define LW3_GET_RO_TYP 3U
#define LW3_BRACKET 1U
#define LW3_EQUALS 2U

#define LW3_MAX_TOKENS 10U                                                      // maximum number of values separated by ; or , in a reply

// Tree structure
#define LW3_ROOT "/"
#define LW3_MEDIA "MEDIA"                                                       // here define the possible directory tree
#define LW3_MED_VIDEO "VIDEO"
#define LW3_MED_AUDIO "AUDIO"
#define LW3_MED_UART "UART"
#define LW3_MED_IR "IR"
#define LW3_MED_ETH "ETHERNET"
#define LW3_MED_MB "MB"
#define LW3_SYS "SYS"
#define LW3_MANAG "MANAGEMENT"
#define LW3_EVENTS "EVENTS"
#define LW3_EDID "EDID"
#define LW3_PRESETS "PRESETS"
#define LW3_REMOTE "REMOTE"

#define LW3_PATH1(A,B)  { sprintf(A,"/%s/",B); }                                // construct a path of 1 variables  e.g. /edid/
#define LW3_PATH2(A,B,C)  { sprintf(A,"/%s/%s/",B,C); }                         // construct a path of 2 variables  e.g. /media/video
#define LW3_CMD_PATH2(A,B,C)  { sprintf(A,"%s /%s/%s/",A,B,C); }                 // construct a path of 2 variables  e.g. CALL /media/video
#define LW3_CMD_PATH1(A,B)  { sprintf(A,"%s /%s/",A,B); }                       // construct a path of 1 variables  e.g. GET /edid/

// method property
#define LW3_CALL_ID_STR "UI:identifyMe()"                                           // CALL /MANAGEMENT/UI:identifyMe() blinks the LED on the rack for 10 seconds
#define LW3_CALL_RESET_STR "SYS:reset()"                                            // CALL /SYS:reset() - resets everything resp. mO /SYS:reset
#define LW3_CALL_DEFAULTS_STR "SYS:factoryDefaults()"                               // CALL /SYS:factoryDefaults()
#define LW3_GET_PROD_NAME_STR ".ProductName"                                        // GET /.ProductName resp. pr /.ProductName=VINX-120-HDMI-ENC
#define LW3_GET_DHCP_STR "NETWORK.DhcpEnabled"                                      // GET /MANAGEMENT/NETWORK.DhcpEnabled resp. pw /MANAGEMENT/NETWORK.DhcpEnabled=true
#define LW3_SET_LOCK_STR "UI.ControlLock="                                          // SET?/MANAGEMENT/UI.ControlLock=0|1|2
#define LW3_LOCK_LOCKED 1U                                                      // allowable values for above function
#define LW3_LOCK_UNLOCKED 0U
#define LW3_LOCK_FORCE 2U
#define LW3_GET_FIRMWR_STR "MB.FirmwareVersion"                                     // GET /SYS/MB.FirmwareVersion replies with pr /SYS/MB.FirmwareVersion=1.2.0b
#define LW3_GET_SOURCE_PORT_STR "XP.SourcePortStatus"                               // e.g. GET /MEDIA/VIDEO/XP.SourcePortStatus resp.  pr /MEDIA/VIDEO/XP.SourcePortStatus=T00AF;T00AF;T000A;T000A  or GET /MEDIA/AUDIO/XP.SourcePortStatus
#define LW3_GET_DEST_PORT_STR "XP.DestinationPortStatus"                            //  GET /MEDIA/VIDEO/XP.DestinationPortStatus resp. pr /MEDIA/VIDEO/XP.DestinationPortStatus=M00BF;T00EF  or /MEDIA/AUDIO
#define LW3_GET_VID_AUD_CROSSPOINT_STR "XP.DestinationConnectionList"               // GET /MEDIA/VIDEO/XP.DestinationConnectionList reps. pr /MEDIA/VIDEO/XP.DestinationConnectionList=I1;I3 or /MEDIA/AUDIO
#define LW3_CALL_VID_AUD_INPUT_SWITCH_STR "XP:switch"                               // e.g. CALL /MEDIA/VIDEO/XP:switch(I2:O1) resp. mO /MEDIA/VIDEO/XP:switch I2 port is connected to O1 port. or CALL /MEDIA/AUDIO/XP:switch(A2:O1) A2 audio connected to O1
#define LW3_CALL_VID_PORT_NAME(A,B,C) { sprintf(A,"%sXP:switch(I%d;O%d) \r\n",A,B,C); }  // XP:switch(I2:O1)
#define LW3_CALL_AUD_PORT_NAME(A,B,C) { sprintf(A,"%sXP:switch(A%d;O%d) \r\n",A,B,C); }  // XP:switch(A2:O1)

#define LW3_CALL_AUTOPORT "XP.DestinationPortAutoselect"                        // get or call CALL /MEDIA/VIDEO/XP:setDestinationPortAutoselect(O1:EPM)
#define LW3_GET_PORT_PRIO "XP.PortPriorityList"                                 // GET /MEDIA/VIDEO/XP.PortPriorityList resp. pr /MEDIA/VIDEO/XP.PortPriorityList=0,1,2,3;0,1,2,3
#define LW3_SET_PORT_PRIO "XP:setAutoselectionPriority"                         // CALL /MEDIA/VIDEO/XP:setAutoselectionPriority(I1\(O1\):3;I2\(O1\):3) The priority number of input 1 and Input 2 has been set to 3 on output 1
#define LW3_CALL_MUTE_INPUT_RANGE(A,B,C) { sprintf(A,"%sXP:muteSource(I%d;I%d) \r\n",A,B,C); } // pass a range of ports rather than a specific number
#define LW3_CALL_MUTE_INPUT(A,B) { sprintf(A,"%sXP:muteSource(I%d) \r\n",A,B); }         // mute input port specified CALL /MEDIA/VIDEO/XP:muteSource(I1) resp. mO /MEDIA/VIDEO/XP:muteSource
#define LW3_CALL_UNMUTE_INPUT_RANGE(A,B,C) { sprintf(A,"%sXP:unmuteSource(I%d;I%d) \r\n",A,B,C); }
#define LW3_CALL_UNMUTE_INPUT(A,B) { sprintf(A,"%sXP:unmuteSource(I%d) \r\n",A,B); }     // unmute input port
#define LW3_CALL_LOCK_INPUT_RANGE(A,B,C) { sprintf(A,"%sXP:lockSource(I%d;I%d) \r\n",A,B,C); }
#define LW3_CALL_LOCK_INPUT(A,B) { sprintf(A,"%s XP:lockSource(I%d) \r\n",A,B); }         // lock input port   CALL /MEDIA/VIDEO/XP:lockSource(I1)
#define LW3_CALL_UNLOCK_INPUT_RANGE(A,B,C) { sprintf(A,"%sXP:unlockSource(I%d;I%d) \r\n",A,B,C); }
#define LW3_CALL_UNLOCK_INPUT(A,B) { sprintf(A,"%sXP:unlockSource(I%d) \r\n",A,B); }     // unlock input port   CALL /MEDIA/VIDEO/XP:unlockSource(I1)
#define LW3_CALL_MUTE_AUDIO_RANGE(A,B,C) { sprintf(A,"%sXP:muteSource(I%d;I%d) \r\n",A,B,C); } // pass a range of ports rather than a specific number
#define LW3_CALL_MUTE_AUDIO(A,B) { sprintf(A,"%sXP:muteSource(I%d) \r\n",A,B); }         // mute input port specified CALL /MEDIA/VIDEO/XP:muteSource(I1) resp. mO /MEDIA/VIDEO/XP:muteSource
#define LW3_CALL_UNMUTE_AUDIO_RANGE(A,B,C) { sprintf(A,"%sXP:unmuteSource(A%d;A%d) \r\n",A,B,C); }
#define LW3_CALL_UNMUTE_AUDIO(A,B) { sprintf(A,"%sXP:unmuteSource(A%d) \r\n",A,B); }     // unmute input port
#define LW3_CALL_LOCK_AUDIO_RANGE(A,B,C) { sprintf(A,"%sXP:lockSource(A%d;A%d) \r\n",A,B,C); }
#define LW3_CALL_LOCK_AUDIO(A,B) { sprintf(A,"%sXP:lockSource(A%d) \r\n",A,B); }         // lock input port   CALL /MEDIA/AUDIO/XP:lockSource(I2)
#define LW3_CALL_UNLOCK_AUDIO_RANGE(A,B,C) { sprintf(A,"%sXP:unlockSource(A%d;A%d) \r\n",A,B,C); }
#define LW3_CALL_UNLOCK_AUDIO(A,B) { sprintf(A,"%sXP:unlockSource(A%d) \r\n",A,B); }     // unlock input port   CALL /MEDIA/AUDIO/XP:unlockSource(A1)
#define LW3_CALL_MUTE_OUTPUT_RANGE(A,B,C) { sprintf(A,"%sXP:muteDestination(O%d;O%d) \r\n",A,B,C); }
#define LW3_CALL_MUTE_OUTPUT(A,B) { sprintf(A,"%sXP:muteDestination(O%d) \r\n",A,B); }   // mute output port
#define LW3_CALL_UNMUTE_OUTPUT_RANGE(A,B,C) { sprintf(A,"%sXP:unmuteDestination(O%d;O%d) \r\n",A,B,C); }
#define LW3_CALL_UNMUTE_OUTPUT(A,B) { sprintf(A,"%sXP:unmuteDestination(O%d) \r\n",A,B); } // unmute output port
#define LW3_CALL_LOCK_OUTPUT_RANGE(A,B,C) { sprintf(A,"%sXP:lockDestination(O%d;O%d) \r\n",A,B,C); }
#define LW3_CALL_LOCK_OUTPUT(A,B) { sprintf(A,"%sXP:lockDestination(O%d) \r\n",A,B); }   // lock output port   CALL /MEDIA/VIDEO/XP:lockDestination(O1)
#define LW3_CALL_UNLOCK_OUTPUT_RANGE(A,B,C) { sprintf(A,"%sXP:unlockDestination(O%d;O%d) \r\n",A,B,C); }
#define LW3_CALL_UNLOCK_OUTPUT(A,B) { sprintf(A,"%sXP:unlockDestination(O%d) \r\n",A,B); } // unlock output port   CALL /MEDIA/VIDEO/XP:unlockDestination(O2)
#define LW3_CALL_IN_HDCP_ON(A,B) { sprintf(A"%sI%d.HdcpEnable=true \r\n",A,B); }         // HDCP capability can be enabled/disabled on the input ports, thus, non-encrypted content can be seen on a non-HDCP compliant display
#define LW3_CALL_IN_HDCP_OFF(A,B) { sprintf(A,"%sI%d.HdcpEnable=false \r\n",A,B); }      // SET /MEDIA/VIDEO/I2.HdcpEnable=true resp.  pw /MEDIA/VIDEO/I2.HdcpEnable=true
#define LW3_GET_HDCP_STATE(A,B) { sprintf(A,"%sI%d.HdcpState \r\n",A,B ); }              // get hdcp state for a port  resp. pr /MEDIA/VIDEO/I1.HdcpState=1 if its on or 0 for off
#define LW3_SET_OUT_HDCP_ALWAYS(A,B) { sprintf(A"%sO%d.HdcpModeSetting=1 \r\n",A,B); }   // SET /MEDIA/VIDEO/O1.HdcpModeSetting=1 resp. pw /MEDIA/VIDEO/O1.HdcpModeSetting=1
#define LW3_SET_OUT_HDCP_AUTO(A,B) { sprintf(A,"%sO%d.HdcpModeSetting=0 \r\n",A,B); }    // HDCP capability can be set to Auto/Always on the output ports, thus, non-encrypted content can be transmitted to a non-HDCP compliant display.
#define LW3_GET_SIG_PRESENT(A,B) { sprintf(A,"%sI%d.SignalPresent \r\n",A,B); } // check for signal being present on input GET /MEDIA/VIDEO/I1.SignalPresent reply =1 yes =0 no

// testing
#define LW3_SET_TEST_PAT(A,B,C) { sprintf(A,"%sO%d.TpgMode=%d \r\n",A,B,C }; }           // set test pattern on output of type 0=off 1=pattern on output 2=pattern displayed if no signal on output
#define LW3_SET_TPG_PAT_RED(A,B) { sprintf(A,"%sO%d.TpgPattern=RED \r\n",A,B); }
#define LW3_SET_TPG_PAT_GREEN(A,B) { sprintf(A,"%sO%d.TpgPattern=GREEN \r\n",A,B); }     // SET /MEDIA/VIDEO/O1.TpgPattern=GREEN resp. pw /MEDIA/VIDEO/O1.TpgPattern=GREEN
#define LW3_SET_TPG_PAT_BLUE(A,B) { sprintf(A,"%sO%d.TpgPattern=BLUE \r\n",A,B); }
#define LW3_SET_TPG_PAT_BLACK(A,B) { sprintf(A,"%sO%d.TpgPattern=BLACK \r\n",A,B); }
#define LW3_SET_TPG_PAT_WHITE(A,B) { sprintf(A,"%sO%d.TpgPattern=WHITE \r\n",A,B); }
#define LW3_SET_TPG_PAT_RAMP(A,B) { sprintf(A,"%sO%d.TpgPattern=RAMP \r\n",A,B); }
#define LW3_SET_TPG_PAT_CHESS(A,B) { sprintf(A,"%sO%d.TpgPattern=CHESS \r\n",A,B); }
#define LW3_SET_TPG_PAT_BAR(A,B) { sprintf(A,"%sO%d.TpgPattern=BAR \r\n",A,B); }
#define LW3_SET_TPG_PAT_CYCLE(A,B) { sprintf(A,"%sO%d.TpgPattern=CYCLE \r\n",A,B); }
#define LW3_SET_TPG_CLOCK_FREQ(A,B,C) { sprintf(A,"%sO%d.TpgClockSource=%d \r\n",A,B,C); }  //  ? SET /MEDIA/VIDEO/O1.TpgClockSource=576  allowed 576,480  resp. pw /MEDIA/VIDEO/O1.TpgClockSource=576
#define LW3_SET_TPG_CLOCK_EXT(A,B) { sprintf(A,"%sO%d.TpgClockSource=EXT \r\n",A,B); }    //  ? SET /MEDIA/VIDEO/O1.TpgClockSource=EXT  external clock  resp. pw /MEDIA/VIDEO/O1.TpgClockSource=EXT

// Audio TYP=I or O, B=num C=value
#define LW3_SET_AUDIO_VOL(A,TYP,B,C) { sprintf(A,"%s%s%d.VolumedB=%d \r\n",A,TYP,B,C); }          // set the audio volume of the analog audio output port specified e.g. SET /MEDIA/AUDIO/O2.VolumedB=-15
#define LW3_SET_AUDIO_VOL_PERC(A,TYP,B,C) { sprintf(A,"%s%s%d.VolumePercent=%d \r\n",A,TYP,B,C); }     // SET /MEDIA/AUDIO/O3.VolumePercent=50 resp. pw /MEDIA/AUDIO/O3.VolumePercent=50.00  TYP= either O or I (input or output)
#define LW3_SET_AUDIO_BALANCE(A,TYP,B,C) { sprintf(A,"%s%s%d.Balance=%d \r\n",A,TYP,B,C); }       // SET /MEDIA/AUDIO/O3.Balance=0  resp. pw /MEDIA/AUDIO/O3.Balance=0
#define LW3_CALL_AUDIO_OUT_LVL(A,TYP,B,C) { sprintf(A,"%s%s%d:stepVolumedB(%d) \r\n",A,TYP,B,C); }  // CALL /MEDIA/AUDIO/I5:stepVolumedB(-1) resp. m0 /MEDIA/AUDIO/I5:stepVolumedB Volume is increased or decreased with the given value in dB
#define LW3_CALL_AUDIO_STEP_VOL(A,TYP,B,C) { sprintf(A,"%s%s%d:stepVolumePercent(%d) \r\n",A,TYP,B,C); } // Volume is increased or decreased with the given value in percent
#define LW3_CALL_AUDIO_STEP_BAL(A,TYP,B,C) { sprintf(A,"%s%s%d:stepBalance(%d) \r\n",A,TYP,B,C); }   //  Balance is shifted to left or right depends on the given value. -100 means left balance, 100 means right balance, step is 1. Center is 0 (default). CALL /MEDIA/AUDIO/I5:stepBalance(1)
#define LW3_SET_AUDIO_GAIN(A,TYP,B,C) { sprintf(A,"%s%s%d.Gain=%d \r\n",A,TYP,B,C); } // gain SET /MEDIA/AUDIO/I5.Gain=3 resp. pw /MEDIA/AUDIO/I5.Gain=3.00
#define LW3_SET_AUDIO_MUTE(A,TYP,B) { sprintf(A,"%s%s%d.Mute=true \r\n",A,TYP,B); }   // Mute Analog Audio input or output
#define LW3_SET_AUDIO_UNMUTE(A,TYP,B) { sprintf(A,"%s%s%d.Mute=false \r\n",A,TYP,B); }  // Unmute Analog Audio input or output
#define LW3_SET_SOURCE AUDIOIN_OR_HDMI(A,B) { sprintf(A,"SET /SYS/MB/LEGACYAUDIOXP.EnableAnalogPassthrough=%s \r\n",B); } // B=true(audio input)/false(hdmi) The Analog audio out port can transmit the the analog audio from the analog audio input line or the deembedded audio from the HDMI out 2.

// io mapping
#define LW3_CALL_GPIO_PIN(A,B) { sprintf(A,"CALL /MEDIA/GPIO/P1:toggle(%d) \r\n",B); }   //  trigger e.g. for a connected relay on pin 1 reply mO /MEDIA/GPIO/P1:toggle

// message sending
#define LW3_CALL_TCP_TXT_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:tcpText(%s:%d=%s) \r\n",IP,PORT,MSG); }     // port e.g. 9715
#define LW3_CALL_TCP_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:tcpMessage(%s:%d=%s\x0d\x0a) \r\n",IP,PORT,MSG); } // sends a tcp message
#define LW3_CALL_TCP_BIN_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:tcpBinary(%s:%d=%d) \r\n",IP,PORT,MSG); }     // sends a tcp binary CALL /MEDIA/ETHERNET:tcpBinary(192.168.0.20:5555=0100000061620000cdcc2c40) resp. mO /MEDIA/ETHERNET:tcpBinary
#define LW3_CALL_UDP_TXT_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:udpText(%s:%d=%s) \r\n",IP,PORT,MSG); }     // port e.g. 9715
#define LW3_CALL_UDP_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:udpMessage(%s:%d=%s\x0d\x0a) \r\n",IP,PORT,MSG); } // sends udp message
#define LW3_CALL_UDP_BIN_MSG(A,IP,PORT,MSG) { sprintf(A,"CALL /MEDIA/ETHERNET:udpBinary(%s:%d=%d) \r\n",IP,PORT,MSG); }     // sends a udp binary CALL /MEDIA/ETHERNET:udpBinary(192.168.0.20:5555=0100000061620000cdcc2c40) resp. mO /MEDIA/ETHERNET:udpBinary
#define LW3_CALL_SERIAL_MSG(A,MSG) { sprintf(A,"CALL /MEDIA/UART/P1:sendMessage(%s/r/n) \r\n",MSG); } // send message B  out via the P1 (local) serial port resp. mO /MEDIA/UART/P1:sendMessage
#define LW3_CALL_SERIAL_TXT(A,MSG) { sprintf(A,"CALL /MEDIA/UART/P1:sendText(%s) \r\n",MSG); }   // serial text message
#define LW3_CALL_SERIAL_BIN(A,MSG) { sprintf(A,"CALL /MEDIA/UART/P1.sendBinaryMessage(%s) \r\n",MSG); }  // serial binary message CALL /MEDIA/UART/P1.sendBinaryMessage(0100000061620000cdcc2c40)

// serial
#define LW3_SET_BAUD_RATE(A,B) { sprintf(A,"SET /MEDIA/UART/P1.Baudrate=%d \r\n",B); }  // setting ‘2’ means 9600 BAUD resp. pw /MEDIA/UART/P1.Baudrate=2
#define LW3_BAUD_4800 0U                                                        // allowable baud rates
#define LW3_BAUD_7200 1U
#define LW3_BAUD_9600 2U
#define LW3_BAUD_14400 3U
#define LW3_BAUD_19200 4U
#define LW3_BAUD_38400 5U
#define LW3_BAUD_57600 6U
#define LW3_BAUD_115200 7U
#define LW3_SET_DATA_BITS(A,B) { sprintf(A,"SET /MEDIA/UART/P1.DataBits=%d \r\n",B); }  // SET /MEDIA/UART/P1.DataBits=8 or 9
#define LW3_SET_CONTROL_PROTO(A,B) { sprintf(A,"SET /MEDIA/UART/P1.ControlProtocol=%d \r\n",B); } // SET /MEDIA/UART/P1.ControlProtocol=1 1=LW3(this) 0=LW2(other)
#define LW3_SET_STOP_BITS(A,B) { sprintf(A,"SET /MEDIA/UART/P1.StopBits=%d \r\n",B); }  // SET /MEDIA/UART/P1.StopBits=0
#define LW3_STOP_BIT_1 0U                                                       // 1 stop bit
#define LW3_STOP_BIT_1_5 1U                                                     // 1.5 stop bit
#define LW3_STOP_BIT_2 2U                                                       // 2 stop bit
#define LW3_SET_PARITY(A,B) { sprintf(A,"SET /MEDIA/UART/P1.Parity=%d \r\n",B); }    // SET /MEDIA/UART/P1.Parity=0
#define LW3_PARITY_NONE 0U                                                      // none
#define LW3_PARITY_ODD 1U                                                       // odd
#define LW3_PARITY_EVN 2U                                                       // even
#define LW3_SET_MODE(A,B) { sprintf(A,"SET /MEDIA/UART/P1.Rs232Mode=%d \r\n",B); }   // SET /MEDIA/UART/P1.Rs232Mode=0
#define LW3_PASSTHRO 0U                                                         // passthrough
#define LW3_CONTROL 1U                                                          // control
#define LW3_INJECTED 2U                                                         // command injection
#define LW3_SET_INJ(A,B) { sprintf(A,"SET /MEDIA/UART/P1.CommandInjectionEnable=%s \r\n",B); }   // SET /MEDIA/UART/P1.CommandInjectionEnable=true or false

// infa red
#define LW3_SET_IR_INJ(A,B) { sprintf(A,"SET /MEDIA/IR/S1.CommandInjectionEnable=%s \r\n",B); }  // enable inject mode true/false
#define LW3_SET_IR_INJ_PORT(A,B) { sprintf(A,"SET /MEDIA/IR/S1.CommandInjectionPort=%d \r\n",B); }  // set injection port
#define LW3_SET_IR_SIG_MOD(A,B) { sprintf(A,"SET /MEDIA/IR/D2.EnableModulation=%s \r\n",B); }  // signal modulation true/false

// network
#define LW3_GET_DHCP(A) { sprintf(A,"GET /MANAGEMENT/NETWORK.DhcpEnabled \r\n"); } // get DHCP status
#define LW3_SET_DHCP(A,B) { sprintf(A,"SET /MANAGEMENT/NETWORK.DhcpEnabled=%s \r\n",B); }  // set DHCP status
#define LW3_GET_NET_IP(A) { sprintf(A,"GET /MANAGEMENT/NETWORK.IpAddress \r\n"); } // get IP address
#define LW3_SET_NET_IP(A,B) { sprintf(A,"SET /MANAGEMENT/NETWORK.StaticIpAddress=%s \r\n",B); } // set IP address where B = dotted ip e.g. 192.1.1.34
#define LW3_GET_SUBNET_MASK(A) { sprintf(A,"GET /MANAGEMENT/NETWORK.NetworkMask \r\n"); } // set subnet mask
#define LW3_SET_SUBNET_MASK(A,B) { sprintf(A,"SET /MANAGEMENT/NETWORK.StaticNetworkMask=%s \r\n",B); } // set subnet mask where B = dotted ip e.g. 255.255.255.0
#define LW3_GET_GW_IP(A) { sprintf(A,"GET /MANAGEMENT/NETWORK.GatewayAddress \r\n"); } // get gateway ip
#define LW3_SET_GW_IP(A,B) { sprintf(A,"SET /MANAGEMENT/NETWORK.StaticGatewayAddress=%s \r\n",B); } // set gateway ip address where B = dotted ip e.g. 255.255.255.0
#define LW3_CALL_NETWORK_RESTART(A,B) { sprintf(A,"CALL /MANAGEMENT/NETWORK:ApplySettings() \r\n",B); }  // make changes above valid and re-start the network

#define LW3_SET_IR_INJ(A,B) { sprintf(A,"SET /MEDIA/IR/S1.CommandInjectionEnable=%s \r\n",B); }

// edid management
// Extended Display Identification Data (EDID) is a metadata format for display devices to describe their capabilities to a video source (e.g. graphics card or set-top box). The data format is defined by a standard published by the Video Electronics Standards Association (VESA).
// The EDID data structure includes manufacturer name and serial number, product type, phosphor or filter type, timings supported by the display, display size, luminance data and (for digital displays only) pixel mapping data.
// DisplayID is a VESA standard targeted to replace EDID and E-EDID extensions with a uniform format suited for both PC monitor and consumer electronics devices
#define LW3_GET_EDID_STATUS(A) { sprintf(A,"GET /EDID.EdidStatus \r\n"); }      // get edid status
#define LW3_GET_EDID_VALID(A) { sprintf(A,"GET /EDID/D/D1.Validity \r\n"); }   // query edid validity GET /EDID/D/D1.Validity resp.
#define LW3_GET_EDID_RESOL(A) { sprintf(A,"GET /EDID/U/U2.PreferredResolution \r\n"); }   // edid preferred resolution e.g. reply pr /EDID/U/U2.PreferredResolution=1920x1080p60.00Hz
#define LW3_CALL_EDID_2IN_PORT(A,B,C) { sprintf(A,"CALL /EDID:switch(F%d:E%d) \r\n",B,C); } // CALL /EDID:switch(F49:E2) Emulating an EDID to an Input Port
#define LW3_CALL_EDID_CPY_2MEM(A,B,C) { sprintf(A,"CALL /EDID:copy(D%d:U%d) \r\n",B,C); } // copy edid to user memory
#define LW3_CALL_EDID_DEL(A,B) { sprintf(A,"CALL /EDID:delete(U%d) \r\n",B); }   // delete edid
#define LW3_CALL_EDID_RST(A) { sprintf(A,"CALL /EDID:reset() \r\n"); }          // reset edid

#define LW3_GET_TPS(A,B) { sprintf(A,"%sS%d.tpsMode \r\n",A,B); }               // Query the Recent TPS Mode  Only MMX4x2-HT200 model has TPS input port.  GET /REMOTE/S1.tpsMode  resp. pr /REMOTE/S1.tpsMode=H
#define LW3_SET_TPS(A,B,C) { sprintf(A,"%sS%d.tpsModeSetting=%s \r\n",A,B,C); } // SET /REMOTE/S1.tpsModeSetting=A  resp. pw /REMOTE/S1.tpsModeSetting=A
#define tpsNAMES TPS("A")TPS("H")TPS("L")TPS("1")TPS("2")                       // allowable tps names TPS mode Auto=A HDBaseT=H Longreach=L LPPF1=1 LPPF2=2
#define TPS(x) #x,
const char * const tps_modes[] = { tpsNAMES };                                  // string array of tps modes

// cable diagnostics
#define LW3_GET_CABLE_DIAG(A,B) { sprintf(A,"GET /REMOTE/S%d.HdbtStat \r\n",B); } // The response contains the measured error rates of the four transmission channels (twisted par) in the CATx cable. The values are in dB; the lowest the best. (‘S1’ node means Source 1 = Input 1).
                                                                                // resp. pr /REMOTE/S1.HdbtStat=-22; -22; -23; -21
#define LW3_GET_CABLE_BER(A,B) { sprintf(A,"GET /REMOTE/S1.TxBer \r\n",B); }    // The value of the property is the Bit Error Ratio (BER) of the transmitter side. The 1e-10 (10 -10 ) value means there is 1 bad pixel on average after transmitting 10 10 pixels; e.g. there is 1 bad pixel in every 80 seconds when the transmitted signal is 1920x1080p @ 60 Hz.
                                                                                // resp. pr /REMOTE/S1.TxBer=1e-10
                                                                                
// power
#define LW3_SET_POWER(A,B,C) { sprintf(A,"SET /MEDIA/VIDEO/O%d.Pwr5vMode=%d \r\n",B,C); }   //  A= msg   B=( port e.g. 1)    C={0 - Auto, 1 - Always On, 2 - Always Off}

// health
#define LW3_GET_CPU_TEMP(A) { strcpy(A,"GET /MANAGEMENT/STATUS.CpuTemperature \r\n"); } // get cpu temp response is like pr /MANAGEMENT/STATUS.CpuTemperature=42 C; 0;75; 0;79;22;54; (42 deg c)
#define LW3_GET_CPU_POWER(A) { strcpy(A,"GET /MANAGEMENT/STATUS.5VMain \r\n"); }  // reply pr /MANAGEMENT/STATUS.5VMain=5.06 V;4.50;5.50;4.28;5.77;4.12;5.09; According to the first value the current Voltage of the ‘5V’ is 5.06V

// parameter value
#define LW3_VS_IN_OUT(A,B,C) { sprintf(A,"%s(I%d:O%d) \r\n"A,B,C); }            // e.g. (I1:O3) for input port mapping to output ports on video switch
#define LW3_AUTO_PORT_ON(A,B) { sprintf(A,"%s(O%d:EPM) \r\n"A,B); }             // (O1:EPM) - enable autoport
#define LW3_AUTO_PORT_OFF(A,B) { sprintf(A,"%s(O%d:D) \r\n",A,B); }             // (O1:D)  - disable autoport

// subscription
#define LW3_SUBSCRIBE_VIDEO(A) { strcpy(A,"OPEN /MEDIA/VIDEO/\*"); }            // subscribes all video events to be reported back on change
#define LW3_UNSUBSCRIBE_VIDEO(A) { strcpy(A,"CLOSE /MEDIA/VIDEO/\*"); }         // un-subscribes all video events to be reported back on change
#define LW3_SUBSCRIBE_AUDIO(A) { strcpy(A,"OPEN /MEDIA/AUDIO/\*"); }            // subscribes all audio events to be reported back on change
#define LW3_UNSUBSCRIBE_AUDIO(A) { strcpy(A,"CLOSE /MEDIA/AUDIO/\*"); }         // un-subscribes all audio events to be reported back on change

#define LW3_SET_DEV_LABEL(X,Y) { sprintf(X,"SET /MANAGEMENT/UID.DeviceLabel=%s \r\n",Y); }   // set device label
#define LW3_GET_FIRM_VER(X) { strcpy(X,"GET /SYS/MB.FirmwareVersion \r\n"); }   // get firware version

LW3PACKED(
typedef struct {                                                                // a value of 1 means active 0 inactive 2 unknown
      uint8_t muted : 1U;
      uint8_t locked : 1U;
      uint8_t connect : 2U;
      uint8_t signal : 2U;
      uint8_t encrypted : 2U;
      uint8_t embedded_aud : 2U;
      uint8_t spare : 6U;
}) LW3Status_t;                                                                 // structure holding the status of a link

LW3PACKED(
typedef struct {
      uint8_t autoselect : 1U;                                                  // 1=active 0=inactive
      uint8_t choice: 3U;                                                       // 1=first 2=last 3=priority 0=undefined
      uint8_t spare : 4U;
}) LW3AutoSel_t;                                                                // structure holding the status of the video autoselect settings

// end of message \r\n                                                          0x0D 0x0A CR LF end sequence

// example to compose a message using library
//
//    LW3_CALL(stringStore);                                                    // sets command
//    LW3_CMD_PATH2(stringStore,LW3_MEDIA,LW3_MED_VIDEO);                       // sets path to video
//    LW3_CALL_MUTE_INPUT(stringStore,6U);                                      // sets mute at input no 6
//
// will make CALL /MEDIA/VIDEO/XP:muteSource(I6)
//
// to check checkLW3Reply(strReplyLW3, stringStore, LW3_CALL_OBJ_TYP, LW3_BRACKET)
// returns zero if response matches
//
#endif