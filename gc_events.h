#ifndef gc_events_H
#define gc_events_H
#include "Struts.h"
#include "stdint.h"
#include "definitions.h"
#include "SBGC_cmd_helpers.h"
#include "camera.h"
//#include "lwNx.h"

#include "__NetEthInternal.h"

#define ETHER_ENABLE_MAC_FILTER                                                 // enables MAC Address filtering
#define ETHER_USE_DEFINED_MAC                                                   // use the hardcoded MAC address rather than that in config.h

#define MAX_NUM_BAD_UDP 10                                                      // Reset if you get this many bad UDP sends

#define Ethernet_HALFDUPLEX     0U
#define Ethernet_FULLDUPLEX     1U

// ================= Global Functions used =====================================
#ifdef TIMER1_NEEDED                                                            // if you use Ethernet IP
extern void Init_Timer1();                                                      // Initialisation routine for Timer 1
#endif
#ifdef TIMER2_NEEDED
extern void Init_Timer2();                                                      // Initialisation routine for Timer 2
extern void Init_Timer2_3();                                                    // Initialisation routine for Timer 2_3
#endif
extern void Init_Timer5();                                                      // Initialisation routine for Timer 5
extern void Init_PHYPins();                                                     // Initialise physical pins
extern void Init_UART( uint16_t parityValue );                                  // Initialise and set-up UART
extern void Init_MCU();                                                         // Initialise the MCU
extern void Init_Ether();                                                       // Initialise ethernet
extern void Status(int16_t address, int16_t state);                             // Report Status to uart5
extern unsigned int CRC16_Calc( unsigned char *Packet,unsigned char Packet_length);          // Do CRC16 UDP CRC calculation to check if packet okay
extern void Init_Node();                                                        // Initialise Nodes
extern void boot_arp();                                                         // Do Arp and wait for response
extern uint8_t SBGC_process_UDP_InBuffer( SBGC_cmd_board_info_t *boardinf );    // Function to process the incoming UDP datagram from SBGC Gimbal
extern int8_t XS_process_UDP_InBuffer();                                        // Function to process the incoming UDP datagram from XSStream encoder
extern uint8_t ReadRunCam( unsigned char *pdata, uint16_t psz );                // Function to process the incoming UDP datagram from RunCam Camera

#ifdef UART4_INTERUPT
extern int8_t XS_process_SERIAL_InBuffer();                                     // Function to process the incoming UDP datagram from XSStream encoder
#endif
uint8_t sizeToChar( unsigned char* stringX, unsigned char val );                // the size of a string (arg.1) up to the specified char (argument No.2)
extern unsigned char checksum(unsigned char *fdata, unsigned int sz);           // A Function to calculate a checksum for a struct
extern uint8_t DataChangeDetected( uint16_t dataVal, uint16_t dataLast, uint8_t delta);
extern float32_t Scale_Raw_Value( int16_t raw_min, int16_t raw_max, float32_t scale_min, float32_t scale_max, int16_t raw_value);
extern void InitEther();
extern void Test_CRC();
extern char ARP(unsigned char *NodeIPAddr);
extern unsigned char Pay_checksum(unsigned char *pdata, unsigned int sz);
extern void StartTCPCommunication();                                            // Initialise the TCP Stack
extern uint8_t XY_OpenTCPSocketRawJson( SOCKET_Intern_Dsc **sock1 );            // Opens RAW tcp socket for JSON communication for Yi Action Cam
extern uint8_t XY_CloseTCPSocketRawJson( SOCKET_Intern_Dsc *sock1 );            // Closes RAW tcp socket for JSON communication for Yi Action Cam
extern void TCPStateCheck( SOCKET_Intern_Dsc *used_tcp_socket);                 // Check the State of the YiCam socket

// ============= External variables used =======================================
extern unsigned char Air_MacAddr[6];
extern unsigned char Air_IpAddr[4];
extern unsigned char MasGs_MacAddr[6];
extern unsigned char MasGs_IpAddr[4];
extern unsigned char FirGs_MacAddr[6];
extern unsigned char FirGs_IpAddr[4];
extern unsigned char Com1Gs_MacAddr[6];
extern unsigned char Com1Gs_IpAddr[4];

#ifdef USE_CAN
extern void Init_Can();                                                         // Initialise CAN Bus ?
extern void Init_CAN();                                                         // Initialise CAN Bus ?
extern unsigned int  Can1_RX_MSG_ID;
extern unsigned int  Can1_TX_MSG_ID;
extern unsigned int  Can2_RX_MSG_ID;
extern unsigned int  Can2_TX_MSG_ID;
extern unsigned int  Can1_Rcv_Flags;
extern unsigned int  Can2_Rcv_Flags;
#endif

extern unsigned char EtherConnState;
extern unsigned char Ether_Link_Present;
extern unsigned int UDP_CRC_Reg;
extern unsigned int UDP_CRC_Good_Reg;
extern char Node;
extern unsigned char UART2_Test_Mode;
//extern unsigned char Pay_checksum();

// =============== Communication Objects =======================================
#ifdef UART2_INTERUPT
extern SerialObject_t UART2;                                                    // External declarations for Serial Communication direct to gimbal
#endif
extern EthUDPObject_t XS_DATA;                                                  // External declarations for Ethernet Communication to COMPIC (encoder)
extern EthUDPObject_t SBGC_DATA;                                                // External declarations for Ethernet Communication to COMPIC (gimbal)
extern EthUDPObject_t RunC_DATA;                                                // External declarations for Ethernet Communication to COMPIC (run cam)
extern EthUDPObject_t YiCam_DATA;                                               // External declarations for Ethernet Communication to Yi Action Cam JSON server
#ifdef UART4_INTERUPT
extern SerialObject_t UART4;                                                    // External declarations for Serial Communication direct to AMP Camera Encoder
#endif
#ifdef UART6_INTERUPT
extern SerialObject_t UART6;                                                    // External declarations for Serial Communication for liddar
//extern lwResponsePacket LwNxResponse;                                           // LightWare liddar packet and status
#endif
#ifdef GEF_EGD_PLC
extern EthUDPObject_t EGD_DATA;                                                 // Define struct to store EGD Data from GEC Fanuc PLC
#endif
extern uint8_t g_YiCamReqState;                                                 // State of YiCam write messages
extern COMGS_YiOptActive_t hmiReqActive;                                        // structure to hold requests for each send message
extern uint8_t XYRequest;                                                       // Current button request (HMI Action Queue)

// ============== Define the nodes =============================================
extern Node Air;                                                                // Air COM PIC
extern Node MasGs;                                                              // Master ground station
extern Node FirGs;                                                              // Fire ground station
extern Node This_Node;                                                          // Node set up using Init_Node()

extern unsigned char SYN;
extern unsigned char bad_ether;

// ================ Declare the global volatiles used in interrupt =============
extern uint64_t g_timer5_counter;                                               // Global Timer Counter
extern uint8_t g_SBGC_Stx_Char;                                                 // STX char for SimpleBGC

#ifdef UART4_INTERUPT
extern uint8_t g_extended;                                                      // Camera encoder reply message was extended type
#endif
extern Net_Ethernet_Intern_arpCacheStruct gRemoteUnit;                          // REmote Unit MAC Address
#ifdef GPS_INCLUDED2
extern GPS_Info_t g_posDataGPS;                                                 // GPS position data for UBLOX or futano type
extern unsigned char navStatGPS[50];                                            // navigation Status description
extern unsigned char positionMode[20];                                          // description of the position mode to be used with checksum for validity
#endif

//================= GLOBAL HMI SCREEN VALUES FROM XS ENCODER ===================
extern unsigned char g_diskStatus[30];                                          // State of the recording disk as a string
extern unsigned char g_ch1Status[20];                                           // Channel 1 state i.e recording or not
extern unsigned char g_ch2Status[20];                                           // Channel 2 state i.e recording or not
extern unsigned char g_ch3Status[20];                                           // Channel 3 state i.e recording or not
extern unsigned char g_ch4Status[20];                                           // Channel 4 state i.e recording or not
extern unsigned char g_sysStatus[30];                                           // State of the recording system
extern uint16_t g_totalSizeXS;                                                  // Total size of the disk in MB
extern uint16_t g_remSizeXS;                                                    // Remaining size of the disk in MB
extern unsigned char g_encTime[20];                                             // Date and Time
extern unsigned char g_strField1[20];                                           // String field 1 after +ok, comma
extern unsigned char g_strField2[20];                                           // String field 2 after +ok,, comma
extern unsigned char g_strField3[20];                                           // String field 3 after +ok,,, comma
extern unsigned char g_strField4[20];                                           // String field 4 after +ok,,,, comma
extern unsigned char g_strField5[20];                                           // String field 5 after +ok,,,,, comma
extern int32_t g_okField1;                                                      // numeric field extracted from +ok, { $GET<cmd> }
extern int32_t g_okField2;                                                      // numeric field extracted from +ok,, { $GET<cmd> }
extern int32_t g_okField3;                                                      // numeric field extracted from +ok,,, { $AVGBITRATE<cmd> }
extern int32_t g_okField4;                                                      // numeric field extracted from +ok,,,, { $AVGBITRATE<cmd> }
extern int32_t g_okField5;                                                       // numeric field extracted from +ok,,,,, { $ENUMSTREAMS<cmd> }
extern int32_t g_okField6;                                                       // numeric field extracted from +ok,,,,,,
extern int32_t g_okField7;                                                       // numeric field extracted from +ok,,,,,,,
extern int32_t g_okField8;                                                       // numeric field extracted from +ok,,,,,,,,

#ifdef GPS_INCLUDED2
//================ GLOBAL SATELITE INFO FROM GPS ===============================
extern uint8_t elevSatelData[4];                                                // possible 4 satelite elevations
extern uint8_t azimuthSatelData[4];                                             // possible 4 satelite azimuth
extern uint8_t sigStrengthSatelData[4];                                         // possible 4 signal stgrengths
extern uint8_t signalIDData;                                                    // signal id 1: GPGSV or GLGSV 7: GAGSV
extern uint8_t satNumberData[4];                                                // satelite number stored in byte
#endif
extern unsigned char g_XYtcpBuffer[XY_MSG_MAX_SND_LEN];                         // Buffer to send JSON requests to the webserver

//extern H_B Heart;                                                               // Heart beat type
#endif //Transceiver_events_H