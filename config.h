//Node Config
// Ethernet II, Src: Microchi_dc:e1:67 (54:10:ec:dc:e1:67), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
// Ethernet II, Src: Microchi_dc:e1:a1 (54:10:ec:dc:e1:a1), Dst: Microchi_dc:e1:67 (54:10:ec:dc:e1:67)
// ether host 54:10:ec:dc:e1:67 or ether host 54:10:ec:dc:e1:a1
#ifndef Config_H
#define Config_H
#include "definitions.h"
#include "Struts.h"
#include "camera.h"

//#include "Struts.h"
unsigned char   Node = GIMJOY;                                                  // definitions.h :: AIR 1 MASGS 2 FIRGS 3 COMGS 4 GIMJOY 5
unsigned char   UART2_Test_Mode =FALSE;                                         // Do fast 1 byte for TCP testing

//unsigned char   UART2_Timeout_Enable = 0x0000;                                   // Ennable = 0x8008; Disable = 0X0000
unsigned char   Server = FALSE;
unsigned char   TCP = FALSE;                                                    // If TCP = 1 then TCP mode If =0 then UDP mode
unsigned char   UDP_Test_Mode    =FALSE;                                        // If Test 1 then ping back to PC
unsigned char   TCP_Test_Mode    =FALSE;                                        // If Test 1 then ping back to PC

#ifdef USE_TCP                                                                                //1 = Server Mode 0 = Client
unsigned char   UART1TCP =FALSE;                                                //1= UART1 is TCP mode                                                        //1 = Server Mode 0 = Client
//unsigned char   UART2TCP =FALSE;
unsigned char   UART3TCP =FALSE;
unsigned char   UART4TCP =FALSE;
unsigned char   UART5TCP =FALSE;
#endif

#ifdef USE_CAN
unsigned char   CAN1TCP =FALSE;
unsigned char   CAN2TCP =FALSE;
unsigned char   CAN1_Test_Mode   =0U;                                           // If Test 1 then gen data on CAN1 (Do nothing else)
unsigned char   CAN2_Test_Mode   =0U;                                           // If Test 1 then gen data on CAN2 (Do nothing else)
unsigned int    Can1_TX_MSG_ID;
unsigned int    Can2_TX_MSG_ID;
#endif

Node This_Node;
Node Remote_Node[10];
Node Air;                                                                       // Air COM PIC
Node MasGs;                                                                     // Master Ground station
Node FirGs;                                                                     // Fire Ground Station
Node YiCam;                                                                     // Yi Cam JSON server

unsigned char   Air_MacAddr[6] = {0x54U, 0x10U, 0xecU, 0xdcU, 0xe1U, 0xa1U};    //  MAC address
unsigned char   Air_IpAddr[4]    = {192U, 172U,  3U, 2U };                      //  IP address
unsigned char   Air_Link;                                                       //  Air COM PIC is linked

unsigned char   MasGs_MacAddr[6] = {0x54U, 0x10U, 0xecU, 0xdcU, 0xe1U, 0x67U};  //  MAC address
unsigned char   MasGs_IpAddr[4]  = {192U, 172U, 3U, 3U };                       //  IP address
unsigned char   MasGs_Link;                                                     //  Master Ground Station is linked

unsigned char   FirGs_MacAddr[6] = {0x54U, 0x10U, 0xecU, 0xdcU, 0xe1U, 0x68U};  //  MAC address
unsigned char   FirGs_IpAddr[4]  = {192U, 172U, 3U, 4U };                       //  IP address
unsigned char   FirGs_Link;                                                     //  Fire Ground Station is linked

unsigned char   Com1Gs_MacAddr[6] = {0x54U, 0x10U, 0xecU, 0xdcU, 0xe1U, 0x69U}; //  MAC address
unsigned char   Com1Gs_IpAddr[4]  = {192U, 172U, 3U, 5U };                      //  IP address
unsigned char   Com1Gs_Link;                                                    //  Commander 1 Ground station is linked

unsigned char   GimJoy_MacAddr[6] = {0x54U, 0x10U, 0xecU, 0xdcU, 0xe1U, 0x70U}; //  MAC address
unsigned char   GimJoy_IpAddr[4]  = {192U, 172U, 3U, 10U };                     //  IP address
unsigned char   GimJoy_Link;                                                    //  Gimbal Joystick controller station is linked

unsigned char   gwIpAddr[4]  = {192U, 172U,  3U, 1U };                          //  gateway (router) IP address
unsigned char   ipMask[4]    = {255U, 255U,  255U, 0U };                        //  network mask (for example : 255.255.255.0)
unsigned char   dnsIpAddr[4] = {192U, 172U,  3U, 1U };                          //  DNS server IP address

//================= GLOBAL HMI SCREEN VALUES FROM XS ENCODER ===================
volatile unsigned char g_diskStatus[30];                                        // State of the recording disk
volatile unsigned char g_ch1Status[20];                                         // Channel 1 state i.e recording or not
volatile unsigned char g_ch2Status[20];                                         // Channel 2 state i.e recording or not
volatile unsigned char g_ch3Status[20];                                         // Channel 3 state i.e recording or not
volatile unsigned char g_ch4Status[20];                                         // Channel 4 state i.e recording or not
volatile unsigned char g_sysStatus[30];                                         // State of the recording system
volatile uint16_t g_totalSizeXS;                                                // Total size of the disk in MB
volatile uint16_t g_remSizeXS;                                                  // Remaining size of the disk in MB
volatile unsigned char g_encTime[20];                                           // Date and Time
volatile unsigned char g_strField1[20];                                         // String field 1 after +ok, comma
volatile unsigned char g_strField2[20];                                         // String field 2 after +ok,, comma
volatile unsigned char g_strField3[20];                                         // String field 3 after +ok,,, comma
volatile unsigned char g_strField4[20];                                         // String field 4 after +ok,,,, comma
volatile unsigned char g_strField5[20];                                         // String field 5 after +ok,,,,, comma
volatile unsigned char g_strField6[20];                                         // String field 5 after +ok,,,,, comma
volatile unsigned char g_strField7[20];                                         // String field 5 after +ok,,,,, comma
volatile unsigned char g_strField8[20];                                         // String field 5 after +ok,,,,, comma
volatile int32_t g_okField1;                                                    // numeric field extracted from +ok, { $GET<cmd> }
volatile int32_t g_okField2;                                                    // numeric field extracted from +ok,, { $GET<cmd> }
volatile int32_t g_okField3;                                                    // numeric field extracted from +ok,,, { $AVGBITRATE<cmd> }
volatile int32_t g_okField4;                                                    // numeric field extracted from +ok,,,, { $AVGBITRATE<cmd> }
volatile int32_t g_okField5;                                                    // numeric field extracted from +ok,,,,, { $ENUMSTREAMS<cmd> }
volatile int32_t g_okField6;                                                    // numeric field extracted from +ok,,,,,, { $GETFRAMERATE<cmd> }
volatile int32_t g_okField7;                                                    // numeric field extracted from +ok,,,,,,, { $GETFRAMERATE<cmd> }
volatile int32_t g_okField8;                                                    // numeric field extracted from +ok,,,,,,, { $GETFRAMERATE<cmd> }
#ifdef GPS_INCLUDED2
//============== GLOBAL HMI SATELITE DATA ======================================
volatile uint8_t elevSatelData[4];                                              // possible 4 satelite elevations
volatile uint8_t azimuthSatelData[4];                                           // possible 4 satelite azimuth
volatile uint8_t sigStrengthSatelData[4];                                       // possible 4 signal stgrengths
volatile uint8_t signalIDData;                                                  // signal id 1: GPGSV or GLGSV 7: GAGSV
volatile uint8_t satNumberData[4];                                              // satelite number stored in byte
#endif
// UART CONFIG
//      UART1
#ifdef OTHER_UART
unsigned int    UART1_Baud        = 57600U;                                     // Baudrate for serial 1
unsigned int    UART1_from_Port    = 10021U;                                    // UDP scource port for UART 1
unsigned int    UART1_dest_Port    = 10001U;                                    // UDP Destination port for UART 1
//      UART2
/*unsigned int    UART2_Baud       = 9600;                                      // Baudrate for serial 1
unsigned int    UART2_from_Port  = 10021;                                       // UDP scource port for UART 2
unsigned int    UART2_dest_Port  = 10002;                                       // UDP Destination port for UART 2
unsigned int    UART2_ACK_Port   = 10022;
unsigned char   UART2_Packet     =FALSE;
unsigned char   U2Buff[32];
unsigned char   UART2_Process_State = CLEAR;*/
//       UART3
unsigned int    UART3Baud        = 57600U;                                      // Baudrate for serial 3
unsigned int    UART3fromPort    = 10003U;                                      // UDP scource port for UART 3
unsigned int    UART3destPort    = 10003U;                                      // UDP Destination port for UART 3
unsigned int    UART4Baud        = 57600U;                                      // Baudrate for serial 4
unsigned int    UART4fromPort    = 10004U;                                      // UDP scource port for UART 4
unsigned int    UART4destPort    = 10004U;                                      // UDP Destination port for UART 4
unsigned int    UART5Baud        = 57600U;                                      // Baudrate for serial 5
unsigned int    UART5fromPort    = 10005U;                                      // UDP scource port for UART 5
unsigned int    UART5destPort    = 10005U;                                      // UDP Destination port for UART 5
unsigned int    UART6Baud        = 57600U;                                      // Baudrate for serial 6
unsigned int    UART6fromPort    = 10006U;                                      // UDP scource port for UART 6
unsigned int    UART6destPort    = 20006U;
unsigned char   U1Buff[32];


unsigned char   U3Buff[32];
unsigned char   U4Buff[32];
unsigned char   U5Buff[32];
unsigned char   U6Buff[32];
unsigned char   U1BuffInd =0;
unsigned char   U2BuffInd =0;
unsigned char   U3BuffInd =0;
unsigned char   U4BuffInd =0;
unsigned char   U5BuffInd =0;
unsigned char   U6BuffInd =0;
#endif

// Network Port Definitions appear here                                         used in Net_Ethernet_Intern_UserUDP() switch case

#ifdef USE_CAN
//**************CAN cinfig********************
unsigned int    Can1fromPort    = 20041U;                                       // UDP scource port for UART 6
unsigned int    Can1destPort    = 20041U;
unsigned int    Can2fromPort    = 20042U;
unsigned int    Can2destPort    = 20042U;                                       // UDP scource port for UART 6


const unsigned int CAN_CONFIG_FLAGS =
                                       _CAN_CONFIG_SAMPLE_ONCE &
                                       _CAN_CONFIG_PHSEG2_PRG_ON &
                                       _CAN_CONFIG_STD_MSG       &
                                       _CAN_CONFIG_MATCH_MSG_TYPE &
                                       _CAN_CONFIG_LINE_FILTER_OFF;
//CAN // CAN Initializations constants
// Baud rate is set according to following formula
// Fbaud = Fosc/(2*N*BRP),  N = SYNC + PHSEG1 + PHSEG2 + PROPSEG = 16
// In this case Fbaud = 125000
const unsigned int SJW = 1U;
const unsigned int BRP = 32U;
const unsigned int PHSEG1 = 1U;
const unsigned int PHSEG2 = 3U;
const unsigned int PROPSEG = 5U;


const unsigned int  Can2_Send_Flags =                                                              // form value to be used
                   _CAN_TX_STD_FRAME &                                          // with CAN2Write
                   _CAN_TX_NO_RTR_FRAME;
                   

char            Can1_Msg_Rcvd[8];
int             Can_msg_rcvd;
char            Can1_Rx_Data1[8];
char            Can1_Tx_Data[8];
char            Can2_Rx_Data[8];
char            Can2_Tx_Data[8];

unsigned int    PLen;                                                           // Packet Length
unsigned char   Packet;
unsigned int    Test            =0U;                                            // If Test 1 then ping back to PC
char MSG[6];
char            Can2_Msg_Rcvd[8];
int             Can2_MSG_Len;
char           Can_RxTx_Data1[8];                                               // Can TEST data
int             Can2_MSG_ID;


char FIFObuffers[2*8*16] absolute 0xA0000000U;
#endif
// reserve space for 2 buffers with 8 messages (each message is 16 bytes)
// beggining of the buffer must be 32bit aligned

 #endif /*Config_H*/