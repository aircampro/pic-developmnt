//
//  @file crc.c  *   * @brief Compact CRC generator for embedded systems, with brute force and table-  * driven algorithm options.
//  Supports CRC-CCITT, CRC-16, and CRC-32 standards.  *  * @par
//           CRC-8 added for RunCam implementation
//  * COPYRIGHT NOTICE: (c) 2000, 2018 Michael Barr.  This software is placed in the   * public domain and may be used for any purpose.
//  However, this notice must not  * be changed or removed.  No warranty is expressed or implied by the publication  * or distribution of this source code.  
//
#ifndef CRC_C
#define CRC_C

#include "stdint.h"
#include "crc.h"
 // Algorithmic parameters based on CRC elections made in crc.h. //
#define BITS_PER_BYTE      8
#define WIDTH  (BITS_PER_BYTE * sizeof(crc_t))
#define TOPBIT             (1 << (WIDTH - 1))

// Allocate storage for the byte-wide CRC lookup table. //
#define CRC_TABLE_SIZE     256
static crc_t g_crc_table[CRC_TABLE_SIZE];

// Further algorithmic configuration to support the selected CRC standard.
//
#if defined(CRC_CCITT)

#define POLYNOMIAL             ((crc_t) 0x1021)
#define INITIAL_REMAINDER      ((crc_t) 0xFFFF)
#define FINAL_XOR_VALUE        ((crc_t) 0x0000)
#define REFLECT_DATA(X)        (X)
#define REFLECT_REMAINDER(X)   (X)

#elif defined(CRC_16)

#define POLYNOMIAL             ((crc_t) 0x8005)
#define INITIAL_REMAINDER      ((crc_t) 0x0000)
#define FINAL_XOR_VALUE        ((crc_t) 0x0000)
#define REFLECT_DATA(X)        ((uint8_t) reflect((X), BITS_PER_BYTE))
#define REFLECT_REMAINDER(X)   ((crc_t) reflect((X), WIDTH))

#elif defined(CRC_32)

#define POLYNOMIAL             ((crc_t) 0x04C11DB7)
#define INITIAL_REMAINDER      ((crc_t) 0xFFFFFFFF)
#define FINAL_XOR_VALUE        ((crc_t) 0xFFFFFFFF)
#define REFLECT_DATA(X)        ((uint8_t) reflect((X), BITS_PER_BYTE))
#define REFLECT_REMAINDER(X)   ((crc_t) reflect((X), WIDTH))

#endif

#define USE_CRC_FAST                                                            // If defined then use the 32 bit CRC fast computation otherwise use slow

uint32_t reflect(uint32_t dataD, uint8_t n_bits);
void crc_init (void);
crc_t crc_slow (uint8_t const * const p_message, uint8_t n_bytes);
crc_t crc_fast (uint8_t const * const p_message, uint8_t n_bytes);
void compute_hashtable_fast32X();
void compute_hashtable_fast32( unsigned char *addData );
uint8_t crc8(uint8_t const *dataX, int32_t length);
uint8_t crc8_dvb_s2(uint8_t crc, unsigned char a);
uint8_t crc8_dvb_s2_update(uint8_t crc, const void *dataX, uint32_t length);
uint16_t crc16_arc_calculate(uint16_t length, uint8_t const * const p_message);
uint16_t crc16_arc_fast(uint16_t crc, unsigned char const *buffer, uint16_t len);
uint16_t usMBCRC16( unsigned char * pucFrame, uint16_t usLen );
uint8_t usMBAsciiLRC(unsigned char *auchMsg, uint16_t usLen);

uint16_t const crc16arc_table[256] = {                                          // Table for CRC 16 ARC FAST
        0x0000u, 0xC0C1U, 0xC181U, 0x0140U, 0xC301U, 0x03C0U, 0x0280U, 0xC241U,
        0xC601u, 0x06C0u, 0x0780u, 0xC741u, 0x0500u, 0xC5C1u, 0xC481u, 0x0440u,
        0xCC01u, 0x0CC0u, 0x0D80u, 0xCD41u, 0x0F00u, 0xCFC1u, 0xCE81u, 0x0E40u,
        0x0A00u, 0xCAC1u, 0xCB81u, 0x0B40u, 0xC901u, 0x09C0u, 0x0880u, 0xC841u,
        0xD801u, 0x18C0u, 0x1980u, 0xD941u, 0x1B00u, 0xDBC1u, 0xDA81u, 0x1A40u,
        0x1E00u, 0xDEC1u, 0xDF81u, 0x1F40u, 0xDD01u, 0x1DC0u, 0x1C80u, 0xDC41u,
        0x1400u, 0xD4C1u, 0xD581u, 0x1540u, 0xD701u, 0x17C0u, 0x1680u, 0xD641u,
        0xD201u, 0x12C0u, 0x1380u, 0xD341u, 0x1100u, 0xD1C1u, 0xD081u, 0x1040u,
        0xF001u, 0x30C0u, 0x3180u, 0xF141u, 0x3300u, 0xF3C1u, 0xF281u, 0x3240u,
        0x3600u, 0xF6C1u, 0xF781u, 0x3740u, 0xF501u, 0x35C0u, 0x3480u, 0xF441u,
        0x3C00u, 0xFCC1u, 0xFD81u, 0x3D40u, 0xFF01u, 0x3FC0u, 0x3E80u, 0xFE41u,
        0xFA01u, 0x3AC0u, 0x3B80u, 0xFB41u, 0x3900u, 0xF9C1u, 0xF881u, 0x3840u,
        0x2800u, 0xE8C1u, 0xE981u, 0x2940u, 0xEB01u, 0x2BC0u, 0x2A80u, 0xEA41u,
        0xEE01u, 0x2EC0u, 0x2F80u, 0xEF41u, 0x2D00u, 0xEDC1u, 0xEC81u, 0x2C40u,
        0xE401u, 0x24C0u, 0x2580u, 0xE541u, 0x2700u, 0xE7C1u, 0xE681u, 0x2640u,
        0x2200u, 0xE2C1u, 0xE381u, 0x2340u, 0xE101u, 0x21C0u, 0x2080u, 0xE041u,
        0xA001u, 0x60C0u, 0x6180u, 0xA141u, 0x6300u, 0xA3C1u, 0xA281u, 0x6240u,
        0x6600u, 0xA6C1u, 0xA781u, 0x6740u, 0xA501u, 0x65C0u, 0x6480u, 0xA441u,
        0x6C00u, 0xACC1u, 0xAD81u, 0x6D40u, 0xAF01u, 0x6FC0u, 0x6E80u, 0xAE41u,
        0xAA01u, 0x6AC0u, 0x6B80u, 0xAB41u, 0x6900u, 0xA9C1u, 0xA881u, 0x6840u,
        0x7800u, 0xB8C1u, 0xB981u, 0x7940u, 0xBB01u, 0x7BC0u, 0x7A80u, 0xBA41u,
        0xBE01u, 0x7EC0u, 0x7F80u, 0xBF41u, 0x7D00u, 0xBDC1u, 0xBC81u, 0x7C40u,
        0xB401u, 0x74C0u, 0x7580u, 0xB541u, 0x7700u, 0xB7C1u, 0xB681u, 0x7640u,
        0x7200u, 0xB2C1u, 0xB381u, 0x7340u, 0xB101u, 0x71C0u, 0x7080u, 0xB041u,
        0x5000u, 0x90C1u, 0x9181u, 0x5140u, 0x9301u, 0x53C0u, 0x5280u, 0x9241u,
        0x9601u, 0x56C0u, 0x5780u, 0x9741u, 0x5500u, 0x95C1u, 0x9481u, 0x5440u,
        0x9C01u, 0x5CC0u, 0x5D80u, 0x9D41u, 0x5F00u, 0x9FC1u, 0x9E81u, 0x5E40u,
        0x5A00u, 0x9AC1u, 0x9B81u, 0x5B40u, 0x9901u, 0x59C0u, 0x5880u, 0x9841u,
        0x8801u, 0x48C0u, 0x4980u, 0x8941u, 0x4B00u, 0x8BC1u, 0x8A81u, 0x4A40u,
        0x4E00u, 0x8EC1u, 0x8F81u, 0x4F40u, 0x8D01u, 0x4DC0u, 0x4C80u, 0x8C41u,
        0x4400u, 0x84C1u, 0x8581u, 0x4540u, 0x8701u, 0x47C0u, 0x4680u, 0x8641u,
        0x8201u, 0x42C0u, 0x4380u, 0x8341u, 0x4100u, 0x81C1u, 0x8081u, 0x4040u
};
static const unsigned char aucCRCHi[] = {                                       // for modbus RTU fast
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40
};

static const unsigned char aucCRCLo[] = {                                       // for modbus RTU fast
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
    0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
    0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
    0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
    0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
    0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
    0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
    0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
    0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
    0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
    0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
    0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
    0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
    0x41, 0x81, 0x80, 0x40
};
uint32_t reflect(uint32_t dataD, uint8_t n_bits)
{
   uint32_t reflection = 0x00000000;
   uint8_t bitcount;

   // NOTE: For efficiency sake, n_bits is not verified to be <= 32.

   // Reflect the data about the center bit.
   //
   for (bitcount = 0; (bitcount < n_bits); bitcount++)
   {

       if (dataD & 0x01)
       {
           reflection |= (1 << ((n_bits - 1) - bitcount));                      // If the LSB bit is set, set the reflection of it.
       }

       dataD = (dataD >> 1);
   }

  return (reflection);

}  /* reflect() */

// Initialise the CRC used with the fast function (noramlly you do it at boot-up)
void crc_init (void)
{
   crc_t dividend;
   crc_t remainder;
   int bitcounter;
   
   for (dividend = 0; dividend < CRC_TABLE_SIZE; dividend++)                    // Compute the remainder of each possible dividend.
   {
       remainder = dividend << (WIDTH - BITS_PER_BYTE);                         // Start with the dividend followed by zeros.

       for (bitcounter = BITS_PER_BYTE; (bitcounter > 0); bitcounter--)         // Perform modulo-2 division, a bit at a time.
       {
           if (remainder & TOPBIT)                                              // Try to divide the current data bit.
           {
               remainder = (remainder << 1) ^ POLYNOMIAL;
           }
           else
           {
               remainder = (remainder << 1);
           }
   }
   
   g_crc_table[dividend] = remainder;                                            // Store the result into the table.
  }
}  /* crc_init() */

// CRC SLOW ? Slow Calculation byte by byte of CRC
//
// crcSlow(): 185 instructions per byte of message data
//
crc_t crc_slow (uint8_t const * const p_message, uint8_t n_bytes)
{
   crc_t remainder = INITIAL_REMAINDER;
   uint8_t byte;
   uint8_t bitcount;

   for (byte = 0; (byte < n_bytes); byte++)                                     // Perform modulo-2 division, one byte at a time.
   {

       remainder ^= (REFLECT_DATA(p_message[byte]) << (WIDTH - BITS_PER_BYTE)); // Bring the next byte into the remainder.

       for (bitcount = BITS_PER_BYTE; bitcount > 0; bitcount--)                 // Perform modulo-2 division, one bit at a time.
       {

           if (remainder & TOPBIT)                                              // Try to divide the current data bit.
           {
               remainder = (remainder << 1) ^ POLYNOMIAL;
           }
           else
           {
               remainder = (remainder << 1);
           }
       }
   }
   
   return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);                     // The final remainder is the CRC result.

}  /* crc_slow() */

//
// Fast CRC computation :: crcFast(): 36 instructions per byte of message data (needs to call crc_init(); at boot
//
crc_t crc_fast (uint8_t const * const p_message, uint8_t n_bytes)
{
   crc_t remainder = INITIAL_REMAINDER;
   uint8_t byte;
   uint8_t dataZ;
   
   for (byte = 0;(byte < n_bytes); byte++)                                      // Divide the message by the polynomial, a byte at a time.
   {
      dataZ = (REFLECT_DATA(p_message[byte])) ^ (remainder >> (WIDTH - BITS_PER_BYTE));
      remainder = g_crc_table[dataZ] ^ (remainder << BITS_PER_BYTE);
   }

   return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);                     // The final remainder is the CRC.
}


//
// Compute the 32 bit CRC and enter in hash table (this is a test function for testing hence X prefix)
//
void compute_hashtable_fast32X()
{
   unsigned char  test[] = "123456789";
   crc_t crc32Result;                                                           // 32bit CRC computation
   uint8_t byteRep;                                                             // byte representation of 32 bit CRC for hash table

   ETHRXFCSET = (0x8000 | ETHRXFCSET);                                          // Enable Ethernet Filtering
#ifdef USE_CRC_FAST
   crc_init();                                                                  // initialise look up table
   crc32Result=crc_fast( test, strlen(test));                                   // calculate fast CRC32 bit
#else
   crc32Result=crc_slow( test, strlen(test));                                   // calculate fast CRC32 bit
#endif
   byteRep = (uint8_t) ((crc32Result & INT32_TO_BYTE_MASK) >> INT32_TO_BYTE_MOVE);
                                                                                // calculate a byte from the 32bit CRC
   if (byteRep <= 63)
   {
     ETHHT0 = ((uint64_t) pow(2,byteRep) | ETHHT0);                             // Set the relevant bit in hash table 1
   }
   else if ((byteRep >=64) && (byteRep <= 127))
   {
    ETHHT1 = ((uint64_t) pow(2,(byteRep >> 64)) | ETHHT1);                      // Set the relevant bit in hash table 2
   }

}

//
// Compute the 32 bit CRC and enter in hash table
// Function is passed the MAC Address of the destination address as AddData
//
void compute_hashtable_fast32( unsigned char *addData )
{
   //unsigned char  test[] = "123456789";
   crc_t crc32Result;                                                           // 32bit CRC computation
   uint8_t byteRep;                                                             // byte representation of 32 bit CRC for hash table

   ETHRXFCSET = (0x8000 | ETHRXFCSET);                                          // Enable Ethernet Filtering
#ifdef USE_CRC_FAST
   crc_init();                                                                  // initialise look up table
   crc32Result=crc_fast( addData, strlen(addData) );                            // calculate fast CRC32 bit
#else
   crc32Result=crc_slow( addData, strlen(addData) );                            // calculate slow CRC32 bit
#endif
   byteRep = (uint8_t) ((crc32Result & INT32_TO_BYTE_MASK) >> INT32_TO_BYTE_MOVE);
                                                                                // calculate a byte from the 32bit CRC
   if (byteRep <= 63)
   {
     ETHHT0 = ((uint64_t) pow(2,byteRep) | ETHHT0);                             // Set the relevant bit in hash table 1
   }
   else if ((byteRep >=64) && (byteRep <= 127))
   {
    ETHHT1 = ((uint64_t) pow(2,(byteRep >> 64)) | ETHHT1);                      // Set the relevant bit in hash table 2
   }

}

uint8_t crc8(uint8_t const *dataX, int32_t length)                              // CRC 8 Calculation
{
   unsigned char crc = 0x00U;
   unsigned char extract;
   unsigned char sum;
   int16_t i;
   uint8_t tempI;
   
   for(i=0;i<length;i++)
   {
      extract = *dataX;
      for (tempI = 8; tempI; tempI--)
      {
         sum = (crc ^ extract) & 0x01U;
         crc >>= 1U;
         if (sum)
            crc ^= 0x8CU;
         extract >>= 1U;
      }
      dataX++;
   }
   return crc;
}

uint8_t crc8_dvb_s2(uint8_t crc, unsigned char a)                               // CRC8 DVB S2 algorythm used for RunCam
{
   int8_t ii;
   
   crc ^= a;
   for (ii = 0U; ii < 8U; ++ii)
   {
      if (crc & 0x80U)
      {
         crc = (crc << 1) ^ 0xD5U;
      }
      else 
      {
         crc = crc << 1;
      }
    }
    return crc;
}


uint8_t crc8_dvb_s2_update(uint8_t crc, const void *dataX, uint32_t length)     // CRC8 DVB-T iteration
{

    const uint8_t *p = (const uint8_t *) dataX;                                 // type cast the unsigned char* to an int to allow for iteration
    const uint8_t *pend = p + (uint8_t) length;
    
    for (; p != pend; p++)                                                      // to the length of data
    {
        crc = crc8_dvb_s2(crc, (unsigned char) *p);                             // calculate the iterative crc
    }

    return crc;

}

// CRC for SimpleBGC version > 2.688 : Think this is type CRC16_ARC
//
// CRC for SimpleBGC version > 2.688 : Think this is type CRC16_ARC
//
uint16_t crc16_arc_calculate(uint16_t length, uint8_t const * const p_message)
{
  uint16_t counter;
  uint16_t polynom = 0x8005U;                                                   // 0x8005 poly and crc[0][1] = 0 is ARC implementation
  uint16_t crc_register = 0U;                                                   // Initial value is a zero
  uint8_t shift_register;
  uint8_t data_bit, crc_bit;
  for (counter = 0; counter < length; counter++)
  {
     for (shift_register = 0x01U; shift_register > 0x00U; shift_register <<= 1U)
         {
        data_bit = (p_message[counter] & shift_register) ? 1U : 0U;
                crc_bit = crc_register >> 15U;
        crc_register <<= 1U;
        if (data_bit != crc_bit) crc_register ^= polynom;
    }
  }

  return crc_register;                                                          // Return the calculated CRC
}

// This is the CRC ARC FAST (Consider testing and replacing after testing)
//
uint16_t crc16_arc_fast(uint16_t crc, unsigned char const *buffer, uint16_t len)
{
   while (len--)
      crc = (uint16_t)((crc >> 8U)^(crc16arc_table[(crc^(*buffer++))&0xffu]));
   return crc;
}

//
// modbus RTU CRC function
//
uint16_t usMBCRC16( unsigned char * pucFrame, uint16_t usLen )
{
    unsigned char ucCRCHi = 0xFFU;
    unsigned char ucCRCLo = 0xFFU;
    int16_t iIndex;

    while( usLen-- )
    {
        iIndex = ucCRCLo ^ *( pucFrame++ );
        ucCRCLo = ( unsigned char)( ucCRCHi ^ aucCRCHi[iIndex] );
        ucCRCHi = aucCRCLo[iIndex];
    }
    return ( uint16_t )( ucCRCHi << 8U | ucCRCLo );
}

/* the function returns the LRC as a type unsigned char */
uint8_t usMBAsciiLRC(unsigned char *auchMsg, uint16_t usLen)
{
   uint8_t uchLRC = 0U;                                                         /* LRC char initialized */

    while( usLen-- )
    {
        uchLRC += *auchMsg++;                                                   /* add buffer byte without carry */
    }

   return ( (uint8_t) (-((char)uchLRC)) );                                      /* return twos complement */
}
#endif