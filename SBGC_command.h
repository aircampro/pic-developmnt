 /*

        SimpleBGC Serial API  library - definition of commands

        More info: http://www.basecamelectronics.com/serialapi/





        Copyright (c) 2014-2015 Aleksei Moskalenko

        All rights reserved.



        See license info in the SBGC.h

*/



#ifndef  __SBGC_command__

#define  __SBGC_command__



//#include <inttypes.h>
#include "inttypes.h"

#include "SBGC_rc.h"

#define SBGC_NO_CONFIRM 0
#define SBGC_CONFIRM 1

// Size of header and checksums

#define SBGC_CMD_NON_PAYLOAD_BYTES 5

// Max. size of a command after packing to bytes
#define SBGC_CMD_MAX_BYTES 255

// Max. size of a payload data
#define SBGC_CMD_DATA_SIZE (SBGC_CMD_MAX_BYTES - SBGC_CMD_NON_PAYLOAD_BYTES)

// For MOTORS CMD
#define SBGC_MOTOR_OFF_NORMAL 0
#define SBGC_MOTOR_ON 1
#define SBGC_MOTOR_OFF_BREAK 1
#define SBGC_MOTOR_OFF_SAFE 2

#define SBGC_NO_CMD_FOUND 200                                                   // defined this state as No Message Found.

////////////////////// Command ID definitions ////////////////

#define SBGC_CMD_READ_PARAMS  82

#define SBGC_CMD_WRITE_PARAMS  87

#define SBGC_CMD_REALTIME_DATA  68

#define SBGC_CMD_BOARD_INFO  86

#define SBGC_CMD_CALIB_ACC  65

#define SBGC_CMD_CALIB_GYRO  103

#define SBGC_CMD_CALIB_EXT_GAIN  71

#define SBGC_CMD_USE_DEFAULTS  70

#define SBGC_CMD_CALIB_POLES  80

#define SBGC_CMD_RESET  114

#define SBGC_CMD_HELPER_DATA 72

#define SBGC_CMD_CALIB_OFFSET  79

#define SBGC_CMD_CALIB_BAT  66

#define SBGC_CMD_MOTORS_ON   77

#define SBGC_CMD_MOTORS_OFF  109

#define SBGC_CMD_CONTROL   67

#define SBGC_CMD_TRIGGER_PIN  84

#define SBGC_CMD_EXECUTE_MENU 69

#define SBGC_CMD_GET_ANGLES  73

#define SBGC_CMD_CONFIRM  67

#define SBGC_CMD_CONTROL_CONFIG 90



// Starting from board ver.3.0

#define SBGC_CMD_BOARD_INFO_3  20

#define SBGC_CMD_READ_PARAMS_3 21

#define SBGC_CMD_WRITE_PARAMS_3 22

#define SBGC_CMD_REALTIME_DATA_3  23

#define SBGC_CMD_SELECT_IMU_3 24

#define SBGC_CMD_REALTIME_DATA_4  25

#define SBGC_CMD_ENCODERS_CALIB_OFFSET_4  26

#define SBGC_CMD_ENCODERS_CALIB_FLD_OFFSET_4 27

#define SBGC_CMD_READ_PROFILE_NAMES 28

#define SBGC_CMD_WRITE_PROFILE_NAMES 29



#define SBGC_CMD_QUEUE_PARAMS_INFO_3 30

#define SBGC_CMD_SET_ADJ_VARS_VAL 31

#define SBGC_CMD_SAVE_PARAMS_3 32

#define SBGC_CMD_READ_PARAMS_EXT 33

#define SBGC_CMD_WRITE_PARAMS_EXT 34

#define SBGC_CMD_AUTO_PID 35

#define SBGC_CMD_SERVO_OUT 36

#define SBGC_CMD_BODE_TEST_START_STOP 37

#define SBGC_CMD_BODE_TEST_DATA 38

#define SBGC_CMD_I2C_WRITE_REG_BUF 39

#define SBGC_CMD_I2C_READ_REG_BUF 40

#define SBGC_CMD_WRITE_EXTERNAL_DATA 41

#define SBGC_CMD_READ_EXTERNAL_DATA 42

#define SBGC_CMD_READ_ADJ_VARS_CFG 43

#define SBGC_CMD_WRITE_ADJ_VARS_CFG 44

#define SBGC_CMD_API_VIRT_CH_CONTROL 45

#define SBGC_CMD_ADJ_VARS_STATE 46

#define SBGC_CMD_EEPROM_WRITE 47

#define SBGC_CMD_EEPROM_READ 48

#define SBGC_CMD_CALIB_INFO 49

#define SBGC_CMD_SIGN_MESSAGE_3 50

#define SBGC_CMD_BOOT_MODE_3 51

#define SBGC_CMD_SYSTEM_STATE 52

#define SBGC_CMD_READ_FILE 53

#define SBGC_CMD_WRITE_FILE 54

#define SBGC_CMD_FS_CLEAR_ALL 55

#define SBGC_CMD_AHRS_HELPER 56

#define SBGC_CMD_RUN_SCRIPT 57

#define SBGC_CMD_SCRIPT_DEBUG 58

#define SBGC_CMD_CALIB_MAG 59

#define SBGC_CMD_UART_BYPASS 60

#define SBGC_CMD_GET_ANGLES_EXT 61

#define SBGC_CMD_READ_PARAMS_EXT2 62

#define SBGC_CMD_WRITE_PARAMS_EXT2 63

#define SBGC_CMD_GET_ADJ_VARS_VAL 64

#define SBGC_CMD_CALIB_MOTOR_MAG_LINK 74

#define SBGC_CMD_GYRO_CORRECTION 75

#define SBGC_CMD_DATA_STREAM_INTERVAL 85
#define SBGC_CMD_REALTIME_DATA_CUSTOM 88
#define SBGC_CMD_BEEP_SOUND 89
#define SBGC_CMD_ENCODERS_CALIB_OFFSET_4  26
#define SBGC_CMD_ENCODERS_CALIB_FLD_OFFSET_4 27
#define SBGC_CMD_CONTROL_CONFIG 90
#define SBGC_CMD_CALIB_ORIENT_CORR 91
#define SBGC_CMD_COGGING_CALIB_INFO 92
#define SBGC_CMD_CALIB_COGGING 93
#define SBGC_CMD_CALIB_ACC_EXT_REF 94
#define SBGC_CMD_PROFILE_SET 95
#define SBGC_CMD_CAN_DEVICE_SCAN 96
#define SBGC_CMD_CAN_DRV_HARD_PARAMS 97
#define SBGC_CMD_CAN_DRV_STATE 98
#define SBGC_CMD_CAN_DRV_CALIBRATE 99
#define SBGC_CMD_READ_RC_INPUTS 100
#define SBGC_CMD_REALTIME_DATA_CAN_DRV 101
#define SBGC_CMD_EVENT 102
#define SBGC_CMD_READ_PARAMS_EXT3 104
#define SBGC_CMD_WRITE_PARAMS_EXT3 105
#define SBGC_CMD_EXT_IMU_DEBUG_INFO 106
#define SBGC_CMD_SET_DEVICE_ADDR 107
#define SBGC_CMD_AUTO_PID2 108
#define SBGC_CMD_EXT_IMU_CMD 110
#define SBGC_CMD_READ_STATE_VARS 111
#define SBGC_CMD_WRITE_STATE_VARS 112
#define SBGC_CMD_SERIAL_PROXY 113
#define SBGC_CMD_IMU_ADVANCED_CALIB 115
#define SBGC_CMD_API_VIRT_CH_HIGH_RES 116

#define CMD_SET_DEBUG_PORT 249 
#define CMD_MAVLINK_INFO 250 
#define CMD_MAVLINK_DEBUG 25

#define SBGC_CMD_DEBUG_VARS_INFO_3 253

#define SBGC_CMD_DEBUG_VARS_3  254

#define SBGC_CMD_ERROR  255

// Whwn requesting the board info define resend time
#define SBGC_FIRMWR_RSND_DELAY 2000                                             // Time to wait in ticks for a good status to comeback after a reply
#define SBGC_FIRMWR_NOREP_DELAY 10000                                           // Time in ticks to wait until resending the other firmware version request





#endif //__SBGC_command__