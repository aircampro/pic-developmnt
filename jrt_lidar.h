// An API container and command library for the JRT Lidar IT03M
//
#ifndef  __jrt__

#define  __jrt__

// #include "defintions.h"
#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define JRTPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define JRTPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

// define the start commands and message id for the protocol
#define JRT_READ_MODULE_STATUS_STX 0xFAU                                        // Byte means read the module status (module status write)
#define JRT_READ_MODULE_STATUS_MSG 0x04U                                        // 2nd byte READ MODULE STATUS

#define JRT_WRITE_MODULE_STATUS_STX 0xFBU                                       // Byte means read the module status (module status read)
#define JRT_WRITE_MODULE_STATUS_MSG 0x05U                                       // 2nd byte READ MODULE STATUS

#define JRT_WRITE_START_MEASURE_STX 0xFAU                                       // Byte means read the module status (start/stop measure request)
#define JRT_WRITE_START_MEASURE_MSG 0x01U                                       // 2nd byte START MEASURE
#define JRT_WRITE_START_MEASURE_CMD 0x01U                                       // 1st byte of payload START MEASURE
#define JRT_WRITE_STOP_MEASURE_CMD 0x00U                                        // 1st byte of payload START MEASURE

#define JRT_READ_STARTSTOP_MEASURE_STX 0xFBU                                    // Byte means read the module status (reply to start/stop request)
#define JRT_READ_STARTSTOP_MEASURE_MSG 0x02U                                    // 2nd byte START MEASURE

#define JRT_READ_DATA_MEASURE_STX 0xFBU                                         // Byte means read the module status (polled reply data once started above)
#define JRT_READ_DATA_MEASURE_MSG 0x03U                                         // 2nd byte START MEASURE

// values for data_valid_ind of JRT_read_measure_t structure
#define JRT_READ_DATA_VALID_IND_OK 0U                                           // ok
#define JRT_READ_DATA_VALID_IND_OOR 1U                                          // out range
#define JRT_READ_DATA_VALID_IND_WEAK1_S1 2U                                     // weak signal (scene 1)
#define JRT_READ_DATA_VALID_IND_WEAK1_S2 3U                                     // weak signal (scene 2)
#define JRT_READ_DATA_VALID_IND_WEAK1_S3 4U                                     // weak signal (scene 3)
#define JRT_READ_DATA_VALID_IND_LO_TEMP 5U                                      // low temp
#define JRT_READ_DATA_VALID_IND_HI_TEMP 6U                                      // high temp
#define JRT_READ_DATA_VALID_IND_WEAK4 7U                                        // weak signal (scene 4)

// values for the BrdStatus of the JRT_read_module_status_read_t structure
#define JRT_READ_MODULE_ERROR1 1U                                               // input power low
#define JRT_READ_MODULE_ERROR2 2U                                               // internal error (reboot) cycle power of unit exernally to do so
#define JRT_READ_MODULE_ERROR3 3U                                               // temperature too high
#define JRT_READ_MODULE_ERROR4 4U                                               // temperature too low
#define JRT_READ_MODULE_ERROR5 5U                                               // power too high
#define JRT_READ_MODULE_ERROR6 6U                                               // internal error (reboot) cycle power of unit exernally to do so
#define JRT_READ_MODULE_ERROR7 7U                                               // internal error (reboot) cycle power of unit exernally to do so
#define JRT_READ_MODULE_ERROR8 8U                                               // internal error (reboot) cycle power of unit exernally to do so
#define JRT_READ_MODULE_ERROR9 9U                                               // internal error (reboot) cycle power of unit exernally to do so
#define JRT_READ_MODULE_ERROR10 10U                                             // internal error (reboot) cycle power of unit exernally to do so

#define JRT_DEFAULT_BAUD 115200UL                                               // Default serial baud rate

 // READ MODULE STATUS
JRTPACKED(
typedef struct {                                                                // Write to instrument request to READ MODULE STATUS

        uint8_t MsgType;

        uint8_t MsgCode;

        uint8_t TransId;

        uint8_t PayLoadLen;


}) JRT_read_module_status_write_t;

JRTPACKED(
typedef struct {                                                                // Read reply to READ MODULE STATUS

        uint8_t MsgType;

        uint8_t MsgCode;

        uint8_t TransId;

        uint8_t PayLoadLen;
        
        uint16_t Temp;
        
        uint16_t VIN;
        
        uint16_t Freq;
        
        uint16_t BrdStatus;


}) JRT_read_module_status_read_t;

// Measure request
JRTPACKED(
typedef struct {                                                                // Write container or mesaure instruction request (start / stop) change payload  - reply is same struct

        uint8_t MsgType;

        uint8_t MsgCode;

        uint8_t TransId;

        uint8_t PayLoadLen;

        uint8_t Payload[4u];

}) JRT_write_measure_t;

// Measure data reply once statred with measure request
JRTPACKED(
typedef struct {                                                                // Read container for mesaure instruction (periodic reply)

        uint8_t MsgType;

        uint8_t MsgCode;

        uint8_t TransId;

        uint8_t PayLoadLen;

        uint16_t data_valid_ind;
        
        uint16_t filter_dist;
        
        uint16_t current_dist;
        
        uint16_t temp;
        
        uint32_t SigRef;

}) JRT_read_measure_t;
#endif                                                                          // end __jrt___ header