//=====================================================================================================
// MahonyAHRS.h
//=====================================================================================================
//
// Madgwick's implementation of Mayhony's AHRS algorithm.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
//
// Date                        Author                        Notes
// 29/09/2011        SOH Madgwick    Initial release
// 02/10/2011        SOH Madgwick        Optimised for reduced CPU load
//
//=====================================================================================================
#ifndef MahonyAHRS_h
#define MahonyAHRS_h
//#include "ComGS.h"
//extern COMGS_quarternion quartCoOrd;

//----------------------------------------------------------------------------------------------------
// Variable declaration

extern volatile float32_t twoKp;                                                // 2 * proportional gain (Kp)
extern volatile float32_t twoKi;                                                // 2 * integral gain (Ki)
extern volatile float32_t q0, q1, q2, q3;                                        // quaternion of sensor frame relative to auxiliary frame

//---------------------------------------------------------------------------------------------------
// Function declarations done in COMGs as works on global structure (comment out if you prefer to use volatiles)

//void MahonyAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
//void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);

#endif
//=====================================================================================================
// End of file
//=====================================================================================================