#ifndef __definitions_h
#define __definitions_h

#define __GNUC__                                                                // define the compiler as a derivative of gcc
#define CAM_USE_ASCII                                                           // define if you want to use ascii for the data in the Xstream message

#define Lo(param) ((char *)&param)[0]                                           // Lo byte
#define Hi(param) ((char *)&param)[1]                                           // Hi byte
#define ROUND(x,y) floor(x * pow(10, y))/pow(10, y)                             // No round in mikroE so define it here
#define FMOD(x,y) (x-y*floor(x/y))                                              // Floating modulus
#define PI (double) (4.0f * atan(1.0f))                                         // Define PI constant alt to const double pi = 4.0 * atan(1.0);
#define NaN (255U<<23U)                                                         // Not a number defined as all exponent bits set
#define IsaNan(v) (((uint32_t) v & 0x7fffffffUL) > 0x7f800000UL)                // Function to check for a NaN (not a number or invalid float)
#define NULL '\0'                                                               // Define the NULL string terminator
#define UNIX_REFERENCE_YEAR 1970U                                               // Year for UNIX Timestamp (time difference or sending EGD)
#define BITCOUNT(x) (((BX_(x)+(BX_(x)>>4U)) & 0x0F0F0F0FUL) % 255U)
#define BX_(x) ((x) - (((x)>>1)&0x77777777UL) - (((x)>>2U)&0x33333333UL) - (((x)>>3U)&0x11111111UL))
#define MD5_MAX_LEN 32U                                                         // length of the string returned by the getFileMd5() java call and returned in an AJAX reply

#ifdef __GNUC__
#define ALIGNED(x) __attribute__ ((aligned(x)))                                 // Allign to boundaries of x
#endif

// --------------- Set-up the system -------------------------------------------
//
//
#define TIMER1_NEEDED                                                           // To process UDP stack timer for retry
///  #define GPS_INCLUDED                                                       If we want GPS from the ducati open source version
//#define ROBOT_HELPER                                                            // If we want the Robot helper library from NATE 711 DOGGO Project
///  #define PID_LOOPS                                                          If we want the PID Loops and Auto Tuning
//   #define GEF_EGD_PLC                                                        // GEFanuc PLC Connected helper library for EGD over UDP
#define GPS_INCLUDED2                                                           // If want UBLOX or Futano GPS code
#define GPS_BAUD_RATE 9600U                                                     // Define the baud rate to be used for the GPS

#define SEQ_CAM_USED                                                            // sequoia camera is connected
//#define YI_CAM_USED                                                             // Yi Action Cam used
//#define RUN_CAM_USED                                                            // Run Cam Used
#define SBGC_GIMBAL_HMI                                                         // SBGC Gimbal is being used
#define SBGC_GIMBAL_JOY                                                         // SBGC Gimbal joystick is connected
#define CAMERA_TYPE XS_CAM                                                      // Define the encoder set-up we have
#define UART4_INTERUPT                                                          // If you want to have serial XS encoder commands
//#define CANON_CAM_USED                                                          // Canon library
//#define PENTAX_CAM_USED                                                         // Pentax library
//#define DMX_USED                                                                // DMX used
//#define DALI_USED                                                               // Dali Used
//#define BOSCH_BME680_USED                                                       // Humidity sensor attached on i2c
#define ALARM_BANNER_USED                                                       // Alarm Banner used
//#define PELCO_PTZ_USED                                                          // Pelco-D PTZ protocol required
//#define SMPTE_USED                                                              // SMPTE or Midi MTC required for timestamping video
//#define ART_NET_USED                                                            // Art-Net support for DMX512 required
//#define LW3_SWITCH_USED                                                         // Lightwave LW3 protocol used for audio/video switch control
//#define COLOR_CODEC_USED                                                        // Color codec conversion library included
//#define LWNX_LIDDAR_USED                                                        // Lightware OptoElectronics Liddar attached
//#define MODBUS_NEEDED                                                           // Modbus protocol conenction
//#define LEDDARTECH_LIDDAR_USED                                                  // LeddarTech Liddar being used
//#define JRT_LIDDAR_USED                                                         // JRT Liddar sensor used
//#define CBOR_COMS_USED                                                          // Using CBOR for iOt
//#define JSON_COMS_USED                                                          // Using JSON for webservice
//#define CAN_UDP_USED                                                            // Can over UDP being used

// ============== CAMERA TYPES Supported =======================================
#define XS_CAM (1<<0)                                                           // Bit set for we have AMP XS Encoder Cameras
#define RC_CAM (1<<1)                                                           // Bit set to say we have Run Cam Cameras
#define XY_CAM (1<<2)                                                           // Bit set to say we have Xiong Yi Action Camera

//Node
#define AIR 1U
#define MASGS 2U
#define FIRGS 3U
#define COMGS 4U
#define GIMJOY 5U

// ===== Serial Communication ==================================================
//
// If we want to use UART2 as serial for simpleBGC (direct serial)
#define UART2_INTERUPT                                                          //  Add this if you want to use the UART2 serial port for SimpleSBGC

#define ACK 0x6U                                                                // acknowledge char
#define NAK 0x15U                                                               // not acknowledge char

#define TRUE 1U
#define FALSE 0U
#define CLEAR 0U
#define SET 1U
#define U1STA_Add 0X6010U
#define U2STA_Add 0X6810U
#define U3STA_Add 0X6410U
#define U4STA_Add 0X6210U
#define U5STA_Add 0X6A10U

#define UDPSTA_Add 0X123U
#define COMCLSTA_Add 0XB102U

#define TCPSTL_Add 0XB354U

#define U1STA_OERR 0X01U
#define U2STA_OERR 0X01U
#define U3STA_OERR 0X01U
#define U4STA_OERR 0X01U
#define U5STA_OERR 0X01U
#define U1STA_FERR 0X02U
#define U2STA_FERR 0X02U
#define U3STA_FERR 0X02U
#define U4STA_FERR 0X02U
#define U5STA_FERR 0X02U
#define U1STA_PERR 0X03U
#define U2STA_FERR 0X02U
#define U3STA_FERR 0X02U
#define U4STA_FERR 0X02U
#define U5STA_FERR 0X02U
#define U2STA_B2UR 0X01U

#define UDPSTA_UART2 0X01U

// ================ UART2 Serial state engine
#define UART2_BUFFER_EMPTY 0U
#define UART2_BYTE_IN_BUFFER 1U
#define UART2_PACKET_IN_BUFFER 2U

// =============== UART4.State
#define UART4_BUFFER_EMPTY 0U
#define UART4_BYTE_IN_BUFFER 1U
#define UART4_SEND_UDP 3U
#define UART4_CHECK_ACK_CRC 4U
#define UART4_WAIT_ACK_CRC 5U
#define UART4_CHECK_ACK_TIME_OUT 6U
#define UART4_ACK_TIME_OUT 7U
#define UART4_RECEIVED_ACK_CRC 2U
#define UART4_PACKET_IN_BUFFER 8U                                               // plus carry states of g_extended

//U2STA_Add
#define U2STA_B2TO 1U
#define UART2_BUFFER_OVERFLOW 2U
#define UART2_BAD_CRC 3U
// UDP UDPSTA_Add
#define UDP_PACKET_SEND_FAIL 0U
#define UDP_PACKET_SEND_GOOD 1U
#define UDP_UART2_PACKET_ACK_TIMEOUT 2U
#define UDP_PACKET_ACK_RECEIVED 3U
#define UDP_UART2_SEND_PACKET_FAIL 4U
#define UDP_UART2_Packet_RECEIVED 5U
#define UDP_UART2_Packet_BAD_CRC 6U

#define COMCLSTA_ 0Xxx
#define COMCLSTA_INIT 0X01U
#define COMSESTA_INIT 0x01U

// ===== Ethernet UDP and TCP Frame send state engine ==========================
#define ETH_SEND_UDP 3U
#define ETH_CHECK_ACK_CRC 4U
#define ETH_WAIT_ACK_CRC 5U                                                     // UDP or TCP packet was received
#define ETH_CHECK_ACK_TIME_OUT 6U
#define ETH_ACK_TIME_OUT 7U
#define ETH_RECEIVED_ACK_CRC 8U
#define ETH_ACK_SENT_TCP 9U                                                     // For TCP a dummy ack was sent
#define ETH_PROCESSED_TCP 10U                                                   // We have processed the incoming packet
#define ETH_ACK_OLD_DATA_REQ 11U                                                // We were asked to re-transmit the existing data
#define ETH_ACK_RESENT 12U                                                      // We re-sent an ACK from the SYN_ACK received stage
#define ETH_NEW_TCP_DATA 13U                                                    // New TCP data ready to send
#define ETH_OLD_DATA_SENT 14U                                                   // Old data was re-transmitted

#define ETHER_LINK_STATE 1U
#define SOCKET_STATE 2U                                                         // Socket status (used_socket ->state)
#define Eth_Progress 3U                                                         // Eth_Process_Status
#define CONNECT_TCP_STATE 4U                                                    // = Net_Ethernet_Intern_connectTCP

// EatherConState
#define UNDEFINED 0U
#define CLOSE_SOCKET 1U
#define OPEN_SOCKET 2U
#define WAIT_SOCKET_OPEN 3U
#define SEND_TCP_MSG 4U
#define CHECK_MSG_GONE 5U
#define TCP_SEND_ERROR 6U
#define ETHUNDEFINED 7U
#define TX_SUCCESS 8U
#define WAIT4CLOSE 9U


#define AIR_LINK_UP 10U
#define AIR_LINK_DOWN 11U
#define MASTER_LINK_UP 12U
#define MASTER_LINK_DOWN 13U
#define FIRE_LINK_UP 14U
#define FIRE_LINK_DOWN 15U
#define LINK_DOWN 16U

// Open SktStatus
#define BAD_SYN_TX 0U
#define GOOD_SYN_TX 1U
#define NO_SOCKET_AVAIL 2U
#define NO_ARP_RESOLVE 3U
#define DUMMY_ACK_ERROR 6U
#define RESPONSE_TIMEOUT 5U
#define FIN_CLOSE_ERROR 7U
#define FIN_PROCESS_ERR 8U
#define FIN_TIMEOUT_ERR 9U
// Timer 2
#define ENABLE_T2  0x8008U                                                      // 0x0000
#define DISABLE_T2 0x0000U

// Control of the INTCON (Global and periferal interrupts)
#define GIE_ON 0b10000000                                                       // Global interrupt enable flag
#define GIE_OFF 0b01111111
#define PEIE_ON 0b01000000                                                      // Periferal interrupt enable flag
#define PEIE_OFF 0b10111111
#define T0IE_ON 0b00100000                                                      // Timer 0 overflow interrupt enable flag
#define T0IE_OFF 0b11011111
#define INTE_ON 0b00010000                                                      // External interrupt enable flag
#define INTE_OFF 0b11101111
#define RBIE_ON 0b00001000                                                      // RB Port change interrupt enable flag
#define RBIE_OFF 0b11110111
#define T0IF_ON 0b00000100                                                      // T0 overflow interrupt flag
#define T0IF_OFF 0b11111011
#define INTF_ON 0b00000010                                                      // external interrupt flag
#define INTF_OFF 0b11111101
#define RBIF_ON 0b00000001                                                      // RB port interrupt flag
#define RBIF_OFF 0b11111110

// PIE1 and PIE2 periferal interrupts are defined in the relevant datasheets
// The following are available
// Timer 1 overflow
// TMR2 to PR2 match
// CCP1 and 2 interrupt
// SSPIE synchronous serial port  (needs to be enabled for i2c interrupts)
// RCIE USART Rx interrupt
// TXIE UDSTY Tx interrupt
// ADIE A/D converter
// ADCIE slope A/D trip interrupt
// OVFIE A/D timer
// PSPIE parallel slave port
// EEIE write complete interrupt
// LCDIE LCD interrupt
// CMIE Comparator interrupt enable

// ======================== TCP Socket States ==================================
//      TCP A                                                TCP B
//
//  1.  TCP_STATE_CLOSED                                 TCP_STATE_LISTEN
//
//  2.  SYN-SENT    --> <SEQ=100><CTL=SYN>               --> SYN-RECEIVED
//
//  3.  ESTABLISHED <-- <SEQ=300><ACK=101><CTL=SYN,ACK>  <-- SYN-RECEIVED/
//
//  4.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK>       --> ESTABLISHED
//
//  5.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK><DATA> --> ESTABLISHED
//
#define TCP_STATE_CLOSED 0U                                                     // This is the default state that each connection starts in before the process of establishing it begins
#define TCP_STATE_LISTEN 1U                                                     // A device (normally a server) is waiting to receive a synchronize (SYN) message from a client. It has not yet sent its own SYN message
#define TCP_STATE_SYN_SENT 4U                                                   // The device (normally a client) has sent a synchronize (SYN) message and is waiting for a matching SYN from the other device (usually a server)
#define TCP_STATE_SYN_RECEIVED 9U                                               // The device has both received a SYN from its partner and sent its own SYN. It is now waiting for an ACK to its SYN to finish connection setup
#define TCP_STATE_ESTABLISHED 3U                                                // Data can be exchanged freely once both devices in the connection enter this state. This will continue until the connection is closed
#define TCP_STATE_CLOSE_WAIT 10U                                                // The device has received a close request (FIN) from the other device. It must now wait for the application to acknowledge this request and generate a matching request
#define TCP_STATE_LAST_ACK 11U                                                  // A device that has already received a close request and acknowledged it, has sent its own FIN and is waiting for an ACK to this request
#define TCP_STATE_FIN_WAIT_1 5U                                                 // A device in this state is waiting for an ACK for a FIN it has sent, or is waiting for a connection termination request from the other device
#define TCP_STATE_FIN_WAIT_2 6U                                                 // A device in this state has received an ACK for its request to terminate the connection and is now waiting for a matching FIN from the other device
#define TCP_STATE_CLOSING 12U                                                   // The device has received a FIN from the other device and sent an ACK for it, but not yet received an ACK for its own FIN message
#define TCP_STATE_TIME_WAIT 13U                                                 // The device has now received a FIN from the other device and acknowledged it, and sent its own FIN and received an ACK for it. We are done, except for waiting to ensure the ACK is received and prevent potential overlap with new connections
#define TCP_STATE_RETRANSMIT 7U                                                 // Need to re-transmit the last message

#define TCP_NO_SYN_SEND 0U                                                      // 0 - no successful transmit SYN segment.
#define TCP_SYN_SUCCESS 1U                                                      // 1 - successful transmit SYN segment.
#define TCP_NO_SOCK 2U                                                          // 2 - no available socket.
#define TCP_NO_ARP 3U                                                           // 3 - no ARP resolve

#define UART6_INTERUPT                                                          // Liddar is requested connected on UART6 at 115200 baud
#ifdef UART6_INTERUPT
#define LIDDAR_BAUD_RATE 115200U                                                // slowest rate but its the fastest for the PIC32
#define LIDDAR_PORT 6U                                                          // speaks on UART6
#ifndef LWNX_LIDDAR_USED                                                        // Lightware OptoElectronics Liddar attached
#define LWNX_LIDDAR_USED                                                        // Lightware OptoElectronics Liddar attached
#endif
#endif

#ifdef LWNX_LIDDAR_USED
#ifndef UART6_INTERUPT
#define UART6_INTERUPT
#define LIDDAR_BAUD_RATE 115200U                                                // slowest rate but its the fastest for the PIC32
#define LIDDAR_PORT 6U                                                          // speaks on UART6
#endif
#endif

#define UART5_INTERUPT                                                          // RunCam is requested
#define RC_BAUD_RATE 9600U                                                      // baud rate for run cam

#define OD_BAUD_RATE 9600U                                                      // Odrive Baud Rate

#define MCU_NAME PIC32MX795F512L                                                // define the MCU you are using

// use type_str to return the type of T as a string
// Add you're own structures as A
#define type_str(T) _Generic( (T), int: "int",\
long: "long",\
uint8_t: "uint8_t", \
uint16_t: "uint16_t", \
uint32_t: "uint32_t", \
uint64_t: "uint64_t", \
int8_t: "int8_t", \
int16_t: "int16_t", \
int32_t: "int32_t", \
int64_t: "int64_t", \
float32_t: "float32_t", \
float64_t: "float64_t", \
char: "char", \
float: "float", \
unsigned char: "uchar", \
size_t: "size_t", \
A: "A",\
default: "Unknown type")

// show the number of arguments passed to the macro (considser use if prob with va_arg)
#define PP_NARG(...) \
    PP_NARG_(__VA_ARGS__,PP_RSEQ_N())

#define PP_NARG_(...) \
    PP_ARG_N(__VA_ARGS__)

#define PP_ARG_N( \
     _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
    _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
    _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
    _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
    _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
    _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
    _61,_62,_63,  N, ...) N

#define PP_RSEQ_N() \
    63,62,61,60,                   \
    59,58,57,56,55,54,53,52,51,50, \
    49,48,47,46,45,44,43,42,41,40, \
    39,38,37,36,35,34,33,32,31,30, \
    29,28,27,26,25,24,23,22,21,20, \
    19,18,17,16,15,14,13,12,11,10, \
     9, 8, 7, 6, 5, 4, 3, 2, 1, 0
     
#define NUM_ARGS_PASSED(...) PP_NARG(__VA_ARGS__)                               // usage NUM_ARGS_PASSED(1,22,3,40,15) returns 5 as num_arg


#endif