//-------------------------------------------------------------------------------------------------
// The LWNX protocol is a binary based protocol for reading and writing data to LightWare devices.
// Taken from Robert Gowans original library
// https://github.com/LightWare-Optoelectronics/sf40-samples/tree/master/src
//
//-------------------------------------------------------------------------------------------------
// =============== Ported to PIC32 device miKroE C by ACP Aviation ===============================
//                 External declarations for the C Code
//
#ifndef LwNx_H                                                                  // if not defined then define (include once)
#define LwNx_H
#include"lwNx_defs.h"

#ifdef lwserial                                                                 // lwserial means original library not pic32 version being used. use definitions.h
#pragma once
#endif

#ifdef lwserial
#include "common.h"
#endif

//----------------------------------------------------------------------------------------------------------------------------------
// Helper utilities.
//----------------------------------------------------------------------------------------------------------------------------------
// Create a CRC-16-CCITT 0x1021 hash of the specified data.
//extern uint16_t lwnxCreateCrc(uint8_t* Data, uint16_t Size);

// Breaks an integer firmware version into Major, Minor, and Patch.
extern void lwnxConvertFirmwareVersionToStr(uint32_t Version, char* String);
//----------------------------------------------------------------------------------------------------------------------------------
// LWNX protocol implementation.
//----------------------------------------------------------------------------------------------------------------------------------
// Prepare a response packet for a new incoming response.
extern void lwnxInitResponsePacket(lwResponsePacket* Response);

#ifdef lwserial                                                                 // for linux or windows device
// Waits to receive a packet of specific command id.
// Does not return until a response is received or a timeout occurs.
extern uint8_t lwnxRecvPacket(lwSerialPort* Serial, uint8_t CommandId, lwResponsePacket* Response, uint32_t TimeoutMs);

// Returns true if full packet was received, otherwise finishes immediately and returns false while waiting for more data.
extern uint8_t lwnxRecvPacketNoBlock(lwSerialPort* Serial, uint8_t CommandId, lwResponsePacket* Response);

// Composes and sends a packet.
extern void lwnxSendPacketBytes( lwSerialPort* Serial, uint8_t CommandId, uint8_t Write, uint8_t* Data, uint32_t DataSize );

// Handle both the sending and receving of a command.
// Does not return until a response is received or all retries have expired.
extern uint8_t lwnxHandleManagedCmd(lwSerialPort* Serial, uint8_t CommandId, uint8_t* Response, uint32_t ResponseSize, bool Write = false, uint8_t* WriteData = NULL, uint32_t WriteSize = 0);

//----------------------------------------------------------------------------------------------------------------------------------
// Command functions.
//----------------------------------------------------------------------------------------------------------------------------------
// Issue read commands.
extern uint8_t lwnxCmdReadInt8(lwSerialPort* Serial, uint8_t CommandId, int8_t* Response);
extern uint8_t lwnxCmdReadInt16(lwSerialPort* Serial, uint8_t CommandId, int16_t* Response);
extern uint8_t lwnxCmdReadInt32(lwSerialPort* Serial, uint8_t CommandId, int32_t* Response);
extern uint8_t lwnxCmdReadUInt8(lwSerialPort* Serial, uint8_t CommandId, uint8_t* Response);
extern uint8_t lwnxCmdReadUInt16(lwSerialPort* Serial, uint8_t CommandId, uint16_t* Response);
extern uint8_t lwnxCmdReadUInt32(lwSerialPort* Serial, uint8_t CommandId, uint32_t* Response);
extern uint8_t lwnxCmdReadString(lwSerialPort* Serial, uint8_t CommandId, char* Response);
extern uint8_t lwnxCmdReadData(lwSerialPort* Serial, uint8_t CommandId, uint8_t* Response, uint32_t ResponseSize);

// Issue write commands.
extern uint8_t lwnxCmdWriteInt8(lwSerialPort* Serial, uint8_t CommandId, int8_t Value);
extern uint8_t lwnxCmdWriteInt16(lwSerialPort* Serial, uint8_t CommandId, int16_t Value);
extern uint8_t lwnxCmdWriteInt32(lwSerialPort* Serial, uint8_t CommandId, int32_t Value);
extern uint8_t lwnxCmdWriteUInt8(lwSerialPort* Serial, uint8_t CommandId, uint8_t Value);
extern uint8_t lwnxCmdWriteUInt16(lwSerialPort* Serial, uint8_t CommandId, uint16_t Value);
extern uint8_t lwnxCmdWriteUInt32(lwSerialPort* Serial, uint8_t CommandId, uint32_t Value);
extern uint8_t lwnxCmdWriteString(lwSerialPort* Serial, uint8_t CommandId, char* String);
extern uint8_t lwnxCmdWriteData(lwSerialPort* Serial, uint8_t CommandId, uint8_t* Data, uint32_t DataSize);
#else                                                                           // for pic32 processor

// Waits to receive a packet of specific command id.
// Does not return until a response is received or a timeout occurs.
extern uint8_t lwnxRecvPacket(uint8_t CommandId, lwResponsePacket* Response, uint32_t TimeoutTicks, uint8_t uART_no);

// Returns true if full packet was received, otherwise finishes immediately and returns false while waiting for more data.
extern uint8_t lwnxRecvPacketNoBlock(uint8_t CommandId, lwResponsePacket* Response, uint8_t uART_no);

// Composes and sends a packet.
extern void lwnxSendPacketBytes(uint8_t CommandId, uint8_t Write, uint8_t* Data, uint32_t DataSize, uint8_t uART_no );

// Handle both the sending and receving of a command.
// Does not return until a response is received or all retries have expired.
extern uint8_t lwnxHandleManagedCmd(uint8_t CommandId, uint8_t* Response, uint32_t ResponseSize, uint8_t Write, uint8_t* WriteData, uint32_t WriteSize, uint8_t uART_no);

//----------------------------------------------------------------------------------------------------------------------------------
// Command functions.
//----------------------------------------------------------------------------------------------------------------------------------
// Issue read commands.
extern uint8_t lwnxCmdReadInt8(uint8_t CommandId, int8_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadInt16(uint8_t CommandId, int16_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadInt32(uint8_t CommandId, int32_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadUInt8(uint8_t CommandId, uint8_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadUInt16(uint8_t CommandId, uint16_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadUInt32(uint8_t CommandId, uint32_t* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadString(uint8_t CommandId, char* Response, uint8_t uART_no);
extern uint8_t lwnxCmdReadData(uint8_t CommandId, uint8_t* Response, uint32_t ResponseSize, uint8_t uART_no);

// Issue write commands.
extern uint8_t lwnxCmdWriteInt8(uint8_t CommandId, int8_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteInt16(uint8_t CommandId, int16_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteInt32(uint8_t CommandId, int32_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteUInt8(uint8_t CommandId, uint8_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteUInt16(uint8_t CommandId, uint16_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteUInt32(uint8_t CommandId, uint32_t Value, uint8_t uART_no);
extern uint8_t lwnxCmdWriteString(uint8_t CommandId, char* String, uint8_t uART_no);
extern uint8_t lwnxCmdWriteData(uint8_t CommandId, uint8_t* Data, uint32_t DataSize, uint8_t uART_no);
#endif
extern lwResponsePacket LwNxResponse;
extern uint8_t lwnxParseData(lwResponsePacket* Response, uint8_t Data1);
#endif