//
//  This library is a series of helpers for robotics
//  It has been ported from the open source NATE711_DOGO
//
//

#ifndef Robo_h
#define Robo_h

#include "definitions.h"

typedef struct {

      float32_t stance_height;
      float32_t down_amp;
      float32_t up_amp;
      float32_t flight_percent;
      float32_t step_length;
      float32_t freq;
      float32_t step_diff;

} GaitParams;                                                                   // the leg gait

typedef struct {

     float32_t kp_theta;
     float32_t kd_theta;
     float32_t kp_gamma;
     float32_t kd_gamma;
     
} LegGain;                                                                      // gain structure

typedef enum {                                                                  // The States of the robot

    RobSTOP = 0,
    RobTROT = 1,
    RobBOUND = 2,
    RobWALK = 3,
    RobPRONK = 4,
    RobJUMP = 5,
    RobDANCE = 6,
    RobHOP = 7,
    RobTEST = 8,
    RobROTATE = 9,
    RobFLIP = 10,
    RobTURN_TROT = 11,
    RobRESET = 12

} RobStates;

GaitParams state_gait_params[13] = {                                            // This structure matches the enum types above and is of type gait

    //{s.h, d.a., u.a., f.p., s.l., fr., s.d.}

    {NaN, NaN, NaN, NaN, NaN, NaN, NaN},                                        // STOP

    {0.17, 0.04, 0.06, 0.35, 0.15, 2.0, 0.0},                                   // TROT

    {0.17, 0.04, 0.06, 0.35, 0.0, 2.0, 0.0},                                    // BOUND

    {0.15, 0.00, 0.06, 0.25, 0.0, 1.5, 0.0},                                    // WALK

    {0.12, 0.05, 0.0, 0.75, 0.0, 1.0, 0.0},                                     // PRONK

    {NaN, NaN, NaN, NaN, NaN, NaN, NaN},                                        // JUMP

    {0.15, 0.05, 0.05, 0.35, 0.0, 1.5, 0.0},                                    // DANCE

    {0.15, 0.05, 0.05, 0.2, 0.0, 1.0, 0.0},                                     // HOP

    {NaN, NaN, NaN, NaN, NaN, 1.0, NaN},                                        // TEST

    {NaN, NaN, NaN, NaN, NaN, NaN, NaN},                                        // ROTATE

    {0.15, 0.07, 0.06, 0.2, 0.0, 1.0, 0.0},                                     // FLIP

    {0.17, 0.04, 0.06, 0.35, 0.1, 2.0, 0.06},                                   // TURN_TROT

    {NaN, NaN, NaN, NaN, NaN, NaN, NaN}                                         // RESET

};

LegGain gait_gains = {80, 0.5, 50, 0.5};

#endif