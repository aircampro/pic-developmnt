//
//    2  * Copyright (C) 2015 Martine Lenders <mlenders@inf.fu-berlin.de>
//    3  *
//    4  * This file is subject to the terms and conditions of the GNU Lesser
//    5  * General Public License v2.1. See the file LICENSE in the top level
//    6  * directory for more details.
//    7  */

#ifndef INTTYPES_H
#define INTTYPES_H

//#include_next <inttypes.h>

#ifdef __cplusplus
 extern "C" {
#endif

#define PRIo64  "llo"
#define PRIx64  "llx"
#define PRIu64  "llu"
#define PRId64  "lld"
#ifdef __cplusplus
}
#endif

#endif /* INTTYPES_H */