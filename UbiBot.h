// Library for  UbiBot livestock monitoring
// https://www.ubibot.io/category/platform-api/
//
//  UbiHTTP
//
// Required parameters
// Description
// Name
// Enter a unique name for your UbiHTTP request.
// API Key
// API key generated automatically for UbiHTTP requests.
// Url
// Enter the web site address where the data is requested or written, starting with http:// or https://.
// Method
// Select one of the following HTTP request methods to access the web site url: GET,POST,PUT,DELETE.
// Additional parameters can be specified depending on the nature of your request. For example, UbiHTTP requests to servers that require authentication require a user name and password.
// Optional parameters
// Description
// HTTP authorized user name
// If your URL requires authentication, enter an authentication user name to access a private channel or website.
// HTTP authentication password
// If your URL needs authentication, enter the authentication password to access the private channel or website.
// Content type
// Enter the MIME or form type of the requested content. For example, application/x-www-form-ubibot.
// Host
// If your ThingHTTP request requires a host address, enter the domain name. For example, api.ubibot.cn.
// Content
// Enter the message you want to include in the request.
//
#ifndef UBI_BOT_CAM
#define UBI_BOT_CAM

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define UNIBPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define UNIBPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif
// Overview
// The Get Channel Feed Summaries API is used to read feed summaries from all the sensor fields in a channel. 
// This method can return either a JSON or CSV object. Each feed summary record consists hourly sum total, 
// average, number of records, standard deviation, maximum and minimum values of each individual field.

// doing the get
//     curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
//    curl_setopt( $ch, CURLOPT_USERAGENT , 'JuheData' );
//    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
//    curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
//    curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
//    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//    if( $ispost )
//    {
//        curl_setopt( $ch , CURLOPT_POST , true );
//        curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
//        curl_setopt( $ch , CURLOPT_URL , $url );
//    }
//    else
//    {
//        if($params){
//            curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
//        }else{
//            curl_setopt( $ch , CURLOPT_URL , $url);
//        }
//    }


// Where CHANNEL_ID is the ID of the target channel HTTP Method  (GET)
//
#define UBIBOT_URL_JSON_FEED(url,chan_id,parameters) { sprintf(url,"https:\/\/api.ubibot.io\/channels\/%s\/summary.json?%s",chan_id,parameters); }   // To return a JSON object
#define UBIBOT_MAX_RESULTS 8000U                                                // max number of records to retrieve

// url parameters
// php example
// $params = array(
//                "api_key" => $apikey,// (string) is Read or Write key for this specific channel (no key required for public channels)
//                 "results" => 30,//(integer) Number of entries to retrieve, 8000 max (optional)
//                 "start" => "",//(datetime) Start date in format YYYY-MM-DD%20HH:NN:SS (optional)
//                 "end" => "",//(datetime) End date in format YYYY-MM-DD%20HH:NN:SS (optional)
//                 "timezone" => "",//(string) Identifier from Time Zones Reference for this request (optional)
//                 "callback" => "",//(string) Function name to be used for JSONP cross-domain requests (optional)
UNIBPACKED(
typedef struct {
      unsigned char api_key[30U];                                               // Specify the API write key or read key of the channel, or token_id obtained after the user logged in.
      uint16_t results;                                                         // number of results to retrieve
      unsigned char start[25U];                                                 // start date format YYYY-MM-DD%20HH:NN:SS.
      unsigned char end[25U];                                                   // end date format YYYY-MM-DD%20HH:NN:SS.
      unsigned char timezone[25U];                                              // Identifier from Time Zones Reference for this request.
      unsigned char callback[50U];                                              // Function name to be used for JSONP cross-domain requests.
}) UBIBOT_Config_t;                                                             // structure holding options for url parameters

// http return codes
#define UBIBOT_HTTP_RET_CODE 200U                                               // success code
#define UBIBOT_HTTP_400ERR(code) { if((code>=400u) && (code<=400u)) { return 1u; } else { return 0u; } }
#define UBIBOT_HTTP_500ERR(code) { if((code>=500u) && (code<=500u)) { return 1u; } else { return 0u; } }
// error codes returned in ajax reply
// e.g. {"result":"error","server_time":"2017-10-09T08:53:18Z","errorCode":"permission_denied_force_log_off","desp":"account_key, or token_id is not correct"}
#define ubERRORS UB("permission_denied_force_log_off")UB("missing_data")UB("invalid_format")UB("over_limit")UB("error_method_invalid")UB("invalid_created_at")UB("invalid_json_format")UB("invalid_channel_id")UB("invalid_api_key")UB("invalid_field_value")UB("invalid_read_key")UB("invalid_timezone")UB("missing_field_data")UB("request_too_fast")UB("low_balance")UB("field_length_over_limit")UB("group_name_exist")UB("openid_not_binded")UB("account_require_verify")UB("wrong_password")
#define UB(x) #x,
const char * const UBIBOT_error_codes[] = { ubERRORS };                         // Array of error codes for json string parsing

// This API call is used to generate a read-only key for a specified user on this channel.
#define UBIBOT_URL_JSON_ROKEY(url,chan_id,parameters) { sprintf(url,"https:\/\/api.ubibot.io\/channels\/%s\/api_keys?action=generate_read_key&%s",chan_id,parameters); }
//  Examples
//  POST http://api.ubibot.io/channels/CHANNEL_ID/api_keys?action=generate_read_key&account_key=xxxxxx-xxxxxx-xxxxxx-xxxxx
//  {“result”:”success”,”server_time”:”2017-09-04T08:59:30Z”,”read_key”:”9b11Xxxx5XbacbXa0e8dd53?}
//
// valid parameters 
// [account_key or token_id] Specify the account_key from the user account or token_id obtained after login.

// This API call is used to delete a specified read-only key for a channel.
#define UBIBOT_URL_JSON_DELKEY(url,chan_id,parameters) { sprintf(url,"https:\/\/api.ubibot.io\/channels\/%s\/api_keys?action=delete_read_key&%s",chan_id,parameters); }
// valid parameters 
// [account_key or token_id] Specify the account_key from the user account or token_id obtained after login.
// [ read_key ] the key you wish to delete

// The Import Feeds call is used to import data to a channel from a CSV file. The imported CSV file must be in the correct format.
#define UBIBOT_URL_IN_CSV(url,filnam) { sprintf(url,"https:\/\/api.ubibot.io\/%s.csv?parameters",filnam); }

// Data Forwarding Message Format
// Data forwarding is achieved by sending POST requests to the given endpoint URL. The Content-Type header is set to “application/json”. 
// The original feed data is contained in the request body as follows:
// The feed data will be forwarded in JSON format to the given URL
// The unique device channel_id: (string) can be used to distinguish the data from different devices.
// The feeds are an array consisting of:
// created_at:  the time when the data feed was sampled in ISO 8901 format.
// field1..field10: sensor readings
// status:  status information, such as SSID and ICCID
// Note that you will need to send a response to the UbiBot platform within 15 seconds for each message
// Example Forwarded Data:
// {“result”:”success”,”server_time”:”2017-09-04T06:53:34Z”,”timezone”:null,”channel”:{"channel_id":"735",
// "feeds":[{"created_at":"2018-01-05T05:51:52Z","field3":2293.119873},{"created_at":"2018-01-05T05:51:52Z",
// "field1":18.118561},{"created_at":"2018-01-05T05:51:52Z","field2":20},{"created_at":"2018-01-05T05:56:52Z",
// "field3":2180.479980},{"created_at":"2018-01-05T05:56:52Z","field1":18.382927},{"created_at":"2018-01-05T05:56:52Z",
// "field2":20},{"created_at":"2018-01-05T06:01:52Z","field3":2117.760010},{"created_at":"2018-01-05T06:01:52Z",
// "field1":18.738079},{"created_at":"2018-01-05T06:01:52Z","field2":20},{"created_at":"2018-01-05T06:02:09Z","field5":-62}]}
//
//  Requirements for Endpoint Response
// Please ensure the endpoint’s response is made within 15 seconds, otherwise the platform will close the connection.
// If the endpoint sends a response with the string “SUCCESS”, the platform will mark this forward request as successful. 
// If the endpoint sends the string “ERROR” it means the request was unsuccessful. The UbiBot platform keeps track of all the response results 
// for statistics purposes
// example
//                self.protocal_version = 'HTTP/1.1'
//                self.send_response(200)
//                self.send_header("Welcome", "Contect")
//                self.end_headers()
//                self.wfile.write(bytes("SUCCESS", "utf-8")) or self.wfile.write(bytes("ERROR", "utf-8"))
#define UBIBOT_DFMF_OK "SUCCESS"
#define UBIBOT_DFMF_FAIL "ERROR"
#define UBIBOT_DFMF_PORT 8080U

// -------------------- Example --------------------------------------------------------------------------------
//
// GET https://api.ubibot.io/channels/123/summary?api_key=XXXXXXXXXXXXX
//
// JSON Obj -> The response is a JSON object, for example:
// {"result":"success","server_time":"2019-02-07T13:13:15Z","is_truncated":false,"start":"2019-02-07T02:00:00+00:00","end":
// "2019-02-07T11:00:00+00:00","timezone":"Europe/London","num_records":10,"results":10,"channel":{"channel_id":"1419","name":"C-1419","field1"
// :"Temperature","field2":"Humidity","field3":"Light","field4":"Voltage","field5":"WIFI RSSI","field6":"Vibration Index","field7":"Knocks","field8":
// "External Temperature Probe","field9":"Reed Sensor","field10":null,"latitude":"41.7922","longitude":"123.4328","elevation":null,"created_at":"2018-12-07T03:15:40Z",
// "public_flag":"false","user_id":"8D5F3ACB-87A5-4D80-AA5F-FC64E8647990","last_entry_date":"2019-02-07T13:10:26Z","last_entry_id":"50982","vconfig":"{\"field1\":{\"h\":\"0\",\"u\":\"1\"},
// \"field2\":{\"h\":\"0\",\"u\":\"3\"},\"field3\":{\"h\":\"0\",\"u\":\"4\"},\"field4\":{\"h\":\"0\",\"u\":\"5\"},\"field5\":{\"h\":\"0\",\"u\":\"6\"},\"field6\":{\"h\":\"0\",\"u\":\"7\"},
// \"field7\":{\"h\":\"0\",\"u\":\"8\"},\"field8\":{\"h\":\"0\",\"u\":\"1\"},\"field9\":{\"h\":\"0\",\"u\":\"9\"}}","full_dump":"0","plan_code":"ubibot_free",
// "username":"cloudleader"},"feeds":[{"created_at":"2019-02-07T11:00:00+00:00","field3":{"sum":0.24,"avg":0.06,"count":4,"sd":0,"max":0.06,"min":0.06},"field1":{"sum":94.515136,"avg":23.628784,
// "count":4,"sd":0.018257971122225,"max":23.646141,"min":23.603416},"field2":{"sum":40,"avg":10,"count":4,"sd":0,"max":10,"min":10},"field5":{"sum":-160,"avg":-40,"count":4,"sd":0,"max":-40,"min":-40}},
// {"created_at":"2019-02-07T10:00:00+00:00","field3":{"sum":0.69,"avg":0.062727272727273,"count":11,"sd":0.0044536177141512,"max":0.07,"min":0.06},"field1":{"sum":260.85257,"avg":23.71387,"count":11,
// "sd":0.035359001690453,"max":23.803696,"min":23.675514},"field2":{"sum":110,"avg":10,"count":11,"sd":0,"max":10,"min":10},"field5":{"sum":-487,"avg":-44.272727272727,"count":11,"sd":4.3294112362875,
// "max":-40,"min":-49},"field4":{"sum":4.472982,"avg":4.472982,"count":1,"sd":0,"max":4.472982,"min":4.472982}},{"created_at":"2019-02-07T09:00:00+00:00","field3":{"sum":22.48,"avg":11.24,
// "count":2,"sd":1.74,"max":12.98,"min":9.5},"field1":{"sum":48.264282,"avg":24.132141,"count":2,"sd":0.021362,"max":24.153503,"min":24.110779},"field2":{"sum":20,"avg":10,"count":2,"sd":0,"max":10,
// "min":10},"field5":{"sum":-80,"avg":-40,"count":2,"sd":0,"max":-40,"min":-40}},{"created_at":"2019-02-07T08:00:00+00:00","field3":{"sum":457.879989,"avg":38.15666575,"count":12,"sd":12.868984722494,
// "max":57.32,"min":16.779999},"field1":{"sum":294.736777,"avg":24.561398083333,"count":12,"sd":0.27719641719199,"max":25.056076,"min":24.209579},"field2":{"sum":113,"avg":9.4166666666667,"count":12,
// "sd":0.49300664859163,"max":10,"min":9},"field5":{"sum":-512,"avg":-42.666666666667,"count":12,"sd":4.0892813821284,"max":-40,"min":-51},"field4":{"sum":4.475632,"avg":4.475632,"count":1,
// "sd":0,"max":4.475632,"min":4.475632}},{"created_at":"2019-02-07T07:00:00+00:00","field3":{"sum":200.879997,"avg":100.4399985,"count":2,"sd":2.6000025,"max":103.040001,"min":97.839996},
// "field1":{"sum":56.227211,"avg":28.1136055,"count":2,"sd":0.2456705,"max":28.359276,"min":27.867935},"field2":{"sum":16,"avg":8,"count":2,"sd":0,"max":8,"min":8},
// "field5":{"sum":-90,"avg":-45,"count":2,"sd":4,"max":-41,"min":-49}},{"created_at":"2019-02-07T06:00:00+00:00","field3":{"sum":31344.398927,"avg":2612.0332439167,
// "count":12,"sd":2824.6816531297,"max":7016.959961,"min":116.199997},"field1":{"sum":378.384835,"avg":31.532069583333,"count":12,"sd":2.9701401037999,"max":35.892273,
// "min":26.377892},"field2":{"sum":87,"avg":7.25,"count":12,"sd":1.0103629710818,"max":9,"min":6},"field5":{"sum":-491,"avg":-40.916666666667,
// "count":12,"sd":0.27638539919628,"max":-40,"min":-41},"field4":{"sum":4.487029,"avg":4.487029,"count":1,"sd":0,"max":4.487029,"min":4.487029}},
// {"created_at":"2019-02-07T05:00:00+00:00","field3":{"sum":197.159996,"avg":98.579998,"count":2,"sd":1.579998,"max":100.159996,"min":97},
// "field1":{"sum":46.082627,"avg":23.0413135,"count":2,"sd":0.0146865,"max":23.056,"min":23.026627},
// "field2":{"sum":20,"avg":10,"count":2,"sd":0,"max":10,"min":10},"field5":{"sum":-89,"avg":-44.5,"count":2,"sd":4.5,"max":-40,"min":-49}},
// {"created_at":"2019-02-07T04:00:00+00:00","field3":{"sum":1133.039978,"avg":94.419998166667,"count":12,"sd":6.4416674668395,"max":115.040001,
// "min":89.68},"field1":{"sum":277.075209,"avg":23.08960075,"count":12,"sd":0.015318618498007,"max":23.114746,"min":23.069351},"field2":{"sum":120,"avg":10,
// "count":12,"sd":0,"max":10,"min":10},"field5":{"sum":-535,"avg":-44.583333333333,"count":12,"sd":5.3456888133232,"max":-40,"min":-52},"field4":{"sum":4.469537,"avg":4.469537,"count":1,
// "sd":0,"max":4.469537,"min":4.469537}},{"created_at":"2019-02-07T03:00:00+00:00","field3":{"sum":153.099998,"avg":76.549999,"count":2,"sd":0.549999,"max":77.099998,"min":76},"field1":{"sum":46.242844,"avg":23.121422,
// "count":2,"sd":0.0066760000000006,"max":23.128098,"min":23.114746},"field2":{"sum":20,"avg":10,"count":2,"sd":0,"max":10,"min":10},"field5":{"sum":-82,"avg":-41,"count":2,"sd":0,"max":-41,"min":-41}},
// {"created_at":"2019-02-07T02:00:00+00:00","field3":{"sum":1153.739984,"avg":96.144998666667,"count":12,"sd":50.714404305812,"max":256.320007,"min":73.059998},"field1":{"sum":278.933775,"avg":23.24448125,
// "count":12,"sd":0.069417701157708,"max":23.352409,"min":23.141449},"field2":{"sum":122,"avg":10.166666666667,"count":12,"sd":0.37267799624997,"max":11,"min":10},"field5":{"sum":-536,"avg":-44.666666666667,
// "count":12,"sd":7.3861732687201,"max":-40,"min":-66},"field4":{"sum":4.497895,"avg":4.497895,"count":1,"sd":0,"max":4.497895,"min":4.497895}}]}
//
// keywords channel vconfig
// 
//  Filter channels that need to be displayed
//          $filter_fields = array('field1','field2','field3','field4','field5','field6','field7','field8','field9','field10');
//  Sensor unit
//            $unit = array('temperature_celcius','temperature_feh','humidity','light_lux','voltage','WIFI_dbm','vibration','knocks','magnetic_switch','soil_absolute_moisture');

#define UBIBOT_URL_CSV_FEED(url,chan_id,parameters) { sprintf(url,"https:\/\/api.ubibot.io\/channels\/%s\/summary.csv??%s",chan_id,parameters); } // To return a CSV file:

#define UBIBOT_IP_ADDR "192.168.1.1"
#define UBIBOT_TCP_PORT 5001U


#endif