/*
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Also includes code from Copyright © 2001-2011 Stéphane Raimbault <stephane.raimbault@gmail.com>
 * and Copyright (C) 2010 Doc Walker and Stephen Makonin.  All right reserved.
 * Copyright (C) 2010-2019 Oryx Embedded SARL. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Rebuilt and ported by ACP Aviation including support for the Leddar Vu Vu-8
 */

#ifndef _MB_PROTO_H
#define _MB_PROTO_H

#ifdef __cplusplus
PR_BEGIN_EXTERN_C
#endif
#include "crc.h"

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define MODPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define MODPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

//--------------- Endian Conversion --------------------------------------------
// 32-bit integer manipulation macros (little endian)
#ifndef GET_UINT32_LE                                                           // to use : GET_UINT32_LE( X[ 0], data,  0 );
#define GET_UINT32_LE(n,b,i)                              \
{                                                         \
    (n) = ( (uint32_t) (b)[(i)    ]       )               \
        | ( (uint32_t) (b)[(i) + 1U] <<  8U )             \
        | ( (uint32_t) (b)[(i) + 2U] << 16U )             \
        | ( (uint32_t) (b)[(i) + 3U] << 24U );            \
}
#endif

#ifndef GET_UINT16_LE                                                           // to use : GET_UINT16_LE( X[ 0], data,  0 );
#define GET_UINT16_LE(n,b,i)                              \
{                                                         \
    (n) = ( (uint32_t) (b)[(i)    ]       )               \
        | ( (uint32_t) (b)[(i) + 1U] <<  8U )             \
}
#endif

#ifndef PUT_UINT32_LE                                                           // to use : PUT_UINT32_LE( ctx->state[0], output,  0 );  PUT_UINT32_LE( ctx->state[1], output,  4 );
#define PUT_UINT32_LE(n,b,i)                                 \
{                                                            \
    (b)[(i)    ] = (uint8_t) ( ( (n)       ) & 0xFFU );      \
    (b)[(i) + 1U] = (uint8_t) ( ( (n) >>  8U ) & 0xFFU );    \
    (b)[(i) + 2U] = (uint8_t) ( ( (n) >> 16U ) & 0xFFU );    \
    (b)[(i) + 3U] = (uint8_t) ( ( (n) >> 24U ) & 0xFFU );    \
}
#endif

#ifndef PUT_UINT16_LE                                                           // to use : PUT_UINT16_LE( ctx->state[0], output,  0 );  PUT_UINT16_LE( ctx->state[1], output,  4 );
#define PUT_UINT16_LE(n,b,i)                                 \
{                                                            \
    (b)[(i)    ] = (uint8_t) ( ( (n)       ) & 0xFFU );      \
    (b)[(i) + 1U] = (uint8_t) ( ( (n) >>  8U ) & 0xFFU );    \
}
#endif

// 32-bit integer manipulation macros (big endian)
#ifndef GET_UINT32_BE                                                           // to use : GET_UINT32_BE( X[ 0], data,  0 );
#define GET_UINT32_BE(n,b,i)                     \
{                                                \
    (n) = ( (uint32_t) (b)[(i)    ] << 24U );    \
        | ( (uint32_t) (b)[(i) + 1U] << 16U );   \
        | ( (uint32_t) (b)[(i) + 2U] << 8U );    \
        | ( (uint32_t) (b)[(i) + 3U] );          \
}
#endif

#ifndef GET_UINT16_BE                                                           // to use : GET_UINT16_BE( X[ 0], data,  0 );
#define GET_UINT16_BE(n,b,i)                        \
{                                                   \
    (n) = ( (uint32_t) (b)[(i) ] << 8U )            \
        | ( (uint32_t) (b)[(i) + 1U]  )             \
}
#endif

#ifndef PUT_UINT32_BE                                                           // to use : PUT_UINT32_BE( ctx->state[0], output,  0 );  PUT_UINT32_BE( ctx->state[1], output,  4 );
#define PUT_UINT32_BE(n,b,i)                                \
{                                                           \
    (b)[(i)    ] = (uint8_t) ( ( (n) >> 24U ) & 0xFFU );    \
    (b)[(i) + 1U] = (uint8_t) ( ( (n) >> 16U ) & 0xFFU );   \
    (b)[(i) + 2U] = (uint8_t) ( ( (n) >> 8U ) & 0xFFU );    \
    (b)[(i) + 3U] = (uint8_t) ( ( (n) ) & 0xFFU );          \
}
#endif

#ifndef PUT_UINT16_BE
#define PUT_UINT16_BE(n,b,i)                                 \
{                                                            \
    (b)[(i)    ] = (uint8_t) ( ( (n) >>  8U ) & 0xFFU );     \
    (b)[(i) + 1U] = (uint8_t) ( ( (n) ) & 0xFFU );           \
}
#endif

#define SWAP_UINT16(x) (((x) >> 8U) | ((x) << 8U))
#define SWAP_INT16(x) (x << 8U) | ((x >> 8U) & 0xFFU)
#define SWAP_UINT32(x) (((x) >> 24U) | (((x) & 0x00FF0000UL) >> 8U) | (((x) & 0x0000FF00UL) << 8U) | ((x) << 24U))
#define SWAP_INT32(x) ((x>24)&0xffu) | ((x<<8)&0xff0000ul) | ((x>>8)&0xff00u) | ((x<<24)&0xff000000ul)


/* ----------------------- Defines ------------------------------------------*/
#define MB_ADDRESS_BROADCAST 0U                                                 /*! Modbus broadcast address. */
#define MB_ADDRESS_MIN 1U                                                       /*! Smallest possible slave address. */
#define MB_ADDRESS_MAX 247U                                                     /*! Biggest possible slave address. */
//
// should reply back with same (loopback response) use to test comms w/o register info
//
#define MB_FUNC_NONE ( 0U )

/**
 Modbus function 0x01 Read Coils.
 This function code is used to read from 1 to 2000 contiguous status of
 coils in a remote device. The request specifies the starting address,
 i.e. the address of the first coil specified, and the number of coils.
 Coils are addressed starting at zero.

 The coils in the response buffer are packed as one coil per bit of the
 data field. Status is indicated as 1=ON and 0=OFF. The LSB of the first
 data word contains the output addressed in the query. The other coils
 follow toward the high order end of this word and from low order to high
 order in subsequent words.

 If the returned quantity is not a multiple of sixteen, the remaining
 bits in the final data word will be padded with zeros (toward the high
 order end of the word).

 @param ReadAddress address of first coil (0x0000..0xFFFF)
 @param BitQty quantity of coils to read (1..2000, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup discrete
 */
#define MB_FUNC_READ_COILS ( 1U )
/**
 * @brief Read Coils request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t startingAddr;                                                       //1-2
   uint16_t quantityOfCoils;                                                    //3-4
}) ModbusReadCoilsReq;                                                          // Request header
/**
 * @brief Read Coils response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1
   uint8_t coilStatus[];                                                        //2  warning dynamic memory allocation you might want to define max number here
}) ModbusReadCoilsResp;

/**
 Modbus function 0x02 Read Discrete Inputs.

 This function code is used to read from 1 to 2000 contiguous status of
 discrete inputs in a remote device. The request specifies the starting
 address, i.e. the address of the first input specified, and the number
 of inputs. Discrete inputs are addressed starting at zero.

 The discrete inputs in the response buffer are packed as one input per
 bit of the data field. Status is indicated as 1=ON; 0=OFF. The LSB of
 the first data word contains the input addressed in the query. The other
 inputs follow toward the high order end of this word, and from low order
 to high order in subsequent words.

 If the returned quantity is not a multiple of sixteen, the remaining
 bits in the final data word will be padded with zeros (toward the high
 order end of the word).

 @param ReadAddress address of first discrete input (0x0000..0xFFFF)
 @param BitQty quantity of discrete inputs to read (1..2000, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup discrete
 */
#define MB_FUNC_READ_DISCRETE_INPUTS ( 2U )
/**
 * @brief Read Discrete Inputs request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t startingAddr;                                                       //1-2
   uint16_t quantityOfInputs;                                                   //3-4
}) ModbusReadDiscreteInputsReq;
/**
 * @brief Read Discrete Inputs response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1
   uint8_t inputStatus[];                                                       //2 warning dynamic memory allocation you might want to define max number here
}) ModbusReadDiscreteInputsResp;
/**
 Modbus function 0x05 Write Single Coil.

 This function code is used to write a single output to either ON or OFF
 in a remote device. The requested ON/OFF state is specified by a
 constant in the state field. A non-zero value requests the output to be
 ON and a value of 0 requests it to be OFF. The request specifies the
 address of the coil to be forced. Coils are addressed starting at zero.

 @param WriteAddress address of the coil (0x0000..0xFFFF)
 @param State 0=OFF, non-zero=ON (0x0000..0xFF00)
 @return 0 on success; exception number on failure
 @ingroup discrete
 */
#define MB_FUNC_WRITE_SINGLE_COIL ( 5U )
/**
 * @brief Write Single Coil request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;  //0
   uint16_t outputAddr;   //1-2
   uint16_t outputValue;  //3-4
}) ModbusWriteSingleCoilReq;
/**
 * @brief Write Single Coil response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;  //0
   uint16_t outputAddr;   //1-2
   uint16_t outputValue;  //3-4
}) ModbusWriteSingleCoilResp;

/**
 Modbus function 0x0F Write Multiple Coils.

 This function code is used to force each coil in a sequence of coils to
 either ON or OFF in a remote device. The request specifies the coil
 references to be forced. Coils are addressed starting at zero.

 The requested ON/OFF states are specified by contents of the transmit
 buffer. A logical '1' in a bit position of the buffer requests the
 corresponding output to be ON. A logical '0' requests it to be OFF.

 @param WriteAddress address of the first coil (0x0000..0xFFFF)
 @param BitQty quantity of coils to write (1..2000, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup discrete
 */
#define MB_FUNC_WRITE_MULTIPLE_COILS  ( 15U )
/**
 * @brief Write Multiple Coils request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;       //0
   uint16_t startingAddr;      //1-2
   uint16_t quantityOfOutputs; //3-4
   uint8_t byteCount;          //5
   uint8_t outputValue[];      //6
}) ModbusWriteMultipleCoilsReq;
/**
 * @brief Write Multiple Coils response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;       //0
   uint16_t startingAddr;      //1-2
   uint16_t quantityOfOutputs; //3-4
}) ModbusWriteMultipleCoilsResp;

/**
 Modbus function 0x03 Read Holding Registers.

 This function code is used to read the contents of a contiguous block of
 holding registers in a remote device. The request specifies the starting
 register address and the number of registers. Registers are addressed
 starting at zero.

 The register data in the response buffer is packed as one word per
 register.

 @param ReadAddress address of the first holding register (0x0000..0xFFFF)
 @param ReadQty quantity of holding registers to read (1..125, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup register
 
 Read holding register (function code 0x3) on Leddar Tech Page 50
 As per the Modbus protocol, register values are returned in big-endian format
 
 These are read write parameters and also can be used with commands
 write register (function code 0x6), write multiple register (function code 0x10), and read/write multiple register (function code 0x17)
 */
#define MB_FUNC_READ_HOLDING_REGISTER ( 3U )
#define LEDDAR_ADDR_EXP1 0U                                                     // Exponent for the number of accumulations (that is, if the content of this register is n, 2n accumulations are performed)
#define LEDDAR_ADDR_EXP2 1U                                                     // Exponent for the number of oversamplings (that is, if the content of this register is n, 2n oversamplings are performed)
#define LEDDAR_ADDR_BASE_SAM 2U                                                 // Number of base samples
#define LEDDAR_ADDR_DET_THRESH 4U                                               // Detection threshold as a fixed-point value with a 6-bit fractional part (i.e. threshold value is this register divided by 64)
#define LEDDAR_ADDR_LIGHT_POW 5U                                                // Light source power in percentage of the maximum. A value above 100 is an error. If a value is specified that is not one of the pre-defined values, the closest pre-defined value will be used. The register can be read back to know the actual value set. 6
#define LEDDAR_ADDR_BIT_FIELD 6U                                                // Bit field of acquisition options: Bit-0: Automatic light source power enabled Bit-1: Demerge object enabled Bit-2: Static noise removal enabled Bit-3: Precision enabled Bit-4: Saturation compensation enabled Bit-5: Overshoot management enabled
#define LEDDAR_ADDR_AUTO_LIGHT 7U                                               // Auto light source power change delay in number of measurements
#define LEDDAR_ADDR_NUM_ECHO 9U                                                 // Number of echoes for saturation acceptance: The number of echoes can be saturated to avoid decreasing the light source power in automatic mode
#define LEDDAR_ADDR_OP_MODE 10U                                                 // Operation mode  Write mode: 0: Stop (stop acquisition) 1: Continuous 2: Single (acquisition of a single detection frame) Read mode: 10: Stopped (sensor is stopped) 11: Continuous acquisition mode 12: Single frame busy (acquisition in progress) 13: Sensor is busy
#define LEDDAR_ADDR_SMOOTHING 11U                                               // Smoothing: Stabilizes the module measurements. The behavior of the smoothing algorithm can be adjusted by a value ranging from –16 through 16
#define LEDDAR_ADDR_LOW_SEGMENT 12U                                             // Low 16 bits of segment enabled: Bit-field of enabled segment
#define LEDDAR_ADDR_HIGH_SEGMENT 13U                                            // High 16 bits of segment enabled

#define LEDDAR_ADDR_MAX 14U                                                     // maximum number
// ENRON modbus Hourly and Daily Archive Collection
#define ENRON_ADDR_HISTORY1 0x02BDU                                             // first history table address follow this word with the #index number needed
/**
 * @brief Read Holding Registers request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t startingAddr;                                                       //1-2
   uint16_t quantityOfRegs;                                                     //3-4
}) ModbusReadHoldingRegsReq;
/**
 * @brief Read Holding Registers response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1
   uint16_t regValue[];                                                         //2 warning dynamically allocated memory you might want to define at maximum number e.g LEDDAR_ADDR_MAX
}) ModbusReadHoldingRegsResp;

MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1
   uint8_t dataBytes;
   float32_t datestamp;                                                         // Date Stamp 32 bit float MMDDYY (120304.0 = Dec 3, 2004)
   float32_t timestamp;                                                         // Time Stamp 32 bit float HHMMSS (080000.0 = 8am)
   float32_t item1;                                                             // 1st item logged 32-bit float
   float32_t item2;                                                             // 2nd item logged 32-bit float
   float32_t item3;                                                             // 3rd item logged 32-bit float
}) ModbusReadHistoryResp;

/**
 Modbus function 0x04 Read Input Registers.

 This function code is used to read from 1 to 125 contiguous input
 registers in a remote device. The request specifies the starting
 register address and the number of registers. Registers are addressed
 starting at zero.

 The register data in the response buffer is packed as one word per
 register.

 @param ReadAddress address of the first input register (0x0000..0xFFFF)
 @param ReadQty quantity of input registers to read (1..125, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup register
 
 Read input register (function code 0x4) LeddarTech manual page 48
 As per the Modbus protocol, register values are returned in big-endian format.

 */
#define MB_FUNC_READ_INPUT_REGISTER ( 4U )
#define LEDDAR_ADDR_DET 1U                                                      // Detection status for polling mode:  0 = Detections not ready 1 = Detections ready: this status flag is reset to 0 after reading this register
#define LEDDAR_ADDR_NUM_SEG 2U                                                  // Number of segments (N)
#define LEDDAR_ADDR_NUM_DET 11U                                                 // Number of detections
#define LEDDAR_ADDR_LIGHT_POWER 12U                                             // Current percentage of light source power
#define LEDDAR_ADDR_ACQ_STAT 13U                                                // Bit field of acquisition status: Reserved
#define LEDDAR_ADDR_LOW_TIME 14U                                                // Low 16 bits of timestamp (number of milliseconds since the module was started)
#define LEDDAR_ADDR_HIGH_TIME 15U                                               // High 16 bits of timestamp
#define LEDDAR_ADDR_DIST1_START 16U                                             // Distance of first detection for each segment, zero if no detection in a segment. The distance unit is defined by the serial port parameters
#define LEDDAR_ADDR_DIST1_STOP(N) (16U + N-1U)                                  // Distance of last detection
#define LEDDAR_ADDR_AMP1_START(N) (16U + N)                                     // Amplitude
#define LEDDAR_ADDR_AMP1_STOP(N) (16U + (2U*N) - 1U)                            // last amplitude
#define LEDDAR_ADDR_FLAG1_START(N) (16U + (2U*N))                               // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG1_STOP(N) (16U + (3U*N) - 1U)                           // last flag
#define LEDDAR_ADDR_DIST2_START(N) (16U + (3U*N))                               // distance to 2nd detection
#define LEDDAR_ADDR_DIST2_STOP(N) (16U + (4U*N) - 1U)                           // distance to 2nd detection end
#define LEDDAR_ADDR_AMP2_START(N) (16U + (4U*N))                                // amplitude 2nd detection start
#define LEDDAR_ADDR_AMP2_STOP(N) (16U + (5U*N) - 1U)                            // amplitude 2nd detection stop
#define LEDDAR_ADDR_FLAG2_START(N) (16U + (5U*N))                               // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG2_STOP(N) (16U + (6U*N) - 1U)                           // last flag
#define LEDDAR_ADDR_DIST3_START(N) (16U + (6U*N))                               // distance to 3rd detection
#define LEDDAR_ADDR_DIST3_STOP(N) (16U + (7U*N) - 1U)                           // distance to 3rd detection end
#define LEDDAR_ADDR_AMP3_START(N) (16U + (7U*N))                                // amplitude 3rd detection start
#define LEDDAR_ADDR_AMP3_STOP(N) (16U + (8U*N) - 1U)                            // amplitude 3rd detection stop
#define LEDDAR_ADDR_FLAG3_START(N) (16U + (8U*N))                               // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG3_STOP(N) (16U + (9U*N) - 1U)                           // last flag
#define LEDDAR_ADDR_DIST4_START(N) (16U + (9U*N))                               // distance to 4th detection
#define LEDDAR_ADDR_DIST4_STOP(N) (16U + (10U*N) - 1U                           // distance to 4th detection end
#define LEDDAR_ADDR_AMP4_START(N) (16U + (10U*N))                               // amplitude 4th detection start
#define LEDDAR_ADDR_AMP4_STOP(N) (16U + (11U*N) - 1U)                           // amplitude 4th detection stop
#define LEDDAR_ADDR_FLAG4_START(N) (16U + (11U*N))                              // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG4_STOP(N)  (16U + (12U*N) - 1U)                         // last flag
#define LEDDAR_ADDR_DIST5_START(N) (16U + (12U*N))                              // distance to 4th detection
#define LEDDAR_ADDR_DIST5_STOP(N) (16U + (13U*N) - 1U)                          // distance to 4th detection end
#define LEDDAR_ADDR_AMP5_START(N) (16U + (13U*N))                               // amplitude 4th detection start
#define LEDDAR_ADDR_AMP5_STOP(N)  (16U + (14U*N) - 1U)                          // amplitude 4th detection stop
#define LEDDAR_ADDR_FLAG5_START(N) (16U + (14U*N))                              // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG5_STOP(N) (16U + (15U*N) - 1U)                          // last flag
#define LEDDAR_ADDR_DIST6_START(N) (16U + (15U*N))                              // distance to 4th detection
#define LEDDAR_ADDR_DIST6_STOP(N) (16U + (16U*N) - 1U)                          // distance to 4th detection end
#define LEDDAR_ADDR_AMP6_START(N) (16U + (16U*N))                               // amplitude 4th detection start
#define LEDDAR_ADDR_AMP6_STOP(N) (16U + (17U*N) - 1U)                           // amplitude 4th detection stop
#define LEDDAR_ADDR_FLAG6_START(N) (16U + (17U*N))                              // Flag of the first detection for each segment: Bit 0: Detection is valid (will always be set)  Bit 1: Detection was the result of object demerging Bit 2: Reserved Bit 3: Detection is saturated
#define LEDDAR_ADDR_FLAG6_STOP(N) (16U + (18U*N) - 1U)                          // last flag
/**
 * @brief Read Holding Input request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t startingAddr;                                                       //1-2
   uint16_t quantityOfRegs;                                                     //3-4
}) ModbusReadInputRegsReq;
/**
 * @brief Read Holding Input response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1
   uint16_t regValue[];                                                         //2 dynamic memory alloc consider LEDDAR_ADDR_FLAG6_STOP(N)+1 as MAX
}) ModbusReadInputRegsResp;

/**
 Modbus function 0x06 Write Single Register.

 This function code is used to write a single holding register in a
 remote device. The request specifies the address of the register to be
 written. Registers are addressed starting at zero.

 @param WriteAddress address of the holding register (0x0000..0xFFFF)
 @param WriteValue value to be written to holding register (0x0000..0xFFFF)
 @return 0 on success; exception number on failure
 @ingroup register
 
 write register (function code 0x6) page 50 for Leddartech
 Big Endian format
 */
#define MB_FUNC_WRITE_REGISTER  ( 6U )
/**
 * @brief Write Single Register request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode; //0
   uint16_t regAddr;     //1-2
   uint16_t regValue;    //3-4
}) ModbusWriteSingleRegReq;
/**
 * @brief Write Single Register response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode; //0
   uint16_t regAddr;     //1-2
   uint16_t regValue;    //3-4
}) ModbusWriteSingleRegResp;

/**
 Modbus function 0x10 Write Multiple Registers.

 This function code is used to write a block of contiguous registers (1
 to 123 registers) in a remote device.

 The requested written values are specified in the transmit buffer. Data
 is packed as one word per register.

 @param WriteAddress address of the holding register (0x0000..0xFFFF)
 @param WriteQty quantity of holding registers to write (1..123, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup register
 
 write multiple register (function code 0x10) page 50 for Leddartech
 Big Endian format
 */
#define MB_FUNC_WRITE_MULTIPLE_REGISTERS ( 16U )
/**
 * @brief Write Multiple Registers request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;    //0
   uint16_t startingAddr;   //1-2
   uint16_t quantityOfRegs; //3-4
   uint8_t byteCount;       //5
   uint16_t regValue[];     //6
}) ModbusWriteMultipleRegsReq;
/**
 * @brief Write Multiple Registers response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;     //0
   uint16_t startingAddr;    //1-2
   uint16_t quantityOfRegs;  //3-4
}) ModbusWriteMultipleRegsResp;

/**
 Modbus function 0x17 Read Write Multiple Registers.

 This function code performs a combination of one read operation and one
 write operation in a single MODBUS transaction. The write operation is
 performed before the read. Holding registers are addressed starting at
 zero.

 The request specifies the starting address and number of holding
 registers to be read as well as the starting address, and the number of
 holding registers. The data to be written is specified in the transmit
 buffer.

 @param ReadAddress address of the first holding register (0x0000..0xFFFF)
 @param ReadQty quantity of holding registers to read (1..125, enforced by remote device)
 @param WriteAddress address of the first holding register (0x0000..0xFFFF)
 @param WriteQty quantity of holding registers to write (1..121, enforced by remote device)
 @return 0 on success; exception number on failure
 @ingroup register
 
  Read Write Multiple Registers (function code 0x17) page 50 for Leddartech
  Big Endian format
 */
#define MB_FUNC_READWRITE_MULTIPLE_REGISTERS ( 23U )
/**
 * @brief Read/Write Multiple Registers request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t readStartingAddr;                                                   //1-2
   uint16_t quantityToRead;                                                     //3-4
   uint16_t writeStartingAddr;                                                  //5-6
   uint16_t quantityToWrite;                                                    //7-8
   uint8_t writeByteCount;                                                      //9
   uint16_t writeRegValue[];                                                    //10  Dynamic you might want to make the max and define static memory
}) ModbusReadWriteMultipleRegsReq;
/**
 * @brief Read/Write Multiple Registers response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t readByteCount;                                                       //1
   uint16_t readRegValue[];                                                     //2 dynamic memory you may want to limit to max and make memory alloc static
}) ModbusReadWriteMultipleRegsResp;

// Read Exception Status
#define MB_FUNC_DIAG_READ_EXCEPTION (  7U )
/**
 * @brief Exception response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;  //0
   uint8_t exceptionCode; //1
}) ModbusExceptionResp;
// Diagnostic
#define MB_FUNC_DIAG_DIAGNOSTIC (  8U )

#define MB_SUBFUNC_LOOPBACK 0U                                                  // 08 0000 <data> = loopback
// this can also be used with subfunction 01
#define MB_SUBFUNC_RESET ( 1U )
#define MB_TESTDAT_RESTART 0U                                                   // 08 0001 0000 resets communication
#define MB_TESTDAT_RESET 0xFF00U                                                // 08 0001 FF00 resets event logs
#define MB_SUBFUNC_DIAG ( 2U )                                                  // 08 0002 0000 call for diagnostic word
#define MB_SUBFUNC_DELIM ( 3U )                                                 // 08 0003 <char> replaces LF with char in ascii mode
#define MB_SUBFUNC_LISTEN ( 4U )                                                // 08 0004 0000 listen only
#define MB_SUBFUNC_DIAG_RESET ( 0x0AU )                                         // resets diagnostic counters below
#define MB_SUBFUNC_DIAG_BUS_MSG 0x0BU                                           // Quantity of messages that the remote device has detected on the communications system since its last restart, clear counters operation, or power–up.  Messages with bad CRC are not taken into account
#define MB_SUBFUNC_DIAG_BUS_ERR 0x0CU                                           // Quantity of CRC errors encountered by the remote device since its last restart, clear counters operation, or power–up.  In case of an error detected on the character level, (overrun, parity error), or in case of a message length < 3 bytes, the receiving device is not able to calculate the CRC.  In such cases, this counter is also incremented
#define MB_SUBFUNC_DIAG_SLAVE_EXC_ERR 0x0DU                                     // Quantity of MODBUS exception error detected by the remote device since its last restart, clear counters operation, or power–up.  It comprises also the error detected in broadcast messages even if an exception message is not returned in this case
#define MB_SUBFUNC_DIAG_SLAVE_MSG 0x0EU                                         // Quantity of messages addressed to the remote device,  including broadcast messages, that the remote device has processed since its last restart, clear counters operation, or power–up.
#define MB_SUBFUNC_DIAG_SLAVE_NORESP 0x0FU                                      // Quantity of messages received by the remote device for which it returned no response (neither a normal response nor an exception response), since its last restart, clear counters operation, or power–up.  Then, this counter counts the number of broadcast messages it has received.
#define MB_SUBFUNC_DIAG_SLAVE_NAK 0x10U                                         // Quantity of messages addressed to the remote device for which it returned a Negative Acknowledge (NAK) exception response, since its last restart, clear counters operation, or power–up.
#define MB_SUBFUNC_DIAG_SLAVE_BUSY 0x11U                                        // Quantity of messages addressed to the remote device for which it returned a Slave Device Busy exception response, since its last restart, clear counters operation, or power–up.
#define MB_SUBFUNC_DIAG_BUS_OVERRUN 0x12U                                       // Quantity of messages addressed to the remote device that it could not handle due to a character overrun condition, since its last restart, clear counters operation, or power–up. A character overrun is caused by data characters arriving at the port faster than they can be stored, or by the loss of a character due to a hardware malfunction
#define MB_SUBFUNC_DIAG_IOP_OVERRUN 0x13U                                       // iop overrun
#define MB_SUBFUNC_DIAG_884_RESET 0x14U                                         // reset counters in 884
#define MB_SUBFUNC_DIAG_MODPLUS 0x15U                                           // modbus plus
#define MB_TESTDAT_MODPLUS_GET 3U                                               // 08 0015 0003 gets the data (53 words as per defined in spec)
#define MB_TESTDAT_MODPLUS_CLR 0xFF00U                                          // 08 0015 0004 resets the data

MODPACKED(
typedef struct
{
   uint8_t functionCode;
   uint16_t subfunction;
   uint16_t testdata[];
}) ModbusLoopBackReq;                                                           // Loop back Request header
MODPACKED(
typedef struct
{
   uint8_t functionCode;
   uint16_t subfunction;
   uint16_t testdata[];
}) ModbusLoopBackResp;                                                          // Loop back function returns same
MODPACKED(
typedef struct
{
   uint8_t functionCode;
   uint16_t subfunction;
}) ModbusDiagReq;                                                               // Diagnostics Request header
MODPACKED(
typedef struct
{
   uint8_t functionCode;
   uint16_t subfunction;
}) ModbusDiagResp;                                                             // Diagnostics function returns same
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t subfunction;
   uint16_t cont_on_err : 1;                                                    // continues on error
   uint16_t run_light_fail : 1;                                                 // run light fail
   uint16_t tbus_tst_fail : 1;                                                  // tbus test fail
   uint16_t bus_tst_fail : 1;                                                   // bus test fail
   uint16_t force_listen : 1;                                                   // force listen only mode
   uint16_t notused : 2;
   uint16_t rom_tst_fail : 1;                                                   // rom 0 chip test faild
   uint16_t rom_chksum : 1;                                                     // continuous rom checksum
   uint16_t rom1_fail : 1;                                                      // rom chip 1 fail
   uint16_t rom2_fail : 1;
   uint16_t rom3_fail : 1;
   uint16_t ram_5000_fail : 1;                                                  // RAM chip 5000 53FF fail
   uint16_t ram_6000_even_fail : 1;                                             // RAM chip 6000 63FF even addresses failure
   uint16_t ram_6000_odd_fail : 1;                                              // RAM chip 6000 63FF odd addresses failure
   uint16_t timer_chip_failed : 1;                                              // timer chip failed
}) Modbus184384DiagResp;                                                        // 02 call for diagnostics 184/384 response
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t subfunction;
   uint16_t cont_on_err : 1;                                                    // continues on error
   uint16_t run_light_fail : 1;                                                 // run light fail
   uint16_t para_tst_fail : 1;                                                  // parallel port test fail
   uint16_t bus_tst_fail : 1;                                                   // bus test fail
   uint16_t tim0_fail : 1;                                                      // timer 0 fail
   uint16_t tim1_fail : 1;                                                      // timer 1 fail
   uint16_t tim2_fail : 1;                                                      // timer 2 fail
   uint16_t rom_tst_fail : 1;                                                   // rom 0 chip test faild
   uint16_t rom_chksum : 1;                                                     // continuous rom checksum
   uint16_t rom800_fail : 1;                                                    // rom chip 1 fail
   uint16_t rom1000_fail : 1;
   uint16_t rom1800_fail : 1;
   uint16_t ram_4000_fail : 1;                                                  // RAM chip 4000 fail
   uint16_t ram_4100_even_fail : 1;                                             // RAM chip 4100 failure
   uint16_t ram_4200_odd_fail : 1;                                              // RAM chip 4200 failure
   uint16_t ram_4300_failed : 1;                                                // RAM chip 4300 failure
}) Modbus484DiagResp;                                                           // 02 call for diagnostics 484 response
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t subfunction;
   uint16_t config_err : 1;                                                     // config illegal
   uint16_t backup_chk_fail : 1;                                                // back up chacksum fail
   uint16_t logic_chk_fail : 1;                                                 // logic checksum fail
   uint16_t node_fail : 1;                                                      // invalid node
   uint16_t traffic_fail : 1;                                                   // traffic invalid
   uint16_t cpu_fail : 1;                                                       // cpu fail
   uint16_t rtc_fail : 1;                                                       // real time clock fail
   uint16_t wdog_fail : 1;                                                      // watch dog faild
   uint16_t no_end_logic : 1;                                                   // no end to the logic
   uint16_t state_ram_fail : 1;                                                 // state ram fail
   uint16_t son_not_start : 1;                                                  // son did not begin
   uint16_t bad_solve_table : 1;                                                // bad order of solve table
   uint16_t illegal_perf : 1;                                                   // illegal perfiferal
   uint16_t dim_aware : 1;                                                      // DIM awareness
   uint16_t spare : 1;                                                          //
   uint16_t perf_port_stop : 1;                                                 // periferal port stop
}) Modbus584DiagResp;                                                           // 02 call for diagnostics 584 response
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t subfunction;
   uint16_t iop_overrun : 1;                                                    // iop overrrun
   uint16_t mb_overrun : 1;                                                     // modbus overrun
   uint16_t mb_iop_fail : 1;                                                    // mb iop fail
   uint16_t mb_opt_fail : 1;                                                    // mb options failed
   uint16_t iop_fail : 1;                                                       // iop fail
   uint16_t rem_io_fail : 1;                                                    // remote io fail
   uint16_t cpu_fail : 1;                                                       // main cpu fail
   uint16_t ram_chksum_fail : 1;                                                // ram checksum fail
   uint16_t too_much_logic : 1;                                                 // too much logic
   uint16_t spare : 7;                                                          //
}) Modbus884DiagResp;                                                           // 02 call for diagnostics 584 response
/**
Report server ID (function code 0x11) This function returns information on the LeddarVu module in the following format:
Number of bytes of information (excluding this one).
Currently 0x99 since the size of information returned is fixed. 
Serial number as an ASCII string 
Run status 0: OFF, 0xFF: ON. Should always return 0 FF, otherwise the module is defective. 
The device name as an ASCII string 
The hardware part number as an ASCII string 
The software part number as an ASCII string 
The full firmware version as 4 16-bit values 
The full bootloader version as 4 16-bit values 
The FPGA-build version 
Internal Use 
Module identification code (9 for the module
Report Slave ID
*/
#define MB_FUNC_OTHER_REPORT_SLAVEID  ( 17U )
MODPACKED(
typedef struct {
   uint8_t info;                                                                // 0x99 info is fixed
   unsigned char serialNo[32U];                                                 // serial number
   uint8_t moduleStat;                                                          // should be 00 for fault FF for okay
   unsigned char devName[32U];                                                  // device
   unsigned char hwName[32U];                                                   // hard ware name
   unsigned char swName[32U];                                                   // soft ware name
   uint16_t firmVer[4U];                                                        // firmware version as 4 16 bit values
   uint16_t bootVer[4U];                                                        // bootloader version as 4 16 bit values
   uint16_t fpgaBuild;                                                          // fpga build id
   uint32_t internal;
   uint16_t moduleId;                                                           // should read 9
}) mbCmd11_leddar_resp_t;                                                       // response packet structure
#define LEDDAR_C11_START_ADDR 0U                                                // start address
#define LEDDAR_C11_STOP_ADDR 152U                                               // end address for 0x11 command

// Get Com Event Counter
#define MB_FUNC_DIAG_GET_COM_EVENT_CNT ( 11U )                                  // request is function code only
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t status;                                                             //1-2
   uint16_t ev_count;                                                           //3-4
}) ModbusGetComEventResp;

//Get Com Event Log
#define MB_FUNC_DIAG_GET_COM_EVENT_LOG ( 12U )                                  // request is function code only
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t status;                                                             //1-2
   uint16_t ev_count;                                                           //3-4
   uint16_t msg_count;                                                          // 5-6
   uint8_t events[];
}) ModbusGetEventLogResp;

// the events returned have defintion as one of the 4 below
// Remote device MODBUS Receive Event
MODPACKED(
typedef struct
{
   uint8_t spare : 1;                                                           // Not Used
   uint8_t comm_err : 1;                                                        // Communication Error
   uint8_t spare1 : 2;
   uint8_t char_ovr : 1;                                                        // Character Overrun
   uint8_t listen : 1;                                                          // Currently in Listen Only Mode
   uint8_t broadcst : 1;                                                        // Broadcast Received
   uint8_t type : 1;                                                            // 1 for this type
}) ModbusRemoteRcv_t;
// Remote device MODBUS Send Event
MODPACKED(
typedef struct
{
   uint8_t exc_snt : 1;                                                         // Read Exception Sent (Exception Codes 1-3)
   uint8_t abt_snt : 1;                                                         // Server Abort Exception Sent (Exception Code 4)
   uint8_t busy_snt : 1;                                                        // Server Busy Exception Sent (Exception Codes 5-6)
   uint8_t nak_snt : 1;                                                         // Server Program NAK Exception Sent (Exception Code 7)
   uint8_t write_to : 1;                                                        // Write Timeout Error Occurred
   uint8_t listen : 1;                                                          // Currently in Listen Only Mode
   uint8_t type : 1;                                                            // 1 for this type
   uint8_t spare : 1;                                                           //
}) ModbusRemoteSnd_t;
// Remote device Entered Listen Only Mode
#define MB_REMOTE_LISTEN 0x04U
// Remote device Initiated Communication Restart
#define MB_REMOTE_RESTART 0U

/**
 Modbus function 0x16 Mask Write Register.

 This function code is used to modify the contents of a specified holding
 register using a combination of an AND mask, an OR mask, and the
 register's current contents. The function can be used to set or clear
 individual bits in the register.

 The request specifies the holding register to be written, the data to be
 used as the AND mask, and the data to be used as the OR mask. Registers
 are addressed starting at zero.

 The function's algorithm is:
 Result = (Current Contents && And_Mask) || (Or_Mask && (~And_Mask))

 @param WriteAddress address of the holding register (0x0000..0xFFFF)
 @param AndMask AND mask (0x0000..0xFFFF)
 @param OrMask OR mask (0x0000..0xFFFF)
 @return 0 on success; exception number on failure
 @ingroup register
 */
#define MB_MASK_WRITE ( 22U )
/**
 * @brief Mask Write Register request PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;   //0
   uint16_t referenceAddr; //1-2
   uint16_t andMask;       //3-4
   uint16_t orMask;        //5-6
}) ModbusMaskWriteRegReq;
/**
 * @brief Mask Write Register response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;   //0
   uint16_t referenceAddr; //1-2
   uint16_t andMask;       //3-4
   uint16_t orMask;        //5-6
}) ModbusMaskWriteRegResp;

/*
Get detections (function code 0x41) This function returns the detections/measurements

returns number of detections then a structure as below for every detection
*/
#define MB_GET_DETECTIONS ( 65U )
MODPACKED(
typedef struct {
   uint16_t distance;                                                           // distance
   uint16_t amplitude;                                                          // amplitude x64
   uint8_t valid : 1U;                                                          // object valid
   uint8_t demerging : 1U;                                                      // demerging
   uint8_t resrved : 1U;
   uint8_t saturated : 1U;                                                      // saturated
   uint8_t spare : 4U;
   uint8_t segment;                                                             // segment number
   uint32_t timestamp;                                                          // ms since device booted
   uint8_t lightpower;                                                          // light source power
   uint16_t bitfield;                                                           // bit field aquisition
}) mbCmd41_leddar_detection_t;                                                  // one single detection field (little endian format)

/*
Read module data (function code 0x42) for Leddar Vu
*/
#define MB_READ_LEDDAR_VU  ( 66U )
#define MB_LEDDAR_MIN_ADDR ( 1U )
#define MB_LEDDAR_MAX_ADDR ( 247U )
MODPACKED(
typedef struct {
   uint32_t base_addr;                                                          // base address
   uint8_t byte_cnt;                                                            // byte count
}) mbCmd42_leddar_send_read_t;                                                  // send a read to the leddar

/*
Write module data (function code 0x43)
*/
#define MB_WRITE_LEDDAR_VU ( 67U )
/*
Send opcode command (function code 0x44)
*/
#define MB_SND_OPCODE_LEDDAR_VU 68U
#define LEDDAR_OPCODE_READ_STATUS 0x5U                                          // valid leddar vu opcodes
#define LEDDAR_OPCODE_WRITE_ENAB 0x6U
#define LEDDAR_OPCODE_WRITE_DISAB 0x4U
#define LEDDAR_OPCODE_RESET_CONFIG 0xC7U
#define LEDDAR_OPCODE_SOFT_RESET 0x99U
MODPACKED(
typedef struct {
   uint8_t opcode;                                                              // opcode
   uint8_t value;                                                               // 0x00 for send =opcode sent for reply
}) mbCmd44_leddar_opcode_t;                                                     // send and read an opcode message
/*
Get serial port settings (function code 0x45, sub code 0x00)
Set serial port settings (function code 0x45, sub code 0x01)
Get carrier firmware information (function code 0x45, 0x02)
Get carrier device information (function code 0x45, 0x03)
Get CAN port settings (function code 0x45, 0x04)
Set CAN port settings (function code 0x45, 0x05)
*/
#define MB_PORT_LEDDAR_VU 69U
#define MB_GET PORT_LEDDAR_VU 0U
MODPACKED(
typedef struct {
   uint8_t subfunc;                                                             // sub function
   uint8_t no_of_ports;                                                         // number of serial ports
   uint8_t port_no;                                                             // port number
   uint32_t baud_rate;                                                          // Baud rate, supported rates:  9,600 19,200 38,400 57,600 115,200
   uint8_t data_sz;                                                             // Date size: 8 = 8-bit size
   uint8_t parity;                                                              // Parity: 0 = None 1 = Odd 2 = Even
   uint8_t stopbit;                                                             // Stop bit: 1 = 1 stop bit 2 = 2 stop bits
   uint8_t flowcont;                                                            // Flow control: 0 = None
   uint8_t mod_addr;                                                            // modbus address
   uint8_t echo;                                                                // Max. echoes per transactions. Used for the Get Detection command (function code 0x41), max. of 40 echoes.
   uint16_t dist_res;                                                           // Distance resolution: ? 1 = m ? 10 = dm ? 100 = cm ? 1,000 = m                                                              // number of serial ports
}) mbCmd45_sub0_reply_t;                                                        // reply from a 0x45 0x00 command
#define MB_SET_PORT_LEDDAR_VU 1U
MODPACKED(
typedef struct {
   uint8_t port_no;                                                             // port number
   uint32_t baud_rate;                                                          // Baud rate, supported rates:  9,600 19,200 38,400 57,600 115,200
   uint8_t data_sz;                                                             // Date size: 8 = 8-bit size
   uint8_t parity;                                                              // Parity: 0 = None 1 = Odd 2 = Even
   uint8_t stopbit;                                                             // Stop bit: 1 = 1 stop bit 2 = 2 stop bits
   uint8_t flowcont;                                                            // Flow control: 0 = None
   uint8_t mod_addr;                                                            // modbus address
   uint8_t echo;                                                                // Max. echoes per transactions. Used for the Get Detection command (function code 0x41), max. of 40 echoes.
   uint16_t dist_res;                                                           // Distance resolution: ? 1 = m ? 10 = dm ? 100 = cm ? 1,000 = m                                                              // number of serial ports
}) mbCmd45_sub1_reply_t;                                                        // reply from a 0x45 0x01 command
#define MB_GET_FIRM_LEDDAR_VU 2U
MODPACKED(
typedef struct {
   uint8_t opcode ;                                                             // opcode
   unsigned char firmWare[32U];                                                 // firmware part number
   uint64_t version;                                                            // firmware version                                                             // number of serial ports
}) mbCmd45_sub2_reply_t;                                                        // reply from a 0x45 0x02 command
#define MB_GET_CAR_DEV_LEDDAR_VU 3U
MODPACKED(
typedef struct {
   uint8_t opcode ;                                                             // opcode
   unsigned char hwPart[32U];                                                   // hardware part number
   unsigned char serialNo[32U];                                                 // serial number
   uint32_t option;                                                             // option number                                                             // number of serial ports
}) mbCmd45_sub3_reply_t;                                                        // reply from a 0x45 0x03 command
#define MB_GET_CAN_PORT_LEDDAR_VU 4U
MODPACKED(
typedef struct {
   uint8_t opcode ;                                                             // opcode
   uint8_t No_of_port;                                                          // number of ports
   uint8_t logical_port;                                                        // logical port
   uint32_t baud;                                                               // Baud rate, supported rates: 10,000 20,000 50,000 100,000 125,000 250,000 500,000 1,000,000
   uint8_t frame_fmt;                                                           // Frame format: 0 = Standard 11 bits 1 = Extended 29 bits
   uint32_t tx_base;                                                            // Tx base ID
   uint32_t rx_base;                                                            // Rx base ID
   uint8_t max_det;                                                             // Maximum number of detections (measurements) returned per CAN detection message transaction: 1 through 96
   uint16_t dist_res;                                                           // Distance resolution: ? 1 = m ? 10 = dm ? 100 = cm ? 1,000 = m
   uint16_t msg_delay;                                                          // Inter-message delay 0 through 65535 milliseconds
   uint16_t cyc_delay;                                                          // Inter-cycle delay 0 through 65535 milliseconds
}) mbCmd45_sub4_reply_t;                                                        // reply from a 0x45 0x04 command
#define MB_SET_CAN_PORT_LEDDAR_VU 5U
MODPACKED(
typedef struct {
   uint8_t set;                                                                 // Settings of corresponding logical CAN port number to set
   uint32_t baud;                                                               // Baud rate, supported rates: 10,000 20,000 50,000 100,000 125,000 250,000 500,000 1,000,000
   uint8_t frame_fmt;                                                           // Frame format: 0 = Standard 11 bits 1 = Extended 29 bits
   uint32_t tx_base;                                                            // Tx base ID
   uint32_t rx_base;                                                            // Rx base ID
   uint8_t max_det;                                                             // Maximum number of detections (measurements) returned per CAN detection message transaction: 1 through 96
   uint16_t dist_res;                                                           // Distance resolution: ? 1 = m ? 10 = dm ? 100 = cm ? 1,000 = m
   uint16_t msg_delay;                                                          // Inter-message delay 0 through 65535 milliseconds
   uint16_t cyc_delay;                                                          // Inter-cycle delay 0 through 65535 milliseconds
}) mbCmd45_sub5_reply_t;                                                        // reply from a 0x45 0x05 command
// Encapsulated Interface Transport  CANopen General Reference Request and Response PD
#define MB_READ_DEVICE_ID 43U
#define MB_READ_DEVICE_ID2 14U                                                  // alternate command for some vendor

#define MB_MEI_TYPE 0x0DU                                                       //  MODBUS Assigned Number licensed to CiA for the CANopen general reference
#define MB_MEI_TYPE1 0x0EU                                                      // alternate for some

#define MB_OBJ_ID_VENDOR_NM 0x00U                                               // object id's
#define MB_OBJ_ID_PROD_CODE 0x01U
#define MB_OBJ_ID_MAJ_MIN_REV 0x02U
#define MB_OBJ_ID_VENDOR_URL 0x03U
#define MB_OBJ_ID_PROD_NM 0x04U
#define MB_OBJ_ID_MOD_NM 0x05U
#define MB_OBJ_ID_USER_APP_NM 0x06U

#define MB_CONFORM_BASIC 0x01U                                                  // basic identification (stream access only) 
#define MB_CONFORM_REG 0x02U                                                    // regular identification (stream access only) 
#define MB_CONFORM_EXT 0x03U                                                    // extended identification (stream access only)
#define MB_CONFORM_BASIC_STREAM 0x81U                                           // basic identification (stream access and individual access)
#define MB_CONFORM_REG_STREAM 0x82U                                             // regular identification (stream access and individual access) 
#define MB_CONFORM_EXT_STREAM 0x83U                                             // extended identification(stream access and individual

MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t MEIType;                                                             //1-2
   uint8_t MEIData[];                                                           // dynamic consider fixing to what you want max size 253 byte
}) ModbusEITReq;
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t MEIType;                                                             //1-2
   uint8_t MEIData[];                                                           // dynamic consider fixing to what you want max size 253 byte
}) ModbusEITResp;
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t MEIType;                                                             //1-2
   uint8_t devId;                                                               //3-4 allowable values 01 / 02 / 03 / 04
   uint8_t objId;                                                               //5-6 0x00 to 0xFF known ones are defined above
}) ModbusDeviceInfoReq;
MODPACKED(
typedef struct
{
   uint8_t id;                                                                  // 0 object id
   uint8_t len;                                                                 // 1 object length in bytes
   unsigned char value[];                                                       // value can be typically a string
}) ModbusObject_t;
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t MEIType;                                                             //1-2
   uint8_t devId;                                                               //3-4 allowable values 01 / 02 / 03 / 04
   uint8_t conformity;                                                          //5-6 conformity
   uint8_t more_follows;                                                        // 00 / FF
   uint8_t next_obj_id;
   uint8_t number_obj;
   ModbusObject_t objects[];                                                    // modbus objects returned
}) ModbusDeviceInfoResp;
// Read FIFO Queue
#define MB_READ_FIFO_Q 24U
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint16_t fifoPointerAddr;                                                    //1-2
}) ModbusFIFOQueReq;
/**
 * @brief Mask Write Register response PDU
 **/
MODPACKED(
typedef struct
{
   uint8_t functionCode;   //0
   uint16_t byteCount;                                                          //1-2
   uint16_t fifoCount;                                                          //3-4
   uint16_t fifoValues[];                                                       //5-6
}) ModbusFIFOQueResp;
// Read File Record
#define MB_READ_FILE_REC 20U
MODPACKED(
typedef struct
{
   uint8_t refType;                                                             //0
   uint16_t fileNumber;                                                         //1-2
   uint16_t recNumber;                                                          //3-4
   uint16_t recLen;                                                             //5-6
}) FileRecord_t;
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1-2
   FileRecord_t fileRecords[];
}) ModbusReadFileRecReq;
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t respLen;                                                             //1-2
   uint8_t fileRespLen;                                                         //3-4
   uint8_t refType;                                                             //5-6
   uint16_t recData[];                                                          //
}) ModbusReadFileRecResp;
//Write File Record
#define MB_WRITE_FILE_REC 21U
MODPACKED(
typedef struct
{
   uint8_t functionCode;                                                        //0
   uint8_t byteCount;                                                           //1-2
   FileRecord_t fileRecords[];
   uint16_t fileData[];
}) ModbusWriteFileRecReqResp;

#define MB_FUNC_ERROR                         ( 128U )
/* ----------------------- Type definitions ---------------------------------*/

typedef enum                                                                    // alternative to the define
{
   MODBUS_FUNCTION_READ_COILS                = 1,
   MODBUS_FUNCTION_READ_DISCRETE_INPUTS      = 2,
   MODBUS_FUNCTION_READ_HOLDING_REGS         = 3,
   MODBUS_FUNCTION_READ_INPUT_REGS           = 4,
   MODBUS_FUNCTION_WRITE_SINGLE_COIL         = 5,
   MODBUS_FUNCTION_WRITE_SINGLE_REG          = 6,
   MODBUS_FUNCTION_READ_EXCEPTION_STATUS     = 7,
   MODBUS_FUNCTION_DIAGNOSTICS               = 8,
   MODBUS_FUNCTION_GET_COMM_EVENT_COUNTER    = 11,
   MODBUS_FUNCTION_GET_COMM_EVENT_LOG        = 12,
   MODBUS_FUNCTION_WRITE_MULTIPLE_COILS      = 15,
   MODBUS_FUNCTION_WRITE_MULTIPLE_REGS       = 16,
   MODBUS_FUNCTION_REPORT_SLAVE_ID           = 17,
   MODBUS_FUNCTION_READ_FILE_RECORD          = 20,
   MODBUS_FUNCTION_WRITE_FILE_RECORD         = 21,
   MODBUS_FUNCTION_MASK_WRITE_REG            = 22,
   MODBUS_FUNCTION_READ_WRITE_MULTIPLE_REGS  = 23,
   MODBUS_FUNCTION_READ_FIFO_QUEUE           = 24,
   MODBUS_FUNCTION_GET_DETECTIONS            = 65,
   MODBUS_FUNCTION_READ_LEDDAR               = 66,
   MODBUS_FUNCTION_WRITE_LEDDAR              = 67,
   MODBUS_FUNCTION_OPCODE_LEDDAR             = 68,
   MODBUS_FUNCTION_PORT_LEDDAR               = 69,
   MODBUS_FUNCTION_ENCAPSULATED_IF_TRANSPORT = 43
} eMBFunctionCode;

typedef enum
{
    MB_EX_NONE = 0x00U,                                                         // MBSuccess
    MB_EX_ILLEGAL_FUNCTION = 0x01U,                                             // MBIllegalFunction
    MB_EX_ILLEGAL_DATA_ADDRESS = 0x02U,                                         // MBIllegalDataAddress A request for a register that does not exist will return error code 2
    MB_EX_ILLEGAL_DATA_VALUE = 0x03U,                                           // MBIllegalDataValue Trying to set a register to an invalid value will return error code 3
    MB_EX_SLAVE_DEVICE_FAILURE = 0x04U,                                         // MBSlaveDeviceFailure If an error occurs while trying to execute the function, error code 4 will be returned
    MB_EX_ACKNOWLEDGE = 0x05U,                                                  // Acknowledge
    MB_EX_SLAVE_BUSY = 0x06U,                                                   // Slave Device Busy
    MB_EX_NACK = 0x07U,                                                         // NACK
    MB_EX_MEMORY_PARITY_ERROR = 0x08U,                                          // Memory Parity Error
    MB_EX_GATEWAY_PATH_FAILED = 0x0AU,                                          // Gateway Path Unavailable
    MB_EX_GATEWAY_TGT_FAILED = 0x0BU                                            // Gateway Target Device Failed to Respond
} eMBException;

typedef enum
{
   MB_COIL_STATE_OFF = 0x0000,
   MB_COIL_STATE_ON  = 0xFF00
} eMBCoilState;

typedef enum
{
    MBSuccess                    = 0x00U,                                       // no error occurred
    MBInvalidSlaveID             = 0xE0U,                                       // wrong slave id
    MBInvalidFunction            = 0xE1U,                                       // invalid function code sent
    MBResponseTimedOut           = 0xE2U,                                       // time out
    MBInvalidCRC                 = 0xE3U                                        // wrong crc
} eMBClassException;

/*!
 * Constants which defines the format of a modbus frame. The example is
 * shown for a Modbus RTU/ASCII frame. Note that the Modbus PDU is not
 * dependent on the underlying transport.
 *
 * <code>
 * <------------------------ MODBUS SERIAL LINE PDU (1) ------------------->
 *              <----------- MODBUS PDU (1') ---------------->
 *  +-----------+---------------+----------------------------+-------------+
 *  | Address   | Function Code | Data                       | CRC/LRC     |
 *  +-----------+---------------+----------------------------+-------------+
 *  |           |               |                                   |
 * (2)        (3/2')           (3')                                (4)
 *
 * (1)  ... MB_SER_PDU_SIZE_MAX = 256
 * (2)  ... MB_SER_PDU_ADDR_OFF = 0
 * (3)  ... MB_SER_PDU_PDU_OFF  = 1
 * (4)  ... MB_SER_PDU_SIZE_CRC = 2
 *
 * (1') ... MB_PDU_SIZE_MAX     = 253
 * (2') ... MB_PDU_FUNC_OFF     = 0
 * (3') ... MB_PDU_DATA_OFF     = 1
 * </code>
 */

/* ----------------------- Defines ------------------------------------------*/

#define MB_RTU_PDU_SIZE_MAX     253U                                            /*!< Maximum size of a PDU. */
#define MB_RTU_PDU_SIZE_MIN     1U                                              /*!< Function Code */
#define MB_ASC_PDU_SIZE_MIN     3U                                              /*!< Minimum size of a Modbus ASCII frame. */
#define MB_ASC_PDU_SIZE_MAX     256U                                            /*!< Maximum size of a Modbus ASCII frame. */

#define MB_PDU_FUNC_OFF     0U                                                  /*!< Offset of function code in PDU. */
#define MB_PDU_DATA_OFF     1U                                                  /*!< Offset for response data in PDU. */

// modbus ascii
#define MB_ASCII_START :                                                        // colon is start char
#define MB_ASCII_DEFAULT_CR     '\r'                                            /*!< Default CR character for Modbus ASCII. */
#define MB_ASCII_DEFAULT_LF     '\n'                                            /*!< Default LF character for Modbus ASCII. */
#define MB_SER_PDU_SIZE_LRC     1U                                              /*!< Size of LRC field in PDU. includes the : and the CRLF */
#define MB_SER_PDU_ADDR_OFF     0U                                              /*!< Offset of slave address in Ser-PDU. */
#define MB_SER_PDU_PDU_OFF      1U                                              /*!< Offset of Modbus-PDU in Ser-PDU. */

// modbus BIN If a byte with the value 123 (0x7B) or with the value 125 (0x7D) occurs it has to be duplicated (i.e. written twice)..
#define MB_BIN_START 0x7B                                                       // modbus bin is in binary like rtu but within braces { 01 01 01 44 } pauses allowed
#define MB_BIN_STOP 0x7D

// modbus tcp
#define MB_TCP_PORT 502U                                                        //Modbus/TCP port number
#define MB_TCP_SECURE_PORT 802U                                                 //Secure Modbus/TCP port number

#define MB_PROTOCOL_ID 0U                                                       //Modbus protocol identifier
#define MB_DEFAULT_UNIT_ID 255U                                                 //Default unit identifier

#define MB_MAX_PDU_SIZE 253U                                                    //Maximum size of Modbus PDU
#define MB_MAX_ADU_SIZE 260U                                                    //Maximum size of Modbus/TCP ADU

#define MB_FUNCTION_CODE_MASK 0x7FU                                             //Function code mask
#define MB_EXCEPTION_MASK 0x80U                                                 //Exception response mask

#define MB_SET_COIL(a, n) ((a)[(n) / 8U] |= (1U << ((n) % 8U)))                 //Set coil value
#define MB_RESET_COIL(a, n) ((a)[(n) / 8U] &= ~(1U << ((n) % 8U)))              //Reset coil value
#define MB_TEST_COIL(a, n) ((a[(n) / 8U] >> ((n) % 8U)) & 1U)                   //Test coil value

/**
 * @brief MBAP header (Modbus Application Protocol)
 **/
MODPACKED(
typedef struct
{
   uint16_t transactionId;                                                      // 0-1 the invocation identification (2 bytes) used for transaction pairing; formerly called transaction identifier
   uint16_t protocolId;                                                         // 2-3 the protocol identifier (2 bytes), is 0 for Modbus by default; reserved for future extensions
   uint16_t length;                                                             // 4-5  the length (2 bytes), a byte count of all following bytes
   uint8_t unitId;                                                              // 6 the unit identifier (1 byte) used to identify a remote unit located on a non-TCP/IP network
   uint8_t pdu[];                                                               // 7 payload dynamically assigned may wish to make size of max_pdu or what you are sending
}) ModbusEthHeader;                                                             // for modbus tcp or udp

#define ENRON_ADDR_BOOL_START 0x03E9U                                             // Address boundaries as defined by ENRON
#define ENRON_ADDR_BOOL_STOP 0x07CF
#define ENRON_ADDR_UINT16_START 0x0BB9U
#define ENRON_ADDR_UINT16_STOP 0x0F9FU
#define ENRON_ADDR_UINT32_START 0x1389U
#define ENRON_ADDR_UINT32_STOP 0x176FU
#define ENRON_ADDR_FLOAT32_START 0x1B59U
#define ENRON_ADDR_FLOAT32_STOP 0x1F3FU

#ifdef __cplusplus
PR_END_EXTERN_C
#endif

// These functions can be used as alternative to structs by sending a string also useful for modbus ascii

/* Builds a RTU master request header */
static int8_t modbus_rtu_build_request(uint8_t slave, uint8_t function, uint16_t addr, uint16_t nb,  uint8_t *req)
{
    if ((((slave==NULL) || (function==NULL)) || (addr==NULL)) || (nb==NULL))
       return 0U;                                                               // return error
    req[0U] = slave;                                                            // create the message
    req[1U] = function;
    req[2U] = addr >> 8U;
    req[3U] = addr & 0x00ffU;
    req[4U] = nb >> 8U;
    req[5U] = nb & 0x00ffU;
    return 1U;                                                                   // return success
}

/* Builds a RTU slave response header */
static int8_t modbus_rtu_build_response(uint8_t slave, uint8_t function, uint8_t *rsp)
{
    if ((slave==NULL) || (function==NULL))
       return 0U;
    rsp[0] = slave;
    rsp[1] = function;
    return 1U;
}

// tag crc for RTU
static void modbus_rtu_tag_crc(uint8_t *req, uint16_t req_length)
{
    uint16_t crc;
    crc=crc=usMBCRC16((unsigned char *) req, req_length);                       // included in crc.h
    req[req_length++] = crc >> 8U;
    req[req_length++] = crc & 0x00FFU;
}

// tag crc for ASCII
static void modbus_ascii_tag_crc(uint8_t *req, uint16_t req_length)
{
    uint8_t crc;
    crc=crc=usMBAsciiLRC((unsigned char *) req, req_length);                    // included in crc.h
    req[req_length++] = crc & 0x00FFU;
}

#endif