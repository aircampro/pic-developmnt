// This file is a "Hello, world!" in C language by gcc for wandbox.
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "definitions.h"
#include "struct.h"
#include "jsmn.h"
// #include <boost/algorithm/string/replace.hpp>
//const char * const xy_reply[] = { "{ \"rval\": 0, \"msg_id\": 259, \"groups\": [ { \"something else\": 1 } ] }" };                            // Array of string definitions as per enum
const char * const xy_reply[] = { "{ \"rval\": 0, \"msg_id\": 3, \"param\": [ { \"camera_clock\": \"2015-04-07 02:32:29\" },{ \"video_resolution\": \"1920x1080 60P 16:9\" }, { \"video_standard\": \"NTSC\" }, { \"video_quality\": \"S.Fine\" }, { \"photo_size\": \"16M (4608x3456 4:3)\" }, { \"app_status\": \"idle\" },  { \"video_stamp\": \"off\" } , { \"video_rotate\": \"on\" }  ] }" }; // { \"app_status\": \"idle\" },  { \"video_stamp\": \"off\" }, { \"video_quality\": \"S.Fine\" }, { \"timelapse_video\": \"off\" }, { \"capture_mode\": \"precise quality\" }, { \"photo_size\": \"16M (4608x3456 4:3)\" }, { \"photo_stamp\": \"off\" }, { \"photo_quality\": \"S.Fine\" }, { \"timelapse_photo\": \"60\" }, { \"preview_status\": \"on\" }, { \"buzzer_volume\": \"mute\" }, { \"buzzer_ring\": \"off\" }, { \"capture_default_mode\": \"precise quality\" }, { \"precise_cont_time\": \"60.0 sec\" }, { \"burst_capture_number\": \"7 p / s\" }, { \"restore_factory_settings\": \"on\" }, { \"led_mode\": \"all enable\" }, { \"dev_reboot\": \"on\" }, { \"meter_mode\": \"center\" }, { \"sd_card_status\": \"insert\" }, { \"video_output_dev_type\": \"tv\" }, { \"sw_version\": \"YDXJv22_1.0.7_build-20150330113749_b690_i446_s699\" }, { \"hw_version\": \"YDXJ_v22\" }, { \"dual_stream_status\": \"on\" }, { \"streaming_status\": \"off\" }, { \"precise_cont_capturing\": \"off\" }, { \"piv_enable\": \"off\" }, { \"auto_low_light\": \"on\" }, { \"loop_record\": \"off\" }, { \"warp_enable\": \"off\" }, { \"support_auto_low_light\": \"on\" }, { \"precise_selftime\": \"5s\" }, { \"precise_self_running\": \"off\" }, { \"auto_power_off\": \"5 minutes\" }, { \"serial_number\": \"xxxxx\" }, { \"system_mode\": \"capture\" }, { \"system_default_mode\": \"capture\" }, { \"start_wifi_while_booted\": \"off\" }, { \"quick_record_time\": \"0\" }, { \"precise_self_remain_time\": \"0\" }, { \"sdcard_need_format\": \"no-need\" }, { \"video_rotate\": \"off\" } ] }" };                            // Array of string definitions as per enum
//const char * const xy_reply[] = { "{\"user\": \"johndoe\", \"admin\": false, \"msg_id\": 1000, \"groups\": [\"users\", \"wheel\", \"audio\", \"video\"] }" };unsigned 

XY_reply_t XYJsonReply;
XY_config_t XYConfig;
int16_t XY_ParseState;
char old,new;
unsigned char stringStore[2200];
uint16_t numberX;

int main(void)
{
    puts("Hello, Wandbox!");
    XY_ParseState=xy_parse_reply((unsigned char*) xy_reply[0], &XYJsonReply,XY_READ_ALL,&XYConfig);
    printf("key %d param %s returned %d\r",XYJsonReply.msg_id,XYJsonReply.param_str,XY_ParseState);
    //---- Replace_AllOccurrence((unsigned char*) &XYJsonReply.param_str,old,new)
    //---- numberX=replacechar((char*) &XYJsonReply.param_str,(char*) "r",(char*) ",");
    //---- removeAll( &XYJsonReply.param_str, ":" );
    //---- removeSubstr((char*) &XYJsonReply.param_str,(char*) ":");
    RemoveCharFromString( (char*) &XYJsonReply.param_str, ':');
    RemoveCharFromString( (char*) &XYJsonReply.param_str, '{');
    RemoveCharFromString( (char*) &XYJsonReply.param_str, '}');
    //---- removeSubstr((char*) &XYJsonReply.param_str,(char*) "{");
    //---- removeSubstr((char*) &XYJsonReply.param_str,(char *) "}");
    //printf("param %s \r",(unsigned char*) &XYJsonReply.param_str);
    sprintf((char*)stringStore,"{\"groups\" : %s }\r",XYJsonReply.param_str);
    XY_ParseState=xy_parse_reply((unsigned char*) stringStore, &XYJsonReply,XY_READ_ALL,&XYConfig);
    // printf("key %d param %s returned %d\r",XYJsonReply.msg_id,XYJsonReply.param_str,XY_ParseState);
    //printf("key %d param %s returned %d string %s\r",XYJsonReply.msg_id,stringStore,XY_ParseState,XYConfig.camera_clock);
    printf(" ==== clock %s res %s qual %s pho_sz %s vid Std %s rotate %s ======= \r",XYConfig.camera_clock,XYConfig.video_resolution,XYConfig.video_quality,XYConfig.photo_size,XYConfig.video_standard,XYConfig.video_rotate);
    return EXIT_SUCCESS;
}

// GCC reference:
//   https://gcc.gnu.org/

// C language references:
//   https://msdn.microsoft.com/library/fw5abdx6.aspx
//   https://www.gnu.org/software/gnu-c-manual/
