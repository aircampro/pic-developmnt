// by jeremie miller - 2015-2017
// public domain UNLICENSE, contributions/improvements welcome via github at https://github.com/quartzjer/cb0r
//
// CBOR
// RFC 7049 Concise Binary Object Representation
// “The Concise Binary Object Representation (CBOR) is a data format whose design goals include the possibility 
// of extremely small code size, fairly small message size, and extensibility without the need for version negotiation.”
//
// Internet of Things (IoT) is the term used to describe the increasing number of connected embedded systems used in a wide variety of applications. 
// M2M (Machine to Machine) and constrained nodes are also common terms. 
// The distinction between the expressions is some what fuzzy but inbroad terms both IoT and M2M are networked systems 
// which are comprised partly or fully out of constrained nodes.
// changed for mikroE C by ACP Aviation
//
#include "cb0r.h"
#include "definitions.h"
#include "stdint.h"
#include "cpu_endian_defs.h"

// unhelpful legacy GCC warning noise for syntax used in cb0r()
//#pragma GCC diagnostic ignored "-Wunknown-pragmas"
//#pragma GCC diagnostic ignored "-Wpragmas"
//#pragma GCC diagnostic ignored "-Winitializer-overrides"
//#pragma GCC diagnostic ignored "-Woverride-init"

// start at bin, returns end pointer (== stop if complete), either skips items or extracts result of current item
uint8_t *cb0r(uint8_t *start, uint8_t *stop, uint32_t skip, cb0r_t result)
{

  cb0r_e cbType = CB0R_ERR;
  uint8_t size = 0u;
  uint32_t count = 0u;
#ifdef jump_tables_work                                                         // jump tables dont work in mikroE C

  // type byte is fully unrolled for structure only      THIS STRUT DEFINES THE JUMP TABLE ADDRESSES
  //static const void *go[] RODATA_SEGMENT_CONSTANT =
  static void *go[] =
  {
    //                                                                          [0x00 ... 0xff] =
       &&l_ebad,
    // first 5 bits
    //                                                                          [0x00 ... 0x17] =
        &&l_int,
    // Major type 1 CB0R_INT
    //[0x18] =
        &&l_int1, //
        // [0x19] =
        &&l_int2,
        //[0x1a] =
        &&l_int4,
    //        [0x1b] =
        &&l_int8,
    //                                                                          [0x20 ... 0x37] =
        &&l_int,
    // Major type 2 CB0R_NEG
    // [0x38] =
        &&l_int1,
        // [0x39] =
        &&l_int2,
        // [0x3a] =
        &&l_int4,
    //        [0x3b] =
        &&l_int8,
    //                                                                          [0x40 ... 0x57] =
        &&l_byte,
    // Major type 3 CB0R_BYTE
    // [0x58] =
        &&l_byte1,
        // [0x59] =
        &&l_byte2,
        // [0x5a] =
        &&l_byte4,
        //[0x5b] =
        &&l_ebig,
    // [0x5f] =
        &&l_until,
    // Major type 4 CB0R_UTF8
    //                                                                          [0x60 ... 0x77] =
        &&l_byte,
    // [0x78] =
        &&l_byte1,
        // [0x79] =
        &&l_byte2,
        // [0x7a] =
        &&l_byte4,
        // [0x7b] =
        &&l_ebig,
    // [0x7f] =
        &&l_until,
    // Major type 5 CB0R_ARRAY
    //                                                                          [0x80 ... 0x97] =
        &&l_array,
    // [0x98] =
        &&l_array1,
        // [0x99] =
        &&l_array2,
        // [0x9a] =
        &&l_array4,
        //[0x9b] =
        &&l_ebig,
    // [0x9f] =
        &&l_until,
    // Major type 6 CB0R_MAP
    //                                                                          [0xa0 ... 0xb7] =
        &&l_array,
    // [0xb8] =
        &&l_array1,
        // [0xb9] =
        &&l_array2,
        //[0xba] =
        &&l_array4,
        // [0xbb] =
        &&l_ebig,
    // [0xbf] =
        &&l_until,
    // Major type 7 CB0R_TAG
    //                                                                          [0xc0 ... 0xd7] =
        &&l_tag,
    // [0xd8] =
        &&l_tag1,
        //[0xd9] =
        &&l_tag2,
        //[0xda] =
        &&l_tag4,
        // [0xdb] =
        &&l_tag8,
    // Major type 8 CB0R_SIMPLE / CB0R_FLOAT
    //                                                                          [0xe0 ... 0xf7] =
        &&l_int,
    //[0xf8] =
        &&l_int1,
        // [0xf9] =
        &&l_int2,
        //[0xfa] =
        &&l_int4,
        // [0xfb] =
        &&l_int8,
    //[0xff] =
        &&l_break
  };
#endif

  uint8_t *end = start + 1u;
  if(end > stop) {
    if(result) result->type = CB0R_ERR;
    return stop;
  }

#ifdef jump_tables_work
  goto *go[*start];                                                             // ======= DOESNT SEEM TO WORK in mikroE C ============
#else
  switch(*start)
  {
       case 0u:
       goto l_ebad;
       break;
    // first 5 bits
    //                                                                          [0x00 ... 0x17] =
       case 1u:
       goto l_int;
       break;
       
    // Major type 1 CB0R_INT
    //[0x18] =
       case 2u:
       goto l_int1;
       break;
        // [0x19] =
        
       case 3u:
       goto l_int2;
       break;
       
       case 4u:
        //[0x1a] =
       goto l_int4;
       break;
       
    //        [0x1b] =
       case 5u:
       goto l_int8;
       break;
    //                                                                          [0x20 ... 0x37] =
       case 6u:
       goto l_int;
       break;
    // Major type 2 CB0R_NEG
    // [0x38] =
       case 7u:
       goto l_int1;
       break;
        // [0x39] =
       case 8u:
       goto l_int2;
       break;
        // [0x3a] =
       case 9u:
       goto l_int4;
       break;
    //        [0x3b] =
       case 10u:
       goto l_int8;
       break;
    //                                                                          [0x40 ... 0x57] =
       case 11u:
       goto l_byte;
       break;
    // Major type 3 CB0R_BYTE
    // [0x58] =
       case 12u:
       goto l_byte1;
       break;
        // [0x59] =
       case 13u:
       goto l_byte2;
       break;
        // [0x5a] =
       case 14u:
       goto l_byte4;
       break;
        //[0x5b] =
       case 15u:
       goto l_ebig;
       break;
    // [0x5f] =
       case 16u:
       goto l_until;
       break;
    // Major type 4 CB0R_UTF8
    //                                                                          [0x60 ... 0x77] =
       case 17u:
       goto l_byte;
       break;
    // [0x78] =
       case 18u:
       goto l_byte1;
       break;
        // [0x79] =
       case 19u:
       goto l_byte2;
       break;
        // [0x7a] =
       case 20u:
       goto l_byte4;
        // [0x7b] =
       case 21u:
       goto l_ebig;
       break;
    // [0x7f] =
       case 22u:
       goto l_until;
       break;
    // Major type 5 CB0R_ARRAY
    //                                                                          [0x80 ... 0x97] =
       case 23u:
       goto l_array;
       break;
    // [0x98] =
       case 24u:
       goto l_array1;
       break;
        // [0x99] =
       case 25u:
       goto l_array2;
       break;
        // [0x9a] =
       case 26u:
       goto l_array4;
       break;
        //[0x9b] =
       case 27u:
       goto l_ebig;
       break;
    // [0x9f] =
       case 28u:
       goto l_until;
       break;
    // Major type 6 CB0R_MAP
    //                                                                          [0xa0 ... 0xb7] =
       case 29u:
       goto l_array;
       break;
    // [0xb8] =
       case 30u:
       goto l_array1;
       break;
        // [0xb9] =
       case 31u:
       goto l_array2;
       break;
        //[0xba] =
       case 32u:
       goto l_array4;
       break;
        // [0xbb] =
       case 33u:
       goto l_ebig;
       break;
    // [0xbf] =
       case 34u:
       goto l_until;
       break;
    // Major type 7 CB0R_TAG
    //                                                                          [0xc0 ... 0xd7] =
       case 35u:
       goto l_tag;
       break;
    // [0xd8] =
       case 36u:
       goto l_tag1;
       break;
        //[0xd9] =
       case 37u:
       goto l_tag2;
       break;
        //[0xda] =
       case 38u:
       goto l_tag4;
       break;
        // [0xdb] =
       case 39u:
       goto l_tag8;
       break;
    // Major type 8 CB0R_SIMPLE / CB0R_FLOAT
    //                                                                          [0xe0 ... 0xf7] =
       case 40u:
       goto l_int;
       break;
    //[0xf8] =
       case 41u:
       goto l_int1;
       break;
        // [0xf9] =
       case 42u:
       goto l_int2;
       break;
        //[0xfa] =
       case 43u:
       goto l_int4;
       break;
        // [0xfb] =
       case 44u:
       goto l_int8;
       break;
    //[0xff] =
       case 45u:
       goto l_break;
       break;
       
       default:
       cbType = CB0R_ERR;
       goto l_fail;
       break;
  }
#endif
  // all types using integer structure
  l_int8:
    end += 4u;
  l_int4:
    end += 2u;
  l_int2:
    end += 1u;
  l_int1:
    end += 1u;
  l_int:
    goto l_finish;
  // bytes and string structures
  l_byte4:
    size = 2u;
    end += (uint32_t)(start[1u]) << 24u;
    end += (uint32_t)(start[2u]) << 16u;
  l_byte2:
    size += 1u;
    end += (uint32_t)(start[size]) << 8u;
  l_byte1:
    size += 1u;
    end += start[size] + size;
    goto l_finish;
  l_byte:
    end += (start[0u] & 0x1fu);
    goto l_finish;

  // array and map structures
  l_array4:
    size = 2u;
    count += (uint32_t)(start[1u]) << 24u;
    count += (uint32_t)(start[2u]) << 16u;
  l_array2:
    size += 1u;
    count += (uint32_t)(start[size]) << 8u;
  l_array1:
    size += 1u;
    count += start[size];
    goto l_skip;
  l_array:
    count = (start[0u] & 0x1fu);
    goto l_skip;

  // skip fixed count of items in an array/map
  l_skip:

    if(count) {
      // double map for actual count
      if(start[0u] & 0x20u) count <<= 1;
      end = cb0r(start+size+1u,stop,count-1u,NULL);
    }else{
      end += size;
    }
    goto l_finish;

  // cross between l_int and l_array
  l_tag8:
    size = 4u;

  l_tag4:
    size += 2u;

  l_tag2:
    size += 1u;

  l_tag1:
    size += 1u;

  l_tag:
    // tag is like an array of 1, just grabs next item
    end = cb0r(start+size+1u,stop,0u,NULL);
    goto l_finish;

  // indefinite length wrapper
  l_until:
    count = CB0R_STREAM;
    end = cb0r(start+1u,stop,count,NULL);
    goto l_finish;

  l_break: {
    if(skip == CB0R_STREAM) return end;
    goto l_eparse;
  }

  l_ebad:
    cbType = CB0R_EBAD;
    goto l_fail;

  l_eparse:
    cbType = CB0R_EPARSE;
    goto l_fail;

  l_ebig:
    cbType = CB0R_EBIG;
    goto l_fail;

  l_fail: // all errors
    skip = 0u;

  l_finish: // only first 7 types
    cbType = ((start[0u] >> 5u) & UINT8_MAX);

  // done done, extract value if result requested
  if(!skip)
  {
    if(!result) return end;
    result->start = start;
    result->end = end;
    result->type = cbType;
    result->value = 0u;
    switch(cbType)
    {
      case CB0R_INT:
      case CB0R_NEG:
        size = end - (start + 1);
      case CB0R_TAG: {
        switch(size)
        {
          case 8u:
            result->value |= (uint64_t)(start[size - 7u]) << 56u;
            result->value |= (uint64_t)(start[size - 6u]) << 48u;
            result->value |= (uint64_t)(start[size - 5u]) << 40u;
            result->value |= (uint64_t)(start[size - 4u]) << 32u;

          case 4u:
            result->value |= (uint32_t)(start[size - 3u]) << 24u;
            result->value |= (uint32_t)(start[size - 2u]) << 16u;

          case 2u:
            result->value |= (uint32_t)(start[size - 1u]) << 8u;

          case 1u:
            result->value |= start[size];
            break;

          case 0u:
            result->value = start[0u] & 0x1fu;
            if(cbType == CB0R_TAG) switch(result->value)
            {
              case 0u: result->type = CB0R_DATETIME; break;
              case 1u: result->type = CB0R_EPOCH; break;
              case 2u: result->type = CB0R_BIGNUM; break;
              case 3u: result->type = CB0R_BIGNEG; break;
              case 4u: result->type = CB0R_FRACTION; break;
              case 5u: result->type = CB0R_BIGFLOAT; break;
              case 21u: result->type = CB0R_BASE64URL; break;
              case 22u: result->type = CB0R_BASE64; break;
              case 23u: result->type = CB0R_HEX; break;
              case 24u: result->type = CB0R_DATA; break;
            }
        }
      } break;

      case CB0R_BYTE:
      case CB0R_UTF8: {
        if(count == CB0R_STREAM) result->count = count;
        else result->length = end - (start + 1u);
      } break;

      case CB0R_ARRAY:
      case CB0R_MAP: {
        result->count = count;
      } break;


      case CB0R_SIMPLE: {
        result->value = (start[0u] & 0x1fu);
        switch(result->value)
        {
          case 20u: result->type = CB0R_FALSE; break;
          case 21u: result->type = CB0R_TRUE; break;
          case 22u: result->type = CB0R_NULL; break;
          case 23u: result->type = CB0R_UNDEF; break;
          case 24u:
            if(start[1u] >= 32u) result->value = start[1u];
            else result->type = CB0R_EBAD;
            break;
          case 25u:
            result->type = CB0R_FLOAT;
            result->length = 2u;
            break;

          case 26u:
            result->type = CB0R_FLOAT;
            result->length = 4u;
            break;

          case 27u:
            result->type = CB0R_FLOAT;
            result->length = 8u;
            break;
        }
      } break;

      default: {
        if(result->type < CB0R_ERR) result->type = CB0R_ERR;
      }
    }
    result->header = size + 1u;
    return end;
  }

  // max means indefinite mode skip
  if(skip != CB0R_STREAM) skip--;
  else if(result) result->count++;

  // tail recurse while skipping to not stack bloat
  return cb0r(end, stop, skip, result);
}

// safer high-level wrapper to read raw CBOR
bool cb0r_read(uint8_t *in, uint32_t len, cb0r_t result)
{
  if(!in || !len || !result) return false;
  cb0r(in, in+len, 0u, result);
  if(result->type >= CB0R_ERR) return false;
  return true;
}



// fetch a given item from an array (or map), 0 index
bool cb0r_get(cb0r_t array, uint32_t index, cb0r_t result)
{
  if(!array || !result) return false;
  if(array->type != CB0R_ARRAY && array->type != CB0R_MAP) return false;
  cb0r(array->start+array->header, array->end, index, result);
  if(result->type >= CB0R_ERR) return false;
  return true;
}

// get the value of a given key from a map, number/bytes only used for some types
bool cb0r_find(cb0r_t map, cb0r_e type, uint64_t number, uint8_t *bytes, cb0r_t result)
{
  uint32_t i;
  if(!map || !result) return false;
  if(map->type != CB0R_MAP) return false;

  for(i = 0u; i < map->length * 2u; i += 2u) 
  {
    if(!cb0r_get(map, i, result)) return false;
    if(result->type != type) continue;
    // either number compare or number+bytes compare
    switch(type) {
      case CB0R_INT:

      case CB0R_NEG:

      case CB0R_SIMPLE:

      case CB0R_DATETIME:

      case CB0R_EPOCH:

      case CB0R_BIGNUM:

      case CB0R_BIGNEG:

      case CB0R_FRACTION:

      case CB0R_BIGFLOAT:

      case CB0R_BASE64URL:

      case CB0R_BASE64:

      case CB0R_HEX:

      case CB0R_DATA:

      case CB0R_FALSE:

      case CB0R_TRUE:

      case CB0R_NULL:

      case CB0R_UNDEF:
        if(number == result->value) break;
        continue;

      case CB0R_BYTE:

      case CB0R_UTF8:

      case CB0R_FLOAT:
        // compare value by given length
        if(number == result->length && memcmp(bytes, result->start + result->header, number) == 0u) break;
        continue;

      case CB0R_MAP:

      case CB0R_ARRAY:

      case CB0R_TAG:
        // compare value by parsed byte length
        if(number == (uint64_t)(result->end - (result->start + result->header)) && memcmp(bytes, result->start + result->header, number) == 0u) break;
        continue;

      default:
        continue;
    }

    // key matched
    if(!cb0r_get(map, i+1u, result)) return false;
    return true;
  }

  return false;
}

// simple wrappers to return start and length
uint8_t *cb0r_value(cb0r_t dataV)
{
  if(!dataV) return NULL;
  return dataV->start + dataV->header;
}

uint32_t cb0r_vlen(cb0r_t dataV)
{
  if(!dataV) return 0u;
  return dataV->end - cb0r_value(dataV);
}

// defined everywhere but OSX, copied from https://gist.github.com/yinyin/2027912
#ifdef __APPLE__
#include <libkern/OSByteOrder.h>
#define htobe16(x) OSSwapHostToBigInt16(x)
#define htole16(x) OSSwapHostToLittleInt16(x)
#define be16toh(x) OSSwapBigToHostInt16(x)
#define le16toh(x) OSSwapLittleToHostInt16(x)
#define htobe32(x) OSSwapHostToBigInt32(x)
#define htole32(x) OSSwapHostToLittleInt32(x)
#define be32toh(x) OSSwapBigToHostInt32(x)
#define le32toh(x) OSSwapLittleToHostInt32(x)
#define htobe64(x) OSSwapHostToBigInt64(x)
#define htole64(x) OSSwapHostToLittleInt64(x)
#define be64toh(x) OSSwapBigToHostInt64(x)
#define le64toh(x) OSSwapLittleToHostInt64(x)
#else
//                                               #include "cpu_endian.h"
#endif /* __APPLE__ */

uint8_t cb0r_write(uint8_t *out, cb0r_e type, uint64_t number)
{
  if(type >= CB0R_ERR) return 0u;

  // built-in types
  switch(type) {

    case CB0R_DATETIME: number = 0u; break;

    case CB0R_EPOCH: number = 1u; break;

    case CB0R_BIGNUM: number = 2u; break;

    case CB0R_BIGNEG: number = 3u; break;

    case CB0R_FRACTION: number = 4u; break;

    case CB0R_BIGFLOAT: number = 5u; break;

    case CB0R_BASE64URL: number = 21u; break;

    case CB0R_BASE64: number = 22u; break;

    case CB0R_HEX: number = 23u; break;

    case CB0R_DATA: number = 24u; break;

    case CB0R_FALSE: number = 25u; break;

    case CB0R_TRUE: number = 20u; break;

    case CB0R_NULL: number = 21u; break;

    case CB0R_UNDEF: number = 22u; break;

    case CB0R_FLOAT: { // incoming number is size of float

      if(number == 2u) number = 25u;

      else if(number == 4u) number = 26u;

      else if(number == 8u) number = 27u;

      else return 0u;
    }

    default:;
  }

  out[0u] = type << 5u;

  if(number <= 23u) {
    out[0u] |= number;
    return 1u;
  }

  if(number >= UINT32_MAX) {
    out[0u] |= 27u;
    number = htobe64(number);
    memcpy(out + 1u, &number, 8u);
    out[9u] = '\0';                                                             // terminate the copy
    return 9u;
  }

  if(number > UINT16_MAX) {
    out[0u] |= 26u;
    number = htobe32(number);
    memcpy(out + 1u, &number, 4u);
    out[5u] = '\0';                                                             // terminate the copy
    return 5u;
  }

  if(number >= UINT8_MAX) {
    out[0u] |= 25u;
    number = htobe16(number);
    memcpy(out + 1u, &number, 2u);
    out[3u] = '\0';                                                             // terminate the copy
    return 3u;
  }

  out[0u] |= 24u;
  out[1u] = number;
  return 2u;
}