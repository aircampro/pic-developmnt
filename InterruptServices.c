//
//  Interrupt Code
//  Enabled timer5 (global counter every period = 100 ms)
//          UART2 to read serial for simpleBGC (csn be turned off using UART2_INTERUPT define)
//
#include <stdio.h>
#include "definitions.h"                                                        // Global defines for all
#include "Struts.h"                                                             // Common Structures
#include "gc_events.h"                                                          // Global variable and function definitions

#include "SBGC_COMMAND.h"                                                       // SimpleBGC commands
#include "SBGC_PARSER.h"
#include "SBGC_cmd_helpers.h"
#include "SBGC_adj_vars.h"

#include "lwNx.h"                                                               // liddar from lightware

// #define XS_CMD_REPLY_STX 0x2B                                                   // Encoder REplies with a + sign

//#define SSCANF_GSV(X, Y, Z)         { sscanf(X,"%d,%f", Y, Z); }              sscanf not allowed as parser for ascii in pic32 mikeroe C
//#include "ComGS.h"

#define  UART2_INTERUPT                                                         // Add this if you want to use the serial UART2 for SimpleSBGC messages as well
unsigned char timer_tmp1 = 0u;

//unsigned char          timer_tmp5_2 = 0;
#ifdef UART2_INTERUPT
unsigned char start =0u;
//unsigned char T2Lap =2;
//unsigned char T2LaoT =0;
//unsigned char T2LapEnable =0;

unsigned int typeMsgInvalid;                                                    // Set the type for the msg_id
unsigned int dataSize;                                                          // Length of the payload message used in check
unsigned int checkSumInHead;                                                    // Checksum in the message header
unsigned int checkSumInPayload;                                                 // Checksum for the message payload
unsigned int checkSumCalc;                                                      // Calculated checksum

unsigned char *ptr_buf = (unsigned char *) &UART2.Buffer;                       // Define the pointer to the container struct
#endif

#if defined(GPS_INCLUDED) || defined(GPS_INCLUDED2)                             // Things accociated with GPS interrupts
#define MAX_GPS_MSG_LEN 768U
unsigned char GPStxt[MAX_GPS_MSG_LEN];
volatile uint8_t g_GPS_ready;

/**
 * hex2int
 * take a hex string and convert it to a 32bit number (max 8 hex digits)
 * although the NMEA data is 16 bit checksum we allow for a max of 32 bit CRC
 * also take away 0* if used for 0x infront of digits (UBOX GPS)
 */
uint32_t hex2int(unsigned char *hex)
{
    uint32_t val = 0u;
    unsigned char byte;

    while (*hex)
    {
        byte = *hex++;                                                          // get current character in hex then increment
        if (!((byte == '0') || (byte == '*')))                                  // strip 0 or * from what we have if they are in the data
        {
          if (byte >= '0' && byte <= '9') byte = byte - '0';                    // transform hex character to the 4bit equivalent number, using the ascii table indexes
          else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
          else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
          val = (val << 4) | (byte & 0xF);                                      // shift 4 to make space for new digit, and add the 4 bits of the new digit
        }
    }
    return val;
}
#endif                                                                          // END OF GPS INCLUSION ------------------------------

#ifdef UART4_INTERUPT
unsigned char XSstart =0u;                                                      // STX found
unsigned int XStypeMsgInvalid;                                                  // Set the type for the msg_id
uint8_t XSlen;                                                                  // message length
#endif

#ifdef UART6_INTERUPT
#include "lwNx.h"
/*typedef struct {
                uint8_t dataVal[1024U];
                int32_t size;
                int32_t payloadSize;
                uint8_t parseState;
} lwResponsePacket; */

#endif

#ifdef TIMER1_NEEDED                                                            // This is used to increment Net_Ethernet_Intern_userTimerSec every second to prevent lock up
/*-----------------------------------------------------------------------------
 *      Timer1_Interrupt() :  his is used to increment Net_Ethernet_Intern_userTimer
 *  every second to prevent lock up
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Timer1_interrupt() iv IVT_TIMER_1 ilevel 7 ics ICS_AUTO {
  asm EI;                                                                       // Reenable global interrupts MikroE forum suggests needed for int priority
  timer_tmp1++ % UINT8_MAX;
  if(timer_tmp1 >= 5u)                                                          // 5 times 200 ms is every second
  {
    timer_tmp1 = CLEAR;
    Net_Ethernet_Intern_userTimerSec++ % UINT32_MAX;                            // Global ethernet counter ref : https://forum.mikroe.com/viewtopic.php?f=194&t=61238
  }
  TMR1     = CLEAR;
  T1IF_bit = CLEAR;                                                             // clear interrupt flag
}
#endif

#ifdef TIMER2_NEEDED                                                            // Disabled UDP ACK check to here atm
/*-----------------------------------------------------------------------------
 *      Timer2_3Interrupt() :  Timer 2
 *
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Timer2_3Interrupt() iv IVT_TIMER_3 ilevel 6 ics ICS_AUTO{

  asm EI;                                                                       //Reenable global interrupts
  T3IF_bit      = CLEAR;
  TMR2          = CLEAR;
  TMR3          = CLEAR;
  //LATD.B2 = ~ PORTD.B2;
  switch (SBGC_DATA.State)
  {

   case  UART2_CHECK_ACK_TIME_OUT :
         {
            T2CON = DISABLE_T2;
            SBGC_DATA.UDP_Send_Slow_ACK++ % UINT8_MAX;
            SBGC_DATA.UDP_Send_Slow_ACK_Timeout++ % UINT8_MAX;
            if(SBGC_DATA.UDP_Send_Slow_ACK_Timeout >=8u)
            {
              SBGC_DATA.State = UART2_ACK_TIME_OUT;
              SBGC_DATA.UDP_Send_Slow_ACK_Timeout=CLEAR;
            }
            break;
         }
   default:
         {
            T2CON = DISABLE_T2;
         }
  }

}
#endif

/*-----------------------------------------------------------------------------
 *      Timer_5Interrupt() :  Timer 5 runs to provide us with a tick counter which
 *  can be used for checking time e.g. debounce
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void Timer_5Interrupt() iv IVT_TIMER_5 ilevel 7 ics ICS_AUTO
{
  asm EI;                                                                       //Reenable global interrupts
  g_timer5_counter=++g_timer5_counter % UINT64_MAX;                             // Global Timer counter to be used for ms elapsed

  TMR5 = CLEAR;
  T5IF_bit         = 0u;                                                        // Release the timer
}

#ifdef UART2_INTERUPT                                                           // This is set if we are enabling UART2 with SimpleBGC serial
// ======================= SimpleBGC ===========================================
//
/*-----------------------------------------------------------------------------
 *      UART2_interrupt() :  This interrupts on chars that are recieved at UART2 serial port
 *  It will process these if they are supported SimpleBGC commands
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void UART2_interrupt() iv IVT_UART_2 ilevel 6 ics ICS_AUTO
{

   asm EI;                                                                      // Reenable global interrupts
   UART2.Bytes_In++ % UINT16_MAX;                                               // Global counter on Bytes arriving at UART2
        
   if(U2STA &0x0Eu | U2EIF_Bit)                                                 // Has bit 1 of the U2STA in the UART2 (the buffer overrun indicator) been set ?
   {
      T2CON = DISABLE_T2;
     /*while(URXDA_U2STA_bit)
     {
      dump= U2RXREG;
     }*/
      if (OERR_U2STA_bit==1u)                                                   // Overrun error (bit 1)
      {
        OERR_U2STA_bit = 0u;
        UART2.Buffer_Overrun++ % UINT8_MAX;                                     // Count overruns
      }
      else if (FERR_U2STA_bit==1u)                                              // Framing error (bit 2)
        FERR_U2STA_bit = 0u;
      else if (PERR_U2STA_bit==1u)                                              // Parity error (bit 3)
        PERR_U2STA_bit = 0u;
        
      UART2.Index =0u;                                                          // Set the index to zero
      U2STA &= 0xFFFFFFFDUL;                                                    // Clear the Error Status
      U2EIF_Bit =CLEAR;                                                         // Clear the Error IF
      Status(U2STA_Add, UART2_BUFFER_OVERFLOW);                                 // Report the error
      U2RXIF_bit=CLEAR;                                                         // Clear the UART2 IF
   }

   if (U2RXIF_Bit)
   {
      if ((!start) || typeMsgInvalid)                                           // if we dont already have a STX start byte or we got an invalid msg_id or checksum
      {
          UART2.Buffer[0u] = U2RXREG;
          if(UART2.Buffer[0u] == g_SBGC_Stx_Char)                               // Look for the simpleBGC start transmission
          {
             //T2CON = ENABLE_T2;       // Enable Timeout
             UART2.Index=1;
             start =TRUE;
             typeMsgInvalid=0u;                                                 // Set the message type to valid unless we dont get a valid type
             UART2.State = UART2_BYTE_IN_BUFFER;
          }
      }
      else
      {
         while(URXDA_U2STA_bit)                                                 // if there is a char to receive
         {
            UART2.Buffer[UART2.Index] = U2RXREG;                                // put the char from the UART into the buffer
            switch(UART2.Index)                                                 // For the byte position in the incoming message
            {
               case 1:                                                          // Should be the message index
               switch(UART2.Buffer[1u])
               {
                 case SBGC_CMD_GET_ANGLES:                                      // command was a SBGC get Angles
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_AUTO_PID:                                        // command was a SBGC auto pid response
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_GET_ANGLES_EXT:                                  // Command get angles ext
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_REALTIME_DATA_3:                                 // command was a SBGC realtime data.
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_REALTIME_DATA_4:
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_CONFIRM:                                         // command was a SBGC confirm set the relevant request to stop sending once confirmed
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_ERROR:                                           // command was a SBGC error response
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_SET_ADJ_VARS_VAL:                                // command was a SBGC set adj vars val of 7 in response to our get adj vars val
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_READ_PARAMS_3:                                   // command was a pide auto tune or config read/write part1
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_BOARD_INFO:                                      // request for board firmware
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_SCRIPT_DEBUG:                                    // request for script running state
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 case SBGC_CMD_EVENT:                                           // requested event occurred
                 UART2.Index=++UART2.Index % UINT16_MAX;
                 break;
                 default:                                                       // Message Id was not recognised
                 typeMsgInvalid=1u;
                 UART2.Index=1u;                                                // Restart message collection
                 break;
               }
               break;

               case SBGC_PAYLOAD_LENGTH_POS:                                    // CASE 2 : Should be data size
               dataSize=UART2.Buffer[SBGC_PAYLOAD_LENGTH_POS];
               UART2.Index=++UART2.Index % UINT16_MAX;
               break;

               case 3u:                                                         // Should be checksum
               checkSumInHead=UART2.Buffer[3u];
               checkSumCalc=(UART2.Buffer[2u]+UART2.Buffer[1u])%256u;           // Calculate the checksum
               if (checkSumInHead == checkSumCalc)                              // message matches calculation header is good
               {
                  UART2.Index=++UART2.Index % UINT16_MAX;                       // go to collect the paylaod
               }
               else
               {
                  typeMsgInvalid=1u;
                  UART2.Index=1u;                                               // Restart message collection
               }
               break;

               default:                                                         // Should be payload - reset if you got snother STX or payload is too long
               //if (UART2.Buffer[UART2.Index] == SBGC_CMD_START_BYTE)            // another STX was found this is invalid go back to start
               //{
               //   UART2.Buffer[0]= UART2.Buffer[UART2.Index];                   // reset to the start of the message buffer.
               //   UART2.Index=1;                                                // reset to put the reset of the message from here
               //}
               //else                                                           ---- Commented out as may be allowed in simpleBGC message
                                                                                // re-enable if required

               if (UART2.Index >= (dataSize+1+SBGC_LEN_OF_HEADER))              // message too long - > expected
               {
                  typeMsgInvalid=1u;
                  UART2.Index=1u;                                               // Restart message collection
               }
               else if (UART2.Index >= SBGC_CMD_MAX_BYTES)                      // message too long - > max size
               {
                  typeMsgInvalid=1u;
                  UART2.Index=1u;                                               // Restart message collection
               }
               else
               {
                  UART2.Index=++UART2.Index % UINT16_MAX;                       // increment and fill the buffer with data from the payload
               }
               break;
            }
         }
      }

      if (UART2.Index == (dataSize+SBGC_LEN_OF_HEADER+1))                       // If we got the length of the full message
      {

        checkSumInPayload=UART2.Buffer[UART2.Index-1u];                         // Body Checksum
        UART2.Index=++UART2.Index % UINT16_MAX;
        start=FALSE;                                                            // Complete message collected start again

        checkSumCalc = Pay_checksum((ptr_buf+SBGC_LEN_OF_HEADER),(sizeof(UART2.Buffer)-SBGC_LEN_OF_HEADER));
        //checkSumCalc = Pay_checksum(ptr_buf,55);
         if (checkSumCalc == checkSumInPayload)                                 // Checksum matches what is expected then message integrity is good
         {
            switch(UART2.Buffer[1u])                                            // For each message id copy into the right data location
            {
               case SBGC_CMD_GET_ANGLES:                                        // command was a SBGC get Angles
               memcpy(&getangles, &UART2.Buffer[0u], sizeof(getangles));        // Copy the payload to the get angles readback message container.
               break;
               case SBGC_CMD_AUTO_PID:                                          // command was a SBGC auto pid response
               memcpy(&pidreadbk, &UART2.Buffer[0u], sizeof(pidreadbk));        // Copy the payload to the pid readback message container.
               break;
               case SBGC_CMD_GET_ANGLES_EXT:                                    // Command get angles ext
               memcpy(&getanglesext, &UART2.Buffer[0u], sizeof(getanglesext));  // Copy the payload to the get ext angles readback message container.
               break;
               case SBGC_CMD_REALTIME_DATA_3:                                   // command was a SBGC realtime data.
               memcpy(&realdata3, &UART2.Buffer[0u], sizeof(realdata3));        // Copy the payload to the realtime data 3 readback message container.
               break;
               case SBGC_CMD_REALTIME_DATA_4:
               memcpy(&realdata4, &UART2.Buffer[0u], sizeof(realdata4));        // Copy the payload to the realtime data 4 readback message container.
               break;
               case SBGC_CMD_CONFIRM:                                           // command was a SBGC confirm
               memcpy(&cmdconf, &UART2.Buffer[0u], sizeof(cmdconf));            // Copy the payload to the command confirmation readback message container.
               break;
               case SBGC_CMD_ERROR:                                             // command was a SBGC error response
               memcpy(&cmderror, &UART2.Buffer[0u], sizeof(cmderror));          // Copy the payload to the command confirmation readback message container.
               break;
               case SBGC_CMD_SET_ADJ_VARS_VAL:                                  // command was a SBGC set adj vars val of 7 in response to our get adj vars val
               memcpy(&getvar_rd, &UART2.Buffer[0u], sizeof(getvar_rd));        // Copy the payload to the command confirmation readback message container.
               break;
               case SBGC_CMD_BOARD_INFO:                                        // command was request for information on the firmware/state ofthe board
               memcpy(&boardinforb, &UART2.Buffer[0u], sizeof(boardinforb));
               break;
               case SBGC_CMD_READ_PARAMS_3:                                     // command was a pide auto tune or config read/write part1
               memcpy(&readparam3, &UART2.Buffer[0u], sizeof(readparam3));
               break;
               case SBGC_CMD_RESET:                                             // Reset issued feedback response
               break;
               case SBGC_CMD_I2C_READ_REG_BUF:                                  // i2c read either EEPROM or IMU
               break;
               case SBGC_CMD_READ_EXTERNAL_DATA:
               break;
               case SBGC_CMD_SCRIPT_DEBUG:                                      // start / stop script feedback
               memcpy(&scriptstate, &UART2.Buffer[0u], sizeof(scriptstate));
               break;
               case SBGC_CMD_EVENT:                                             // motor state event
               memcpy(&eventCmd, &UART2.Buffer[0u], sizeof(eventCmd));          // copy the data to the object container
               break;
               case SBGC_CMD_REALTIME_DATA_CUSTOM:                              // realtime data custom frame was sent
               memcpy(&realtimedatacust, &UART2.Buffer[0u], sizeof(realtimedatacust));
               default:                                                         // Message Id was not recognised
               typeMsgInvalid=1u;
               UART2.Index=1u;                                                  // Restart message collection
               break;
            }
            if (!typeMsgInvalid)                                                // Message is good and supported
            {
               UART2.State = UART2_PACKET_IN_BUFFER;                            // Declare good packet to top level
               UART2.Msg_id_recv = UART2.Buffer[1u];                            // Put the message id into the field
            }
      }
    }
  }  // End U2RXIFBIT
  U2RXIF_bit=CLEAR;

} // End Interrupt
#endif

/*-----------------------------------------------------------------------------
 *      interruptGPS() :  This interrupts on chars that are recieved at UART3 serial port
 *  It will process these if they are supported Nmea or GPS commands
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
#ifdef GPS_INCLUDED1                                                            // also ensure you init the uart3 interrupt
void interruptGPS() iv IVT_UART_3 ilevel 3 ics ICS_AUTO
{
  uint8_t i;
  asm EI;                                                                       //Reenable global interrupts
  GPStxt[i] = UART3_Read();                                                     // This was from the ducati GPS example
  i++ % UINT8_MAX;
  if (i == MAX_GPS_MSG_LEN)
  {
    i = 0u;
    g_GPS_ready = 1;
  }
  U3RXIF_bit=CLEAR;                                                             // Clear interrupt as it was read
}
#endif                                                                          // END OF GPS INCLUSION -----------------------------------
#ifdef GPS_INCLUDED2                                                            // also ensure you init the uart3 interrupt
void interruptGPS() iv IVT_UART_3 ilevel 3 ics ICS_AUTO
{
  uint8_t i;                                                                    // counter for characture number in buffer
  uint8_t msgIdx;                                                               // message index
  unsigned char msgId[4u];                                                      // characture holding the message id
  uint8_t msgIdPUBX;                                                            // $PUBX message Id number (defines the rest of the fields)
  uint32_t timeGPSPUBX;                                                         // GPS time
  unsigned char timeGPS[15u];                                                   // characture holding the ascii time from the PUBX string message
  float32_t latGPSPUBX;                                                         // latatitude
  unsigned char latGPS[15u];
  unsigned char nsGPS[2u];                                                      // north / south direction
  float32_t longGPSPUBX;                                                        // longditiude
  unsigned char longGPS[15u];
  unsigned char ewGPS[2u];                                                      // east west direction
  float32_t altRefGPSPUBX;                                                      // altitude
  unsigned char altRefGPS[10u];

  float32_t hAccGPSPUBX;
  unsigned char hAccGPS[15u];                                                   // horizontal accuracy
  float32_t vAccGPSPUBX;
  unsigned char vAccGPS[15u];                                                   // vertical accuracy

  float32_t SOGGPSPUBX;                                                         // speed over ground knots
  unsigned char SOGGPS[15u];
  float32_t SOGKGPSPUBX;                                                        // speed over ground kmh
  unsigned char SOGKGPS[15u];
  float32_t COGGPSPUBX;                                                         // course over ground
  unsigned char COGGPS[15u];
  float32_t vVelGPSPUBX;                                                        // vertical velocity
  unsigned char  vVelGPS[15u];
  float32_t diffAgeGPSPUBX;                                                     // age of differential corrections
  unsigned char  diffAgeGPS[15u];
  float32_t HDOPGPSPUBX;                                                        // horizontal dilution of precision
   unsigned char HDOPGPS[15u];
  float32_t VDOPGPSPUBX;                                                        // vertical dilution of precision
  unsigned char  VDOPGPS[15u];
  float32_t TDOPGPSPUBX;                                                        // time of dilution of precision
  unsigned char  TDOPGPS[15u];
  float32_t numSvsGPSPUBX;                                                      // number of satelites being used in this solution
  unsigned char  numSvsGPS[15u];
  float32_t DRGPSPUBX;                                                          // DR used (dead reckoning) compensation from gyro etc when satelite lost
  unsigned char DRGPS[15u];
  uint16_t csGPSPUBX;                                                           // checksum
  unsigned char csGPS[15u];

  unsigned char reservedGPS[5u];                                                // reserved currently set 0

  unsigned char rmcStat[1u];                                                    // single char reads of Status or ununsed
  unsigned char dateStr1[12u];
  unsigned char magVarStr[15u];
  unsigned char magVarEw[1u];
  unsigned char posMode[1u];
  unsigned char navStatus[1u];
  unsigned char charRead[1u];

  unsigned char numSatel[3u];                                                   // string for number of satelites
  uint8_t numOfSatel;                                                           // number of satelites being used for solution
  unsigned char numSatelinSight[3u];                                            // string for satelites in sight
  unsigned char elevSatel[3u];                                                  // string for satelite elevation
  unsigned char azimuthSatel[4u];                                               // string for satelite azimuth
  unsigned char sigStrengthSatel[3u];                                           // string for satelite signal strength
  unsigned char signalID[3u];                                                   // string for signal id
  unsigned char satNumber[3u];                                                  // string containing satelite number

  uint8_t charIdx;                                                              // character index

  uint16_t checkCalc;                                                           // online checksum calculation

  // cant use sscanf ==== unsigned char gnsBuffer[MAX_GPS_MSG_LEN];             // buffer for full GNS type message  (demonstrates different method)
  unsigned char seaLevelAlt[20u];
  unsigned char geoHt[20u];
  float32_t SealevelAltitude;                                                   // Sea level altitude meters
  float32_t GeoidalHt;                                                          // Geoidal height

   asm EI;                                                                      // Reenable global interrupts
   if((U3STA & 0x0E) | U3EIF_Bit)                                               // Error check with UART3 buffer ???
   {
      if (OERR_U3STA_bit==1u)                                                   // Overrun error (bit 1)
        OERR_U3STA_bit = 0u;
      if (FERR_U3STA_bit==1u)                                                   // Framing error (bit 2)
        FERR_U3STA_bit = 0u;
      if (PERR_U3STA_bit==1u)                                                   // Parity error (bit 3)
        PERR_U3STA_bit = 0u;

      U3STA &= 0xFFFFFFF1ul;                                                    //Clear the Error Status 0001 no longer 1101 (D) just the overrun
      U3EIF_Bit =CLEAR;                                                         //Clear the Error IF
      U3RXIF_bit=CLEAR;                                                         //Clear the UART3 IF
   }
   
  if (((i <= MAX_GPS_MSG_LEN) && (msgIdx!=255u)) && (U3RXIF_bit))               // Cant continue reading beyond length of string if we errored just look for $ again
  {
     if (UART3_Data_ready())                                                    // Ready the receieve ?
     {
        GPStxt[i] = UART3_Read();                                               // Read the char from UART 3
        switch (GPStxt[i])
        {
//================= STX ========================================================
        case '$':                                                               // Decimal 36 is a $ (STX for the PUBX message
          msgIdx=1u;                                                            // start of message found
          checkCalc=0u;                                                         // reset checksum calculation
          g_GPS_ready=0u;                                                       // inform top level new collection taking place
          break;
//================ Message Mnemonics ===========================================
        case 'P':                                                               // next char found  dec 80  for PUBX
          if (msgIdx==1u)
           msgIdx=2u;
          break;
        case 'U':                                                               // next char found  dec 85  for PUBX
          if (msgIdx==2u)
           msgIdx=3u;
         break;
        case 'B':                                                               // next char found   dec 66  for PUBX
          if (msgIdx==3u)
           msgIdx=4u;
        break;
        case 'X':                                                               // next char found  dec 88  for PUBX
          if (msgIdx==4u)
           msgIdx=5u;                                                           // ===== START PUBX ================
        break;
        case 'R':                                                               // we are looking for $xxRMC Recommended Minimum data
          if (msgIdx<=3u)                                                       // allow for finding a $PURMC should be $GPRMC
           msgIdx=40u;
        break;
        case 'M':
          if (msgIdx==40u)
           msgIdx=41u;
         break;
        case 'C':
          if (msgIdx==41u)
           msgIdx=42u;                                                          // start you're ===RMC=== parsing from step 42 (Recommended Minimum data) also furano
          break;
        case 'G':                                                               // we are looking for $xxGNS Recommended Minimum data
          if (msgIdx<=3u)                                                       // allow for finding a $PUGNS should be $GPRMC
            msgIdx=60u;
          if (msgIdx==81u)
            msgIdx=82u;                                                         // start you're ===VTG=== parsing from step 82 Course over ground and Ground speed  also furano
        break;
        case 'N':
          if (msgIdx==60u)
            msgIdx=61u;
        break;
        case 'S':
          if (msgIdx==60u)
            msgIdx=100u;
          if (msgIdx==61u)
            msgIdx=62u;                                                         // start you're ===GNS=== parsing from step 62 GNSS fix data also furano
        break;
        case 'V':                                                               // we are looking for $xxVTG Course over ground and Ground speed
          if (msgIdx<=3u)                                                       // allow for finding a $PUVTG should be $GPVTG
            msgIdx=80u;
          if (msgIdx<=100u)                                                     // saw ===$GSV=== also on furano
            msgIdx=101u;
        break;
        case 'T':
          if (msgIdx==80u)
            msgIdx=81u;
        break;
        case 'L':
          if (msgIdx==60u)
          {
             msgIdx=130u;
          }
          else if (msgIdx==130u)
          {
             msgIdx=131u;                                                       // start parsing ===GLL=== message Geographic Position Latitude/Longitude also for furano GPS
          }
        break;
//=================== * star (get checksum) ======================================
        case '*':                                                               // The checksum looks like it doesnt always get preceded by a comma so separate here
          if (msgIdx==72u)                                                      // =====GNS======= message
          {                                                                     // field 13 (skipped 2 blank fields) of the GNS message Navigation status
             navStatGPS[3u]='\0';                                               // terminate the string after 2 chars
             if (strcmp("NF",navStatGPS)==0u)                                   // navigation Status reported No Fix
             {
                strcpy(navStatGPS,"No Fix found\n");
                msgIdx=0u;                                                      // reset we got a failed message
             }
             else if (strcmp("DR",navStatGPS)==0u)                              // navigation Status reported dead reckoning only solution
             {
                strcpy(navStatGPS,"Dead reckoning only solution\n");
             }
             else if (strcmp("G2",navStatGPS)==0u)                              // navigation Status reported Stand alone 2D solution
             {
                strcpy(navStatGPS,"Stand alone 2D solution\n");
             }
             else if (strcmp("G3",navStatGPS)==0u)                              // navigation Status reported Stand alone 3D solution
             {
                strcpy(navStatGPS,"Stand alone 3D solution\n");
             }
             else if (strcmp("D2",navStatGPS)==0u)                              // navigation Status reported Differential 2D solution
             {
                strcpy(navStatGPS,"Differential 2D solution\n");
             }
             else if (strcmp("D3",navStatGPS)==0u)                              // navigation Status reported Differential 3D solution
             {
                strcpy(navStatGPS,"Differential 3D solution\n");
             }
             else if (strcmp("RK",navStatGPS)==0u)                              // navigation Status reported  Combined GPS + dead reckoning solution
             {
                strcpy(navStatGPS,"Combined GPS + dead reckoning solution \n");
             }
             else if (strcmp("TT",navStatGPS)==0u)                              // navigation Status reported  Time only solution
             {
                strcpy(navStatGPS," Time only solution\n");
             }
          }
          else if ((msgIdx==25u) && (msgIdPUBX==0u))                            // should be 21th comma and its a position message (dont think we get here we get a * rather than comma)
          {
             DRGPSPUBX=atof(DRGPS);                                             // convert the DR field from the message
          }
          else if (msgIdx==137u)                                                // last =====GLL====== field
          {
             switch(posMode[1u])
            {
                     case 'A':
                       strcpy(positionMode,"GNSS fix\n");
                     break;
                     case 'D':
                        strcpy(positionMode,"Differential GNSS fix\n");
                     break;
                     case 'N':
                        strcpy(positionMode,"No position fix\n");
                        msgIdx=0;                                               // reset we got a failed message
                     break;
                     default:
                       strcpy(positionMode,"Unknown fix\n");
                       msgIdx=0;                                                // reset we got a failed message
                     break;
            }
          }
          else if (msgIdx==91u)
          {
             switch(posMode[1u])
             {
                case 'A':
                  strcpy(positionMode,"GNSS fix\n");
                break;
                case 'D':
                  strcpy(positionMode,"Differential GNSS fix\n");
                break;
                case 'N':
                  strcpy(positionMode,"No position fix\n");
                  msgIdx=255u;                                                  // reset we got a failed message and wait for a $ start
                break;
                default:
                  strcpy(positionMode,"Unknown fix\n");
                  msgIdx=255u;                                                  // reset we got a failed message and wait for a $ start
                break;
             }
          }
          else if ((msgIdx >= 108u) && (msgIdx <= 120u))
          {
              signalIDData=(uint8_t) atoi(signalID);
              msgIdx=120u;                                                      // will set to 121 with the ++ later (means collect CRC)
          }
          charIdx=1u;                                                           // reset the read buffer to the read the first char into the first byte of next characture array
          msgIdx++ % UINT8_MAX;                                                 // increment to next message index collector slot
        break;
//====================================== Comma ===================================
        case ',':                                                               // Field separator has been found ======================== ,Comma, ==========
          if (!(((((((msgIdx >= 5u) && (msgIdx <= 26u)) || ((msgIdx >= 42u) && (msgIdx <= 56u))) || ((msgIdx >= 62u) && (msgIdx <= 73u))) || ((msgIdx >= 101u) && (msgIdx <= 121u))) || ((msgIdx >= 131u) && (msgIdx <= 138u))) || ((msgIdx >= 82u) && (msgIdx <= 92u))))
          {
             msgIdx=255u;                                                       // go to dormant wait mode for another STX $ as its a message we dont yet support
          }
          else if (msgIdx==5u)                                                  // should be first comma
          {
             msgIdx=6u;                                                         // ====PUBX==== now collect the msgId filed (we dont use)
             charIdx=1u;
          }
          else if (msgIdx==6u)                                                  // should be 2nd comma then convert the collected data
          {
             msgIdPUBX=atoi(msgId);                                             // convert the ascii string received to the message identifier used to read the fields
             msgIdx=7u;                                                         // now collect the msgId filed (we dont use)
             charIdx=1u;
          }
          else if ((msgIdx==7u) && (msgIdPUBX==0u))                             // should be 3rd comma and its a position message
          {
             timeGPSPUBX=atoi(timeGPS);                                         // convert the time from the message
             msgIdx=8u;                                                         // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==8u) && (msgIdPUBX==0u))                             // should be 4th comma and its a position message
          {
             latGPSPUBX=atof(latGPS);                                           // convert the latitude field from the message
             msgIdx=9u;                                                         // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==9u) && (msgIdPUBX==0u))                             // should be 5th comma and its a position message
          {
             if(nsGPS[1u] == 'S')                                               // if the latitude is in the South direction it has minus sign
             {
                latGPSPUBX = 0u - latGPSPUBX;                                   // invert the latitude direction
             }
             msgIdx=10u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==10u) && (msgIdPUBX==0u))                            // should be 6th comma and its a position message
          {
             longGPSPUBX=atof(longGPS);                                         // convert the longditude field from the message
             msgIdx=11u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==11u) && (msgIdPUBX==0u))                            // should be 7th comma and its a position message
          {
             if(ewGPS[1u] == 'W')                                               // if the longitude is in the West direction it has minus sign
             {
                longGPSPUBX = 0u - longGPSPUBX;                                 // invert
             }
             msgIdx=12u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==12u) && (msgIdPUBX==0u))                            // should be 8th comma and its a position message
          {
             altRefGPSPUBX=atof(altRefGPS);                                     // convert the Altitude above user datum ellipsoid field from the message
             msgIdx=13u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==13u) && (msgIdPUBX==0u))                            // should be 9th comma and its a position message
          {
             navStatGPS[3u]='\0';                                               // terminate the string after 2 chars
             if (strcmp("NF",navStatGPS)==0u)                                   // navigation Status reported No Fix
             {
                strcpy(navStatGPS,"No Fix found\n");
                msgIdx=0u;                                                      // reset we got a failed message
             }
             else if (strcmp("DR",navStatGPS)==0u)                              // navigation Status reported dead reckoning only solution
             {
                strcpy(navStatGPS,"Dead reckoning only solution\n");
             }
             else if (strcmp("G2",navStatGPS)==0u)                              // navigation Status reported Stand alone 2D solution
             {
                strcpy(navStatGPS,"Stand alone 2D solution\n");
             }
             else if (strcmp("G3",navStatGPS)==0u)                              // navigation Status reported Stand alone 3D solution
             {
                strcpy(navStatGPS,"Stand alone 3D solution\n");
             }
             else if (strcmp("D2",navStatGPS)==0u)                              // navigation Status reported Differential 2D solution
             {
                strcpy(navStatGPS,"Differential 2D solution\n");
             }
             else if (strcmp("D3",navStatGPS)==0u)                              // navigation Status reported Differential 3D solution
             {
                strcpy(navStatGPS,"Differential 3D solution\n");
             }
             else if (strcmp("RK",navStatGPS)==0u)                              // navigation Status reported  Combined GPS + dead reckoning solution
             {
                strcpy(navStatGPS,"Combined GPS + dead reckoning solution \n");
             }
             else if (strcmp("TT",navStatGPS)==0u)                              // navigation Status reported  Time only solution
             {
                strcpy(navStatGPS," Time only solution\n");
             }
             charIdx=1;
          }
          else if ((msgIdx==14) && (msgIdPUBX==0u))                             // should be 10th comma and its a position message
          {
             hAccGPSPUBX=atof(hAccGPS);                                         // convert the Horizontal accuracy estimate.
             msgIdx=15u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==15u) && (msgIdPUBX==0u))                            // should be 11th comma and its a position message
          {
             vAccGPSPUBX=atof(vAccGPS);                                         // convert the Vertical accuracy estimate
             msgIdx=16u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==16u) && (msgIdPUBX==0u))                            // should be 12th comma and its a position message
          {
             SOGGPSPUBX=atof(SOGGPS);                                           // convert the speed over ground
             msgIdx=17u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==17u) && (msgIdPUBX==0u))                            // should be 13th comma and its a position message
          {
             COGGPSPUBX=atof(COGGPS);                                           // convert the course over ground field from the message
             msgIdx=18u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==18u) && (msgIdPUBX==0u))                            // should be 14th comma and its a position message
          {
             vVelGPSPUBX=atof(vVelGPS);                                         // convert the vertical velocity field from the message
             msgIdx=19u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==19u) && (msgIdPUBX==0u))                            // should be 15th comma and its a position message
          {
             diffAgeGPSPUBX=atof(diffAgeGPS);                                   // convert the Age of differential corrections field from the message
             msgIdx=20u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==20u) && (msgIdPUBX==0u))                            // should be 16th comma and its a position message
          {
             HDOPGPSPUBX=atof(HDOPGPS);                                         // convert the Horizontal Dilution of Precision  field from the message
             msgIdx=21u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==21u) && (msgIdPUBX==0u))                            // should be 17th comma and its a position message
          {
             VDOPGPSPUBX=atof(VDOPGPS);                                         // convert the Vertical Dilution of Precisio field from the message
             msgIdx=22u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==22u) && (msgIdPUBX==0u))                            // should be 18th comma and its a position message
          {
             TDOPGPSPUBX=atof(TDOPGPS);                                         // convert the  Time Dilution of Precision field from the message
             msgIdx=23u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==23u) && (msgIdPUBX==0u))                            // should be 19th comma and its a position message
          {
             numSvsGPSPUBX=atof(numSvsGPS);                                     // convert the Number of satellites used in the navigation solution  field from the message
             msgIdx=24u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==24u) && (msgIdPUBX==0u))                            // should be 20th comma and its a position message
          {
           // reerved field for future                                          // convert the ???? field from the message
             msgIdx=25u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx==25u) && (msgIdPUBX==0u))                            // should be 21th comma and its a position message (dont think we get here we get a * rather than comma)
          {
             DRGPSPUBX=atof(DRGPS);                                             // convert the DR field from the message
             msgIdx=26u;                                                        // now collect the next field
             charIdx=1u;
          }
          else if ((msgIdx>=42u) || (msgIdx==56u))                              // Start of an ===RMC==== message type to End comma means next field response to :: $xxGLQ,msgId*cs<CR><LF>  $EIGLQ,RMC*3A<CR><LF>
          {
             switch(msgIdx)                                                     // For Each field in the RMC message string parse and calculate data values
             {
                case 43u:                                                       // field is time
                   timeGPSPUBX=atoi(timeGPS);
                   break;
                case 44u:                                                       // Status field for RMC message A=ok V=error
                   if (rmcStat[1u]=='V')                                        // invalid data then reset the message collection
                   {
                      i=1u;                                                     // break from collection if Status invalid
                      msgIdx=0u;
                   }
                   break;
                case 45u:
                   latGPSPUBX=atof(latGPS);
                   break;
                case 46u:
                   if(nsGPS[1] == 'S')                                          // if the latitude is in the South direction it has minus sign
                   {
                     latGPSPUBX = 0u - latGPSPUBX;                              // invert the latitude direction
                   }
                   break;
                case 47u:
                   longGPSPUBX=atof(longGPS);
                   break;
                case 48u:
                   if(ewGPS[1] == 'W')                                          // if the longitude is in the West direction it has minus sign
                   {
                      longGPSPUBX = 0u - longGPSPUBX;                           // invert
                   }
                   break;
                case 49u:
                   SOGGPSPUBX=atof(SOGGPS);                                     // speed in knots
                   break;
                case 50u:
                   COGGPSPUBX=atof(COGGPS);                                     // true course
                   break;
                 case 54u:                                                       // position mode satelite
                    switch(posMode[1])
                    {
                        case 'A':
                           strcpy(positionMode,"GNSS fix\n");
                           break;
                        case 'D':
                           strcpy(positionMode,"Differential GNSS fix\n");
                           break;
                        case 'N':
                           strcpy(positionMode,"No position fix\n");
                           msgIdx=0u;                                           // reset we got a failed message
                           break;
                        default:
                           strcpy(positionMode,"Unknown fix\n");
                           msgIdx=0u;                                           // reset we got a failed message
                           break;
                     }
                    break;
                 case 55u:                                                      // navigation Status is always V so ignore will increment to last field CRC after this
                   break;
                 case 56u:                                                      // checksum  (shouldnt collect as it doesnt have a comma separator should do on <CR>
                   csGPSPUBX=((uint16_t)hex2int(csGPS));
                   break;
                 default:
                   break;
             }
             msgIdx++ % UINT8_MAX;                                              // go to the next field
             charIdx=1u;                                                        // reset to start at the begining as we got a comma
        }
        else if ((msgIdx>=131u) || (msgIdx==137u))                              // Start of an ===GLL==== message type to End comma means next field response to
        {
           switch(msgIdx)                                                       // For Each field in the RMC message string parse and calculate data values
           {
              case 135u:                                                        // field is time
                timeGPSPUBX=atoi(timeGPS);
              break;
              case 136u:                                                        // Status field for GLL message A=ok V=error
                if (rmcStat[1u]=='V')                                           // invalid data then reset the message collection
                {
                   i=1u;                                                        // break from collection if Status invalid
                   msgIdx=255u;
                }
              break;
              case 131u:
                 latGPSPUBX=atof(latGPS);
              break;
              case 132u:
                 if(nsGPS[1] == 'S')                                            // if the latitude is in the South direction it has minus sign
                 {
                    latGPSPUBX = 0u - latGPSPUBX;                               // invert the latitude direction
                 }
              break;
              case 133u:
                 longGPSPUBX=atof(longGPS);
              break;
              case 134u:
                 if(ewGPS[1u] == 'W')                                            // if the longitude is in the West direction it has minus sign
                 {
                    longGPSPUBX = 0u - longGPSPUBX;                              // invert
                 }
              break;
              case 137u:                                                         // position mode satelite
                  switch(posMode[1u])
                  {
                     case 'A':
                       strcpy(positionMode,"GNSS fix\n");
                     break;
                     case 'D':
                        strcpy(positionMode,"Differential GNSS fix\n");
                     break;
                     case 'N':
                        strcpy(positionMode,"No position fix\n");
                        msgIdx=255u;                                            // reset we got a failed message
                     break;
                     default:
                       strcpy(positionMode,"Unknown fix\n");
                       msgIdx=255u;                                             // reset we got a failed message
                     break;
                  }
                  break;
              default:
              break;
            }
            msgIdx++ % UINT8_MAX;                                               // go to the next field
            charIdx=1u;                                                         // reset to start at the begining as we got a comma
        }
        else if ((msgIdx>=82u) || (msgIdx==91u))                                // Start of an ====VTG==== message type to End comma means next field response to
        {
           switch(msgIdx)                                                       // For Each field in the VTG message string parse and calculate data values
           {

                case 82u:
                   COGGPSPUBX=atof(COGGPS);                                     // true course
                   break;
                case 86u:
                   SOGGPSPUBX=atof(SOGGPS);                                     // speed knotts
                   break;
                case 88u:
                   SOGKGPSPUBX=atof(SOGKGPS);                                   // speed  kmh
                   break;
                case 91u:                                                       // position mode satelite (shouldnt reach as separated with * not ,
                  switch(posMode[1])
                  {
                     case 'A':
                       strcpy(positionMode,"GNSS fix\n");
                     break;
                     case 'D':
                        strcpy(positionMode,"Differential GNSS fix\n");
                     break;
                     case 'N':
                       strcpy(positionMode,"No position fix\n");
                       msgIdx=0u;                                               // reset we got a failed message
                     break;
                     default:
                       strcpy(positionMode,"Unknown fix\n");
                       msgIdx=0u;                                               // reset we got a failed message
                     break;
                  }
                  break;
               default:
                 break;
            }
            msgIdx++ % UINT8_MAX;                                               // go to the next field
            charIdx=1u;                                                         // reset to start at the begining as we got a comma
          }
          else if ((msgIdx>=101u) || (msgIdx==120u))                            // Start of an ====GSV===== message type to End comma means next field response to
          {
               switch(msgIdx)
               {
                  case 101u:
                    numOfSatel=(uint8_t) atoi(numSatel);                        // convert string with number of satelites to a value
                    msgIdx=102u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 102u:
                    msgIdx=103u;                                                // now collect the next field  (ignore the data)
                    charIdx=1u;
                    break;
                  case 103u:
                    numOfSatel=(uint8_t) atoi(numSatel);                        // convert string with number of satelites in sight to a value
                    msgIdx=104u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 104u:
                    satNumberData[1u]=(uint8_t) atoi(satNumber);                // convert string with the satelite being used number to a value
                    msgIdx=105u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 105u:
                    elevSatelData[1u]=(uint8_t) atoi(elevSatel);                // convert string with number of satelites to a value
                    msgIdx=106u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 106u:
                    azimuthSatelData[1u]=(uint8_t) atoi(azimuthSatel);          // convert string with number of satelites to a value
                    msgIdx=107u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 107u:
                    sigStrengthSatelData[1u]=(uint8_t) atoi(sigStrengthSatel);  // convert string with number of satelites to a value
                    msgIdx=108u;                                                // now collect the next field
                    charIdx=1u;
                    break;
                  case 108u:
                    if (numOfSatel >= 2u)
                    {
                       satNumberData[2u]=(uint8_t) atoi(satNumber);             // convert string with the satelite being used number to a value
                       msgIdx=109u;                                             // now collect the next field
                       charIdx=1u;
                    }
                    else
                    {
                       signalIDData=(uint8_t) atoi(signalID);
                    }
                    break;
                  case 109u:
                    if (numOfSatel >= 2u)
                    {
                       elevSatelData[2u]=(uint8_t) atoi(elevSatel);             // convert string with number of satelites to a value
                       msgIdx=110u;                                             // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 110u:
                    if (numOfSatel >= 2u)
                    {
                       azimuthSatelData[2u]=(uint8_t) atoi(azimuthSatel);       // convert string with number of satelites to a value
                       msgIdx=111u;                                             // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 111u:
                    if (numOfSatel >= 2u)
                    {
                       sigStrengthSatelData[2u]=(uint8_t) atoi(sigStrengthSatel);// convert string with number of satelites to a value
                       msgIdx=112u;                                             // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 112u:
                    if (numOfSatel >= 3u)
                    {
                       satNumberData[3u]=(uint8_t) atoi(satNumber);             // convert string with the satelite being used number to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    else
                    {
                       signalIDData=(uint8_t) atoi(signalID);
                    }
                    break;
                  case 113u:
                    if (numOfSatel >= 3u)
                    {
                       elevSatelData[3]=(uint8_t) atoi(elevSatel);              // convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 114u:
                    if (numOfSatel >= 3u)
                    {
                       azimuthSatelData[3u]=(uint8_t) atoi(azimuthSatel);       // convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 115u:
                    if (numOfSatel >= 3u)
                    {
                       sigStrengthSatelData[3u]=(uint8_t) atoi(sigStrengthSatel);// convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 116u:
                    if (numOfSatel >= 4u)
                    {
                       satNumberData[4u]=(uint8_t) atoi(satNumber);             // convert string with the satelite being used number to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    else
                    {
                       signalIDData=(uint8_t) atoi(signalID);
                    }
                    break;
                  case 117u:
                    if (numOfSatel >= 4u)
                    {
                       elevSatelData[4u]=(uint8_t) atoi(elevSatel);             // convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 118u:
                    if (numOfSatel >= 4u)
                    {
                       azimuthSatelData[4u]=(uint8_t) atoi(azimuthSatel);       // convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 119u:
                    if (numOfSatel >= 4u)
                    {
                       sigStrengthSatelData[4u]=(uint8_t) atoi(sigStrengthSatel);// convert string with number of satelites to a value
                       msgIdx++ % UINT8_MAX;                                    // now collect the next field
                       charIdx=1u;
                    }
                    break;
                  case 120u:
                    signalIDData=(uint8_t) atoi(signalID);
                    break;
               }
          }
          else if ((msgIdx>=62u) || (msgIdx==73u))                              // Start of an ====GNS===== message type to End comma means next field response to
          {
             if (msgIdx==62u)                                                   // =====GNS====== message
             {
               timeGPSPUBX=atoi(timeGPS);                                       // convert the time from the message
               msgIdx=63u;                                                      // now collect the next field
               charIdx=1u;
             }
             else if (msgIdx==63u)                                              // =====GNS======= message
             {
               latGPSPUBX=atoi(latGPS);                                         // convert the latitude from the message
               msgIdx=64u;                                                      // now collect the next field
               charIdx=1u;                                                      // field 2 of the GNS message latitude
             }
             else if (msgIdx==64u)                                              // =====GNS======= message
             {
               if(nsGPS[1u] == 'S')                                             // if the latitude is in the South direction it has minus sign
               {
                  latGPSPUBX = 0u - latGPSPUBX;                                 // invert the latitude direction
               }
               msgIdx=65u;                                                      // now collect the next field
               charIdx=1u;
             }
             else if (msgIdx==65u)                                              // =====GNS======= message
             {                                                                  // field 4 of the GNS message longditude
                longGPSPUBX=atoi(longGPS);                                      // convert longditude field
                msgIdx=66u;                                                     // now collect the next field
                charIdx=1u;
             }
             else if (msgIdx==66u)                                              // =====GNS======= message
             {                                                                  // field 5 of the GNS e/w indicator
                if(ewGPS[1u] == 'W')                                            // if the longitude is in the West direction it has minus sign
                {
                   longGPSPUBX = 0u - longGPSPUBX;                              // invert the latitude direction
                }
                msgIdx=67u;                                                     // now collect the next field
                charIdx=1u;
             }
             else if (msgIdx==67u)                                              // =====GNS======= message
             {                                                                  // field 6 of the GNS message position mode
                switch(posMode[1u])
                {
                  case 'A':
                    strcpy(positionMode,"GNSS fix\n");
                    break;
                  case 'D':
                    strcpy(positionMode,"Differential GNSS fix\n");
                    break;
                  case 'N':
                     strcpy(positionMode,"No position fix\n");
                     msgIdx=255u;                                               // reset we got a failed message
                     break;
                  default:
                    strcpy(positionMode,"Unknown fix\n");
                    msgIdx=255u;                                                // reset we got a failed message
                    break;
                }
                msgIdx=68u;                                                     // now collect the next field
                charIdx=1u;
             }
             else if (msgIdx==68u)                                              // =====GNS======= message
             {                                                                  // field 7 of the GNS message depending on number of satelites
                 numOfSatel=(uint8_t) atoi(numSatel);                           // convert string with number of satelites in sight to a value
                 msgIdx=69u;                                                    // now collect the next field
                 charIdx=1u;
             }
             else if (msgIdx==69u)                                              // =====GNS======= message
             {                                                                  // field 8 of the GNS message horizontal dilution
                HDOPGPSPUBX=atof(HDOPGPS);                                      // convert the Horizontal Dilution of Precision  field from the message
                msgIdx=70u;                                                     // now collect the next field
                charIdx=1u;
             }
             else if (msgIdx==70u)                                              // =====GNS======= message
             {                                                                  // field 9 of the GNS message sea level altitude (meters)
                seaLevelAltitude=atof(seaLevelAlt);                             // convert the sea level altitude (meters)  field from the message
                msgIdx=71u;                                                     // now collect the next field
                charIdx=1u;
             }
             else if (msgIdx==71u)                                              // =====GNS======= message
             {                                                                  // field 10 of the GNS message Geoidal height
                geoidalHt=atof(geoHt);                                          // convert the Geoidal height (difference between ellipsoid and mean sea level)  field from the message
                msgIdx=72u;                                                     // now collect the next field
                charIdx=1u;
             }
          }
          break;
//=================== <CR> 0x0D ================================================
        case 13u:                                                               // Carriage return is second last char  Terminate regardless when found
           csGPSPUBX=((uint16_t)hex2int(csGPS));                                // Convert the checksum field from the message should just preceede it starts with a (*) char
           msgIdx=250u;                                                         // Look for the final line feed
           charIdx=1u;
           break;
//================== <LF> 0x0A =================================================
        case 10u:                                                               // line feed is end of message
           if (msgIdx==250u)
              msgIdx=251u;                                                      // message read state has been set to complete

           //else if (msgIdx==62)  ============ This method cant work in mikroe C for PIC32 =======
           //{
              //sscanf(gnsBuffer,"%d , %f , %s , %f , %s , %s , %d , %f , %f , %f ,,, %s, %s",timeGPSPUBX,latGPSPUBX,nsGPS,longGPSPUBX,ewGPS,posMode,numOfSatel,HDOPGPSPUBX,SealevelAlt,GeoHt,navStatGPS,csGPS);
              //SSCANF_GSV(gnsBuffer, timeGPSPUBX, latGPSPUBX);
           //}
           break;
//================ Fields to collect as data ===================================
        default:
          if (msgIdx==6u)
          {
             if (charIdx <= sizeof(msgId))
               msgId[charIdx]=GPStxt[i];                                        // read msg id
             charIdx=++charIdx % UINT8_MAX;
          }
          else if (msgIdPUBX==0u)                                               // message was ====PUBX===== ==== MSGID==0 ====== (position message)
          {
             if (msgIdx==7u)                                                    // 2nd field in message string and its a position message
             {
                if (charIdx <= sizeof(timeGPS))
                  timeGPS[charIdx]=GPStxt[i];                                   // read time field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==8u)                                                // 3rd field in message string and its a position message
             {
                if (charIdx <= sizeof(latGPS))
                  latGPS[charIdx]=GPStxt[i];                                    // read latitude field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==9u)                                               // 4th field in message string and its a position message
             {
                if (charIdx <= sizeof(nsGPS))
                  nsGPS[charIdx]=GPStxt[i];                                     // read north / south field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==10u)                                              // 5th field in message string and its a position message
             {
                if (charIdx <= sizeof(longGPS))
                  longGPS[charIdx]=GPStxt[i];                                   // read longditude field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==11u)                                              // 6th field in message string and its a position message
             {
                if (charIdx <= sizeof(ewGPS))
                  ewGPS[charIdx]=GPStxt[i];                                     // read east west field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==12u)                                              // 7th field in message string and its a position message
             {
                if (charIdx <= sizeof(altRefGPS))
                  altRefGPS[charIdx]=GPStxt[i];                                 // read Altitude above user datum ellipsoid field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==13u)                                              // 8th field in message string and its a position message
             {
                if (charIdx <= sizeof(navStatGPS))
                  navStatGPS[charIdx]=GPStxt[i];                                // read navigation Status string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==14u)                                              // 9th field in message string and its a position message
             {
                if (charIdx <= sizeof(hAccGPS))
                  hAccGPS[charIdx]=GPStxt[i];                                   // read horizontal accuracy estimate string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==15u)                                              // 10th field in message string and its a position message
             {
                if (charIdx <= sizeof(vAccGPS))
                  vAccGPS[charIdx]=GPStxt[i];                                   // read vertical accuracy estimate string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==16u)                                              // 11th field in message string and its a position message
             {               
                if (charIdx <= sizeof(SOGGPS))
                 SOGGPS[charIdx]=GPStxt[i];                                     // read speed over ground string
               charIdx=++charIdx % UINT8_MAX;;
             }
             else if (msgIdx==17u)                                              // 12th field in message string and its a position message
             {
               if (charIdx <= sizeof(COGGPS))
                 COGGPS[charIdx]=GPStxt[i];                                     // read course over ground string
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==18u)                                              // 13th field in message string and its a position message
            {
               if (charIdx <= sizeof(vVelGPS))
                  vVelGPS[charIdx]=GPStxt[i];                                   // read Vertical velocity (positive downwards)
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==19u)                                               // 14th field in message string and its a position message
            {
               if (charIdx <= sizeof(diffAgeGPS))
                  diffAgeGPS[charIdx]=GPStxt[i];                                // read Vertical velocity (positive downwards)
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==20u)                                               // 15th field in message string and its a position message
            {
               if (charIdx <= sizeof(HDOPGPS))
                  HDOPGPS[charIdx]=GPStxt[i];                                   // read HDOP, Horizontal Dilution of Precision
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==21u)                                               // 16th field in message string and its a position message
            {
               if (charIdx <= sizeof(VDOPGPS))
                  VDOPGPS[charIdx]=GPStxt[i];                                   // read VDOP, Vertical Dilution of Precision
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==22u)                                               // 17th field in message string and its a position message
            {
               if (charIdx <= sizeof(TDOPGPS))
                  TDOPGPS[charIdx]=GPStxt[i];                                   // read TDOP, Time Dilution of Precision
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==23u)                                               // 18th field in message string and its a position message
            {
               if (charIdx <= sizeof(numSvsGPS))
                  numSvsGPS[charIdx]=GPStxt[i];                                 // read number of satelites used in the navigation solution
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==24u)                                               // 19th field in message string and its a position message
            {
               if (charIdx <= sizeof(reservedGPS))
                 reservedGPS[charIdx]=GPStxt[i];                                // read nreserved field (ignore for now)
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==25u)                                               // 20th field in message string and its a position message
            {
               if (charIdx <= sizeof(DRGPS))
                  DRGPS[charIdx]=GPStxt[i];                                     // read DR used
               charIdx=++charIdx % UINT8_MAX;
            }
            else if (msgIdx==26u)                                               // 21th field in message string and its a position message
            {
               if (charIdx <= sizeof(csGPS))
                 csGPS[charIdx]=GPStxt[i];                                      // read hexidecimal characture representation of the checksum
               charIdx=++charIdx % UINT8_MAX;
            }
          }
          else if ((msgIdx >= 43u) && (msgIdx <= 55u))                          // Start of RMC NMEA message parsing------------------
          {
             if (msgIdx==43u)
             {
               if (charIdx <= sizeof(timeGPS))
                 timeGPS[charIdx]=GPStxt[i];                                    // read time field
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==44u)                                              // field 2 of the RMC message
             {
               if (charIdx <= 1u)
                 rmcStat[1u]=GPStxt[i];                                         // read rtc status
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==45u)                                              // field 3 of the RMC message lat
             {
                if (charIdx <= sizeof(latGPS))
                  latGPS[charIdx]=GPStxt[i];                                    // read latitude field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==46u)                                              // field 4 of the RMC message
             {
                if (charIdx <= sizeof(nsGPS))
                  nsGPS[charIdx]=GPStxt[i];                                     // read north / south field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==47u)                                              // field 5 of the RMC message
             {
                if (charIdx <= sizeof(longGPS))
                  longGPS[charIdx]=GPStxt[i];                                   // read longditude field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==48u)                                              // field 5 of the RMC message
             {
                if (charIdx <= sizeof(ewGPS))
                  ewGPS[charIdx]=GPStxt[i];                                     // read east west field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==49u)                                              // field 6 of the RMC message
             {
                if (charIdx <= sizeof(SOGGPS))
                  SOGGPS[charIdx]=GPStxt[i];                                    // read speed over ground string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==50u)                                              // field 7 of the RMC message
             {
                if (charIdx <= sizeof(COGGPS))
                   COGGPS[charIdx]=GPStxt[i];                                   // read course over ground string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==51u)                                              // field 8 of the RMC message
             {
                if (charIdx <= sizeof(dateStr1))
                  dateStr1[charIdx]=GPStxt[i];                                  // date string
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==52u)                                              // field 9 of the RMC message
             {
               if (charIdx <= sizeof(magVarStr))
                  magVarStr[charIdx]=GPStxt[i];                                 // magnetic variation string
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==53u)                                              // field 10 of the RMC message
             {
                if (charIdx <= sizeof(magVarEw))
                   magVarEw[charIdx]=GPStxt[i];                                 // magnet variation e/w
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==54u)                                              // field 11 of the RMC message
             {
                if (charIdx <= sizeof(posMode))
                   posMode[charIdx]=GPStxt[i];                                  // mode
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==55u)                                              // field 12 of the RMC message
             {
                if (charIdx <= sizeof(navStatus))
                   navStatus[charIdx]=GPStxt[i];                                // navigation status string
                charIdx=++charIdx % UINT8_MAX;
             }
          }
          else if ((msgIdx >= 131u) && (msgIdx <= 137u))                        // start of ==== GLL =======
          {
              if (msgIdx==131u)                                                 // field 1 of the GLL message lat
              {
                if (charIdx <= sizeof(latGPS))
                   latGPS[charIdx]=GPStxt[i];                                   // read latitude field
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==132u)                                            // field 2 of the GLL message
              {
                if (charIdx <= sizeof(nsGPS))
                   nsGPS[charIdx]=GPStxt[i];                                    // read north / south field
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==133u)                                            // field 3 of the GLL message
              {
                if (charIdx <= sizeof(longGPS))
                   longGPS[charIdx]=GPStxt[i];                                  // read longditude field
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==134u)                                            // field 4 of the GLL message
              {
                if (charIdx <= sizeof(ewGPS))
                   ewGPS[charIdx]=GPStxt[i];                                    // read east west field
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==135u)                                            // field 5 of the GLL message
              {
                 if (charIdx <= sizeof(timeGPS))
                    timeGPS[charIdx]=GPStxt[i];                                 // read time field
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==136u)                                            // field 6 of the GLL message
              {
                 if (charIdx <= 1u)
                   rmcStat[1]=GPStxt[i];                                        // read rtc status
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==137u)                                             // field 7 of the GLL message
             {
                if (charIdx <= sizeof(posMode))
                   posMode[charIdx]=GPStxt[i];                                  // mode
                charIdx=++charIdx % UINT8_MAX;
             }
          }
          else if ((msgIdx >= 82u) && (msgIdx <= 91u))                          // ==== VTG message ===========
          {
              if (msgIdx==82u)                                                  // field 1 of the VTG message
              {
                 if (charIdx <= sizeof(COGGPS))
                    COGGPS[charIdx]=GPStxt[i];                                  // read course over ground string
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==83u)                                             // field 2 of the VTG message
              {
                 if (charIdx <= 1u)
                    charRead[1]=GPStxt[i];                                      // read T (true course)
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==84u)                                             // field 3 of the VTG message
              {
                 charIdx=++charIdx % UINT8_MAX;                                 // ignore always a NULL
              }
              else if (msgIdx==85u)                                             // field 4 of the VTG message
              {
                if (charIdx <= 1u)
                   charRead[1]=GPStxt[i];                                       // read M (magnetic)
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==86u)                                             // field 5 of the VTG message
              {
                if (charIdx <= sizeof(SOGGPS))
                   SOGGPS[charIdx]=GPStxt[i];                                   // read speed over ground string knots
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==87u)                                             // field 6 of the VTG message
              {
                if (charIdx <= 1u)
                   charRead[1u]=GPStxt[i];                                      // read M (knots)
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==88u)                                             // field 7 of the VTG message
              {
                if (charIdx <= sizeof(SOGGPS))
                   SOGKGPS[charIdx]=GPStxt[i];                                  // read speed over ground string km/h
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==89u)                                             // field 8 of the VTG message
              {
                if (charIdx <= 1u)
                   charRead[1u]=GPStxt[i];                                      // read K (kmh)
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==90u)                                             // field 8 of the VTG message
              {
                if (charIdx <= 1u)
                   charRead[1u]=GPStxt[i];                                      // read K (kmh)
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==91u)                                             // field 8 of the VTG message
              {
                if (charIdx <= sizeof(posMode))
                  posMode[charIdx]=GPStxt[i];                                   // mode
                charIdx=++charIdx % UINT8_MAX;
              }
          }
          else if ((msgIdx>=101u) && (msgIdx<=120u))                            //============ GSV message =====================
          {
              if (msgIdx==101u)                                                 // field 1 of the GSV message
              {
                if (charIdx <= sizeof(numSatel))
                   numSatel[charIdx]=GPStxt[i];                                 // number of satelites
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==102u)                                            // field 2 of the GSV message
              {
                if (charIdx <= 1u)
                  charRead[1u]=GPStxt[i];                                       // ignore message number for now
                charIdx=++charIdx % UINT8_MAX;
              }
              else if (msgIdx==103u)                                            // field 3 of the GSV message
              {
                if (charIdx <= sizeof(numSatelinSight))
                   numSatelinSight[charIdx]=GPStxt[i];                          // number of satelites in sight
                charIdx=++charIdx % UINT8_MAX;
              }
              else if ((((msgIdx==104u) || ((msgIdx==108u) && (numOfSatel>=2u))) || ((msgIdx==112u) && (numOfSatel>=3u))) || ((msgIdx==116u) && (numOfSatel>=4u)))
              {                                                                 // field 4 of the GSV message
                 if (charIdx <= sizeof(satNumber))
                   satNumber[charIdx]=GPStxt[i];                                // satelite elevation
                charIdx=++charIdx % UINT8_MAX;
              }
              else if ((((msgIdx==105u) || ((msgIdx==109u) && (numOfSatel>=2u))) || ((msgIdx==113u) && (numOfSatel>=3u))) || ((msgIdx==117u) && (numOfSatel>=4u)))
              {                                                                 // field 4 of the GSV message
                 if (charIdx <= sizeof(elevSatel))
                   elevSatel[charIdx]=GPStxt[i];                                // satelite elevation
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if ((((msgIdx==106u) || ((msgIdx==110u) && (numOfSatel>=2u))) || ((msgIdx==114u) && (numOfSatel>=3u))) || ((msgIdx==118u) && (numOfSatel>=4u)))
              {                                                                 // field 5 of the GSV message
                 if (charIdx <= sizeof(azimuthSatel))
                    azimuthSatel[charIdx]=GPStxt[i];                            // satelite azimuth
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if ((((msgIdx==107u) || ((msgIdx==111u) && (numOfSatel>=2u))) || ((msgIdx==115u) && (numOfSatel>=3u))) || ((msgIdx==119u) && (numOfSatel>=4u)))
              {                                                                 // field 6 of the GSV message
                 if (charIdx <= sizeof(sigStrengthSatel))
                   sigStrengthSatel[charIdx]=GPStxt[i];                         // satelite signal stength
                 charIdx=++charIdx % UINT8_MAX;
              }
              else if (((((msgIdx==108u) && (numOfSatel==1u)) || ((msgIdx==112u) && (numOfSatel==2u))) || ((msgIdx==116u) && (numOfSatel==3u))) || ((msgIdx==120u) && (numOfSatel==4u)))
             {                                                                  // field 7 or more of the GSV message depending on number of satelites
                if (charIdx <= sizeof(signalID))
                   signalID[charIdx]=GPStxt[i];                                 // GNSS signal ID
                charIdx=++charIdx % UINT8_MAX;
             }
          }
          else if ((msgIdx >= 62u) && (msgIdx <= 72u))                          // =============== GNS message =========
          {
             if (msgIdx==62u)                                                   // =====GNS======= message
             {                                                                  // field 1 of the GNS message time field
                if (charIdx <= sizeof(timeGPS))
                  timeGPS[charIdx]=GPStxt[i];                                   // read time field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==63u)                                              // =====GNS======= message
             {                                                                  // field 2 of the GNS message latitude
               if (charIdx <= sizeof(latGPS))
                 latGPS[charIdx]=GPStxt[i];                                     // read latitude field
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==64u)                                              // =====GNS======= message
             {                                                                  // field 3 of the GNS message n/s indication
                if (charIdx <= sizeof(nsGPS))
                  nsGPS[charIdx]=GPStxt[i];                                     // read north / south field
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==65u)                                              // =====GNS======= message
             {                                                                  // field 4 of the GNS message longditude
               if (charIdx <= sizeof(longGPS))
                 longGPS[charIdx]=GPStxt[i];                                    // read longditude field
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==66u)                                              // =====GNS======= message
             {                                                                  // field 5 of the GNS e/w indicator
               if (charIdx <= sizeof(ewGPS))
                 ewGPS[charIdx]=GPStxt[i];                                      // read east west field
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==67u)                                              // =====GNS======= message
             {                                                                  // field 6 of the GNS message position mode
                if (charIdx <= sizeof(posMode))
                   posMode[charIdx]=GPStxt[i];                                  // mode
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==68u)                                              // =====GNS======= message
             {                                                                  // field 7 of the GNS message depending on number of satelites
                if (charIdx <= sizeof(numSatel))
                   numSatel[charIdx]=GPStxt[i];                                 // number of satelites
                charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==69u)                                              // =====GNS======= message
             {                                                                  // field 8 of the GNS message horizontal dilution
               if (charIdx <= sizeof(HDOPGPS))
                  HDOPGPS[charIdx]=GPStxt[i];                                   // read HDOP, Horizontal Dilution of Precision
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==70u)                                              // =====GNS======= message
             {                                                                  // field 9 of the GNS message sea level altitude (meters)
               if (charIdx <= sizeof(seaLevelAlt))
                  seaLevelAlt[charIdx]=GPStxt[i];                               // read sea level altitude (meters)
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==71u)                                              // =====GNS======= message
             {                                                                  // field 10 of the GNS message Geoidal height
               if (charIdx <= sizeof(geoHt))
                  geoHt[charIdx]=GPStxt[i];                                     // Geoidal height (difference between ellipsoid and mean sea level)
               charIdx=++charIdx % UINT8_MAX;
             }
             else if (msgIdx==72u)                                              // =====GNS======= message
             {                                                                  // field 13 (skipped 2 blank fields) of the GNS message Navigation Status
                if (charIdx <= sizeof(navStatus))
                   navStatus[charIdx]=GPStxt[i];                                // navigation status string
                charIdx=++charIdx % UINT8_MAX;
             }
          }
          else if (((((msgIdx==56u) || (msgIdx==138u)) || (msgIdx==73u)) || (msgIdx==92u)) || (msgIdx==121u))      // ==== Collect the checksum after a * char =============
          {                                                                     // seen a * so collect the checksum from the end
             if (charIdx <= sizeof(csGPS))
               csGPS[charIdx]=GPStxt[i];                                        // read hexidecimal characture representation of the checksum
             charIdx=++charIdx % UINT8_MAX;
          }
          break;
       }
       i=++i % UINT8_MAX;                                                       // increment index and read the next char into receive buffer at position i
       if (((((((msgIdx >= 5u) && (msgIdx <= 25u)) || ((msgIdx >= 42u) && (msgIdx <= 55u))) || ((msgIdx >= 62u) && (msgIdx <= 72u))) || ((msgIdx >= 101u) && (msgIdx <= 120u))) || ((msgIdx >= 131u) && (msgIdx <= 137u))) || ((msgIdx >= 82u) && (msgIdx <= 91u)))
          checkCalc^=GPStxt[i];                                                 // Online Checksum (XOR) as we collect each char from start of data to before the checksum word in the message string (2 bytes)
     } // ------------ End of UART3_Data_ready()
  } // --------------- End of main U3RXIF_bit & msgIdx!=255
  else if ((msgIdx==255u) && U3RXIF_bit)                                         // === DORMANT STEP WAIT FOR A NEW START ==== Integrity error found look for a new start do nothing else
  {
    i=1;
    if (UART3_Data_ready())                                                     // ready the receieve ?
    {
       GPStxt[i] = UART3_Read();                                                // Read the char from UART 3
       if (GPStxt[i]=='$')                                                      // found a start then collect a new message
       {
          msgIdx=1u;                                                            // new start
          checkCalc=0u;                                                         // reset checksum
          g_GPS_ready=0u;                                                       // inform top level new collection taking place
       }
    }
  }
  else
  {
     i=1;                                                                       // initialise i if value is obscure or out of range
  }

  if (msgIdx==251u)                                                             // at the end of a valid message above CRLF sequence completed
  {
    i=1;                                                                        // reset message collection
    if (checkCalc == csGPSPUBX)                                                 // Online checksum matches the data checksum sent so say the data is valid to display or use
    {
       g_GPS_ready = 1;                                                         // Global represents we read all the NMEA code message from the GPS and its ready for processing
       g_posDataGPS.timestamp =  timeGPSPUBX;                                   // Write the data to the global object for display or control
       g_posDataGPS.lat = latGPSPUBX;
       g_posDataGPS.longd = longGPSPUBX;
       g_posDataGPS.altRef = altRefGPSPUBX;
       g_posDataGPS.hAcc = hAccGPSPUBX;
       g_posDataGPS.vAcc = vAccGPSPUBX;
       g_posDataGPS.SOG =  SOGGPSPUBX;
       g_posDataGPS.COG = COGGPSPUBX;
       g_posDataGPS.vVel = vVelGPSPUBX;
       g_posDataGPS.numOfSatel = numOfSatel;
       g_posDataGPS.SealevelAltitude = SealevelAltitude;
       g_posDataGPS.GeoidalHt = GeoidalHt;
    }
  }
  else if (i >= MAX_GPS_MSG_LEN)                                                // longer than the max or you got a CRLF sequence
  {
     i=1;                                                                       // reset message collection
     g_GPS_ready = 0u;
  }
  U3RXIF_bit=CLEAR;                                                             // Clear interrupt as it was read
}
#endif                                                                          // END OF GPS INCLUSION -----------------------------------

// ------------------- XS Encoder Serial  --------------------------------------

#ifdef UART4_INTERUPT                                                           // This is set if we are enabling UART4 with XS encoder serial

/*-----------------------------------------------------------------------------
 *      UART4_interrupt() :  This interrupts on chars that are recieved at UART4 serial port
 *  It will process these if they are supported XS Encoder commands
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void UART4_interrupt() iv IVT_UART_4 ilevel 4 ics ICS_AUTO
{
   asm EI;                                                                      // Re-enable global interrupts
   UART4.Bytes_In=++UART4.Bytes_In % UINT16_MAX;                                               // Global counter on Bytes arriving at UART4

   if((U4STA & 0x0Eu) | U4EIF_Bit)                                              // Error check with UART4 buffer ???
   {
      if (OERR_U4STA_bit==1u)                                                   // Overrun error (bit 1)
      {
        OERR_U4STA_bit = 0u;
        UART4.Buffer_Overrun++ % UINT8_MAX;                                     // Count overruns
      }
      else if (FERR_U4STA_bit==1u)                                              // Framing error (bit 2)
        FERR_U4STA_bit = 0u;
      else if (PERR_U4STA_bit==1u)                                              // Parity error (bit 3)
        PERR_U4STA_bit = 0u;

      UART4.Index =0u;                                                          // Set the index to zero
      U4STA &= 0xFFFFFFF1ul;                                                    //Clear the Error Status 0001 no longer 1101 (D) just the overrun , clears frame and parity
      U4EIF_Bit =CLEAR;                                                         //Clear the Error IF
      U4RXIF_bit=CLEAR;                                                         //Clear the UART3 IF
   }

   if (U4RXIF_Bit)
   {
      if ((!XSstart) || XStypeMsgInvalid)                                       // if we dont already have a STX start byte or we got an invalid msg_id or checksum
      {
        UART4.Buffer[0u] = U2RXREG;
        if(UART4.Buffer[0u] == XS_CMD_REPLY_STX)                                // Look for the XS serial reply to a message start transmission (+) 0x2B
         {
            //T2CON = ENABLE_T2;       // Enable Timeout
            UART4.Index=1u;
            XSstart =TRUE;                                                      // valid response has been seen
            g_extended=0u;                                                      // extended +ok,<field> response is set to off mode
            XStypeMsgInvalid=0u;                                                // Set the message type to valid unless we dont get a valid type
            UART4.State = UART4_BYTE_IN_BUFFER;
         }
      }
      else
      {
         while(URXDA_U4STA_bit)                                                 // if there is a char to receive
         {
            UART4.Buffer[UART4.Index] = U4RXREG;                                // put the char from the UART4 into the buffer
            switch(UART4.Index)                                                 // For each characture in the response message do.....
            {
               case 1u:                                                         // Identifies the 2nd char which defines the reply response message
               switch(UART4.Buffer[1])                                          // For the 2nd byte position in the incoming message see what you got
               {
                 case 'O':                                                      // command was a XS response +OK
                   XSlen=3u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'A':                                                      // command was a XS response +AUTH
                   XSlen=5u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'o':                                                      // command was a XS response +ok
                   XSlen=3u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'a':                                                      // command was a XS response +auth
                   XSlen=5u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'I':                                                      // command was a XS response +INVALID
                   XSlen=8u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'i':                                                      // command was a XS response +invalid
                   XSlen=8u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'F':                                                      // command was a XS response +FAIL
                   XSlen=5u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'f':                                                      // command was a XS response +fail
                   XSlen=5u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'W':                                                      // command was a XS response +WRONG
                   XSlen=6u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'w':                                                      // command was a XS response +wrong
                   XSlen=6u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'D':                                                      // command was a XS response +DISKSTATUS,val1,val2,val3
                   XSlen=11u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'd':                                                      // command was a XS response +diskstatus,val1,val2,val3
                   XSlen=11u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 'S':                                                      // command was a XS response +SYSSTATUS,val1,val2,val3,val4,val5,val6
                   XSlen=10u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 case 's':                                                      // command was a XS response +sysstatus,val1,val2,val3,val4,val5,val6
                   XSlen=10u;
                   UART4.Index=++UART4.Index % UINT16_MAX;
                   break;
                 default:                                                       // Data preceding the + char was not recognised
                   XStypeMsgInvalid=1u;
                   UART4.Index=1u;                                              // Restart message collection its not an expected response
                   break;
               }
               break;

               default:                                                         // Should be payload - reset if you got snother STX or payload is too long
               if (UART4.Buffer[UART4.Index] == XS_CMD_REPLY_STX)               // another STX (+) was found this is invalid go back to start
               {
                  UART4.Buffer[0u]= UART4.Buffer[UART4.Index];                  // reset to the start of the message buffer.
                  UART4.Index=1u;                                               // reset to put the rest of the message from index 1
                  g_extended=0u;                                                // extended +ok,<field> response is set to off mode
               }
               else if (UART4.Index >= (XSlen+1u))                              // message is now at the end either <CR> or a +ok,<value>
               {
                  if (UART4.Buffer[UART4.Index]==0x0Du)                         // CR is END of message else try (0x04 EOT)
                  {
                     UART4.Index=++UART4.Index % UINT16_MAX;                    // increment the packets in counter
                     XSstart=FALSE;                                             // Complete message collected start again

                     if (!typeMsgInvalid)                                       // Message is good and supported
                     {
                         UART4.State = UART4_PACKET_IN_BUFFER+g_extended;       // Declare good packet to top level (otherwise ignore it)
                     }
                  }
                  else if (((XSlen == 3u) || (XSlen == 11u)) || (XSlen == 10u)) // it may be either +ok +sysstatus or +diskstatus which can be followed legally by a comma
                  {
                     if (UART4.Buffer[UART4.Index]==',')                        // We got a comma set a global bit to explain it has extra extended values to extract from it
                     {
                        g_extended=XSlen;                                       // set a global bit to say we extended using a , separater longer message found
                     }
                     UART4.Index=++UART4.Index % UINT16_MAX;                    // keep collecting the extended message separated by comma's before completing the collection
                  }
                  else
                  {
                    typeMsgInvalid=1u;                                          // it was invalid start collecting it again
                    UART4.Index=1u;                                             // Restart message collection
                  }
               }
               else
               {
                  UART4.Index=++UART4.Index % UINT16_MAX;                       // increment and fill the buffer with data from the payload
               }
               break;
            }
         }
      }
    } // End U2RXIFBIT
  U4RXIF_bit=CLEAR;

} // End Interrupt
#endif

#ifdef UART6_INTERUPT                                                           // This is set if we are enabling UART1 with Liddar
/*-----------------------------------------------------------------------------
 *      UART6_interrupt() :  This interrupts on chars that are recieved at UART6 serial port
 *  It will process these if they are supported by lightware commands
 *  Parameters: (none)
 *  Return:     (none)
 *----------------------------------------------------------------------------*/
void UART6_interrupt() iv IVT_UART_6 ilevel 6 ics ICS_AUTO
{

   if((U6STA & 0x0E) | U6EIF_Bit)                                               // Error check with UART4 buffer ???
   {
      if (OERR_U6STA_bit==1u)                                                   // Overrun error (bit 1)
      {
        OERR_U6STA_bit = 0u;
        UART6.Buffer_Overrun=++UART6.Buffer_Overrun % UINT8_MAX;                // Count overruns
      }
      else if (FERR_U6STA_bit==1u)                                              // Framing error (bit 2)
        FERR_U6STA_bit = 0u;
      else if (PERR_U6STA_bit==1u)                                              // Parity error (bit 3)
        PERR_U6STA_bit = 0u;

      UART6.Index =0u;                                                          // Set the index to zero
      U6STA &= 0xFFFFFFF1UL;                                                    //Clear the Error Status 0001 no longer 1101 (D) just the overrun , clears frame and parity
      U6EIF_Bit =CLEAR;                                                         //Clear the Error IF
      U6RXIF_bit=CLEAR;                                                         //Clear the UART3 IF
   }
   UART6.State=lwnxParseData( &LwNxResponse, U6RXREG);
   U6RXIF_bit=CLEAR;                                                            // clear the interrupt
}
#endif