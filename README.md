SUPER PIC CAMERA CONTROLLER
================================

Simple set of libraries for PIC32 written in MikroE C.
In most I have tried to keep the original libraries as is if I have ported them to mikroE C, otherwise they are wrote to compile with the mikroE C compiler for PIC32.

## Features

- Parses and writes JSON from null-terminated string
- Supports CBOR data transfer to iOT
- Easy to use tree traversal API
- Communicates with JSON server using Ajax commands 
- This communicates with Yi Action Cam and Parrot Sequoia Camera's
- Communiates on serial to AMP Encoder/Decoder
- Implements SimpleBGC protocol to communicate with camera Gimbal
- Communicates with Run Cam Camera using own protocol
- Communicates on serial tcp and udp
- Communicates on Modbus RTU ASCII TCP UDP BIN protocol
- Communicates on GEC Fanuc EGD UDP protocol
- Communicates with Leddartech (modbus) or lightware LWNX protocol to liddar devices
- Communicates with UBLOX and FURANO GPS NMEA full support plus proprietary
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Contains PID loop and auto tune 
- Contains AHRS helper library with Madgewick and Mahony compensations moving average etc
- Contains axis movement robot helper library for gait control via ODRIVE
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Supports Joystick calibration and expo routines
- Also support for JRT lidar and bosch BME680 humidity/pressure sensor
- Color conversion codecs NV12_YUV420P RGB HSV YUV CMY CMYK Y,BY,RY YCbCr
- Support of LW3 protocol for lightware devices and switches
- CRC libraries for all protocols used fast and slow method
- Mini Alarm stack handler 
- SPI and I2C support libraries
- CanBus interface
- ArtNet-4 DMX512 RDM and Dali Support libraries
- OPC (open pixel control) support
- Pelco D PTZ support
- HTTP authenticate support
- Canon and Pentax camera support librarys
- Endian conversion libraries
- XML (via YXML Library) and GeoJSON support


Cameras supported
=================

For up-to-date camera support details, see the canon.c source file.

 - PowerShot A50 (not tested)
 - PowerShot Pro70
 - PowerShot S10
 - PowerShot S20
 - PowerShot S30
 - PowerShot S40
 - PowerShot S100 / Digital IXUS
 - PowerShot S110 / DIGITAL IXUS v
 - PowerShot S200 / Digital IXUS v2
 - PowerShot IXY Digital (not tested)
 - PowerShot S300 / Digital IXUS 300
 - PowerShot IXY Digital 300  (not tested)
 - PowerShot G1
 - PowerShot G2
 - PowerShot A10 (not tested after rewrite)
 - PowerShot A20
 - PowerShot A30
 - PowerShot A40
 - PowerShot A100
 - PowerShot A200
 - PowerShot Pro90 IS (not tested)
 - PowerShot S300 / IXY DIGITAL 300 / Digital IXUS 300
 - EOS D30
 - EOS D60
 - EOS 10D
 - Digital IXUS 330
 - Canon MVX2i / Optura 200 MC
 - EOS 300D / Digital Rebel / KISS Digital
 - a few newer ones, see output of "gphoto2 --list-cameras | grep Canon"

The following cameras operate either with this driver or in PTP mode
with the PTP driver:

 - PowerShot S45 (normal mode)
 - PowerShot G3 (normal mode)
 - PowerShot S230 / Digital IXUS v3 (normal mode)
 - a few newer ones, see output of "gphoto2 --list-cameras | grep Canon"
  
Support for the following cameras is mostly there as they were supported
in gphoto 0.4.x but several API changes and rewrites have probably broken
something along the way. If you are interested in fixing it mail 
gphoto-devel@lists.sourceforge.net:
 
 - PowerShot A5
 - PowerShot A5 Zoom

It doesn't work with the PowerShot 350. However, the PS350 uses a
vaguely similar protocol, so some parts of the sources can probably be
shared.

The following cameras, and probably many cameras to come, operate in
either PTP and Canon mode depending on the first data packets sent to
the camera, i.e. depending on whether you use this driver or the PTP
driver:

 - PowerShot A60
 - PowerShot A70
## Limitations

- Nothing tested as yet

## API

TBD


#### Libraries



## Usage Example

look to each section in the main GCPIC32.c code

##License

LGPL v3

##Copyright

Copyright (c) 2020 ACP Aviation <mark@aircampro.co.uk>
