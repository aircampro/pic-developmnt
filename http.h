//  Header file to support http client functions
//
//  Contains some sections
//  Copyright (C) 2010-2019 Oryx Embedded SARL. All rights reserved.
//  Copyright (c) 2011-2012 Yaroslav Stavnichiy <yarosla@gmail.com> httppress.c
//
//  Which is part of CycloneTCP Open.
//
#ifndef _HTTP_H
#define _HTTP_H

#include "stdint.h"

#ifdef __GNUC__                                                                 // pack the structures so as not to waste memory
  #define HTTPPACKED( __Declaration__ ) __Declaration__ __attribute__((packed))
#else
  #define HTTPPACKED( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#endif

//HTTP port number
#define HTTP_PORT 80
//HTTPS port number (HTTP over TLS)
#define HTTPS_PORT 443

#define HTTP_LIB static                                                         // static or external

/**
 * @brief HTTP version numbers
 **/
typedef enum
{
   HTTP_VERSION_0_9 = 0x0009,
   HTTP_VERSION_1_0 = 0x0100,
   HTTP_VERSION_1_1 = 0x0101
} HttpVersion;

/**
 * @brief HTTP authentication schemes
 **/
typedef enum
{
   HTTP_AUTH_MODE_NONE   = 0,
   HTTP_AUTH_MODE_BASIC  = 1,
   HTTP_AUTH_MODE_DIGEST = 2
} HttpAuthMode;

/**
 * @brief Quality of protection (digest authentication)
 **/
typedef enum
{
   HTTP_AUTH_QOP_NONE     = 0,
   HTTP_AUTH_QOP_AUTH     = 1,
   HTTP_AUTH_QOP_AUTH_INT = 2
} HttpAuthQop;

/**
 * @brief Flags used by I/O functions
 **/
typedef enum
{
   HTTP_FLAG_WAIT_ALL   = 0x0800,
   HTTP_FLAG_BREAK_CHAR = 0x1000,
   HTTP_FLAG_BREAK_CRLF = 0x100A,
   HTTP_FLAG_NO_DELAY   = 0x4000,
   HTTP_FLAG_DELAY      = 0x8000
} HttpFlags;

/**
 * @brief HTTP request states
 */
typedef enum
{
   HTTP_REQ_STATE_INIT                = 0,
   HTTP_REQ_STATE_FORMAT_HEADER       = 1,
   HTTP_REQ_STATE_SEND_HEADER         = 2,
   HTTP_REQ_STATE_SEND_BODY           = 3,
   HTTP_REQ_STATE_SEND_CHUNK_SIZE     = 4,
   HTTP_REQ_STATE_SEND_CHUNK_DATA     = 5,
   HTTP_REQ_STATE_FORMAT_TRAILER      = 6,
   HTTP_REQ_STATE_SEND_TRAILER        = 7,
   HTTP_REQ_STATE_RECEIVE_STATUS_LINE = 8,
   HTTP_REQ_STATE_RECEIVE_HEADER      = 9,
   HTTP_REQ_STATE_PARSE_HEADER        = 10,
   HTTP_REQ_STATE_RECEIVE_BODY        = 11,
   HTTP_REQ_STATE_RECEIVE_CHUNK_SIZE  = 12,
   HTTP_REQ_STATE_RECEIVE_CHUNK_DATA  = 13,
   HTTP_REQ_STATE_RECEIVE_TRAILER     = 14,
   HTTP_REQ_STATE_PARSE_TRAILER       = 15,
   HTTP_REQ_STATE_COMPLETE            = 16
} HttpRequestState;

typedef enum {
     C_CONNECTING, C_HANDSHAKING, C_WRITING, C_READING_HEADERS, C_READING_BODY
} nx_connection_state;

/**
 * @brief HTTP character sets
 */
typedef enum
{
   HTTP_CHARSET_OCTET    = 0x0001,
   HTTP_CHARSET_CTL      = 0x0002,
   HTTP_CHARSET_LWS      = 0x0004,
   HTTP_CHARSET_ALPHA    = 0x0008,
   HTTP_CHARSET_DIGIT    = 0x0010,
   HTTP_CHARSET_HEX      = 0x0020,
   HTTP_CHARSET_VCHAR    = 0x0040,
   HTTP_CHARSET_TCHAR    = 0x0080,
   HTTP_CHARSET_TEXT     = 0x0100,
   HTTP_CHARSET_OBS_TEXT = 0x0200
} HttpCharset;

/**
 * @brief HTTP client states
 */
typedef enum
{
   HTTP_CLIENT_STATE_DISCONNECTED  = 0,
   HTTP_CLIENT_STATE_CONNECTING    = 1,
   HTTP_CLIENT_STATE_CONNECTED     = 2,
   HTTP_CLIENT_STATE_DISCONNECTING = 3
} HttpClientState;

typedef enum {
         CDS_CR1=-2, 
         CDS_LF1=-1, 
         CDS_SIZE=0, 
         CDS_LF2, 
         CDS_DATA
} HttpChunkDecStateCode;

#define HTTP_CLIENT_MAX_USERNAME_LEN 32U
#define HTTP_CLIENT_MAX_PASSWORD_LEN 32U
#define HTTP_CLIENT_MAX_REALM_LEN 32U

HTTPPACKED (
typedef struct
{
   HttpAuthMode mode;                                                           ///<HTTP authentication mode
   unsigned char username[HTTP_CLIENT_MAX_USERNAME_LEN + 1];                    ///<User name
   unsigned char password[HTTP_CLIENT_MAX_PASSWORD_LEN + 1];                    ///<Password
   unsigned char realm[HTTP_CLIENT_MAX_REALM_LEN + 1];                          ///<Realm

}) HttpClientAuthParams_t;

#define HTTP_CLIENT_MAX_METHOD_LEN 8U
#define HTTP_CLIENT_BUFFER_SIZE 2048U

HTTPPACKED (
typedef struct
{
   HttpClientState state;                                                       ///<HTTP client state
   HttpVersion version;                                                         ///<HTTP protocol version
   HttpClientAuthParams_t authParams;                                           ///<HTTP username and password required
   HttpRequestState requestState;                                               ///<HTTP request state
   unsigned char method1[HTTP_CLIENT_MAX_METHOD_LEN + 1];                       ///<HTTP request method
   uint8_t keepAlive : 1;                                                       ///<HTTP persistent connection
   uint8_t chunkedEncoding : 1;                                                 ///<Chunked transfer encoding
   uint8_t spare : 6;
   unsigned char buffer[HTTP_CLIENT_BUFFER_SIZE + 1];                           ///<Memory buffer for input/output operations
   size_t bufferLen;                                                            ///<Length of the buffer, in bytes
   size_t bufferPos;                                                            ///<Current position in the buffer
   size_t bodyLen;                                                              ///<Length of the body, in bytes
   size_t bodyPos;                                                              ///<Current position in the body
   uint8_t statusCode;                                                          ///<HTTP status code
}) HttpClientContext_t;

#define MEM_GUARD 128U
#define NX_CONNECTION_BUF 32768U

HTTPPACKED (
typedef struct
{
  int16_t num_connections;
  int16_t num_requests;
  int16_t num_threads;
  int16_t progress_step;
  // struct addrinfo *saddr;                                                    for linux
  const char* uri_path;
  const char* uri_host;
  const char* ssl_cipher_priority;
  char request_data[4096];
  int16_t request_length;
//#ifdef WITH_SSL                                                                 // We would need to think about this as it doesnt work in the PIC32
//  gnutls_certificate_credentials_t ssl_cred;
//  gnutls_priority_t priority_cache;
//#endif
  int8_t keep_alive:1;
  int8_t secure:1;
  int8_t quiet:1;
  int8_t spare1:5;
  char padding1[MEM_GUARD];                                                    // guard from false sharing
  int16_t request_counter;
  char padding2[MEM_GUARD];
}) HttpConfig_t;                                                                // http configuration structure

HTTPPACKED (
typedef struct {
  HttpChunkDecStateCode state;
  uint8_t final_chunk:1;
  uint8_t monitor_only:1;
  uint8_t spare:6;
  int64_t chunk_bytes_left;
}) HttpChunkDecode_t;

HTTPPACKED (
typedef struct
{
//  struct ev_loop* loop;
//  struct thread_config* tdata;
  int8_t fd;
//  ev_io watch_read;
//  ev_io watch_write;
//  ev_tstamp last_activity;
  HttpChunkDecode_t cdstate;
//#ifdef WITH_SSL
//  gnutls_session_t session;
//#endif
  int8_t write_pos;
  int8_t read_pos;
  int8_t bytes_to_read;
  int8_t bytes_received;
  int8_t alive_count;
  int8_t success_count;
  int8_t keep_alive:1;
  int8_t chunked:1;
  int8_t done:1;
  int8_t secure:1;
  int8_t spare:4;
  char buf[NX_CONNECTION_BUF];
  char* body_ptr;
  HttpChunkDecStateCode state;
}) HttpConnect_t;


#define HTTP_MAKE_GET(c)  { sprintf(c->request_data,  \
           "\"GET %s HTTP/1.1\r\n\"                                             \
           \"Host: %s\r\n\"                                                     \
           \"Connection: %s\r\n\"                                               \
           \"\r\n\",                                                            \
           ?\"keep-alive\":"close\" ",c->uri_path,c->uri_host,c->keep_alive        \
          );  }                                                                 \

#define HTTP_MAKE_PUT(c)  { sprintf(c.request_data,  \
           "\"PUT %s HTTP/1.1\r\n\"                                             \
           \"Host: %s\r\n\"                                                     \
           \"Connection: %s\r\n\"                                               \
           \"\r\n\",                                                            \
           ?\"keep-alive\":"close\" ",c.uri_path,c.uri_host,c.keep_alive     \
          );  }                                                                 \


//static int16_t decode_chunked_stream(nxweb_chunked_decoder_state_t * decoder_state, char* buf, int16_t * buf_len)
HTTP_LIB int16_t decode_chunked_stream(HttpChunkDecode_t * decoder_state, char* buf, int16_t * buf_len)
{
  char* p=buf;
  char* d=buf;
  char* end=buf+*buf_len;
  char c;
  while (p<end) {
    c=*p;
    switch (decoder_state->state) {
      case CDS_DATA:
        if (end-p>=decoder_state->chunk_bytes_left) {
          p+=decoder_state->chunk_bytes_left;
          decoder_state->chunk_bytes_left=0;
          decoder_state->state=CDS_CR1;
          d=p;
          break;
        }
        else {
          decoder_state->chunk_bytes_left-=(end-p);
          if (!decoder_state->monitor_only) *buf_len=(end-buf);
          return 0;
        }
      case CDS_CR1:
        if (c!='\r') return -1;
        p++;
        decoder_state->state=CDS_LF1;
        break;
      case CDS_LF1:
        if (c!='\n') return -1;
        if (decoder_state->final_chunk) {
          if (!decoder_state->monitor_only) *buf_len=(d-buf);
          return 1;
        }
        p++;
        decoder_state->state=CDS_SIZE;
        break;
      case CDS_SIZE: // read digits until CR2
        if (c=='\r') {
          if (!decoder_state->chunk_bytes_left) {
            // terminator found
            decoder_state->final_chunk=1;
          }
          p++;
          decoder_state->state=CDS_LF2;
        }
        else {
          if (c>='0' && c<='9') c-='0';
          else if (c>='A' && c<='F') c=c-'A'+10;
          else if (c>='a' && c<='f') c=c-'a'+10;
          else return -1;
          decoder_state->chunk_bytes_left=(decoder_state->chunk_bytes_left<<4)+c;
          p++;
        }
        break;
      case CDS_LF2:
        if (c!='\n') return -1;
        p++;
        if (!decoder_state->monitor_only) {
          memmove(d, p, end-p);
          end-=(p-d);
          p=d;
        }
        decoder_state->state=CDS_DATA;
        break;
    }
  }
  if (!decoder_state->monitor_only) *buf_len=(d-buf);
  return 0;
}

HTTP_LIB char* find_end_of_http_headers(char* buf, int len, char** start_of_body)
{
  char* p;
  if (len<4) return 0;
  for (p=memchr(buf+3, '\n', len-3); p; p=memchr(p+1, '\n', len-(p-buf)-1)) 
  {
    if (*(p-1)=='\n') { *start_of_body=p+1; return p-1; }
    if (*(p-3)=='\r' && *(p-2)=='\n' && *(p-1)=='\r') { *start_of_body=p+1; return p-3; }
  }
  return 0;
}

HTTP_LIB void parse_headers(HttpConnect_t* conn)
{
  char *p;
  *(conn->body_ptr-1)='\0';
  conn->keep_alive=!strncmp(conn->buf, "HTTP/1.1", 8);
  conn->bytes_to_read=-1;
  for (p=strchr(conn->buf, '\n'); p; p=strchr(p, '\n')) {
    p++;
    if (!strncmp(p, "Content-Length:", 15)) {
      p+=15;
      while (*p==' ' || *p=='\t') p++;
      conn->bytes_to_read=atoi(p);
    }
    else if (!strncmp(p, "Transfer-Encoding:", 18)) {
      p+=18;
      while (*p==' ' || *p=='\t') p++;
      conn->chunked=!strncmp(p, "chunked", 7);
    }
    else if (!strncmp(p, "Connection:", 11)) {
      p+=11;
      while (*p==' ' || *p=='\t') p++;
      conn->keep_alive=!strncmp(p, "keep-alive", 10);
    }
  }

  if (conn->chunked) {
    conn->bytes_to_read=-1;
    memset(&conn->cdstate, 0, sizeof(conn->cdstate));
    conn->cdstate.monitor_only=1;
  }

  conn->bytes_received=conn->read_pos-(conn->body_ptr-conn->buf);               // what already read
}

HTTP_LIB int parse_uri(const char* uri, HttpConfig_t *config)
{
  static char host_buf[1024U];
  const char* p=strchr((char*)uri,'/');
  if (!strncmp((char*)uri, "http://", 7)) uri+=7;
#ifdef WITH_SSL                                                                 // not currently supported
  else if (!strncmp(uri, "https://", 8)) { uri+=8; config->secure=1; }
#endif
  else return -1;

  if (!p) {
    config->uri_host=uri;
    config->uri_path="/";
    return 0;
  }
  if ((p - uri)>sizeof(host_buf)-1) return -1;
  strncpy((char*)host_buf,(char*) uri,(int) (p - uri));
  host_buf[(p - uri)]='\0';
  config->uri_host=host_buf;
  config->uri_path=p;
  return 0;
}

#define ERR_AGAIN -2
#define ERR_ERROR -1 
#define ERR_RDCLOSED -3

#ifdef outfortonight
HTTP_LIB void read_cb(HttpConnect_t* conn, int revents)
{
  int16_t r;
  int16_t room_avail, bytes_received, bytes_received2;
  int16_t bytes_left;
  
  if (conn->state==C_READING_HEADERS)
  {
//   do{
     if (find_end_of_http_headers(conn->buf, conn->read_pos, &conn->body_ptr)) 
     {
        parse_headers(conn);
        if (conn->bytes_to_read<0 && !conn->chunked) 
        {
          // conn_close(conn, 0);   close TCP
          // inc_fail(conn);  count MIB Counter
          // open_socket(conn);  re-open socket
          return;
        }
        if (!conn->bytes_to_read) {                                             // empty body
          // rearm_socket(conn);
          return;
        }
        conn->state=C_READING_BODY;
        if (!conn->chunked) 
        {
          if (conn->bytes_received>=conn->bytes_to_read) 
          {
            // already read all
            // rearm_socket(conn);
            return;
          }
        }
        else 
        {
          r=decode_chunked_stream(&conn->cdstate, conn->body_ptr, &conn->bytes_received);
          if (r<0)                                                              // no data
          {
            //conn_close(conn, 0);   close
            //inc_fail(conn);      count MIB
            //open_socket(conn);   open
            return;
          }
          else if (r>0)                                                         // data
          {
            // read all
            // rearm_socket(conn);  re-arm
            return;
          }
        }
        return;
      }
    //}                                                                          // already parsed into buffer in interrupt while (bytes_received==room_avail);
    return;
  }

  if (conn->state==C_READING_BODY) 
  {
    //do {
      room_avail=sizeof(conn->buf);
      if (conn->bytes_to_read>0)
      {
        bytes_left=conn->bytes_to_read - conn->bytes_received;
        if (bytes_left<room_avail) room_avail=bytes_left;
      }
      //   change this one -----------------------                              bytes_received=conn_read(conn, conn->buf, room_avail);
      if (bytes_received<=0) 
      {
        if (bytes_received==ERR_AGAIN) return;
        if (bytes_received==ERR_RDCLOSED)
        {
        //  nxweb_log_error("body [%d] read connection closed", conn->alive_count);
        //  conn_close(conn, 0);
        //  inc_fail(conn);
        //  open_socket(conn);
//          return;
//        }
        //strerror_r(errno, conn->buf, sizeof(conn->buf));
        //conn_close(conn, 0);
        //inc_fail(conn);
        //open_socket(conn);
        return;
      }

      if (!conn->chunked) 
      {
        conn->bytes_received+=bytes_received;
        if (conn->bytes_received>=conn->bytes_to_read) 
        {
          // read all
          //rearm_socket(conn);
          return;
        }
      }
      else 
      {
        bytes_received2=bytes_received;
        r=decode_chunked_stream(&conn->cdstate, conn->buf, &bytes_received2);
        if (r<0) 
        {
          //conn_close(conn, 0);
          //inc_fail(conn);
          //open_socket(conn);
          return;
        }
        else if (r>0) 
        {
          conn->bytes_received+=bytes_received2;
          // read all
          //rearm_socket(conn);
          return;
        }
      }

    // }                                                                         while (bytes_received==room_avail);
    return;
  }
}
#endif

#endif